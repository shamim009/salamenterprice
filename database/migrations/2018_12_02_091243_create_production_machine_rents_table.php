<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionMachineRentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_machine_rents', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('company_id')->index();
            $table->string('rented_to')->nullable();
            $table->double('produced')->default(0);
            $table->double('rate')->default(0);
            $table->double('amount')->default(0);
            $table->text('details')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_machine_rents');
    }
}
