<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionRawMaterialUsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_raw_material_uses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('raw_material_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->date('date');
            $table->string('invoice')->nullable();
            $table->text('details')->nullable();
            $table->double('quantity');
            $table->string('token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_raw_material_uses');
    }
}
