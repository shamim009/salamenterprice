<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionSaleTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_sale_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('customer_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->string('token')->nullable();
            $table->double('amount');
            $table->integer('purpose');
            $table->text('receiver')->nullable();
            $table->string('invoice')->nullable();
            $table->integer('payment_type')->nullable();
            $table->integer('payment_mode')->nullable();
            //$table->string('bank_id')->nullable();
            $table->string('bank_account_id')->nullable();
            $table->string('cheque_number')->nullable();
            $table->string('mobile_banking_id')->nullable();
            $table->string('phone_number')->nullable();
            $table->text('details')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_sale_transactions');
    }
}
