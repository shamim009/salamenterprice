<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionBankAccountTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_bank_account_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('bank_account_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->string('token')->nullable();
            $table->double('amount');
            $table->integer('purpose');
            $table->string('cheque_number')->nullable();
            $table->text('details')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_bank_account_transactions');
    }
}
