<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessCashBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_cash_books', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('company_id')->index();
            $table->integer('purpose')->nullable();
            $table->unsignedInteger('expense_transaction_id')->nullable();
            $table->unsignedInteger('sale_transaction_id')->nullable();
            $table->unsignedInteger('purchase_transaction_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_cash_books');
    }
}
