<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('expenseItem_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->date('date');
            $table->text('details')->nullable();
            $table->double('amount');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_expenses');
    }
}
