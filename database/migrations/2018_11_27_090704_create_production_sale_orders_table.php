<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_sale_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('customer_id')->index();
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->double('quantity');
            //$table->double('rate')->nullable();
            //$table->double('amount')->nullable();
            $table->string('invoice')->nullable();
            $table->text('details')->nullable();
            //$table->string('token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_sale_orders');
    }
}
