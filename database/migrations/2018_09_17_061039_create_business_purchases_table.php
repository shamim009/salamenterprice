<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('supplier_id')->index();
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->unsignedInteger('order_id')->nullable();
            $table->date('date');
            $table->string('invoice')->nullable();
            $table->text('details')->nullable();
            $table->double('quantity');
            $table->double('rate')->nullable();
            $table->double('amount')->nullable();
            $table->string('token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_purchases');
    }
}
