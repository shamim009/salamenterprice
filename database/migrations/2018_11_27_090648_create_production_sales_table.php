<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('customer_id')->index();
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->unsignedInteger('order_id')->nullable();
            $table->double('quantity');
            $table->double('rate')->nullable();
            $table->double('amount')->nullable();
            $table->double('vat')->nullable();
            $table->double('vat_amount')->nullable();
            $table->string('unload_place')->nullable();
            $table->string('invoice')->nullable();
            $table->text('details')->nullable();
            $table->string('token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_sales');
    }
}
