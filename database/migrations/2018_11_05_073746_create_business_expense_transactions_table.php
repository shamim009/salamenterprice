<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessExpenseTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_expense_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->unsignedInteger('expenseItem_id')->index();
            $table->unsignedInteger('company_id')->index();
            $table->string('token')->nullable();
            $table->double('amount')->default(0);
            $table->text('receiver')->nullable();
            $table->integer('payment_type')->nullable();
            $table->integer('payment_mode')->nullable();
            //$table->string('bank_id')->nullable();
            $table->string('bank_account_id')->nullable();
            $table->string('cheque_number')->nullable();
            $table->string('mobile_banking_id')->nullable();
            $table->string('phone_number')->nullable();
            $table->text('details')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_expense_transactions');
    }
}
