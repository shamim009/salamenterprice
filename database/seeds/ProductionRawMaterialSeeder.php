<?php

use Illuminate\Database\Seeder;

class ProductionRawMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ProductionModels\ProductionRawMaterial::class, 20)->create();
    }
}
