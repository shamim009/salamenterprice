<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 =User::create([
            'name' => 'Tom Hanks',
	        'email' => 'tom@gmail.com',
	        'password' => Hash::make('112358'),
	        'avatar' => 'avatar.jpg',
	        'remember_token' => str_random(10),
        ]);
        $role1 = Role::find(1);

        $user1->attachRole($role1);

        // $user2 =User::create([
        //     'name' => 'Minion Crazy',
	       //  'email' => 'minion@gmail.com',
	       //  'password' => Hash::make('112358'),
	       //  'avatar' => 'avatar.jpg',
	       //  'remember_token' => str_random(10),
        // ]);
        // $role2 = Role::find(2);

        // $user2->attachRole($role2);

        // $user3 =User::create([
        //     'name' => 'Morgan Freeman',
	       //  'email' => 'morgan@gmail.com',
	       //  'password' => Hash::make('112358'),
	       //  'avatar' => 'avatar.jpg',
	       //  'remember_token' => str_random(10),
        // ]);
        // $role3 = Role::find(3);

        // $user3->attachRole($role3);

        // $user4 =User::create([
        //     'name' => 'Benedict Cumberbatch',
	       //  'email' => 'ben@gmail.com',
	       //  'password' => Hash::make('112358'),
	       //  'avatar' => 'avatar.jpg',
	       //  'remember_token' => str_random(10),
        // ]);
        // $role4 = Role::find(4);

        // $user4->attachRole($role4);
    }
}
