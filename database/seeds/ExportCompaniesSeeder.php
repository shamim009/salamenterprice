<?php

use Illuminate\Database\Seeder;

class ExportCompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ExportModels\ExportCompany::class, 5)->create();
    }
}
