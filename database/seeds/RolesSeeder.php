<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'super_admin',
        	'display_name' => 'Super Admin',
        	'description' => 'Super Admin Has Every Permissions',
        ]);

        Role::create([
            'name' => 'business_admin',
        	'display_name' => 'Business Admin',
        	'description' => 'Business Admin Has Permission To Business Module',
        ]);

        Role::create([
            'name' => 'export_import_admin',
        	'display_name' => 'Export Import Admin',
        	'description' => 'Export Import Admin Has Permission To Export Import Module',
        ]);

        Role::create([
            'name' => 'production_admin',
        	'display_name' => 'Production Admin',
        	'description' => 'Production Admin Has Permission To Production Business Module',
        ]);
    }
}
