<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesSeeder::class,
            UsersSeeder::class,
            BankSeeder::class,
            MobileBankingSeeder::class,
            BusinessSuppliersSeeder::class,
            BusinessProductCategoriesSeeder::class,
            BusinessProductsSeeder::class,
            BusinessExpenseItemsSeeder::class,
            BusinessCustomersSeeder::class,
            //BusinessCompaniesSeeder::class,
            //ExportExpenseItemsSeeder::class,
            //ExportCustomersSeeder::class,
            //ExportCompaniesSeeder::class,
            ProductionSupplierSeeder::class,
            ProductionProductSeeder::class,
            ProductionRawMaterialSeeder::class,
            ProductionExpenseItemSeeder::class,
            ProductionCustomerSeeder::class,
        ]);
    }
}
