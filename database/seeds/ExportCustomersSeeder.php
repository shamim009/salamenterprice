<?php

use Illuminate\Database\Seeder;

class ExportCustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ExportModels\ExportCustomer::class, 10)->create();
    }
}
