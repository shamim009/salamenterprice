<?php

use Illuminate\Database\Seeder;

class ProductionProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ProductionModels\ProductionProduct::class, 5)->create();
    }
}
