<?php

use Illuminate\Database\Seeder;

class BusinessCompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\BusinessModels\BusinessCompany::class, 5)->create();
    }
}
