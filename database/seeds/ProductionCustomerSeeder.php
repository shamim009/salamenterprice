<?php

use Illuminate\Database\Seeder;

class ProductionCustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ProductionModels\ProductionCustomer::class, 10)->create();
    }
}
