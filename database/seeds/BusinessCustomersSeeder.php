<?php

use Illuminate\Database\Seeder;

class BusinessCustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\BusinessModels\BusinessCustomer::class, 10)->create();
    }
}
