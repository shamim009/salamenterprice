<?php

use Illuminate\Database\Seeder;

class BusinessProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\BusinessModels\BusinessProduct::class, 20)->create();
    }
}
