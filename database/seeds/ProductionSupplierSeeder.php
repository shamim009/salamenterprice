<?php

use Illuminate\Database\Seeder;

class ProductionSupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ProductionModels\ProductionSupplier::class, 10)->create();
    }
}
