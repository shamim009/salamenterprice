<?php

use Illuminate\Database\Seeder;

class BusinessSuppliersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\BusinessModels\BusinessSupplier::class, 10)->create();
    }
}
