<?php

use Illuminate\Database\Seeder;
use App\Models\ProductionModels\ProductionExpenseItem;

class ProductionExpenseItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        	[
        		'name'				=> 'Felicitation',
        		'details'		=> 'Expense for customer and other persons felicitation',
        	],
        	[   'name'				=> 'Donation',
        		'details'		=> 'Expense for Donation',
        	],
        	[   'name'				=> 'Other',
        		'details'		=> 'Other Expenses',
        	],
        ];

        foreach ($items as $key => $value) {
        	ProductionExpenseItem::create($value);
        }
    }
}
