<?php

use Faker\Generator as Faker;

$factory->define(App\Models\BusinessModels\BusinessProduct::class, function (Faker $faker) {
    return [
        'name'		=> $faker->word,
        'category_id'		=> $faker->numberBetween(1,5),
        'details'	=> $faker->paragraph,
        'stock'	=> '0',
    ];
});
