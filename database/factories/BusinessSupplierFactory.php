<?php

use Faker\Generator as Faker;

$factory->define(App\Models\BusinessModels\BusinessSupplier::class, function (Faker $faker) {
    return [
        'name'		=> $faker->name,
        'mobile'	=> $faker->phoneNumber,
        'address'	=> $faker->address
    ];
});
