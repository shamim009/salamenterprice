<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ExportModels\ExportCompany::class, function (Faker $faker) {
    return [
        'name'		=> $faker->company,
        'address'	=> $faker->address
    ];
});
