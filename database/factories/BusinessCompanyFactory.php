<?php

use Faker\Generator as Faker;

$factory->define(App\Models\BusinessModels\BusinessCompany::class, function (Faker $faker) {
    return [
        'name'		=> $faker->company,
        'address'	=> $faker->address
    ];
});
