<?php

use Faker\Generator as Faker;

$factory->define(App\Models\BusinessModels\BusinessCustomer::class, function (Faker $faker) {
    return [
        'name'		=> $faker->name,
        'mobile'	=> $faker->phoneNumber,
        'address'	=> $faker->address
    ];
});
