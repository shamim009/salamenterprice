<?php

use Faker\Generator as Faker;

$factory->define(App\Models\BusinessModels\BusinessProductCategory::class, function (Faker $faker) {
    return [
        'name'		=> $faker->word,
        'details'	=> $faker->paragraph,
    ];
});
