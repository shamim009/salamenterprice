<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Loan;
use App\LoanProfit;
use Carbon\Carbon;
use DB;
use App\Models\BusinessModels\BusinessExpenseItem;
use App\Models\BusinessModels\BusinessExpense;
use App\Models\BusinessModels\BusinessFiscalYear;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
            $items = BusinessExpenseItem::where('type','=','1')->get();
            foreach ($items as $key => $value) {
                DB::table('business_expenses')->insert(
                    [
                        'date' => Carbon::now()->toDateString(),
                        'expenseItem_id' => $value->id,
                        'fiscal_year' => $fisc_year->id,
                        'amount' => $value->amount,
                        'company_id' => $value->company_id,
                        'details' => 'Auto Monthly Update',
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                    ]
                );
            }
            $loans = Loan::all();
            foreach ($loans as $loan) {
                if ($loan->interest_rate > 0) {
                    $profit = (($loan->amount * $loan->interest_rate) / 100);

                    //$loanProfit = new LoanProfit;

                    //$loanProfit->date = Carbon::now()->toDateString();
                    //$loanProfit->loan_id = $loan->id;
                    //$loanProfit->amount = $profit;
                    //$loanProfit->save();
                    DB::table('loan_profits')->insert(
                        [
                            'date' => Carbon::now()->toDateString(),
                            'loan_id' => $loan->id,
                            'amount' => $profit
                        ]
                    );
                }
            }
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
