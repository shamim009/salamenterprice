<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use DB;
use Hash;
use App\Models\BusinessModels\BusinessSale;
use App\Models\BusinessModels\BusinessPurchase;
use App\Models\BusinessModels\BusinessExpense;
use App\Models\BusinessModels\BusinessProduct;
use App\Models\ProductionModels\ProductionSale;
use App\Models\ProductionModels\ProductionPurchase;
use App\Models\ProductionModels\ProductionExpense;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('pages.company.index',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = null;
        return view('pages.company.create', compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'parent_business'  => ['required', Rule::notIn(['','0'])],
            'name'  =>'required',
            'initial_amount'  =>'required',
            'user_name'  =>'required',
            'email'  =>'required|email|unique:users',
            'password'  =>'required|min:6',
        ]);

        DB::transaction(function () use ($request) {
            //create company first
            $company = Company::create([
                'name' => $request->name,
                'initial_amount' => $request->initial_amount,
                'parent_business' => $request->parent_business,
                'address' => $request->address
            ]);

            //create a new user for the company
            $user = User::create([
                'name' => $request->user_name,
                'email' => $request->email,
                'avatar' => 'avatar.jpg',
                'remember_token' => str_random(10),
                'company_id' => $company->id,
                'password' => Hash::make($request->password),
            ]);

            if ($company->parent_business == 1) {
                $role = Role::find(2);

                $user->attachRole($role);
            }
            else if ($company->parent_business == 2) {
                $role = Role::find(3);

                $user->attachRole($role);
            }
            
            else {
                $role = Role::find(4);

                $user->attachRole($role);
            }
        });
        return redirect()
                    ->route('company.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('pages.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'name'  =>'required',
            'initial_amount'  =>'required',
        ]);

        $company->update([
            'name' => $request->name,
            'initial_amount' => $request->initial_amount,
            'address' => $request->address
        ]);

        return redirect()
                    ->route('company.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        DB::transaction(function () use ($company) {
            $user = User::where('company_id',$company->id)->first();
            $user->delete();
            $company->delete();
        });

        return redirect()
                    ->route('company.index')
                    ->with('warning', 'Deleted Successfully');
    }
    public function returnDatePage($id)
    {
        $company = Company::find($id);
        return view('pages.company.date-page', compact('company'));
    }
    public function report(Request $request, $id)
    {
        $company = Company::find($id);
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($company->parent_business == 1) {
            $purchases = BusinessPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();
            $sales = BusinessSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();
            $expenses = BusinessExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $expenseAmount = BusinessExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $purchaseAmount = BusinessPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $purchaseQuantity = BusinessPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $vat_amount = BusinessSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = BusinessSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $saleAmount = $total_amount - $vat_amount;

            $saleQuantity = BusinessSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $products = BusinessProduct::all();
            $stockAmount = [];
            $totalStock = [];
            foreach ($products as $product) {
                $purchaseStorage = 0;
                $saleStorage = 0;
                foreach ($purchases as $purchase) {
                    if ($product->id == $purchase->product_id) {
                        $purchaseStorage = $purchaseStorage + $purchase->quantity;
                    }
                }

                foreach ($sales as $sale) {
                    if ($product->id == $sale->product_id) {
                        $saleStorage = $saleStorage + $sale->quantity;
                    }
                }

                $remainingStorage = $purchaseStorage - $saleStorage;
                array_push($totalStock, $remainingStorage);
                // foreach ($purchases as $purchase) {
                //     if ($product->id == $purchase->product_id) {
                //         $remainingStorageAmount = $remainingStorage * $purchase->rate;
                //     }
                // }
                $purchase = BusinessPurchase::where([
                    ['company_id',$id],
                    ['product_id',$product->id]
                ])->orderBy('created_at', 'desc')->first();

                //dd($purchase);
                if ($purchase) {
                    $remainingStorageAmount = $remainingStorage * $purchase->rate;
                    array_push($stockAmount, $remainingStorageAmount);
                } 
            }
            $finalStockAmount = array_sum($stockAmount);
            $finalStock = array_sum($totalStock);

            return view('pages.company.businessCompanyReport', compact('company','purchaseAmount','saleAmount','expenseAmount','expenses','sales','purchases','finalStockAmount','start_date','end_date','saleQuantity','purchaseQuantity','finalStock'));
        }
        else if ($company->parent_business == 2) {
            // return view('pages.company.exportCompanyReport', compact('company','purchaseAmount','saleAmount','expenseAmount','expenses','sales','purchases','finalStockAmount','start_date','end_date','saleQuantity','purchaseQuantity','finalStock'));
        }

        else {
            $purchases = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();
            $sales = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();
            $expenses = ProductionExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $expenseAmount = ProductionExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $purchaseAmount = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $purchaseQuantity = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $vat_amount = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $saleAmount = $total_amount - $vat_amount;

            $saleQuantity = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            
            return view('pages.company.productionCompanyReport', compact('company','purchaseAmount','saleAmount','expenseAmount','expenses','sales','purchases','start_date','end_date','saleQuantity','purchaseQuantity'));
        }
        
    }
}
