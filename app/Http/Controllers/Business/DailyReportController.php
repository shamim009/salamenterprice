<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\BusinessModels\BusinessPurchaseTransaction;
use App\Models\BusinessModels\BusinessPurchase;
use App\Models\BusinessModels\BusinessSaleTransaction;
use App\Models\BusinessModels\BusinessSale;
use App\Models\BusinessModels\BusinessCashBook;
use App\Models\BusinessModels\BusinessBankAccountTransaction;
use App\Models\BusinessModels\BusinessExpense;
use App\Models\BusinessModels\BusinessExpenseTransaction;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DailyReportController extends Controller
{
    public function index(){
        $date = Carbon::now()->format('Y-m-d');
        //dd($date);
        $purchase_payments = BusinessPurchaseTransaction::where('date', $date)->where('purpose','=','2')->get();
        $purchase_paid = BusinessPurchaseTransaction::where('date', $date)->where('purpose','=','2')->sum('amount');
        $purchase_returns = BusinessPurchaseTransaction::where('date', $date)->where('purpose','=','1')->get();
        $purchase_returned = BusinessPurchaseTransaction::where('date', $date)->where('purpose','=','1')->sum('amount');
        $purchases = BusinessPurchase::where('date', $date)->get();
        $purchase_quantity = BusinessPurchase::where('date', $date)->sum('quantity');
        $purchase_amount = BusinessPurchase::where('date', $date)->sum('amount');


        $sale=BusinessSale::where('date',$date)->get();
        $totalAmount = BusinessSale::where('date',$date)->sum('amount');
        $totalVat = BusinessSale::where('date',$date)->sum('vat_amount');
        $sale_amount = $totalAmount - $totalVat;
        $sale_quantity = BusinessSale::where('date',$date)->sum('quantity');
        $sale_payments = BusinessSaleTransaction::where('date', $date)->where('purpose','=','2')->get();
        $sale_paid = BusinessSaleTransaction::where('date', $date)->where('purpose','=','2')->sum('amount');
        $sale_returns = BusinessSaleTransaction::where('date', $date)->where('purpose','=','1')->get();
        $sale_returned = BusinessSaleTransaction::where('date', $date)->where('purpose','=','1')->sum('amount');

        $expense=BusinessExpense::where('date',$date)->get();
        $expense_amount=BusinessExpense::where('date',$date)->sum('amount');
        $expense_payments = BusinessExpenseTransaction::where('date', $date)->get();
        $expense_paid = BusinessExpenseTransaction::where('date', $date)->sum('amount');

        return view('pages.daily-report.index',compact('purchases','purchase_payments','purchase_returns','sale','sale_payments','sale_returns','expense','expense_payments','date','purchase_paid','purchase_returned','purchase_quantity','purchase_amount','sale_returned','sale_paid','expense_paid','expense_amount','totalVat','sale_quantity','sale_amount'));
    }
    public function byDate(Request $request){
        $date = $request->date;
        //dd($date);
        $purchase_payments = BusinessPurchaseTransaction::where('date', $date)->where('purpose','=','2')->get();
        $purchase_paid = BusinessPurchaseTransaction::where('date', $date)->where('purpose','=','2')->sum('amount');
        $purchase_returns = BusinessPurchaseTransaction::where('date', $date)->where('purpose','=','1')->get();
        $purchase_returned = BusinessPurchaseTransaction::where('date', $date)->where('purpose','=','1')->sum('amount');
        $purchases = BusinessPurchase::where('date', $date)->get();
        $purchase_quantity = BusinessPurchase::where('date', $date)->sum('quantity');
        $purchase_amount = BusinessPurchase::where('date', $date)->sum('amount');


        $sale=BusinessSale::where('date',$date)->get();
        $totalAmount = BusinessSale::where('date',$date)->sum('amount');
        $totalVat = BusinessSale::where('date',$date)->sum('vat_amount');
        $sale_amount = $totalAmount - $totalVat;
        $sale_quantity = BusinessSale::where('date',$date)->sum('quantity');
        $sale_payments = BusinessSaleTransaction::where('date', $date)->where('purpose','=','2')->get();
        $sale_paid = BusinessSaleTransaction::where('date', $date)->where('purpose','=','2')->sum('amount');
        $sale_returns = BusinessSaleTransaction::where('date', $date)->where('purpose','=','1')->get();
        $sale_returned = BusinessSaleTransaction::where('date', $date)->where('purpose','=','1')->sum('amount');

        $expense=BusinessExpense::where('date',$date)->get();
        $expense_amount=BusinessExpense::where('date',$date)->sum('amount');
        $expense_payments = BusinessExpenseTransaction::where('date', $date)->get();
        $expense_paid = BusinessExpenseTransaction::where('date', $date)->sum('amount');

        return view('pages.daily-report.index',compact('purchases','purchase_payments','purchase_returns','sale','sale_payments','sale_returns','expense','expense_payments','date','purchase_paid','purchase_returned','purchase_quantity','purchase_amount','sale_returned','sale_paid','expense_paid','expense_amount','totalVat','sale_quantity','sale_amount'));
    }
}
