<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BusinessModels\BusinessRemark;
use App\Company;
use Illuminate\Support\Facades\Auth;

class RemarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $remarks = BusinessRemark::latest()->get();
        return view('pages.business.remark.index',compact('remarks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $remark = null;
        $companies = Company::all('id', 'name');
        return view('pages.business.remark.create', compact('remark','companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        if ($user->hasRole('super_admin')) {
            $request->validate([
                'details'  =>'required',
                'company_id'      => 'required',
            ]);
        }
        else {
            $request->validate([
                'details'  =>'required',
            ]);
        }      

        $remark = new BusinessRemark;

        if ($user->hasRole('super_admin')) {
            $remark->company_id = $request->company_id;
        }
        else {
            $remark->company_id = $user->company_id;
        }     
        $remark->details = $request->details;

        $remark->save();

        return redirect()
                    ->route('business.remark.index')
                    ->with('success', 'Remark Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $remark = BusinessRemark::find($id);
        $companies = Company::all('id', 'name');
        return view('pages.business.remark.edit', compact('remark','companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        if ($user->hasRole('super_admin')) {
            $request->validate([
                'details'  =>'required',
                'company_id'      => 'required',
            ]);
        }
        else {
            $request->validate([
                'details'  =>'required',
            ]);
        }      

        $remark = BusinessRemark::find($id);

        if ($user->hasRole('super_admin')) {
            $remark->company_id = $request->company_id;
        }
        else {
            $remark->company_id = $user->company_id;
        }     
        $remark->details = $request->details;

        $remark->save();

        return redirect()
                    ->route('business.remark.index')
                    ->with('success', 'Remark Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $remark = BusinessRemark::find($id);
        $remark->delete();
        return redirect()
                    ->route('business.remark.index')
                    ->with('success', 'Remark Deleted Successfully');
    }
}
