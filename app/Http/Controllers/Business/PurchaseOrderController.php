<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessPurchase;
use App\Models\BusinessModels\BusinessPurchaseOrder;
use App\Models\BusinessModels\BusinessSupplier;
use App\Models\BusinessModels\BusinessPurchaseTransaction;
use App\Models\BusinessModels\BusinessProduct;
use App\Models\BusinessModels\BusinessFiscalYear;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        return view('pages.business.purchase-order.index',compact('fisc_year'));
    }

    //server side datatable super admin view
    public function allOrders(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'supplier_id',
                            4 => 'invoice',
                            5 => 'product_id',
                            6 => 'quantity',
                            7 => 'rate',
                            8 => 'amount',
                            9 => 'purchased',
                            10 => 'remaining',
                            11 => 'actions',
                        );
        
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->count();
        $totalFiltered = $totalData;
        
        $totalSum = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->sum('amount'); 
        $totalQuantity = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->sum('quantity');
        $totalOrders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->get();
        $totalPurchased = 0;
        $totalRemaining = 0;
        
        foreach ($totalOrders as $order)
            {
                $purchased = BusinessPurchase::where('order_id',$order->id)->sum('quantity');
                $totalPurchased += $purchased;
                $totalRemaining += ($order->quantity - $purchased);
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $orders =  BusinessPurchaseOrder::with('supplier','product','company')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('quantity','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('invoice', 'LIKE',"%{$search}%")
                                ->orWhereHas('supplier', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('product', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = BusinessPurchaseOrder::with('supplier','product','company')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('quantity','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('invoice', 'LIKE',"%{$search}%")
                                ->orWhereHas('supplier', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('product', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['invoice'] = $value->invoice;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity; 
                $nestedData['rate'] = $value->rate;
                $nestedData['amount'] = $value->amount;
                $purchased = BusinessPurchase::where('order_id',$value->id)->sum('quantity');
                $nestedData['purchased'] = $purchased;
                $nestedData['remaining'] = $value->quantity - $purchased;                
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/purchase/order/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Purchase">
                                        Purchase
                                    </a>
                                    <a href="'.url('business/purchase/order/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    
                                    <a href="'.route('business.purchase-order.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.purchase-order.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,  
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity,
                    "totalPurchased"            => $totalPurchased,
                    "totalRemaining"            => $totalRemaining
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable business admin view
    public function allOrdersAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'supplier_id',
                            3 => 'invoice',
                            4 => 'product_id',
                            5 => 'quantity',
                            6 => 'rate',                           
                            7 => 'amount',
                            8 => 'purchased',
                            9 => 'remaining',
                            10 => 'actions',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        $totalData = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData; 
        
        $totalSum = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('amount'); 
        $totalQuantity = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('quantity');
        $totalOrders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->get();
        $totalPurchased = 0;
        $totalRemaining = 0;
        
        foreach ($totalOrders as $order)
            {
                $purchased = BusinessPurchase::where('order_id',$order->id)->sum('quantity');
                $totalPurchased += $purchased;
                $totalRemaining += ($order->quantity - $purchased);
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $orders = BusinessPurchaseOrder::where('company_id',$user->company_id)
                        ->where('fiscal_year',$fisc_year->id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $orders =  BusinessPurchaseOrder::with('supplier','product')
                            ->where('company_id',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('invoice', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessPurchaseOrder::with('supplier','product')
                            ->where('company_id',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('invoice', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            ->count();
        }

        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['invoice'] = $value->invoice;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;   
                $nestedData['rate'] = $value->rate;
                $nestedData['amount'] = $value->amount;
                $purchased = BusinessPurchase::where('order_id',$value->id)->sum('quantity');
                $nestedData['purchased'] = $purchased;
                $nestedData['remaining'] = $value->quantity - $purchased;                    
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/purchase/order/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Purchase">
                                        Purchase
                                    </a>
                                    <a href="'.url('business/purchase/order/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('business.purchase-order.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.purchase-order.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data, 
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity ,
                    "totalPurchased"            => $totalPurchased,
                    "totalRemaining"            => $totalRemaining
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = BusinessSupplier::all('id','name');
        $products = BusinessProduct::all('id','name');
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.purchase-order.create',compact('suppliers','products','fisc_years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'supplier_selection'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'invoice'              => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            if ($request->supplier_selection == 2) {
                $supplier = DB::table('business_suppliers')->insertGetId(
                    [
                        'name' => $request->name,
                        'mobile' => $request->mobile,
                        'address' => $request->address
                    ]
                );
            }
            else {
                $supplier = $request->supplier_id;
            }
            //$totalAmount = [];

            for ($i=0; $i < count($request->product_id) ; $i++) {
                //$token = time().str_random(10); 
                $purchase =  new BusinessPurchaseOrder;

                $purchase->date = $request->date;
                $purchase->company_id = $user->company_id;
                $purchase->supplier_id = $supplier;
                $purchase->invoice = $request->invoice;
                $purchase->details = $request->details;
                $purchase->product_id = $request->product_id[$i];
                $purchase->fiscal_year = $request->fiscal_year;
                $purchase->quantity = $request->quantity[$i];
                $purchase->rate = $request->rate[$i];
                $purchase->amount = $request->rate[$i] * $request->quantity[$i];
                //$purchase->token = $token;           

                $purchase->save();

                // $product = BusinessProduct::find($request->product_id[$i]);
                // $product->stock = $product->stock + $request->quantity[$i];
                // $product->save();
            }
        });

        return redirect()
                    ->route('business.purchase-order.index')
                    ->with('success', 'Purchase Order Has Been Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = BusinessPurchaseOrder::find($id);
        return view('pages.business.purchase-order.show',compact('purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = BusinessPurchaseOrder::find($id);
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.purchase-order.edit',compact('order','fisc_years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'fiscal_year'  => ['required', Rule::notIn(['','0'])], 
            'date'              => 'required',
            'quantity'              => 'required',
            'invoice'              => 'required',
        ]);
        $order =  BusinessPurchaseOrder::find($id);
        DB::transaction(function () use ($request, $order) {

            $order->date = $request->date;
            $order->invoice = $request->invoice;
            $order->details = $request->details;
            $order->quantity = $request->quantity;
            $order->rate = $request->rate;
            $order->amount = $request->rate * $request->quantity;
            $order->fiscal_year = $request->fiscal_year;

            $order->save();
        });
        return redirect()
                    ->route('business.purchase-order.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order =  BusinessPurchaseOrder::find($id);
        DB::transaction(function () use ($order) {
            
            // $purchases = BusinessPurchase::where('order_id',$order->id)->get();
            // foreach ($purchases as $purchase) {
            //     $oldProduct = BusinessProduct::find($purchase->product_id);
            //     $oldProduct->stock = $oldProduct->stock - $purchase->quantity;
            //     $oldProduct->sale = $oldProduct->sale - $purchase->quantity;
            //     $oldProduct->save();

            //     $purchase->delete();
            // }

            $order->delete();
        });
        return redirect()
                    ->route('business.purchase-order.index')
                    ->with('success', 'Deleted Successfully');
    }

    //create purchase from delivery
    public function createDelivery($id)
    {
        $order = BusinessPurchaseOrder::find($id);
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.purchase-order.createDelivery', compact('order','fisc_years'));
    }

    //store purchase from delivery
    public function storeDelivery(Request $request, $id)
    {
        $request->validate([
            'fiscal_year'  => ['required', Rule::notIn(['','0'])], 
            'quantity'              => 'required',
            'date'              => 'required',
        ]);

        DB::transaction(function () use ($request, $id) {
            $user = Auth::user();
            $order = BusinessPurchaseOrder::find($id);

            $token = time().str_random(10); 
            $purchase =  new BusinessPurchase;

            $purchase->date = $request->date;
            $purchase->company_id = $user->company_id;
            $purchase->supplier_id = $order->supplier_id;
            $purchase->order_id = $order->id;
            $purchase->details = $request->details;
            $purchase->product_id = $order->product_id;
            $purchase->rate = $request->rate;
            $purchase->quantity = $request->quantity;
            $purchase->amount = $request->quantity * $request->rate;
            $purchase->invoice = $request->invoice;
            $purchase->token = $token;
            $purchase->fiscal_year = $request->fiscal_year;           

            $purchase->save();

            $product = BusinessProduct::find($order->product_id);
            //$product->stock = $product->stock + $request->quantity;
            $product->purchase = $product->purchase + $request->quantity;
            $product->save();
        });

        return redirect()
                    ->route('business.purchase-order.index')
                    ->with('success', 'Delivery Added Successfully');
    }    

    //delivery report for a particular purchase
    public function deliveryReport($id)
    {
        $purchases = BusinessPurchase::where('order_id', $id)->get();
        $order = BusinessPurchaseOrder::find($id);
        $delivered = 0;
        foreach ($purchases as $purchase) {
            $delivered += $purchase->quantity;
        }
        return view('pages.business.purchase-order.report',compact('purchases','order','delivered'));
    }
}
