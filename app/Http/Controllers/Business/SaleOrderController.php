<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessSale;
use App\Models\BusinessModels\BusinessSaleOrder;
use App\Models\BusinessModels\BusinessCustomer;
use App\Models\BusinessModels\BusinessSaleTransaction;
use App\Models\BusinessModels\BusinessProduct;
use App\Models\BusinessModels\BusinessFiscalYear;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Redirect;
use Illuminate\Support\Facades\Auth;

class SaleOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        return view('pages.business.sale-order.index',compact('fisc_year'));
    }

    //server side datatable super admin view
    public function allOrders(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'customer_id',
                            4 => 'product_id',
                            5 => 'invoice',
                            6 => 'quantity',
                            7 => 'rate',
                            8 => 'amount',
                            9 => 'sold',
                            10 => 'remaining',
                            11 => 'actions',
                        );
        
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->count();
        $totalFiltered = $totalData;
        
        $totalSum = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->sum('amount'); 
        $totalQuantity = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->sum('quantity');
        $totalOrders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->get();
        $totalSold = 0;
        $totalRemaining = 0;
        
        foreach ($totalOrders as $order)
            {
                $sold = BusinessSale::where('order_id',$order->id)->sum('quantity');
                $totalSold += $sold;
                $totalRemaining += ($order->quantity - $sold);
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $orders =  BusinessSaleOrder::with('customer','product','company')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('quantity','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('invoice', 'LIKE',"%{$search}%")
                                ->orWhereHas('customer', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('product', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = BusinessSaleOrder::with('customer','product','company')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('quantity','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('invoice', 'LIKE',"%{$search}%")
                                ->orWhereHas('customer', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('product', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['invoice'] = $value->invoice;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['rate'] = $value->rate;
                $nestedData['amount'] = $value->amount;                    
                $sold = BusinessSale::where('order_id',$value->id)->sum('quantity');
                $nestedData['sold'] = $sold;
                $nestedData['remaining'] = $value->quantity - $sold;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/sale/order/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Sale">
                                        Sale
                                    </a>
                                    <a href="'.url('business/sale/order/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('business.sale-order.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.sale-order.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity,
                    "totalSold"            => $totalSold,
                    "totalRemaining"            => $totalRemaining
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable business admin view
    public function allOrdersAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'customer_id',
                            3 => 'invoice',
                            4 => 'product_id',
                            5 => 'quantity',
                            6 => 'rate',
                            7 => 'amount',
                            8 => 'sold',
                            9 => 'remaining',
                            10 => 'actions',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData;
        
        $totalSum = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('amount'); 
        $totalQuantity = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('quantity');
        $totalOrders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->get();
        $totalSold = 0;
        $totalRemaining = 0;
        
        foreach ($totalOrders as $order)
            {
                $sold = BusinessSale::where('order_id',$order->id)->sum('quantity');
                $totalSold += $sold;
                $totalRemaining += ($order->quantity - $sold);
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $orders = BusinessSaleOrder::where('company_id',$user->company_id)
                        ->where('fiscal_year',$fisc_year->id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $orders =  BusinessSaleOrder::with('customer','product')
                            ->where('company_id',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('invoice', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessSaleOrder::with('customer','product')
                            ->where('company_id',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('invoice', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            ->count();
        }

        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['invoice'] = $value->invoice;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;               
                $nestedData['rate'] = $value->rate;
                $nestedData['amount'] = $value->amount;                    
                $sold = BusinessSale::where('order_id',$value->id)->sum('quantity');
                $nestedData['sold'] = $sold;
                $nestedData['remaining'] = $value->quantity - $sold;                    
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/sale/order/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Sale">
                                        Sale
                                    </a>
                                    <a href="'.url('business/sale/order/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('business.sale-order.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.sale-order.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity,
                    "totalSold"            => $totalSold,
                    "totalRemaining"            => $totalRemaining
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = BusinessCustomer::all('id','name');
        $products = BusinessProduct::all('id','name');
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.sale-order.create',compact('customers','products','fisc_years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'customer_selection'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'invoice'              => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            if ($request->customer_selection == 2) {
                $customer = DB::table('business_customers')->insertGetId(
                    [
                        'name' => $request->name,
                        'mobile' => $request->mobile,
                        'address' => $request->address
                    ]
                );
            }
            else {
                $customer = $request->customer_id;
            }
            //$totalAmount = [];

            for ($i=0; $i < count($request->product_id) ; $i++) {
                //$token = time().str_random(10); 
                $sale =  new BusinessSaleOrder;

                $sale->date = $request->date;
                $sale->company_id = $user->company_id;
                $sale->customer_id = $customer;
                $sale->invoice = $request->invoice;
                $sale->details = $request->details;
                $sale->product_id = $request->product_id[$i];
                $sale->fiscal_year = $request->fiscal_year;
                $sale->quantity = $request->quantity[$i];
                $sale->rate = $request->rate[$i];
                $sale->amount = $request->quantity[$i] * $request->rate[$i];
                //$sale->token = $token;           

                $sale->save();

                // $product = BusinessProduct::find($request->product_id[$i]);
                // $product->stock = $product->stock + $request->quantity[$i];
                // $product->save();
            }
        });

        return redirect()
                    ->route('business.sale-order.index')
                    ->with('success', 'Sale Order Has Been Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = BusinessSaleOrder::find($id);
        return view('pages.business.sale-order.show',compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = BusinessSaleOrder::find($id);
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.sale-order.edit',compact('order','fisc_years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'quantity'              => 'required',
            'rate'              => 'required',
            'invoice'              => 'required',
        ]);
        $order =  BusinessSaleOrder::find($id);
        DB::transaction(function () use ($request, $order) {

            $order->date = $request->date;
            $order->invoice = $request->invoice;
            $order->details = $request->details;
            $order->quantity = $request->quantity;
            $order->rate = $request->rate;
            $order->amount = $request->quantity * $request->rate;
            $order->fiscal_year = $request->fiscal_year;

            $order->save();
        });
        return redirect()
                    ->route('business.sale-order.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order =  BusinessSaleOrder::find($id);
        DB::transaction(function () use ($order) {
            
            // $sales = BusinessSale::where('order_id',$order->id)->get();
            // foreach ($sales as $sale) {
            //     $oldProduct = BusinessProduct::find($sale->product_id);
            //     $oldProduct->stock = $oldProduct->stock + $sale->quantity;
            //     $oldProduct->sale = $oldProduct->sale + $sale->quantity;
            //     $oldProduct->save();

            //     $oldTransaction = BusinessSaleTransaction::where([
            //         ['token', '=', $sale->token],
            //         ['purpose', '=', '1']
            //     ])->first();
            //     if ($oldTransaction) {
            //         $oldTransaction->delete();
            //     }
            //     $sale->delete();
            // }

            $order->delete();
        });
        return redirect()
                    ->route('business.sale-order.index')
                    ->with('success', 'Deleted Successfully');
    }

    //create sale from delivery
    public function createDelivery($id)
    {
        $order = BusinessSaleOrder::find($id);
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.sale-order.createDelivery', compact('order','fisc_years'));
    }

    //store purchase from delivery
    public function storeDelivery(Request $request, $id)
    {
        $request->validate([ 
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'quantity'              => 'required|numeric',
            'rate'              => 'required|numeric',
            'date'              => 'required',
        ]);

        $order = BusinessSaleOrder::find($id);

        $product = BusinessProduct::find($order->product_id);

        // if ($product->stock < $request->quantity) {
        //     return redirect()
        //                 ->back()
        //                 ->with('warning', "Sorry You Don't Have Enough Product in Store to Sell");
        // }

        DB::transaction(function () use ($request, $order) {
            $user = Auth::user();

            $token = time().str_random(10); 
            $sale =  new BusinessSale;

            $sale->date = $request->date;
            $sale->company_id = $user->company_id;
            $sale->customer_id = $order->customer_id;
            $sale->order_id = $order->id;
            $sale->details = $request->details;
            $sale->product_id = $order->product_id;
            $sale->rate = $request->rate;
            $sale->quantity = $request->quantity;
            $sale->vat = $request->vat;
            $sale->amount = $request->quantity * $request->rate;
            $sale->vat_amount = ((($request->quantity * $request->rate) * $request->vat) / 100);
            $sale->token = $token;
            $sale->invoice = $request->invoice;
            $sale->unload_place = $request->unload_place;
            $sale->factory_name = $request->factory_name;
            $sale->fiscal_year = $request->fiscal_year;           

            $sale->save();

            $product = BusinessProduct::find($order->product_id);
            //$product->stock = $product->stock - $request->quantity;
            $product->sale = $product->sale + $request->quantity;
            $product->save();

            // $transaction = new BusinessSaleTransaction;

            // $transaction->date = $request->date;
            // $transaction->customer_id = $order->customer_id;
            // $transaction->token = $token;
            // $transaction->company_id = $user->company_id;
            // $transaction->amount = $sale->amount;
            // $transaction->purpose = 1;

            // $transaction->save();
        });

        return redirect()
                    ->route('business.sale-order.index')
                    ->with('success', 'Delivery Added Successfully');
    }

    //delivery report for a particular sale
    public function deliveryReport($id)
    {
        $sales = BusinessSale::where('order_id', $id)->get();
        $order = BusinessSaleOrder::find($id);
        return view('pages.business.sale-order.report',compact('sales','order'));
    }
}
