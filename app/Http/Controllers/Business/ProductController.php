<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessProduct;
use App\Models\BusinessModels\BusinessProductCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = BusinessProduct::latest()->get();
        return view('pages.business.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = null;
        $categories = BusinessProductCategory::all('name','id');
        return view('pages.business.product.create', compact('product','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
            'category_id'  => ['required', Rule::notIn(['','0'])],
        ]);

        BusinessProduct::create([
            'name'   => $request->name,
            'category_id'   => $request->category_id,
            'details'   => $request->details,
        ]);

        return redirect()
                    ->route('business.product.index')
                    ->with('success', 'Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = BusinessProduct::find($id);
        $categories = BusinessProductCategory::all('name','id');
        return view('pages.business.product.edit', compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required|string',
            'category_id'  => ['required', Rule::notIn(['','0'])],
        ]);

        $product = BusinessProduct::find($id);

        $product->update([
            'name'      => $request->name,
            'category_id'   => $request->category_id,
            'details'   => $request->details,
        ]);

        return redirect()
                    ->route('business.product.index')
                    ->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = BusinessProduct::find($id);
        $product->delete();

        return redirect()
                    ->route('business.product.index')
                    ->with('warning', 'Deleted Successfully');
    }

    //autocomplete search of product
    public function autoCompleteSearch(Request $request)
    {
        $query = $request->get('term','');
                
        $products = BusinessProduct::where('name','LIKE','%'.$query.'%')
                            ->get();

        $results=array();                    
        
        if(count($products ) > 0){
            foreach ($products  as $product) {
                $results[] = [ 'id' => $product['id'], 'text' => $product->category->name.' '.$product['name']];                  
            }
            return response()->json($results);
        }
        else{
            $data[] = 'Nothing Found';
            return $data;
        }
    }
}
