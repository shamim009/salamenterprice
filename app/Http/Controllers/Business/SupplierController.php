<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessSupplier;
use App\Models\BusinessModels\BusinessPurchase;
use App\Models\BusinessModels\BusinessPurchaseDelivery;
use App\Models\BusinessModels\BusinessPurchaseOrder;
use App\Models\BusinessModels\BusinessPurchaseTransaction;
use App\Models\BusinessModels\BusinessFiscalYear;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;
use Excel;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.business.supplier.index');
    }

    //server side datatable
    public function allSuppliers(Request $request)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2=> 'mobile',
                            3=> 'address',
                            4=> 'actions',
                        );
  
        $totalData = BusinessSupplier::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $suppliers = BusinessSupplier::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $suppliers =  BusinessSupplier::where('name','LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessSupplier::where('name','LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->count();
        }

        $data = array();
        if(!empty($suppliers))
        {
            foreach ($suppliers as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['name'] = $value->name;
                $nestedData['mobile'] = $value->mobile;
                $nestedData['address'] = $value->address;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/supplier/report',$value->id) .'" class="btn btn-primary btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('business.supplier.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.supplier.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supplier = null;
        return view('pages.business.supplier.create', compact('supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        BusinessSupplier::create($request->all());

        return redirect()
                    ->route('business.supplier.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = BusinessSupplier::find($id);
        return view('pages.business.supplier.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        $supplier = BusinessSupplier::find($id);
        $supplier->update($request->all());

        return redirect()
                    ->route('business.supplier.index')
                    ->with('success','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = BusinessSupplier::find($id);
        $supplier->delete();

        return redirect()
                    ->route('business.supplier.index')
                    ->with('warning','Deleted Successfully');
    }

    //autocomplete search of supplier
    public function autoCompleteSearch(Request $request)
    {
        $query = $request->get('term','');
                
        $suppliers = BusinessSupplier::where('name','LIKE','%'.$query.'%')
                            ->get();

        $results=array();                    
        
        if(count($suppliers ) > 0){
            foreach ($suppliers  as $supplier) {
                $results[] = [ 'id' => $supplier['id'], 'text' => $supplier['name']];                  
            }
            return response()->json($results);
        }
        else{
            $data[] = 'Nothing Found';
            return $data;
        }
    }

    public function returnDatePage($id){
        $supplier = BusinessSupplier::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->sum('amount');

            // $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }

            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->sum('quantity');

            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','2')->sum('amount');
            // $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','1')->sum('amount');
            // $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->sum('amount');
            // $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }

            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('supplier_id','=', $id)->sum('quantity');

            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');
            // $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');
            // $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        
        $balance = ($purchase_amount + $returned_amount) - $paid_amount;
        // $prev_balance = ($other_purchase_amount + $other_returned_amount) - $other_paid_amount;

        return view('pages.business.supplier.date',compact('supplier','purchases','transactions','id','balance','purchase_amount','paid_amount','orders','remainingQuantity','purchaseQuantity','returned_amount','prev_balance','start_date','end_date','deliveries','delivered_quantity','delivered_amount'));
        // return view('pages.business.supplier.date',compact('id'));
    }

    //individual supplier transaction report
    public function report(Request $request)
    {
        $id = $request->id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $supplier = BusinessSupplier::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->orderBy('date', 'desc')->whereBetween('date', [$start_date, $end_date])->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('quantity');
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }

            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('quantity');

            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }      
        
        $balance = ($purchase_amount + $returned_amount) - $paid_amount;
        $prev_balance = ($other_purchase_amount + $other_returned_amount) - $other_paid_amount;

        return view('pages.business.supplier.report',compact('supplier','purchases','transactions','id','balance','purchase_amount','paid_amount','orders','remainingQuantity','purchaseQuantity','returned_amount','prev_balance','start_date','end_date','delivered_amount','delivered_quantity','deliveries'));
    }

    public function printReport(Request $request)
    {
        $id = $request->id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $supplier = BusinessSupplier::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->orderBy('date', 'desc')->whereBetween('date', [$start_date, $end_date])->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('quantity');
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }

            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('quantity');

            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }      
        
        $balance = ($purchase_amount + $returned_amount) - $paid_amount;
        $prev_balance = ($other_purchase_amount + $other_returned_amount) - $other_paid_amount;

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.business.supplier.print',compact('supplier','purchases','transactions','id','balance','purchase_amount','paid_amount','orders','remainingQuantity','purchaseQuantity','date','returned_amount','prev_balance','start_date','end_date','delivered_quantity','delivered_amount','deliveries'));
    }

    public function reportExportPDF(Request $request)
    {
        $id = $request->id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $supplier = BusinessSupplier::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->orderBy('date', 'desc')->whereBetween('date', [$start_date, $end_date])->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('quantity');
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }

            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('quantity');

            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }      
        
        $balance = ($purchase_amount + $returned_amount) - $paid_amount;
        $prev_balance = ($other_purchase_amount + $other_returned_amount) - $other_paid_amount;

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.business.supplier.pdf',compact('supplier','purchases','transactions','id','balance','purchase_amount','paid_amount','orders','remainingQuantity','purchaseQuantity','date','returned_amount','prev_balance','start_date','end_date','deliveries','delivered_quantity','delivered_amount'));
        return $pdf->download('SupplierReport.pdf');
    }

    public function reportExportExcel(Request $request)
    {
        $id = $request->id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $supplier = BusinessSupplier::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->orderBy('date', 'desc')->whereBetween('date', [$start_date, $end_date])->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('quantity');
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $purchase_amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_purchase_amount = BusinessPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }

            $deliveries = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('supplier_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivered_amount = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('amount');
            $delivered_quantity = BusinessPurchaseDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->where('supplier_id','=', $id)->sum('quantity');

            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessPurchaseOrder::where('fiscal_year',$fisc_year->id)->where('supplier_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = BusinessPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }      
        
        $balance = ($purchase_amount + $returned_amount) - $paid_amount;
        $prev_balance = ($other_purchase_amount + $other_returned_amount) - $other_paid_amount;

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        Excel::create('Supplier Report', function($excel) use ($supplier, $purchases, $transactions, $balance, $date, $purchase_amount, $paid_amount, $orders, $remainingQuantity, $purchaseQuantity, $returned_amount, $prev_balance, $start_date, $end_date, $delivered_amount, $delivered_quantity, $deliveries) {
            $excel->sheet('Supplier Report', function($sheet) use ($supplier, $purchases, $transactions, $balance, $date, $purchase_amount, $paid_amount, $orders, $remainingQuantity, $purchaseQuantity, $returned_amount, $prev_balance, $start_date, $end_date, $delivered_amount, $delivered_quantity, $deliveries) {
                $sheet->loadView('pages.business.supplier.excel',compact('supplier','purchases','transactions','balance','date','purchase_amount','otherFiscQuantity','orders','paid_amount','remainingQuantity','purchaseQuantity','returned_amount','prev_balance','start_date','end_date','delivered_quantity','delivered_amount','deliveries'));
            });
        })->download('xls');
    }

    public function payableAmount()
    {
        $suppliers = BusinessSupplier::all();
        $user = Auth::user();
        $pAmount = [];
        $pQuantity = [];
        $pPaid = [];
        $pReturned = [];
        $pBalance = [];
        if ($user->hasRole('super_admin')) {
            foreach ($suppliers as $supplier) {
                $purchase_amount = BusinessPurchase::where('supplier_id','=', $supplier->id)->sum('amount');
                $purchase_quantity = BusinessPurchase::where('supplier_id','=', $supplier->id)->sum('quantity');
                $paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','2')->sum('amount');
                $returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','1')->sum('amount');
                $balance = ($purchase_amount + $returned_amount) - $paid_amount;

                array_push($pAmount, $purchase_amount);
                array_push($pQuantity, $purchase_quantity);
                array_push($pPaid, $paid_amount);
                array_push($pReturned, $returned_amount);
                array_push($pBalance, $balance);

            }
        }
        else {
            foreach ($suppliers as $supplier) {
                $purchase_amount = BusinessPurchase::where('supplier_id','=', $supplier->id)->where('company_id',$user->company_id)->sum('amount');
                $purchase_quantity = BusinessPurchase::where('supplier_id','=', $supplier->id)->where('company_id',$user->company_id)->sum('quantity');
                $paid_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');
                $returned_amount = BusinessPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');

                $balance = ($purchase_amount + $returned_amount) - $paid_amount;

                array_push($pAmount, $purchase_amount);
                array_push($pQuantity, $purchase_quantity);
                array_push($pPaid, $paid_amount);
                array_push($pReturned, $returned_amount);
                array_push($pBalance, $balance);
            }
        }

        return view('pages.business.supplier.payable',compact('suppliers','balance','purchase_amount','paid_amount','purchase_quantity','returned_amount','pAmount','pQuantity','pPaid','pReturned','pBalance'));
    }
}
