<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessSale;
use App\Models\BusinessModels\BusinessSaleDelivery;
use App\Models\BusinessModels\BusinessCustomer;
use App\Models\BusinessModels\BusinessSaleTransaction;
use App\Models\BusinessModels\BusinessSaleOrder;
use App\Models\BusinessModels\BusinessProduct;
use App\Models\BusinessModels\BusinessFiscalYear;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;
use Excel;

class SaleDeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $totalSum = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $totalQuantity = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        else {
            $totalSum = BusinessSaleDelivery::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $totalQuantity = BusinessSaleDelivery::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        return view('pages.business.sale-delivery.index',compact('fisc_year','totalSum','totalQuantity'));
    }

    //server side datatable super admin view
    public function allSaleDeliveries(Request $request)
    {     
        $columns = array( 
                            0 => 'id', 
                            1 => 'date',
                            2 => 'company_id',
                            3 => 'customer_id',
                            4 => 'customer',
                            5 => 'product_id',
                            6 => 'quantity',
                            7 => 'rate',
                            8 => 'amount',
                            9 => 'sold',
                            10 => 'delivered',
                            11 => 'remaining',
                            12 => 'actions',
                        );
        
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->count();
        $totalSum = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->sum('amount');
        $totalQuantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->sum('quantity'); 
        $totalFiltered = $totalData;
        
        $totalOrders = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->get();
        $totalPurchased = 0;
        $totalDelivered = 0;
        $totalRemaining = 0;
        
        foreach ($totalOrders as $order)
            {
                $delivered = BusinessSaleDelivery::where('sale_id',$order->sale_id)->where('id','<',$order->id)->sum('quantity');
                $purchase = BusinessSale::find($order->sale_id);
                $totalPurchased += $purchase->quantity;
                $totalDelivered += ($delivered + $order->quantity);
                $totalRemaining += ($purchase->quantity - ($delivered + $order->quantity));
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $sales = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $sales =  BusinessSaleDelivery::with('customer','product','company')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('quantity','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('rate', 'LIKE',"%{$search}%")
                                ->orWhereHas('customer', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('product', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = BusinessSaleDelivery::with('customer','product','company')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('quantity','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('rate', 'LIKE',"%{$search}%")
                                ->orWhereHas('customer', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('product', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($sales))
        {
            foreach ($sales as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['customer'] = $value->customer_id;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;                    
                $nestedData['rate'] = $value->rate;
                $nestedData['amount'] = $value->amount;
                $delivered = BusinessSaleDelivery::where('sale_id',$value->sale_id)->where('id','<',$value->id)->sum('quantity');
                $sale = BusinessSale::find($value->sale_id);
                $nestedData['sold'] = $sale->quantity;
                $nestedData['delivered'] = $delivered + $value->quantity;
                $nestedData['remaining'] = $sale->quantity - ($delivered + $value->quantity);
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/sale/delivery/view',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.url('business/sale/delivery/edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.url('business/sale/delivery/destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity,
                    "totalPurchased"            => $totalPurchased,
                    "totalRemaining"            => $totalRemaining,
                    "totalDelivered" => $totalDelivered
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable business admin view
    public function allSaleDeliveriesAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'customer_id',
                            3 => 'customer',
                            4 => 'product_id',
                            5 => 'quantity',
                            6 => 'rate',
                            7 => 'amount',
                            8 => 'sold',
                            9 => 'delivered',
                            10 => 'remaining',
                            11 => 'actions',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData;
        $totalSum = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('amount'); 
        $totalQuantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('quantity');
        
        $totalOrders = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->get();
        $totalPurchased = 0;
        $totalDelivered = 0;
        $totalRemaining = 0;
        
        foreach ($totalOrders as $order)
            {
                $delivered = BusinessSaleDelivery::where('sale_id',$order->sale_id)->where('id','<',$order->id)->sum('quantity');
                $purchase = BusinessSale::find($order->sale_id);
                $totalPurchased += $purchase->quantity;
                $totalDelivered += ($delivered + $order->quantity);
                $totalRemaining += ($purchase->quantity - ($delivered + $order->quantity));
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $sales = BusinessSaleDelivery::where('company_id',$user->company_id)
                        ->where('fiscal_year',$fisc_year->id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $sales =  BusinessSaleDelivery::with('customer','product')
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessSaleDelivery::with('customer','product')             
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($sales))
        {
            foreach ($sales as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['customer'] = $value->customer_id;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['rate'] = $value->rate;                     
                $nestedData['amount'] = $value->amount;
                $delivered = BusinessSaleDelivery::where('sale_id',$value->sale_id)->where('id','<',$value->id)->sum('quantity');
                $sale = BusinessSale::find($value->sale_id);
                $nestedData['sold'] = $sale->quantity;
                $nestedData['delivered'] = $delivered + $value->quantity;
                $nestedData['remaining'] = $sale->quantity - ($delivered + $value->quantity); 
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/sale/delivery/view',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.url('business/sale/delivery/edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.url('business/sale/delivery/destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity,
                    "totalPurchased"            => $totalPurchased,
                    "totalRemaining"            => $totalRemaining,
                    "totalDelivered" => $totalDelivered
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $sale = BusinessSale::find($id);
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.sale-delivery.create', compact('sale','fisc_years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->validate([ 
            'quantity'              => 'required',
            'date'              => 'required',
            'rate'              => 'required',
        ]);

        DB::transaction(function () use ($request, $id) {
            $user = Auth::user();
            $sale = BusinessSale::find($id);

            $token = time().str_random(10); 
            $delivery =  new BusinessSaleDelivery;

            $delivery->date = $request->date;
            $delivery->company_id = $sale->company_id;
            $delivery->customer_id = $sale->customer_id;
            $delivery->sale_id = $sale->id;
            $delivery->details = $request->details;
            $delivery->product_id = $sale->product_id;
            $delivery->rate = $request->rate;
            $delivery->quantity = $request->quantity;
            $delivery->amount = $request->quantity * $request->rate;
            $delivery->invoice = $request->invoice;
            $delivery->token = $token;
            $delivery->fiscal_year = $sale->fiscal_year;           

            $delivery->save();

            $product = BusinessProduct::find($sale->product_id);
            $product->stock = $product->stock - $request->quantity;
            //$product->sale = $product->sale + $request->quantity;
            $product->save();
        });

        return redirect()
                    ->route('business.sale.index')
                    ->with('success', 'Delivery Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $delivery = BusinessSaleDelivery::find($id);
        return view('pages.business.sale-delivery.show',compact('delivery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $delivery = BusinessSaleDelivery::find($id);
        return view('pages.business.sale-delivery.edit',compact('delivery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'quantity'              => 'required',
            'date'              => 'required',
            'rate'              => 'required',
        ]);

        DB::transaction(function () use ($request, $id) {
            $user = Auth::user();
            $delivery = BusinessSaleDelivery::find($id);

            $product = BusinessProduct::find($delivery->product_id);
            $product->stock = ($product->stock + $delivery->quantity) - $request->quantity;
            //$product->sale = ($product->sale - $delivery->quantity) + $request->quantity;
            $product->save();

            $delivery->date = $request->date;
            $delivery->details = $request->details;
            $delivery->rate = $request->rate;
            $delivery->quantity = $request->quantity;
            $delivery->amount = $request->quantity * $request->rate;
            $delivery->invoice = $request->invoice;          

            $delivery->save();

        });

        return redirect()
                    ->route('business.sale.index')
                    ->with('success', 'Delivery Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delivery = BusinessSaleDelivery::find($id);

        $oldProduct = BusinessProduct::find($delivery->product_id);
        $oldProduct->stock = $oldProduct->stock + $delivery->quantity;
        //$oldProduct->sale = $oldProduct->sale - $delivery->quantity;
        $oldProduct->save();

        $delivery->delete();

        return back()->with('success', 'Delivery Deleted Successfully');
    }

    //delivery report for a particular sale
    public function deliveryReport($id)
    {
        $deliveries = BusinessSaleDelivery::where('sale_id', $id)->get();
        $sale = BusinessSale::find($id);
        $delivered = 0;
        foreach ($deliveries as $delivery) {
            $delivered += $delivery->quantity;
        }
        return view('pages.business.sale-delivery.report',compact('deliveries','sale','delivered'));
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        return view('pages.business.sale-delivery.reportByDate', compact('sales','start_date','end_date','amount','quantity','fisc_year','otherFiscQuantity','otherFiscAmount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $sales = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.business.sale-delivery.print', compact('sales','start_date','end_date','amount','quantity','date','fisc_year','otherFiscQuantity','otherFiscAmount'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $sales = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.business.sale-delivery.pdf', compact('sales','start_date','end_date','amount','quantity','date','fisc_year','otherFiscAmount','otherFiscQuantity'));
        return $pdf->download('saleDeliveryReport.pdf');
    }

    public function reportExportExcel(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $sales = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessSaleDelivery::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        Excel::create('Sale Delivery Report', function($excel) use ($sales, $start_date, $end_date, $amount, $date, $quantity, $fisc_year, $otherFiscAmount, $otherFiscQuantity) {
            $excel->sheet('Sale Delivery Report', function($sheet) use ($sales, $start_date, $end_date, $amount, $date, $quantity, $fisc_year, $otherFiscAmount, $otherFiscQuantity) {
                $sheet->loadView('pages.business.sale-delivery.excel',compact('sales','start_date','end_date','amount','date','quantity','otherFiscQuantity','otherFiscAmount','fisc_year'));
            });
        })->download('xls');
    }
}
