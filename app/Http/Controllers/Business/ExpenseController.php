<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessExpense;
use App\Models\BusinessModels\BusinessExpenseItem;
use App\Models\BusinessModels\BusinessFiscalYear;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use DateTime;
use Carbon\Carbon;
use PDF;
use Excel;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $totalSum = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
        }
        else {
            $totalSum = BusinessExpense::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');
        }
        return view('pages.business.expense.index',compact('fisc_year','totalSum'));
    }

    //server side datatable
    public function allExpenses(Request $request)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2=> 'company_id',
                            3=> 'expenseItem_id',
                            4=> 'amount',
                            5=> 'details',
                            6=> 'actions',
                            7=> 'type',
                        );
        
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessExpense::where('fiscal_year',$fisc_year->id)->count();
        $totalSum = BusinessExpense::where('fiscal_year',$fisc_year->id)->sum('amount');
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $expenses = BusinessExpense::with('expenseitem','company')
            ->where('fiscal_year',$fisc_year->id)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $expenses = BusinessExpense::with('expenseitem','company')
            ->where('fiscal_year',$fisc_year->id)
            ->where(function ($query) use($search) {
                $query->where('date', 'LIKE',"%{$search}%")
                    ->orWhere('amount', 'LIKE',"%{$search}%")
                    ->orWhereHas('expenseitem', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    })
                    ->orWhereHas('company', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                });
            })         
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = BusinessExpense::with('expenseitem','company')
            ->where('fiscal_year',$fisc_year->id)
            ->where(function ($query) use($search) {
                $query->where('date', 'LIKE',"%{$search}%")
                    ->orWhere('amount', 'LIKE',"%{$search}%")
                    ->orWhereHas('expenseitem', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    })
                    ->orWhereHas('company', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                });
            })
            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['expenseItem_id'] = $value->expenseitem->name;
                $nestedData['amount'] = $value->amount;
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.expense.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.expense.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $nestedData['type'] = $value->expenseItem_id;
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum   
                    );
            
        echo json_encode($json_data);        
    }

    //server side datatable business admin view
    public function allExpensesAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2=> 'expenseItem_id',
                            3=> 'amount',
                            4=> 'details',
                            5=> 'actions',
                            6=> 'type',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessExpense::where('company_id',$user->company_id)->where('fiscal_year',$fisc_year->id)->count();
        $totalSum = BusinessExpense::where('company_id',$user->company_id)->where('fiscal_year',$fisc_year->id)->sum('amount');
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $expenses = BusinessExpense::where('company_id',$user->company_id)
                        ->where('fiscal_year',$fisc_year->id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $expenses =  BusinessExpense::with('expenseitem')
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessExpense::with('expenseitem')             
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['expenseItem_id'] = $value->expenseitem->name;
                $nestedData['amount'] = $value->amount;
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.expense.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.expense.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $nestedData['type'] = $value->expenseItem_id;
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $expenseItems = BusinessExpenseItem::where('company_id',$user->company_id)->get();
        $expense = null;
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.expense.create', compact('expenseItems','expense','fisc_years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'expenseItem_id'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'amount'            => 'required',
        ]);
        
        DB::transaction(function () use ($request) {
            $user = Auth::user();

            $expense =  new BusinessExpense;

            $expense->date = $request->date;
            $expense->company_id = $user->company_id;
            $expense->expenseItem_id = $request->expenseItem_id;
            $expense->fiscal_year = $request->fiscal_year;
            $expense->amount = $request->amount;
            
            $expense->details = $request->details;
            //$expense->product_id = $request->product_id[$i];
            //$expense->rate = $request->rate[$i];
            //$expense->quantity = $request->quantity[$i];
            //$expense->token = $token;           

            $expense->save();
        });
        return redirect()
                    ->route('business.expense.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$companies = BusinessCompany::all('id','name');
        $expense = BusinessExpense::find($id);
        $user = Auth::user();
        $expenseItems = BusinessExpenseItem::where('company_id',$user->company_id)->get();
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.expense.edit', compact('expenseItems','expense','fisc_years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'expenseItem_id'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'amount'            => 'required',
        ]);

        $expense = BusinessExpense::find($id);
        $expense->update([
            'date'      => $request->date,
            'details'   => $request->details,
            'amount'     => $request->amount,
            'expenseItem_id'    => $request->expenseItem_id,
            'fiscal_year'    => $request->fiscal_year,
        ]);

        return redirect()
                    ->route('business.expense.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = BusinessExpense::find($id);
        $expense->delete();

        return redirect()
                    ->route('business.expense.index')
                    ->with('warning', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();
            $amount = BusinessExpense::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscSum = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->sum('amount');            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherFiscSum = BusinessExpense::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');
        }
        return view('pages.business.expense.report', compact('expenses','start_date','end_date','amount','otherFiscSum','fisc_year'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessExpense::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscSum = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->sum('amount');         
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscSum = BusinessExpense::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.business.expense.print', compact('expenses','start_date','end_date','amount','date','otherFiscSum','fisc_year'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();
            $amount = BusinessExpense::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscSum = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscSum = BusinessExpense::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.business.expense.pdf', compact('expenses','start_date','end_date','amount','date','fisc_year','otherFiscSum'));
        return $pdf->download('ExpenseReport.pdf');
    }

    public function reportExportExcel(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();
            $amount = BusinessExpense::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscSum = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscSum = BusinessExpense::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        Excel::create('Expense Report', function($excel) use ($expenses, $start_date, $end_date, $amount, $date, $fisc_year, $otherFiscSum) {
            $excel->sheet('Expense Report', function($sheet) use ($expenses, $start_date, $end_date, $amount, $date, $fisc_year, $otherFiscSum) {
                $sheet->loadView('pages.business.expense.excel',compact('expenses','start_date','end_date','amount','date','fisc_year','otherFiscSum'));
            });
        })->download('xls');
    }
}
