<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessPurchase;
use App\Models\BusinessModels\BusinessPurchaseDelivery;
use App\Models\BusinessModels\BusinessSupplier;
use App\Models\BusinessModels\BusinessPurchaseTransaction;
use App\Models\BusinessModels\BusinessPurchaseOrder;
use App\Models\BusinessModels\BusinessProduct;
use App\Models\BusinessModels\BusinessFiscalYear;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;
use Excel;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $totalSum = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $totalQuantity = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        else {
            $totalSum = BusinessPurchase::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $totalQuantity = BusinessPurchase::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        return view('pages.business.purchase.index',compact('fisc_year','totalSum','totalQuantity'));
    }

    //server side datatable super admin view
    public function allPurchases(Request $request)
    {     
        $columns = array( 
                            0 => 'id', 
                            1 => 'date',
                            2 => 'company_id',
                            3 => 'supplier_id',
                            4 => 'supplier',
                            5 => 'product_id',
                            6 => 'quantity',
                            7 => 'rate',
                            8 => 'amount',
                            9 => 'delivered',
                            10 => 'remaining',
                            11 => 'actions',
                        );
        
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessPurchase::where('fiscal_year',$fisc_year->id)->count();
        $totalSum = BusinessPurchase::where('fiscal_year',$fisc_year->id)->sum('amount');
        $totalQuantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->sum('quantity'); 
        $totalFiltered = $totalData; 
        
        $totalPurchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->get();
        
        $totalDelivered = 0;
        $totalRemaining = 0;
        
        foreach ($totalPurchases as $purchase)
            {
                $delivered = BusinessPurchaseDelivery::where('purchase_id', $purchase->id)->sum('quantity');
                $totalDelivered += $delivered;
                $totalRemaining += ($purchase->quantity - $delivered);
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $purchases =  BusinessPurchase::with('supplier','product','company')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('quantity','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('rate', 'LIKE',"%{$search}%")
                                ->orWhereHas('supplier', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('product', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = BusinessPurchase::with('supplier','product','company')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('quantity','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('rate', 'LIKE',"%{$search}%")
                                ->orWhereHas('supplier', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('product', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($purchases))
        {
            foreach ($purchases as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['supplier'] = $value->supplier_id;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;                    
                $nestedData['rate'] = $value->rate;
                $nestedData['amount'] = $value->amount;
                $delivered = BusinessPurchaseDelivery::where('purchase_id', $value->id)->sum('quantity');
                $totalDelivered += $delivered;
                $totalRemaining += $value->quantity - $delivered;
                $nestedData['delivered'] = $delivered;
                $nestedData['remaining'] = $value->quantity - $delivered;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/purchase/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Delivery">
                                        Delivery
                                    </a>
                                    <a href="'.url('business/purchase/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('business.purchase.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('business.purchase.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.purchase.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity,
                    "totalDelivered"            => $totalDelivered,
                    "totalRemaining"            => $totalRemaining
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable business admin view
    public function allPurchasesAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'supplier_id',
                            3 => 'supplier',
                            4 => 'product_id',
                            5 => 'quantity',
                            6 => 'rate',
                            7 => 'amount',
                            8 => 'delivered',
                            9 => 'remaining',
                            10 => 'actions',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData;
        
        $totalSum = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('amount'); 
        $totalQuantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('quantity');
        
        $totalPurchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->get();
        
        $totalDelivered = 0;
        $totalRemaining = 0;
        
        foreach ($totalPurchases as $purchase)
            {
                $delivered = BusinessPurchaseDelivery::where('purchase_id', $purchase->id)->sum('quantity');
                $totalDelivered += $delivered;
                $totalRemaining += ($purchase->quantity - $delivered);
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $purchases = BusinessPurchase::where('company_id',$user->company_id)
                        ->where('fiscal_year',$fisc_year->id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $purchases =  BusinessPurchase::with('supplier','product')
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessPurchase::with('supplier','product')             
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($purchases))
        {
            foreach ($purchases as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['supplier'] = $value->supplier_id;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['rate'] = $value->rate;                     
                $nestedData['amount'] = $value->amount;
                $delivered = BusinessPurchaseDelivery::where('purchase_id', $value->id)->sum('quantity');
                $totalDelivered += $delivered;
                $totalRemaining += $value->quantity - $delivered;
                $nestedData['delivered'] = $delivered;
                $nestedData['remaining'] = $value->quantity - $delivered; 
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/purchase/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Delivery">
                                        Delivery
                                    </a>
                                    <a href="'.url('business/purchase/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('business.purchase.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('business.purchase.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.purchase.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity,
                    "totalDelivered"            => $totalDelivered,
                    "totalRemaining"            => $totalRemaining
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = BusinessSupplier::all('id','name');
        $products = BusinessProduct::all('id','name');
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.purchase.create', compact('suppliers','products','fisc_years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'supplier_selection'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            if ($request->supplier_selection == 2) {
                $supplier = DB::table('business_suppliers')->insertGetId(
                    [
                        'name' => $request->name,
                        'mobile' => $request->mobile,
                        'address' => $request->address
                    ]
                );
            }
            else {
                $supplier = $request->supplier_id;
            }
            //$totalAmount = [];

            for ($i=0; $i < count($request->product_id) ; $i++) {
                $token = time().str_random(10);
                $order = BusinessPurchaseOrder::where([
                    ['product_id',$request->product_id[$i]],
                    ['supplier_id',$supplier],
                    ['fiscal_year',$request->fiscal_year],
                    ['company_id',$user->company_id],
                ])->first();
                if(!empty($order)){
                    $purchased = BusinessPurchase::where('order_id',$order->id)->sum('quantity');
                    $remaining =  $order->quantity - $purchased;
                }
                else {
                    $remaining = 0;
                }
                
                $purchase =  new BusinessPurchase;

                $purchase->date = $request->date;
                $purchase->company_id = $user->company_id;
                $purchase->supplier_id = $supplier;
                $purchase->invoice = $request->invoice;
                $purchase->details = $request->details;
                $purchase->product_id = $request->product_id[$i];
                $purchase->rate = $request->rate[$i];
                $purchase->quantity = $request->quantity[$i];
                $purchase->amount = $request->quantity[$i] * $request->rate[$i];
                $purchase->token = $token;
                $purchase->fiscal_year = $request->fiscal_year;
                if ($remaining > 0) {
                    $purchase->order_id = $order->id;
                }           

                $purchase->save();

                $product = BusinessProduct::find($request->product_id[$i]);
                //$product->stock = $product->stock + $request->quantity[$i];
                $product->purchase = $product->purchase + $request->quantity[$i];
                $product->save();
            }
        });
        return redirect()
                    ->route('business.purchase.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = BusinessPurchase::find($id);
        return view('pages.business.purchase.show',compact('purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purchase = BusinessPurchase::find($id);
        $suppliers = BusinessSupplier::all('id','name');
        $products = BusinessProduct::all('id','name');
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.purchase.edit', compact('purchase','suppliers','products','fisc_years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'supplier_id'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'quantity'              => 'required',
        ]);

        DB::transaction(function () use ($request, $id) {
            $purchase =  BusinessPurchase::find($id);

            $oldProduct = BusinessProduct::find($purchase->product_id);
            //$oldProduct->stock = ($oldProduct->stock - $purchase->quantity) + $request->quantity;
            $oldProduct->purchase = ($oldProduct->purchase - $purchase->quantity) + $request->quantity;
            $oldProduct->save();

            $purchase->date = $request->date;
            $purchase->details = $request->details;
            $purchase->supplier_id = $request->supplier_id;
            $purchase->rate = $request->rate;
            $purchase->quantity = $request->quantity;
            $purchase->amount = $request->amount;
            $purchase->invoice = $request->invoice;
            $purchase->fiscal_year = $request->fiscal_year;

            $purchase->save();
        });
        return redirect()
                    ->route('business.purchase.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $purchase =  BusinessPurchase::find($id);

            $deliveries = BusinessPurchaseDelivery::where('purchase_id', $id)->get();
            $delivered = 0;
            foreach ($deliveries as $delivery) {
                $delivered += $delivery->quantity;
                $delivery->delete();
            }

            $oldProduct = BusinessProduct::find($purchase->product_id);
            $oldProduct->stock = $oldProduct->stock - $delivered;
            $oldProduct->purchase = $oldProduct->purchase - $purchase->quantity;
            $oldProduct->save();

            $purchase->delete();
        });
        return redirect()
                    ->route('business.purchase.index')
                    ->with('success', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        return view('pages.business.purchase.report', compact('purchases','start_date','end_date','amount','quantity','fisc_year','otherFiscQuantity','otherFiscAmount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.business.purchase.print', compact('purchases','start_date','end_date','amount','quantity','date','fisc_year','otherFiscQuantity','otherFiscAmount'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.business.purchase.pdf', compact('purchases','start_date','end_date','amount','quantity','date','fisc_year','otherFiscAmount','otherFiscQuantity'));
        return $pdf->download('PurchaseReport.pdf');
    }

    public function reportExportExcel(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $purchases = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $otherFiscAmount = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $quantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $otherFiscQuantity = BusinessPurchase::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        Excel::create('Purchase Report', function($excel) use ($purchases, $start_date, $end_date, $amount, $date, $quantity, $fisc_year, $otherFiscAmount, $otherFiscQuantity) {
            $excel->sheet('Purchase Report', function($sheet) use ($purchases, $start_date, $end_date, $amount, $date, $quantity, $fisc_year, $otherFiscAmount, $otherFiscQuantity) {
                $sheet->loadView('pages.business.purchase.excel',compact('purchases','start_date','end_date','amount','date','quantity','otherFiscQuantity','otherFiscAmount','fisc_year'));
            });
        })->download('xls');
    }
}
