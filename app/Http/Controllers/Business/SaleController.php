<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BusinessModels\BusinessSale;
use App\Models\BusinessModels\BusinessSaleDelivery;
use App\Models\BusinessModels\BusinessSaleTransaction;
use App\Models\BusinessModels\BusinessSaleOrder;
use App\Models\BusinessModels\BusinessProduct;
use App\Models\BusinessModels\BusinessCustomer;
use App\Models\BusinessModels\BusinessFiscalYear;
use App\Company;
use Illuminate\Validation\Rule;
use DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;
use Excel;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();

        if ($user->hasRole('super_admin')) {
            $totalSum = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $totalQuantity = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }
        else {
            $totalSum = BusinessSale::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $totalQuantity = BusinessSale::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
        }

        return view('pages.business.sale.index',compact('fisc_year','totalSum','totalQuantity'));
    }

    //server side datatable
    public function allSales(Request $request)
    {      
        $columns = array( 
            0 =>'id', 
            1 =>'date',
            2=> 'company_id',
            3=> 'customer_id',
            4=> 'customer',
            5=> 'product_id',
            6=> 'quantity',
            7=> 'rate',
            8=> 'vat',
            9=> 'amount',
            10 => 'delivered',
            11 => 'remaining',
            12 => 'actions',
        );
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessSale::where('fiscal_year',$fisc_year->id)->count();

        $totalAmount = BusinessSale::where('fiscal_year',$fisc_year->id)->sum('amount');
        $totalVat = BusinessSale::where('fiscal_year',$fisc_year->id)->sum('vat_amount');
        $totalSum = $totalAmount - $totalVat;
        $totalQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->sum('quantity');

        $totalFiltered = $totalData;
        
        $totalSales = BusinessSale::where('fiscal_year',$fisc_year->id)->get();
        
        $totalDelivered = 0;
        $totalRemaining = 0;
        
        foreach ($totalSales as $purchase)
            {
                $delivered = BusinessSaleDelivery::where('sale_id', $purchase->id)->sum('quantity');
                $totalDelivered += $delivered;
                $totalRemaining += ($purchase->quantity - $delivered);
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $sales = BusinessSale::with('product', 'customer', 'company')
            ->where('fiscal_year',$fisc_year->id)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $sales = BusinessSale::with('product', 'customer', 'company')
            ->where('fiscal_year',$fisc_year->id)
            ->where(function ($query) use($search) {
                $query->where('date','LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhere('amount', 'LIKE',"%{$search}%")
                        ->orWhere('quantity', 'LIKE',"%{$search}%")
                        ->orWhereHas('product', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('customer', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        });
            })
            
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = BusinessSale::with('product', 'customer', 'company')
            ->where('fiscal_year',$fisc_year->id)
            ->where(function ($query) use($search) {
                $query->where('date','LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhere('amount', 'LIKE',"%{$search}%")
                        ->orWhere('quantity', 'LIKE',"%{$search}%")
                        ->orWhereHas('product', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('customer', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        });
            })
            ->count();
        }

        $data = array();
        if(!empty($sales))
        {
            foreach ($sales as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['customer'] = $value->customer_id;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['rate'] = $value->rate;
                $nestedData['vat'] = $value->vat_amount;
                $nestedData['amount'] = $value->amount - $value->vat_amount;
                $delivered = BusinessSaleDelivery::where('sale_id', $value->id)->sum('quantity');
                $nestedData['delivered'] = $delivered;
                $nestedData['remaining'] = $value->quantity - $delivered;
                $nestedData['actions'] = '<div class="btn-group">
                <a href="'.url('business/sale/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Delivery">
                    Delivery
                </a>
                <a href="'.url('business/sale/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                    Report
                </a>
                <a href="'.route('business.sale.show',$value->id) .'" class="btn btn-primary btn-sm" title="View">
                View
                </a>
                <a href="'.route('business.sale.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                Update
                </a>
                <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.sale.destroy',$value->id) .'" title="Delete">Delete
                </button>
                </div>';
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data,
            "totalSum"            => $totalSum,
            "totalVat"            => $totalVat,
            "totalQuantity"            => $totalQuantity,
            "totalDelivered"            => $totalDelivered,
            "totalRemaining"            => $totalRemaining
        );

        echo json_encode($json_data);        
    }

    //server side datatable business admin view
    public function allSalesAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'customer_id',
                            3 => 'customer',
                            4 => 'product_id',
                            5 => 'quantity',
                            6 => 'rate',
                            7 => 'vat',
                            8 => 'amount',
                            9 => 'delivered',
                            10 => 'remaining',
                            11 => 'actions',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        $totalData = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->count();
        $totalAmount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('amount');
        $totalVat = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('vat_amount');
        $totalQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->sum('quantity');
        $totalSum = $totalAmount - $totalVat;
        $totalFiltered = $totalData;
        
        $totalPurchases = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->get();
        
        $totalDelivered = 0;
        $totalRemaining = 0;
        
        foreach ($totalPurchases as $purchase)
            {
                $delivered = BusinessSaleDelivery::where('sale_id', $purchase->id)->sum('quantity');
                $totalDelivered += $delivered;
                $totalRemaining += ($purchase->quantity - $delivered);
            }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $sales = BusinessSale::where('company_id',$user->company_id)
                        ->where('fiscal_year',$fisc_year->id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $sales =  BusinessSale::with('customer','product')
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('rate', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessSale::with('customer','product')             
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)          
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('rate', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($sales))
        {
            foreach ($sales as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['customer'] = $value->customer_id;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['rate'] = $value->rate;                    
                $nestedData['vat'] = $value->vat_amount;
                $nestedData['amount'] = $value->amount - $value->vat_amount;
                $delivered = BusinessSaleDelivery::where('sale_id', $value->id)->sum('quantity');
                $nestedData['delivered'] = $delivered;
                $nestedData['remaining'] = $value->quantity - $delivered;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/sale/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Delivery">
                                        Delivery
                                    </a>
                                    <a href="'.url('business/sale/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('business.sale.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('business.sale.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.sale.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,
                    "totalVat"            => $totalVat,
                    "totalQuantity"            => $totalQuantity,
                    "totalDelivered"            => $totalDelivered,
                    "totalRemaining"            => $totalRemaining
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all('id','name');
        $customers = BusinessCustomer::all('id','name');
        $products = BusinessProduct::all('id','name');
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.sale.create', compact('companies','customers','products','fisc_years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'customer_selection'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'unload_place'              => 'required',
        ]);

        // for ($i=0; $i < count($request->product_id) ; $i++) { 
        //     $product = BusinessProduct::find($request->product_id[$i]);
        //     if ($product->stock < $request->quantity[$i]) {
        //         return redirect()
        //                 ->back()
        //                 ->with('warning', "Sorry You Don't Have Enough Product in Store to Sell");
        //     }
        // }

        DB::transaction(function () use ($request) {
            if ($request->customer_selection == 2) {
                $customer = DB::table('business_customers')->insertGetId(
                    [
                        'name' => $request->name,
                        'mobile' => $request->mobile,
                        'address' => $request->address
                    ]
                );
            }
            else {
                $customer = $request->customer_id;
            }

            $user = Auth::user();
            $totalAmount = [];

            for ($i=0; $i < count($request->product_id) ; $i++) { 
                $token = time().str_random(10);
                $order = BusinessSaleOrder::where([
                    ['product_id',$request->product_id[$i]],
                    ['customer_id',$customer],
                    ['fiscal_year',$request->fiscal_year],
                    ['company_id',$user->company_id],
                ])->first();
                if(!empty($order)){
                    $sold = BusinessSale::where('order_id',$order->id)->sum('quantity');
                    $remaining =  $order->quantity - $sold;
                }
                else {
                    $remaining = 0;
                }
                $sale =  new BusinessSale;

                $sale->date = $request->date;
                $sale->company_id = $user->company_id;
                $sale->customer_id = $customer;
                $sale->unload_place = $request->unload_place;
                $sale->factory_name = $request->factory_name;
                $sale->details = $request->details;
                $sale->invoice = $request->invoice;
                $sale->product_id = $request->product_id[$i];
                $sale->rate = $request->rate[$i];
                $sale->quantity = $request->quantity[$i];
                $sale->vat = $request->vat[$i];
                $sale->token = $token;
                $sale->fiscal_year = $request->fiscal_year;
                if ($request->rate[$i]) {
                    $sale->amount = $request->rate[$i] * $request->quantity[$i];
                }
                if ($request->vat[$i]) {
                    $sale->vat_amount = ((($request->rate[$i] * $request->quantity[$i]) * $request->vat[$i]) / 100);
                }
                if ($remaining > 0) {
                    $sale->order_id = $order->id;
                }   

                $sale->save();
                // array_push($totalAmount, $sale->amount);

                $product = BusinessProduct::find($request->product_id[$i]);
                //$product->stock = $product->stock - $request->quantity[$i];
                $product->sale = $product->sale + $request->quantity[$i];
                $product->save();

                // $transaction = new BusinessSaleTransaction;

                // $transaction->date = $request->date;
                // $transaction->customer_id = $customer;
                // $transaction->token = $token;
                // $transaction->company_id = $user->company_id;
                // $transaction->amount = $sale->amount;
                // $transaction->purpose = 1;

                // $transaction->save();
            }
            // $transaction = new BusinessSaleTransaction;

            // $transaction->date = $request->date;
            // $transaction->customer_id = $customer;
            // $transaction->token = $token;
            // $transaction->invoice = $request->invoice;
            // $transaction->amount = array_sum($totalAmount) - $request->discount;
            // $transaction->purpose = 1;

            // $transaction->save();

            // if ($request->payment == 1) {
            //     $transaction = new BusinessSaleTransaction;

            //     $transaction->date = $request->date;
            //     $transaction->customer_id = $customer;
            //     $transaction->token = $token;
            //     $transaction->invoice = $request->invoice;
            //     $transaction->amount = array_sum($totalAmount) - $request->discount;
            //     $transaction->purpose = 2;

            //     $transaction->save();
            // }

            // if ($request->payment == 3) {
            //     $transaction = new BusinessSaleTransaction;

            //     $transaction->date = $request->date;
            //     $transaction->customer_id = $customer;
            //     $transaction->token = $token;
            //     $transaction->invoice = $request->invoice;
            //     $transaction->amount = $request->paid_amount;
            //     $transaction->purpose = 2;
            //     $transaction->details = 'Advance Payment';

            //     $transaction->save();
            // }
        });
        return redirect()
                    ->route('business.sale.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = BusinessSale::find($id);
        return view('pages.business.sale.show',compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = BusinessSale::find($id);
        //$companies = BusinessCompany::all('id','name');
        $products = BusinessProduct::all('id','name');
        $customers = BusinessCustomer::all('id','name');
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.sale.edit', compact('sale','products','customers','fisc_years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'customer_id'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'quantity'              => 'required|numeric',
            'rate'              => 'required|numeric',
        ]);

        
        $sale =  BusinessSale::find($id);

        $product = BusinessProduct::find($sale->product_id);

        // if ((($product->stock + $sale->quantity) - $request->quantity) < 0) {
        //     return redirect()
        //                 ->back()
        //                 ->with('warning', "Sorry You Don't Have Enough Product in Store to Sell");
        // }


        DB::transaction(function () use ($request, $sale) {
            $user = Auth::user();

            $product = BusinessProduct::find($sale->product_id);
            //$product->stock = ($product->stock + $sale->quantity) - $request->quantity;
            $product->sale = ($product->sale - $sale->quantity) + $request->quantity;
            $product->save();

            $sale->date = $request->date;
            $sale->unload_place = $request->unload_place;
            $sale->factory_name = $request->factory_name;
            $sale->details = $request->details;
            $sale->customer_id = $request->customer_id;
            $sale->rate = $request->rate;
            $sale->invoice = $request->invoice;
            $sale->quantity = $request->quantity;
            $sale->vat = $request->vat;
            $sale->amount = $request->amount;
            $sale->vat_amount = (($request->amount * $request->vat) / 100);
            $sale->fiscal_year = $request->fiscal_year;

            $sale->save();

            // $transaction = BusinessSaleTransaction::where([
            //         ['token', '=', $sale->token],
            //         ['purpose', '=', '1']
            //     ])->first();
            // $transaction->customer_id = $sale->customer_id;
            // $transaction->amount = $request->amount;
            // $transaction->save();
        });
        return redirect()
                ->route('business.sale.index')
                ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $sale =  BusinessSale::find($id);

            $deliveries = BusinessSaleDelivery::where('purchase_id', $id)->get();
            $delivered = 0;
            foreach ($deliveries as $delivery) {
                $delivered += $delivery->quantity;
                $delivery->delete();
            }

            $oldProduct = BusinessProduct::find($sale->product_id);
            $oldProduct->stock = $oldProduct->stock + $delivered;
            $oldProduct->sale = $oldProduct->sale - $sale->quantity;
            $oldProduct->save();

            //delete transaction table for the delivery
            // $transaction = BusinessSaleTransaction::where([
            //     ['token', '=', $sale->token],
            //     ['purpose', '=', '1']
            // ])->first();
            // $transaction->delete();

            $sale->delete();
        });
        return redirect()
                    ->route('business.sale.index')
                    ->with('success', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('vat_amount');
            $total_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_total_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $amount = $total_amount - $vat_amount;
            $othet_amount = $other_total_amount - $other_vat_amount;

            $quantity = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $other_quantity = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_total_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('amount');
            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('vat_amount');

            $amount = $total_amount - $vat_amount;
            $other_amount = $other_total_amount - $other_vat_amount;

            $quantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $other_quantity = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('quantity');
        }
        return view('pages.business.sale.report', compact('sales','start_date','end_date','amount','quantity','vat_amount','fisc_year','other_quantity','other_amount','other_vat_amount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('vat_amount');
            $total_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_total_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $amount = $total_amount - $vat_amount;
            $othet_amount = $other_total_amount - $other_vat_amount;

            $quantity = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $other_quantity = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_total_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('amount');
            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('vat_amount');

            $amount = $total_amount - $vat_amount;
            $other_amount = $other_total_amount - $other_vat_amount;

            $quantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $other_quantity = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.business.sale.print', compact('sales','start_date','end_date','amount','quantity','vat_amount','date','other_quantity','other_amount','other_vat_amount','fisc_year'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('vat_amount');
            $total_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_total_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $amount = $total_amount - $vat_amount;
            $othet_amount = $other_total_amount - $other_vat_amount;

            $quantity = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $other_quantity = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_total_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('amount');
            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('vat_amount');

            $amount = $total_amount - $vat_amount;
            $other_amount = $other_total_amount - $other_vat_amount;

            $quantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $other_quantity = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.business.sale.pdf', compact('sales','start_date','end_date','amount','quantity','vat_amount','date','fisc_year','other_amount','other_quantity','other_vat_amount'));
        return $pdf->download('SaleReport.pdf');
    }

    public function reportExportExcel(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('vat_amount');
            $total_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_total_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('amount');
            $amount = $total_amount - $vat_amount;
            $othet_amount = $other_total_amount - $other_vat_amount;

            $quantity = BusinessSale::where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $other_quantity = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_total_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('amount');
            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('vat_amount');

            $amount = $total_amount - $vat_amount;
            $other_amount = $other_total_amount - $other_vat_amount;

            $quantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $other_quantity = BusinessSale::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$id)->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        Excel::create('Sale Report', function($excel) use ($sales, $start_date, $end_date, $amount, $date, $quantity, $fisc_year, $other_amount, $other_quantity, $other_vat_amount, $vat_amount) {
            $excel->sheet('Sale Report', function($sheet) use ($sales, $start_date, $end_date, $amount, $date, $quantity, $fisc_year, $other_amount, $other_quantity, $other_vat_amount, $vat_amount) {
                $sheet->loadView('pages.business.sale.excel',compact('sales','start_date','end_date','amount','date','quantity','other_quantity','other_amount','fisc_year','other_vat_amount','vat_amount'));
            });
        })->download('xls');
    }

    //sale invoice
    public function returnViewPage()
    {
        $customers = BusinessCustomer::all('id','name');
        return view('pages.business.sale-invoice.index', compact('customers'));
    }

    public function invoice(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $customer = BusinessCustomer::find($request->customer_id);

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.business.sale-invoice.invoice', compact('sales','start_date','end_date','amount','quantity','vat_amount','customer','date'));
    }

    public function invoicePrint(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $customer = BusinessCustomer::find($request->customer_id);

        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.business.sale-invoice.print', compact('sales','start_date','end_date','amount','quantity','vat_amount','date','customer'));
    }

    public function invoiceExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $customer = BusinessCustomer::find($request->customer_id);

        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.business.sale-invoice.pdf', compact('sales','start_date','end_date','amount','quantity','vat_amount','date','customer'));
        return $pdf->download('SaleInvoice.pdf');
    }

    public function invoiceExportExcel(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $customer = BusinessCustomer::find($request->customer_id);

        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = BusinessSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = BusinessSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        Excel::create('Sale Invoice', function($excel) use ($sales, $start_date, $end_date, $amount, $date, $quantity, $vat_amount, $customer) {
            $excel->sheet('Sale Invoice', function($sheet) use ($sales, $start_date, $end_date, $amount, $date, $quantity, $vat_amount, $customer) {
                $sheet->loadView('pages.business.sale-invoice.excel',compact('sales','start_date','end_date','amount','date','vat_amount', 'customer','quantity'));
            });
        })->download('xls');
    }
}
