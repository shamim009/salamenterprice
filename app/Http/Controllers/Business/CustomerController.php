<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessCustomer;
use App\Models\BusinessModels\BusinessSale;
use App\Models\BusinessModels\BusinessSaleDelivery;
use App\Models\BusinessModels\BusinessSaleOrder;
use App\Models\BusinessModels\BusinessSaleTransaction;
use App\Models\BusinessModels\BusinessFiscalYear;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;
use Excel;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.business.customer.index');
    }

    //server side datatable
    public function allCustomers(Request $request)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2=> 'mobile',
                            3=> 'address',
                            4=> 'actions',
                        );
  
        $totalData = BusinessCustomer::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $customers = BusinessCustomer::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $customers =  BusinessCustomer::where('name','LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessCustomer::where('name','LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->count();
        }

        $data = array();
        if(!empty($customers))
        {
            foreach ($customers as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['name'] = $value->name;
                $nestedData['mobile'] = $value->mobile;
                $nestedData['address'] = $value->address;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('business/customer/report',$value->id) .'" class="btn btn-primary btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('business.customer.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.customer.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer = null;
        return view('pages.business.customer.create', compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        BusinessCustomer::create($request->all());

        return redirect()
                    ->route('business.customer.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = BusinessCustomer::find($id);
        return view('pages.business.customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        $customer = BusinessCustomer::find($id);
        $customer->update($request->all());

        return redirect()
                    ->route('business.customer.index')
                    ->with('success','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = BusinessCustomer::find($id);
        $customer->delete();

        return redirect()
                    ->route('business.customer.index')
                    ->with('success','Deleted Successfully');
    }

    //autocomplete search of customer
    public function autoCompleteSearch(Request $request)
    {
        $query = $request->get('term','');
                
        $customers = BusinessCustomer::where('name','LIKE','%'.$query.'%')
                            ->get();

        $results=array();                    
        
        if(count($customers ) > 0){
            foreach ($customers  as $customer) {
                $results[] = [ 'id' => $customer['id'], 'text' => $customer['name']];                  
            }
            return response()->json($results);
        }
        else{
            $data[] = 'Nothing Found';
            return $data;
        }
    }

    //date wise report
    public function returnDatePage($id){
        $customer = BusinessCustomer::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->sum('amount');

            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->sum('vat_amount');

            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->where('fiscal_year','<>',$fisc_year->id)->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->orderBy('date', 'desc')->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','2')->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','2')->where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','1')->sum('amount');

            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','1')->where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('amount');
            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');
            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->where('fiscal_year','<>',$fisc_year->id)->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        $balance = ( $sale_amount + $returned_amount ) - $paid_amount;
        $prev_balance = ( $other_sale_amount + $other_returned_amount ) - $other_paid_amount;

        return view('pages.business.customer.date',compact('customer','sales','balance','id','transactions','sale_amount','paid_amount','orders','remainingQuantity','saleQuantity','returned_amount','prev_balance','start_date','end_date','deliveries','delivery_amount','delivery_quantity'));
        // return view('pages.business.customer.date',compact('id'));
    }

    //individual customer transaction report
    public function report(Request $request)
    {
        $id = $request->id;
        $customer = BusinessCustomer::find($id);
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->whereBetween('date', [$start_date, $end_date])->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        $balance = ( $sale_amount + $returned_amount ) - $paid_amount;
        $prev_balance = ( $other_sale_amount + $other_returned_amount ) - $other_paid_amount;

        return view('pages.business.customer.report',compact('customer','sales','balance','id','transactions','sale_amount','paid_amount','orders','remainingQuantity','saleQuantity','returned_amount','prev_balance','start_date','end_date','deliveries','delivery_amount','delivery_quantity'));
    }

    public function printReport(Request $request)
    {
        $id = $request->id;
        $customer = BusinessCustomer::find($id);
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->whereBetween('date', [$start_date, $end_date])->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        $balance = ( $sale_amount + $returned_amount ) - $paid_amount;
        $prev_balance = ( $other_sale_amount + $other_returned_amount ) - $other_paid_amount;

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.business.customer.print',compact('customer','sales','balance','id','transactions','sale_amount','paid_amount','orders','remainingQuantity','saleQuantity','date','returned_amount','prev_balance','start_date','end_date','delivery_quantity','delivery_amount','deliveries'));
    }

    public function reportExportPDF(Request $request)
    {
        $id = $request->id;
        $customer = BusinessCustomer::find($id);
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->whereBetween('date', [$start_date, $end_date])->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        $balance = ( $sale_amount + $returned_amount ) - $paid_amount;
        $prev_balance = ( $other_sale_amount + $other_returned_amount ) - $other_paid_amount;

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.business.customer.pdf',compact('customer','sales','balance','id','transactions','sale_amount','paid_amount','orders','remainingQuantity','saleQuantity','date','returned_amount','prev_balance','start_date','end_date','delivery_quantity','delivery_amount','deliveries'));
        return $pdf->download('CustomerReport.pdf');
    }

    public function reportExportExcel(Request $request)
    {
        $id = $request->id;
        $customer = BusinessCustomer::find($id);
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->whereNotBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $sales = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();

            $sale_amount_wov = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_sale_amount_wov = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $vat_amount = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $other_vat_amount = BusinessSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->whereNotBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $sale_amount = $sale_amount_wov - $vat_amount;
            $other_sale_amount = $other_sale_amount_wov - $other_vat_amount;

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');

            $deliveries = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $delivery_quantity = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            $delivery_amount = BusinessSaleDelivery::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $transactions = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->whereBetween('date', [$start_date, $end_date])->get();

            $paid_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $returned_amount = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $other_returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->whereNotBetween('date', [$start_date, $end_date])->sum('amount');

            $orders = BusinessSaleOrder::where('fiscal_year',$fisc_year->id)->where('customer_id','=', $id)->where('company_id',$user->company_id)->whereBetween('date', [$start_date, $end_date])->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = BusinessSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        $balance = ( $sale_amount + $returned_amount ) - $paid_amount;
        $prev_balance = ( $other_sale_amount + $other_returned_amount ) - $other_paid_amount;

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        Excel::create('Customer Report', function($excel) use ($customer, $sales, $transactions, $balance, $date, $sale_amount, $paid_amount, $orders, $remainingQuantity, $saleQuantity, $returned_amount, $prev_balance, $start_date, $end_date, $delivery_quantity, $delivery_amount, $deliveries) {
            $excel->sheet('Customer Report', function($sheet) use ($customer, $sales, $transactions, $balance, $date, $sale_amount, $paid_amount, $orders, $remainingQuantity, $saleQuantity, $returned_amount, $prev_balance, $start_date, $end_date, $delivery_quantity, $delivery_amount, $deliveries) {
                $sheet->loadView('pages.business.customer.excel',compact('customer','sales','transactions','balance','date','sale_amount','otherFiscQuantity','orders','paid_amount','remainingQuantity','saleQuantity','returned_amount','prev_balance','start_date','end_date','delivery_quantity','delivery_amount','deliveries'));
            });
        })->download('xls');
    }

    public function payableAmount()
    {
        $customers = BusinessCustomer::all();
        $user = Auth::user();
        $pAmount = [];
        $pQuantity = [];
        $pPaid = [];
        $pReturned = [];
        $pBalance = [];

        if ($user->hasRole('super_admin')) {
            foreach ($customers as $customer) {
                $sale_amount_wov = BusinessSale::where('customer_id','=', $customer->id)->sum('amount');

                $vat_amount = BusinessSale::where('customer_id','=', $customer->id)->sum('vat_amount');

                $sale_amount = $sale_amount_wov - $vat_amount;

                $sale_quantity = BusinessSale::where('customer_id','=', $customer->id)->sum('quantity');

                $paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','2')->sum('amount');

                $returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','1')->sum('amount');

                $balance = ( $sale_amount + $returned_amount ) - $paid_amount;

                array_push($pAmount, $sale_amount);
                array_push($pQuantity, $sale_quantity);
                array_push($pPaid, $paid_amount);
                array_push($pReturned, $returned_amount);
                array_push($pBalance, $balance);
            }
        }
        else {
            foreach ($customers as $customer) {
                $sale_amount_wov = BusinessSale::where('customer_id','=', $customer->id)->where('company_id',$user->company_id)->sum('amount');

                $vat_amount = BusinessSale::where('customer_id','=', $customer->id)->where('company_id',$user->company_id)->sum('vat_amount');

                $sale_amount = $sale_amount_wov - $vat_amount;

                $sale_quantity = BusinessSale::where('customer_id','=', $customer->id)->where('company_id',$user->company_id)->sum('quantity');

                $paid_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');

                $returned_amount = BusinessSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');

                $balance = ( $sale_amount + $returned_amount ) - $paid_amount;

                array_push($pAmount, $sale_amount);
                array_push($pQuantity, $sale_quantity);
                array_push($pPaid, $paid_amount);
                array_push($pReturned, $returned_amount);
                array_push($pBalance, $balance);
            }
        }

        return view('pages.business.customer.payable',compact('customers','balance','sale_amount','paid_amount','sale_quantity','returned_amount','pAmount','pQuantity','pPaid','pReturned','pBalance'));
    }
}
