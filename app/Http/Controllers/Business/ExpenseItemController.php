<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessExpenseItem;
use App\Models\BusinessModels\BusinessExpense;
use App\Models\BusinessModels\BusinessExpenseTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\BusinessModels\BusinessFiscalYear;
use DB;
use PDF;
use Excel;
use Carbon\Carbon;

class ExpenseItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $expenseItems = BusinessExpenseItem::all();
        }
        else {
            $expenseItems = BusinessExpenseItem::where('company_id',$user->company_id)->get();
        }
        
        return view('pages.business.expense-item.index',compact('expenseItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $expenseItem = null;
        return view('pages.business.expense-item.create', compact('expenseItem'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>'required',
            'type'  =>'required',
        ]);

        $user = Auth::user();
        $request['company_id'] = $user->company_id;
        //dd($request);
        BusinessExpenseItem::create($request->all());
        return redirect()
                    ->route('business.expense-item.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expenseItem = BusinessExpenseItem::find($id);
        return view('pages.business.expense-item.edit', compact('expenseItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  =>'required',
            'type'  =>'required',
        ]);

        $expenseItem = BusinessExpenseItem::find($id);
        $expenseItem->update($request->all());
        return redirect()
                    ->route('business.expense-item.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expenseItem = BusinessExpenseItem::find($id);
        $expenseItem->delete();
        return redirect()
                    ->route('business.expense-item.index')
                    ->with('warning', 'Deleted Successfully');
    }

    //individual expense item report
    public function report($id)
    {
        $expenseItem = BusinessExpenseItem::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
        }
        else {
            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
        }
        
        return view('pages.business.expense-item.report',compact('expenseItem','id','totalExpense','totalPaid','fisc_year','othertotalPaid','otherExpense'));
    }

    //server side datatable
    public function particularExpense(Request $request, $id)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2=> 'company_id',
                            3=> 'amount',
                            4=> 'actions',
                        );
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->count();
        $totalSum = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $expenses = BusinessExpense::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('fiscal_year',$fisc_year->id)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $expenses = BusinessExpense::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('fiscal_year',$fisc_year->id)
            ->where(function ($query) use($search) {
                $query->where('date', 'LIKE',"%{$search}%")
                    ->orWhere('amount', 'LIKE',"%{$search}%")
                    ->orWhereHas('expenseitem', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    })
                    ->orWhereHas('company', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    });
            })          
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = BusinessExpense::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('fiscal_year',$fisc_year->id)
            ->where(function ($query) use($search) {
                $query->where('date', 'LIKE',"%{$search}%")
                    ->orWhere('amount', 'LIKE',"%{$search}%")
                    ->orWhereHas('expenseitem', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    })
                    ->orWhereHas('company', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    });
            }) 
            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['amount'] = $value->amount;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.expense.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.expense.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum   
                    );
            
        echo json_encode($json_data);        
    }

    //server side datatable business admin view
    public function particularExpenseAdminView(Request $request, $id)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'amount',
                            3 => 'details',
                            4 => 'actions',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $expenses = BusinessExpense::where('company_id',$user->company_id)
                        ->where('expenseItem_id', $id)
                        ->where('fiscal_year',$fisc_year->id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $expenses =  BusinessExpense::with('expenseitem')
                            ->where('company_id','=',$user->company_id)
                            ->where('expenseItem_id', $id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessExpense::with('expenseitem')             
                            ->where('company_id','=',$user->company_id)
                            ->where('expenseItem_id', $id)
                            ->where('fiscal_year',$fisc_year->id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['amount'] = $value->amount;
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.expense.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.expense.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    //server side datatable
    public function particularPayment(Request $request, $id)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2=> 'company_id',
                            3=> 'amount',
                            4=> 'actions',
                        );
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->count();
        $totalSum = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $expenses = BusinessExpenseTransaction::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('fiscal_year',$fisc_year->id)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $expenses = BusinessExpenseTransaction::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('fiscal_year',$fisc_year->id)
            ->where(function ($query) use($search) {
                $query->where('date', 'LIKE',"%{$search}%")
                    ->orWhere('amount', 'LIKE',"%{$search}%")
                    ->orWhereHas('expenseitem', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    })
                    ->orWhereHas('company', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    });
            })
            
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = BusinessExpenseTransaction::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('fiscal_year',$fisc_year->id)
            ->where(function ($query) use($search) {
                $query->where('date', 'LIKE',"%{$search}%")
                    ->orWhere('amount', 'LIKE',"%{$search}%")
                    ->orWhereHas('expenseitem', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    })
                    ->orWhereHas('company', function($query) use($search) {
                        $query->where('name','LIKE',"%{$search}%");
                    });
            })
            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['amount'] = $value->amount;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.expense-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.expense-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum   
                    );
            
        echo json_encode($json_data);        
    }

    //server side datatable business admin view
    public function particularPaymentAdminView(Request $request, $id)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'amount',
                            3 => 'details',
                            4 => 'actions',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $expenses = BusinessExpenseTransaction::where('company_id',$user->company_id)
                        ->where('expenseItem_id', $id)
                        ->where('fiscal_year',$fisc_year->id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $expenses =  BusinessExpenseTransaction::with('expenseitem')
                            ->where('company_id','=',$user->company_id)
                            ->where('expenseItem_id', $id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessExpenseTransaction::with('expenseitem')             
                            ->where('company_id','=',$user->company_id)
                            ->where('expenseItem_id', $id)
                            ->where('fiscal_year',$fisc_year->id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['amount'] = $value->amount;
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.expense-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.expense-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    public function byDateReport(Request $request)
    {
        $id = $request->id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $expenseItem = BusinessExpenseItem::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $expenses = BusinessExpense::where('expenseItem_id', $id)->where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $payments = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
        }
        else {
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();

            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $payments = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
        }
        
        return view('pages.business.expense-item.report-by-date',compact('expenseItem','id','totalExpense','totalPaid','fisc_year','othertotalPaid','otherExpense','expenses','payments','start_date','end_date'));
    }

    public function printReport(Request $request)
    {
        $id = $request->id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $expenseItem = BusinessExpenseItem::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $expenses = BusinessExpense::where('expenseItem_id', $id)->where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $payments = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
        }
        else {
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();

            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $payments = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
        }

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        
        return view('pages.business.expense-item.print',compact('expenseItem','id','totalExpense','totalPaid','fisc_year','othertotalPaid','otherExpense','expenses','payments','date','start_date','end_date'));
    }

    public function reportExportPDF(Request $request)
    {
        $id = $request->id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $expenseItem = BusinessExpenseItem::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $expenses = BusinessExpense::where('expenseItem_id', $id)->where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $payments = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
        }
        else {
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();

            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $payments = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
        }

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.business.expense-item.pdf', compact('expenseItem','totalExpense','totalPaid','othertotalPaid','otherExpense','date','fisc_year','expenses','payments','start_date','end_date'));
        return $pdf->download('ExpenseReport.pdf');
    }

    public function reportExportExcel(Request $request)
    {
        $id = $request->id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $expenseItem = BusinessExpenseItem::find($id);
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $expenses = BusinessExpense::where('expenseItem_id', $id)->where('fiscal_year',$fisc_year->id)->whereBetween('date', [$start_date, $end_date])->get();

            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $payments = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('expenseItem_id', $id)->sum('amount');
        }
        else {
            $expenses = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();

            $totalExpense = BusinessExpense::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $payments = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->get();
            $totalPaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherExpense = BusinessExpense::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
            $othertotalPaid = BusinessExpenseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
        }

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        Excel::create('Expense Report', function($excel) use ($expenses, $expenseItem, $totalExpense, $totalPaid, $date, $othertotalPaid, $fisc_year, $otherExpense, $payments, $start_date, $end_date) {
            $excel->sheet('Expense Report', function($sheet) use ($expenses, $expenseItem, $totalExpense, $totalPaid, $date, $othertotalPaid, $fisc_year, $otherExpense, $payments, $start_date, $end_date) {
                $sheet->loadView('pages.business.expense-item.excel',compact('expenses','expenseItem','totalExpense','totalPaid','date','othertotalPaid','otherExpense','payments','fisc_year','start_date','end_date'));
            });
        })->download('xls');
    }
}
