<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BusinessModels\BusinessFiscalYear;
use Illuminate\Support\Facades\Auth;
use App\Models\BusinessModels\BusinessBankAccountTransaction;
use App\Models\BusinessModels\BusinessExpenseTransaction;
use App\Models\BusinessModels\BusinessPurchaseTransaction;
use App\Models\BusinessModels\BusinessSaleTransaction;
use App\Models\BusinessModels\BusinessBankAccount;

class FiscalYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $years = BusinessFiscalYear::latest()->get();
        //dd($years);
        return view('pages.business.fiscal-year.index',compact('years'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $year = null;
        return view('pages.business.fiscal-year.create', compact('year'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'  =>'required'
        ]);

        $year = new BusinessFiscalYear;

        $year->title = $request->title;
        $year->status = 0;

        $year->save();

        return redirect()
                    ->route('business.fiscal-year.index')
                    ->with('success', 'Year Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $year = BusinessFiscalYear::find($id);
        return view('pages.business.fiscal-year.edit', compact('year'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'  =>'required'
        ]);

        $year = BusinessFiscalYear::find($id);

        $year->title = $request->title;

        $year->save();

        return redirect()
                    ->route('business.fiscal-year.index')
                    ->with('success', 'Year Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //active a year
    public function activateCurrentYear($id)
    {
        $years = BusinessFiscalYear::all();
        foreach ($years as $key => $value) {
            $value->status = 0;
            $value->save();
        }
        $year = BusinessFiscalYear::find($id);
        $year->status = 1;
        $year->save();
        return redirect()
                    ->route('business.fiscal-year.index')
                    ->with('success', 'Fiscal Year Changed');
    }

    public function cashReport()
    {
        $user = Auth::user();
        $bankAccounts = BusinessBankAccount::where('company_id',$user->company_id)->sum('current_amount');
        $purchase_payments = BusinessPurchaseTransaction::where('company_id',$user->company_id)->where('purpose','=','2')->where('payment_mode','=','1')->sum('amount');
        $purchase_returns = BusinessPurchaseTransaction::where('company_id',$user->company_id)->where('purpose','=','1')->where('payment_mode','=','1')->sum('amount');
        $sale_payments = BusinessSaleTransaction::where('company_id',$user->company_id)->where('purpose','=','2')->where('payment_mode','=','1')->sum('amount');
        $sale_returns = BusinessSaleTransaction::where('company_id',$user->company_id)->where('purpose','=','1')->where('payment_mode','=','1')->sum('amount');
        $expense = BusinessExpenseTransaction::where('company_id',$user->company_id)->where('payment_mode','=','1')->sum('amount');
        $bank_deposit = BusinessBankAccountTransaction::where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');
        $bank_withdraw = BusinessBankAccountTransaction::where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');

        $inhand = ($purchase_returns + $sale_payments + $bank_withdraw) - ($purchase_payments + $sale_returns + $expense + $bank_deposit);
        return view('pages.business.cash',compact('inhand','bankAccounts'));
    }
}
