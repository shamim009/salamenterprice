<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\BusinessModels\BusinessSupplier;
use App\Models\BusinessModels\BusinessPurchaseTransaction;
use App\Models\BusinessModels\BusinessCashBook;
use App\Models\BusinessModels\BusinessBankAccount;
use App\Models\BusinessModels\BusinessBankAccountTransaction;
use App\Models\BusinessModels\BusinessFiscalYear;
use App\Bank;
use App\MobileBanking;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;
use PDF;
use Excel;

class PurchaseTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $totalSum = BusinessPurchaseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->sum('amount');
        }
        else {
            $totalSum = BusinessPurchaseTransaction::where('company_id',$user->company_id)->where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->sum('amount');
        }
        return view('pages.business.purchase-transaction.index',compact('fisc_year','totalSum'));
    }

    //server side datatable super admin view
    public function allTransactions(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'supplier_id',
                            4 => 'amount',                    
                            5 => 'payment_mode',
                            6 => 'details',
                            7 => 'actions',
                        );
        
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->count();
        $totalFiltered = $totalData;
        $totalSum = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->sum('amount'); 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $transactions = BusinessPurchaseTransaction::where('purpose','=','2')
                    ->where('fiscal_year',$fisc_year->id)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  BusinessPurchaseTransaction::with('supplier','bank','company','mobileBanking')
                        ->where('purpose','=','2')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('supplier', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('bank', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = BusinessPurchaseTransaction::with('supplier','bank','company','mobileBanking')
                        ->where('purpose','=','2')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('supplier', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('bank', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['amount'] = $value->amount;

                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.purchase-payment.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('business.purchase-payment.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.purchase-payment.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable business admin view
    public function allTransactionsAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'supplier_id',
                            3 => 'amount',                          
                            4 => 'payment_mode',
                            5 => 'payment_type',
                            6 => 'actions',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        $totalData = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->
        where('purpose','=','2')->count();
        $totalSum = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->
        where('purpose','=','2')->sum('amount');
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $transactions = BusinessPurchaseTransaction::where('company_id',$user->company_id)
                        ->where('purpose','=','2')
                        ->where('fiscal_year',$fisc_year->id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $transactions =  BusinessPurchaseTransaction::with('supplier','bank','mobileBanking')
                            ->where('company_id','=',$user->company_id)
                            ->where('purpose','=','2')
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('bank', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessPurchaseTransaction::with('supplier','bank','mobileBanking')            
                            ->where('purpose','=','2')
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('bank', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });;
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['amount'] = $value->amount;

                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }
                $nestedData['details'] = $value->details;            
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.purchase-payment.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('business.purchase-payment.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.purchase-payment.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $purchaseTransaction = null;
        $suppliers = BusinessSupplier::all('id', 'name');
        $banks = Bank::all('id', 'name');
        $mobileBankings = MobileBanking::all('id', 'name');
        $user = Auth::user();
        $accounts = BusinessBankAccount::where('company_id',$user->company_id)->get();
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.purchase-transaction.create', compact('purchaseTransaction','suppliers','banks','mobileBankings','accounts','fisc_years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'      => 'required',
            'supplier_id'  => ['required', Rule::notIn(['','0'])],
            'payment_type'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);

        $user = Auth::user();
        DB::transaction(function () use ($request, $user) {
            $transaction = new BusinessPurchaseTransaction;
            $token = time().str_random(10);

            $transaction->date = $request->date;
            $transaction->supplier_id = $request->supplier_id;
            $transaction->company_id = $user->company_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->token = $token;
            $transaction->payment_type = $request->payment_type;
            $transaction->payment_mode = $request->payment_mode;
            $transaction->fiscal_year = $request->fiscal_year;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->receiver = $request->receiver;
            $transaction->purpose = 2;

            $transaction->save();

            $cashBook = new BusinessCashBook;
            $cashBook->date = $request->date;
            $cashBook->purpose = 2;
            $cashBook->purchase_transaction_id = $transaction->id;
            $cashBook->company_id = $user->company_id;
            $cashBook->fiscal_year = $request->fiscal_year;

            $cashBook->save();

            if ($request->payment_mode == 2) {
                $bankTransaction = new BusinessBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $user->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 4;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;
                $bankTransaction->token = $token;
                $bankTransaction->fiscal_year = $request->fiscal_year;

                $bankTransaction->save();

                $bankAccount = BusinessBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                $bankAccount->save();

            }
        });

        return redirect()
                    ->route('business.purchase-payment.index')
                    ->with('success', 'Payment Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = BusinessPurchaseTransaction::find($id);
        //dd($transaction);
        return view('pages.business.purchase-transaction.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banks = Bank::all('id', 'name');
        $mobileBankings = MobileBanking::all('id', 'name');
        $purchaseTransaction = BusinessPurchaseTransaction::find($id);
        $suppliers = BusinessSupplier::all('id', 'name');
        $user = Auth::user();
        $accounts = BusinessBankAccount::where('company_id',$user->company_id)->get();
        $fisc_years = BusinessFiscalYear::all();
        return view('pages.business.purchase-transaction.edit', compact('purchaseTransaction','suppliers','banks','mobileBankings','accounts','fisc_years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date'      => 'required',
            'supplier_id'  => ['required', Rule::notIn(['','0'])],
            'payment_type'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'fiscal_year'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);
        DB::transaction(function () use ($request, $id) {
            $transaction = BusinessPurchaseTransaction::find($id);

            if ($transaction->payment_mode != 2 && $request->payment_mode == 2) {
                $bankTransaction = new BusinessBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $transaction->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 4;
                $bankTransaction->token = $transaction->token;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;
                $bankTransaction->fiscal_year = $request->fiscal_year;

                $bankTransaction->save();

                $bankAccount = BusinessBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                $bankAccount->save();
            }

            if ($transaction->payment_mode == 2 && $request->payment_mode == 2) {
                $bankTransaction = BusinessBankAccountTransaction::where([
                    ['purpose','=','4'],
                    ['token',$transaction->token]
                ])->first();
                if ($bankTransaction->bank_account_id == $request->bank_account_id) {
                    $bankAccount = BusinessBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = ($bankAccount->current_amount + $bankTransaction->amount) - $request->amount;
                    $bankAccount->save();
                }

                else{
                    //remove balance from the old account
                    $oldBankAccount = BusinessBankAccount::find($bankTransaction->bank_account_id);

                    $oldBankAccount->current_amount = $oldBankAccount->current_amount + $bankTransaction->amount;
                    $oldBankAccount->save();

                    //add balance to the new account
                    $bankAccount = BusinessBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                    $bankAccount->save();
                }

                $bankTransaction->date = $request->date;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;
                $bankTransaction->fiscal_year = $request->fiscal_year;

                $bankTransaction->save();
            }

            if ($transaction->payment_mode == 2 && $request->payment_mode != 2) {
                $bankTransaction = BusinessBankAccountTransaction::where([
                    ['purpose','=','4'],
                    ['token',$transaction->token]
                ])->first();

                $bankAccount = BusinessBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            $transaction->date = $request->date;
            $transaction->supplier_id = $request->supplier_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->payment_type = $request->payment_type;
            $transaction->payment_mode = $request->payment_mode;
            $transaction->fiscal_year = $request->fiscal_year;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->details = $request->details;
            $transaction->receiver = $request->receiver;

            $transaction->save();

            $cashBook = BusinessCashBook::where('purchase_transaction_id',$transaction->id)->first();
            $cashBook->date = $request->date;
            $cashBook->fiscal_year = $request->fiscal_year;
            $cashBook->save();
        });
        return redirect()
                    //->back()
                    ->route('business.supplier.report', ['id' => $request->supplier_id])
                    ->with('success', 'Payment Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $cashBook = BusinessCashBook::where('purchase_transaction_id',$id)->first();
            $cashBook->delete();

            $purchaseTransaction = BusinessPurchaseTransaction::find($id);

            if ($purchaseTransaction->payment_mode == 2) {
                $bankTransaction = BusinessBankAccountTransaction::where([
                    ['purpose','=','4'],
                    ['token',$purchaseTransaction->token]
                ])->first();

                $bankAccount = BusinessBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }
            
            $purchaseTransaction->delete();
        });

        return redirect()
                    //->back()
                    ->route('business.purchase-payment.index')
                    ->with('warning', 'Payment Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount'); 
            $otherFiscAmount = BusinessPurchaseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherFiscAmount = BusinessPurchaseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->sum('amount');
        }
        return view('pages.business.purchase-transaction.report', compact('transactions','start_date','end_date','amount','fisc_year','otherFiscAmount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount'); 
            $otherFiscAmount = BusinessPurchaseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherFiscAmount = BusinessPurchaseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.business.purchase-transaction.print', compact('transactions','start_date','end_date','amount','date','fisc_year','otherFiscAmount'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount'); 
            $otherFiscAmount = BusinessPurchaseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherFiscAmount = BusinessPurchaseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.business.purchase-transaction.pdf', compact('transactions','start_date','end_date','amount','date','fisc_year','otherFiscAmount'));
        return $pdf->download('PurchasePaymentReport.pdf');
    }

    public function reportExportExcel(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();

        if ($user->hasRole('super_admin')) {
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount'); 
            $otherFiscAmount = BusinessPurchaseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $otherFiscAmount = BusinessPurchaseTransaction::where('fiscal_year','<>',$fisc_year->id)->where('purpose','=','2')->where('company_id',$id)->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        Excel::create('Purchase Payment Report', function($excel) use ($transactions, $start_date, $end_date, $amount, $date, $fisc_year, $otherFiscAmount) {
            $excel->sheet('Purchase Payment Report', function($sheet) use ($transactions, $start_date, $end_date, $amount, $date, $fisc_year, $otherFiscAmount) {
                $sheet->loadView('pages.business.purchase-transaction.excel',compact('transactions','start_date','end_date','amount','date','otherFiscAmount','fisc_year'));
            });
        })->download('xls');
    }
}
