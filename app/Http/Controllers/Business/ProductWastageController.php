<?php

namespace App\Http\Controllers\Business;

use App\Models\BusinessModels\BusinessProductWastage;
use App\Models\BusinessModels\BusinessSupplier;
use App\Models\BusinessModels\BusinessProduct;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductWastageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.business.product-wastage.index');
    }

    //server side datatable super admin view
    public function allWastages(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'supplier_id',
                            4 => 'product_id',
                            5 => 'quantity',
                            6 => 'rate',
                            7 => 'amount',
                            8 => 'actions',
                        );
        

        $totalData = BusinessProductWastage::count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $wastages = BusinessProductWastage::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $wastages =  BusinessProductWastage::with('supplier','product','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhereHas('supplier', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('product', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = BusinessProductWastage::with('supplier','product','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhereHas('supplier', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('product', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->count();
        }

        $data = array();
        if(!empty($wastages))
        {
            foreach ($wastages as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;                    
                $nestedData['rate'] = $value->rate;
                $nestedData['amount'] = $value->amount;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.product-wastage.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('business.product-wastage.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.product-wastage.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable business admin view
    public function allWastagesAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'supplier_id',
                            3 => 'product_id',
                            4 => 'quantity',
                            5 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = BusinessProductWastage::where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $wastages = BusinessProductWastage::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $wastages =  BusinessProductWastage::with('supplier','product')
                            ->where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessProductWastage::with('supplier','product')             
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($wastages))
        {
            foreach ($wastages as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;                    
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('business.product-wastage.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('business.product-wastage.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.product-wastage.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = BusinessSupplier::all('id','name');
        $products = BusinessProduct::all('id','name');
        return view('pages.business.product-wastage.create', compact('suppliers','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'supplier_id'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            //$totalAmount = [];

            for ($i=0; $i < count($request->product_id) ; $i++) {
                //$token = time().str_random(10); 
                $wastage =  new BusinessProductWastage;

                $wastage->date = $request->date;
                $wastage->company_id = $user->company_id;
                $wastage->supplier_id = $request->supplier_id;
                //$wastage->invoice = $request->invoice;
                $wastage->details = $request->details;
                $wastage->product_id = $request->product_id[$i];
                //$wastage->rate = $request->rate[$i];
                $wastage->quantity = $request->quantity[$i];
                //$wastage->token = $token;           

                $wastage->save();

                $product = BusinessProduct::find($request->product_id[$i]);
                $product->stock = $product->stock - $request->quantity[$i];
                $product->save();
            }
        });
        return redirect()
                    ->route('business.product-wastage.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $wastage = BusinessProductWastage::find($id);
        return view('pages.business.product-wastage.show',compact('wastage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wastage = BusinessProductWastage::find($id);
        $companies = Company::all('id','name');
        $suppliers = BusinessSupplier::all('id','name');
        $products = BusinessProduct::all('id','name');
        return view('pages.business.product-wastage.edit', compact('wastage','suppliers','companies','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([ 
            'supplier_id'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'quantity'              => 'required',
        ]);

        DB::transaction(function () use ($request, $id) {
            $wastage =  BusinessProductWastage::find($id);

            $oldProduct = BusinessProduct::find($wastage->product_id);
            $oldProduct->stock = ($oldProduct->stock + $wastage->quantity) - $request->quantity;
            $oldProduct->save();

            $wastage->date = $request->date;
            $wastage->details = $request->details;
            $wastage->supplier_id = $request->supplier_id;
            $wastage->rate = $request->rate;
            $wastage->quantity = $request->quantity;
            $wastage->amount = $request->amount;

            $wastage->save();
        });
        return redirect()
                    ->route('business.product-wastage.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $wastage =  BusinessProductWastage::find($id);

            $oldProduct = BusinessProduct::find($wastage->product_id);
            $oldProduct->stock = $oldProduct->stock + $wastage->quantity;
            $oldProduct->save();

            $wastage->delete();
        });
        return redirect()
                    ->route('business.product-wastage.index')
                    ->with('success', 'Deleted Successfully');
    }
}
