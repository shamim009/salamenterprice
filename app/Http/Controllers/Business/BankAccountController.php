<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BusinessModels\BusinessBankAccount;
use App\Models\BusinessModels\BusinessBankAccountTransaction;
use App\Bank;
use Illuminate\Support\Facades\Auth;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $bankAccounts = BusinessBankAccount::latest()->get();
        }
        else {
            $bankAccounts = BusinessBankAccount::where('company_id',$user->company_id)->latest()->get();
        }
        return view('pages.business.bank-account.index',compact('bankAccounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bankAccount = null;
        $banks = Bank::all('id', 'name');
        return view('pages.business.bank-account.create', compact('bankAccount','banks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'account_no'  =>'required',
            'bank_id'      => 'required',
            'current_amount'      => 'required|numeric',
        ]);

        $user = Auth::user();

        $bankAccount = new BusinessBankAccount;

        $bankAccount->account_no = $request->account_no;
        $bankAccount->bank_id = $request->bank_id;
        $bankAccount->company_id = $user->company_id;
        $bankAccount->details = $request->details;
        $bankAccount->current_amount = $request->current_amount;

        $bankAccount->save();

        return redirect()
                    ->route('business.bank-account.index')
                    ->with('success', 'Account Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bankAccount = BusinessBankAccount::find($id);
        $banks = Bank::all('id', 'name');
        return view('pages.business.bank-account.edit', compact('bankAccount','banks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'account_no'  =>'required',
            'bank_id'      => 'required',
            'current_amount'      => 'required|numeric',
        ]);

        $bankAccount = BusinessBankAccount::find($id);

        $bankAccount->account_no = $request->account_no;
        $bankAccount->bank_id = $request->bank_id;
        $bankAccount->details = $request->details;
        $bankAccount->current_amount = $request->current_amount;

        $bankAccount->save();

        return redirect()
                    ->route('business.bank-account.index')
                    ->with('success', 'Account Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bankAccount = BusinessBankAccount::find($id);
        $bankAccount->delete();
        return redirect()
                    ->route('business.bank-account.index')
                    ->with('warning', 'Account Deleted Successfully');
    }

    public function report($id)
    {
        $bankAccount = BusinessBankAccount::find($id);
        $transactions = BusinessBankAccountTransaction::where('bank_account_id', $id)->get();
        return view('pages.business.bank-account.report',compact('bankAccount','transactions','id'));
    }
}
