<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BusinessModels\BusinessBankAccountTransaction;
use Illuminate\Validation\Rule;
use App\Models\BusinessModels\BusinessBankAccount;
use DB;
use Illuminate\Support\Facades\Auth;

class BankAccountTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.business.bank-transaction.index');
    }

    //server side datatable super admin view
    public function allTransactions(Request $request)
    {     
        $columns = array( 
            0 =>'id', 
            1 =>'date',
            2 =>'company_id',
            3 => 'bank_account_id',
            4 => 'amount',
            5 => 'purpose',
            6 => 'cheque_number',
            7 => 'actions',
        );
        

        $totalData = BusinessBankAccountTransaction::count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {

            $transactions = BusinessBankAccountTransaction::offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  BusinessBankAccountTransaction::with('account','company')
            ->where(function ($query) use($search) {
                $query->where('amount','LIKE',"%{$search}%")
                ->orWhere('date', 'LIKE',"%{$search}%")
                ->orWhereHas('account', function($query) use($search) {
                    $query->where('account_no','LIKE',"%{$search}%");
                })
                ->orWhereHas('company', function($query) use($search) {
                    $query->where('name','LIKE',"%{$search}%");
                });
            })
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = BusinessBankAccountTransaction::with('account','company')
            ->where(function ($query) use($search) {
                $query->where('amount','LIKE',"%{$search}%")
                ->orWhere('date', 'LIKE',"%{$search}%")
                ->orWhereHas('account', function($query) use($search) {
                    $query->where('account_no','LIKE',"%{$search}%");
                })
                ->orWhereHas('company', function($query) use($search) {
                    $query->where('name','LIKE',"%{$search}%");
                });
            })
            ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['bank_account_id'] = $value->account->account_no;
                $nestedData['amount'] = $value->amount;
                if ($value->purpose == 1) {
                    $nestedData['purpose'] = 'Deposit';
                }
                else if ($value->purpose == 2) {
                    $nestedData['purpose'] = 'Withdraw';
                }
                else if ($value->purpose == 4) {
                    $nestedData['purpose'] = 'Purchase Payment';
                }
                else if ($value->purpose == 5) {
                    $nestedData['purpose'] = 'Expense Payment';
                }
                else if ($value->purpose == 6) {
                    $nestedData['purpose'] = 'Sale Payment Return';
                }
                else if ($value->purpose == 7) {
                    $nestedData['purpose'] = 'Purchase Payment Return';
                }
                else {
                    $nestedData['purpose'] = 'Sale Payment';
                }
                $nestedData['cheque_number'] = $value->cheque_number;

                if ($value->purpose == 1 || $value->purpose == 2) {
                    $nestedData['actions'] = '<div class="btn-group">
                    <a href="'.route('business.bank-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                    Update
                    </a>
                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.bank-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                    </div>';
                }
                else {
                    $nestedData['actions'] = '';
                }
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data,   
        );

        echo json_encode($json_data);        
    }


    //server side datatable business admin view
    public function allTransactionsAdminView(Request $request)
    {     
        $columns = array( 
            0 =>'id', 
            1 =>'date',
            2 => 'supplier_id',
            3 => 'amount',
            4 => 'purpose',
            5 => 'cheque_number',
            6 => 'actions',
        );
        
        $user = Auth::user();

        $totalData = BusinessBankAccountTransaction::where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {

            $transactions = BusinessBankAccountTransaction::where('company_id',$user->company_id)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $transactions =  BusinessBankAccountTransaction::with('company','account')
            ->where('company_id','=',$user->company_id)
            ->where(function ($query) use($search) {
                $query->where('amount','LIKE',"%{$search}%")
                ->orWhere('date', 'LIKE',"%{$search}%")
                ->orWhereHas('account', function($query) use($search) {
                    $query->where('account_no','LIKE',"%{$search}%");
                })
                ->orWhereHas('company', function($query) use($search) {
                    $query->where('name','LIKE',"%{$search}%");
                });
            })

            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = BusinessBankAccountTransaction::with('company','account')
            ->where('company_id','=',$user->company_id)          
            ->where(function ($query) use($search) {
                $query->where('amount','LIKE',"%{$search}%")
                ->orWhere('date', 'LIKE',"%{$search}%")
                ->orWhereHas('account', function($query) use($search) {
                    $query->where('account_no','LIKE',"%{$search}%");
                })
                ->orWhereHas('company', function($query) use($search) {
                    $query->where('name','LIKE',"%{$search}%");
                });
            })                   
            ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['bank_account_id'] = $value->account->account_no;
                $nestedData['amount'] = $value->amount;
                if ($value->purpose == 1) {
                    $nestedData['purpose'] = 'Deposit';
                }
                else if ($value->purpose == 2) {
                    $nestedData['purpose'] = 'Withdraw';
                }
                else if ($value->purpose == 4) {
                    $nestedData['purpose'] = 'Purchase Payment';
                }
                else if ($value->purpose == 5) {
                    $nestedData['purpose'] = 'Expense Payment';
                }
                else {
                    $nestedData['purpose'] = 'Sale Payment';
                }
                $nestedData['cheque_number'] = $value->cheque_number;

                if ($value->purpose == 1 || $value->purpose == 2) {
                    $nestedData['actions'] = '<div class="btn-group">
                    <a href="'.route('business.bank-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                    Update
                    </a>
                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('business.bank-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                    </div>';
                }
                else {
                    $nestedData['actions'] = '';
                }
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data,   
        );

        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bankTransaction = null;
        $user = Auth::user();
        $bankAccounts = BusinessBankAccount::where('company_id',$user->company_id)->get();
        return view('pages.business.bank-transaction.create', compact('bankAccounts','bankTransaction'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'  =>'required',
            'amount'      => 'required|numeric',
            'bank_account_id'  => ['required', Rule::notIn(['','0'])],
            'purpose'  => ['required', Rule::notIn(['','0'])],
        ]);
        DB::transaction(function () use ($request) {
            $user = Auth::user();

            //$token = time().str_random(10);

            $transaction = new BusinessBankAccountTransaction;

            $transaction->date = $request->date;
            $transaction->company_id = $user->company_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->purpose = $request->purpose;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;

            $transaction->save();

            $bankAccount = BusinessBankAccount::find($request->bank_account_id);
            if ($request->purpose == 1) {
                $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                $bankAccount->save();
            }
            else {
                $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                $bankAccount->save();
            }
        });
        return redirect()
        ->route('business.bank-account.index')
        ->with('success', 'Transaction Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bankTransaction = BusinessBankAccountTransaction::find($id);
        $user = Auth::user();
        $bankAccounts = BusinessBankAccount::where('company_id',$user->company_id)->get();
        return view('pages.business.bank-transaction.edit', compact('bankAccounts','bankTransaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date'  =>'required',
            'amount'      => 'required|numeric',
            'bank_account_id'  => ['required', Rule::notIn(['','0'])],
            'purpose'  => ['required', Rule::notIn(['','0'])],
        ]);
        DB::transaction(function () use ($request,$id) {
            $user = Auth::user();

            $transaction = BusinessBankAccountTransaction::find($id);

            if ($transaction->bank_account_id != $request->bank_account_id) {
                //update previous bank account
                $oldBankAccount = BusinessBankAccount::find($transaction->bank_account_id);
                if ($transaction->purpose == 1) {
                    $oldBankAccount->current_amount = $oldBankAccount->current_amount - $transaction->amount;
                    $oldBankAccount->save();
                }
                else {
                    $oldBankAccount->current_amount = $oldBankAccount->current_amount + $transaction->amount;
                    $oldBankAccount->save();
                }
                //add amount to the new account
                $bankAccount = BusinessBankAccount::find($request->bank_account_id);
                if ($request->purpose == 1) {
                    $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                    $bankAccount->save();
                }
                else {
                    $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                    $bankAccount->save();
                }
            }
            else {
                $bankAccount = BusinessBankAccount::find($request->bank_account_id);

                if ($transaction->purpose == 1) {
                    $bankAccount->current_amount = $bankAccount->current_amount - $transaction->amount;
                    $bankAccount->save();
                }
                else {
                    $bankAccount->current_amount = $bankAccount->current_amount + $transaction->amount;
                    $bankAccount->save();
                }

                if ($request->purpose == 1) {
                    $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                    $bankAccount->save();
                }
                else {
                    $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                    $bankAccount->save();
                }
            }

            $transaction->date = $request->date;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->purpose = $request->purpose;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;

            $transaction->save();
        });
        return redirect()
        ->route('business.bank-account.index')
        ->with('success', 'Transaction Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {

            $transaction = BusinessBankAccountTransaction::find($id);

            $bankAccount = BusinessBankAccount::find($transaction->bank_account_id);

            if ($transaction->purpose == 1) {
                $bankAccount->current_amount = $bankAccount->current_amount - $transaction->amount;
                $bankAccount->save();
            }
            else {
                $bankAccount->current_amount = $bankAccount->current_amount + $transaction->amount;
                $bankAccount->save();
            }

            $transaction->delete();
        });
        return redirect()
        ->route('business.bank-account.index')
        ->with('success', 'Transaction Deleted Successfully');
    }
}
