<?php

namespace App\Http\Controllers;

use App\LoanTransaction;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Loan;
use App\Bank;
use App\MobileBanking;
use DB;

class LoanTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = LoanTransaction::latest()->get();
        return view('pages.loan-transaction.index', compact('transactions'));

    }


    //server side datatable super admin view
    public function allTransactions(Request $request)
    {     
        $columns = array( 
                            0 => 'id', 
                            1 => 'date',
                            2 => 'loan_id',
                            3 => 'purpose',
                            4 => 'amount',
                            5 => 'payment_mode',
                            6 => 'actions',
                        );
        

        $totalData = LoanTransaction::count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $transactions = LoanTransaction::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  LoanTransaction::with('loan')
                        ->where('amount','LIKE',"%{$search}%")
                        ->orWhere('date','LIKE',"%{$search}%")
                        ->orWhereHas('loan', function($query) use($search) {
                            $query->where('loan_from','LIKE',"%{$search}%");
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = LoanTransaction::with('loan')
                        ->where('amount','LIKE',"%{$search}%")
                        ->orWhere('date','LIKE',"%{$search}%")
                        ->orWhereHas('loan', function($query) use($search) {
                            $query->where('loan_from','LIKE',"%{$search}%");
                        })
                        ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['loan_id'] = $value->loan->loan_from;
                if ($value->purpose == 1) {
                    $nestedData['purpose'] = 'Profit Return';
                }
                else {
                    $nestedData['purpose'] = 'Main Amount Return';
                }
                $nestedData['amount'] = $value->amount;
                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('loan-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('loan-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $loanTransaction = null;
        $loans = Loan::all();
        $banks = Bank::all('id', 'name');
        $mobileBankings = MobileBanking::all('id', 'name');
        return view('pages.loan-transaction.create', compact('loanTransaction','loans','banks','mobileBankings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'      => 'required',
            'loan_id'  => ['required', Rule::notIn(['','0'])],
            'purpose'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);
        DB::transaction(function () use ($request) {
            $transaction = new LoanTransaction;

            $transaction->date = $request->date;
            $transaction->loan_id = $request->loan_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->purpose = $request->purpose;
            $transaction->payment_mode = $request->payment_mode;
            $transaction->bank_id = $request->bank_id;
            $transaction->bank_account_number = $request->bank_account_number;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            //$transaction->receiver = $request->receiver;

            $transaction->save();

            if ($request->purpose == 2) {
                $loan = Loan::find($request->loan_id);
                $loan->amount = $loan->amount - $request->amount;
                $loan->save();
            }
        });

        return redirect()
                    ->route('loan.index')
                    ->with('success', 'Payment Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanTransaction  $loanTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(LoanTransaction $loanTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanTransaction  $loanTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanTransaction $loanTransaction)
    {
        $loans = Loan::all();
        $banks = Bank::all('id', 'name');
        $mobileBankings = MobileBanking::all('id', 'name');
        return view('pages.loan-transaction.edit', compact('loanTransaction','loans','banks','mobileBankings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanTransaction  $loanTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanTransaction $loanTransaction)
    {
        $request->validate([
            'date'      => 'required',
            'loan_id'  => ['required', Rule::notIn(['','0'])],
            'purpose'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);
        DB::transaction(function () use ($request, $loanTransaction) {

            if ($loanTransaction->purpose != 2 && $request->purpose == 2) {
                $loan = Loan::find($request->loan_id);
                $loan->amount = $loan->amount - $request->amount;
                $loan->save();
            }

            else if ($loanTransaction->payment_mode == 2 && $request->payment_mode == 2) {
                if ($loanTransaction->loan_id == $request->loan_id) {
                    $loan = Loan::find($request->loan_id);
                    $loan->amount = ($loan->amount + $loanTransaction) - $request->amount;
                    $loan->save();
                }
                else {
                    $old_loan = Loan::find($loanTransaction->loan_id);
                    $old_loan->amount = $old_loan->amount + $loanTransaction;
                    $old_loan->save();

                    $loan = Loan::find($request->loan_id);
                    $loan->amount = $loan->amount - $request->amount;
                    $loan->save();
                }
                
            }

            else if ($loanTransaction->payment_mode == 2 && $request->payment_mode != 2) {
                if ($loanTransaction->loan_id == $request->loan_id) {
                    $loan = Loan::find($request->loan_id);
                    $loan->amount = $loan->amount + $loanTransaction;
                    $loan->save();
                }
                else {
                    $old_loan = Loan::find($loanTransaction->loan_id);
                    $old_loan->amount = $old_loan->amount + $loanTransaction;
                    $old_loan->save();
                }
            }

            $loanTransaction->date = $request->date;
            $loanTransaction->loan_id = $request->loan_id;
            $loanTransaction->details = $request->details;
            $loanTransaction->amount = $request->amount;
            $loanTransaction->purpose = $request->purpose;
            $loanTransaction->payment_mode = $request->payment_mode;
            $loanTransaction->bank_id = $request->bank_id;
            $loanTransaction->bank_account_number = $request->bank_account_number;
            $loanTransaction->cheque_number = $request->cheque_number;
            $loanTransaction->mobile_banking_id = $request->mobile_banking_id;
            $loanTransaction->phone_number = $request->phone_number;
            //$loanTransaction->receiver = $request->receiver;

            $loanTransaction->save();
        });

        return redirect()
                    ->route('loan.index')
                    ->with('success', 'Payment Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanTransaction  $loanTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanTransaction $loanTransaction)
    {
        DB::transaction(function () use ($loanTransaction) {

            if ($loanTransaction->purpose == 2) {
                $loan = Loan::find($loanTransaction->loan_id);
                $loan->amount = $loan->amount + $loanTransaction->amount;
                $loan->save();
            }

            $loanTransaction->delete();
        });

        return redirect()
                    ->route('loan.index')
                    ->with('success', 'Payment Deleted Successfully');
    }
}
