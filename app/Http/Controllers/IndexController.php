<?php

namespace App\Http\Controllers;

use App\Index;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.index.index');
    }

    //server side datatable super admin view
    public function allIndices(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2 =>'address',
                            3 => 'mobile',
                            4 => 'contacts',
                            5 => 'actions',
                        );
        

        $totalData = Index::count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $indices = Index::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $indices =  Index::where('name','LIKE',"%{$search}%")
                        ->orWhere('address','LIKE',"%{$search}%")
                        ->orWhere('mobile','LIKE',"%{$search}%")
                        ->orWhere('contacts','LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = Index::where('name','LIKE',"%{$search}%")
                        ->orWhere('address','LIKE',"%{$search}%")
                        ->orWhere('mobile','LIKE',"%{$search}%")
                        ->orWhere('contacts','LIKE',"%{$search}%")
                        ->count();
        }

        $data = array();
        if(!empty($indices))
        {
            foreach ($indices as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['name'] = $value->name;
                $nestedData['address'] = $value->address;
                $nestedData['mobile'] = $value->mobile;
                $nestedData['contacts'] = $value->contacts;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('index.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('index.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $index = null;
        return view('pages.index.create', compact('index'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type'      => 'required',
            'name'    => 'required',
        ]);

        Index::create($request->all());

        return redirect()
                    ->route('index.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Index  $index
     * @return \Illuminate\Http\Response
     */
    public function show(Index $index)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Index  $index
     * @return \Illuminate\Http\Response
     */
    public function edit(Index $index)
    {
        return view('pages.index.edit', compact('index'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Index  $index
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Index $index)
    {
        $request->validate([
            'name'      => 'required|string',
            'type'    => 'required',
        ]);

        $index->update($request->all());

        return redirect()
                    ->route('index.index')
                    ->with('success','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Index  $index
     * @return \Illuminate\Http\Response
     */
    public function destroy(Index $index)
    {
        $index->delete();

        return redirect()
                    ->route('index.index')
                    ->with('success','Deleted Successfully');
    }
}
