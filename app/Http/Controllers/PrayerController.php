<?php

namespace App\Http\Controllers;

use App\Prayer;
use Illuminate\Http\Request;

class PrayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.prayer.index');
    }

    //server side datatable super admin view
    public function allPrayers(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'prayer_name',
                            3 => 'status',
                            4 => 'actions',
                        );
        

        $totalData = Prayer::count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $prayers = Prayer::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $prayers =  Prayer::where('prayer_name','LIKE',"%{$search}%")
                        ->orWhere('date','LIKE',"%{$search}%")
                        ->orWhere('details','LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = Prayer::where('prayer_name','LIKE',"%{$search}%")
                        ->orWhere('date','LIKE',"%{$search}%")
                        ->orWhere('details','LIKE',"%{$search}%")
                        ->count();
        }

        $data = array();
        if(!empty($prayers))
        {
            foreach ($prayers as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['prayer_name'] = $value->prayer_name;
                if ($value->status == 1) {
                    $nestedData['status'] = '<div class="btn-group">
                                    <button class="btn btn-sm btn-success btn-update"  title="Completed">Completed</button>
                                </div>';
                }
                else{
                    $nestedData['status'] = '<div class="btn-group">
                                    <button class="btn btn-sm btn-danger btn-delete" title="Pending">Pending</button>
                                </div>';
                }
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('prayer.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('prayer.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prayer = null;
        return view('pages.prayer.create', compact('prayer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'      => 'required',
            'prayer_name'    => 'required',
            'status'    => 'required',
        ]);

        Prayer::create($request->all());

        return redirect()
                    ->route('prayer.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prayer  $prayer
     * @return \Illuminate\Http\Response
     */
    public function show(Prayer $prayer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prayer  $prayer
     * @return \Illuminate\Http\Response
     */
    public function edit(Prayer $prayer)
    {
        return view('pages.prayer.edit', compact('prayer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prayer  $prayer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prayer $prayer)
    {
        $request->validate([
            'date'      => 'required',
            'prayer_name'    => 'required',
            'status'    => 'required',
        ]);

        $prayer->update($request->all());

        return redirect()
                    ->route('prayer.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prayer  $prayer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prayer $prayer)
    {
        $prayer->delete();

        return redirect()
                    ->route('prayer.index')
                    ->with('success','Deleted Successfully');
    }
}
