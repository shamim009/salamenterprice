<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductionModels\ProductionProduct;
use App\Models\ProductionModels\ProductionProducedProduct;
use App\Company;
use Illuminate\Validation\Rule;
use DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class ProducedProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.produced-product.index');
    }

    //server side datatable
    public function allProductions(Request $request)
    {      
        $columns = array( 
            0 =>'id', 
            1 =>'date',
            2 => 'company_id',
            3 => 'product_id',
            4 => 'quantity',
            5 => 'actions',
        );

        $totalData = ProductionProducedProduct::count();

        $totalQuantity = ProductionProducedProduct::sum('quantity');

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $productions = ProductionProducedProduct::with('product', 'company')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $productions = ProductionProducedProduct::with('product','company')
            ->where('date','LIKE',"%{$search}%")
            ->orWhere('rate', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhere('quantity', 'LIKE',"%{$search}%")
            ->orWhereHas('product', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = ProductionProducedProduct::with('product','company')
            ->where('date','LIKE',"%{$search}%")
            ->orWhere('rate', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhere('quantity', 'LIKE',"%{$search}%")
            ->orWhereHas('product', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->count();
        }

        $data = array();
        if(!empty($productions))
        {
            foreach ($productions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['actions'] = '<div class="btn-group">
                <a href="'.route('production.produced-product.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                Update
                </a>
                <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.produced-product.destroy',$value->id) .'" title="Delete">Delete
                </button>
                </div>';
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data,
            "totalQuantity"            => $totalQuantity   
        );

        echo json_encode($json_data);        
    }

    //server side datatable Production admin view
    public function allProductionsAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'product_id',
                            3 => 'quantity',
                            4 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionProducedProduct::where('company_id',$user->company_id)->count();
        $totalQuantity = ProductionProducedProduct::where('company_id',$user->company_id)->sum('quantity');

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $productions = ProductionProducedProduct::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $productions =  ProductionProducedProduct::with('product')
                            ->where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('rate', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionProducedProduct::with('product')             
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('rate', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($productions))
        {
            foreach ($productions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.produced-product.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.produced-product.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalQuantity"            => $totalQuantity   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = ProductionProduct::all('id','name');
        return view('pages.production.produced-product.create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'date'              => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            $totalAmount = [];

            for ($i=0; $i < count($request->product_id) ; $i++) { 
                $token = time().str_random(10);
                $production =  new ProductionProducedProduct;

                $production->date = $request->date;
                $production->company_id = $user->company_id;
                $production->details = $request->details;
                $production->invoice = $request->invoice;
                $production->product_id = $request->product_id[$i];
                $production->quantity = $request->quantity[$i];

                $production->save();
                // array_push($totalAmount, $sale->amount);

                $product = ProductionProduct::find($request->product_id[$i]);
                $product->stock = $product->stock + $request->quantity[$i];
                $product->produced = $product->produced + $request->quantity[$i];
                $product->save();
            }
        });
        return redirect()
                    ->route('production.produced-product.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = ProductionProduct::all('id','name');
        $production = ProductionProducedProduct::find($id);
        return view('pages.production.produced-product.edit', compact('products','production'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'date'              => 'required',
            'quantity'              => 'required|numeric',
        ]);

        
        $production =  ProductionProducedProduct::find($id);

        DB::transaction(function () use ($request, $production) {
            $user = Auth::user();

            $product = ProductionProduct::find($production->product_id);
            $product->stock = ($product->stock - $production->quantity) + $request->quantity;
            $product->produced = ($product->produced - $production->quantity) + $request->quantity;
            $product->save();

            $production->date = $request->date;
            $production->details = $request->details;
            $production->invoice = $request->invoice;
            $production->quantity = $request->quantity;

            $production->save();
        });
        return redirect()
                ->route('production.produced-product.index')
                ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $production =  ProductionProducedProduct::find($id);

            $product = ProductionProduct::find($production->product_id);
            $product->stock = $product->stock - $production->quantity;
            $product->production = $product->production - $production->quantity;
            $product->save();

            $production->delete();
        });
        return redirect()
                    ->route('production.produced-product.index')
                    ->with('success', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $productions = ProductionProducedProduct::whereBetween('date', [$start_date, $end_date])->get();

            $quantity = ProductionProducedProduct::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $productions = ProductionProducedProduct::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $quantity = ProductionProducedProduct::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        return view('pages.production.produced-product.report', compact('productions','start_date','end_date','quantity'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $productions = ProductionProducedProduct::whereBetween('date', [$start_date, $end_date])->get();

            $quantity = ProductionProducedProduct::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $productions = ProductionProducedProduct::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $quantity = ProductionProducedProduct::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.production.produced-product.print', compact('productions','start_date','end_date','quantity','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $productions = ProductionProducedProduct::whereBetween('date', [$start_date, $end_date])->get();

            $quantity = ProductionProducedProduct::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $productions = ProductionProducedProduct::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $quantity = ProductionProducedProduct::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.production.produced-product.pdf', compact('productions','start_date','end_date','quantity','date'));
        return $pdf->download('ProductionReport.pdf');
    }
}
