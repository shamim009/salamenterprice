<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\ProductionModels\ProductionInvestment;
use App\Models\ProductionModels\ProductionInvestmentTransaction;
use App\Models\ProductionModels\ProductionCashBook;
use App\Models\ProductionModels\ProductionBankAccount;
use App\Models\ProductionModels\ProductionBankAccountTransaction;
use App\Bank;
use App\MobileBanking;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;
use PDF;

class InvestmentTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.investment-transaction.index');
    }

    //server side datatable super admin view
    public function allTransactions(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'investment_id',
                            4 => 'amount',
                            5 => 'purpose',
                            6 => 'payment_mode',
                            7 => 'actions',
                        );
        

        $totalData = ProductionInvestmentTransaction::count();
        $totalFiltered = $totalData;
        $totalInvest = ProductionInvestmentTransaction::where('purpose','=','1')->sum('amount');
        $totalReturn = ProductionInvestmentTransaction::where('purpose','=','2')->orWhere('purpose','=','3')->sum('amount'); 
        $totalSum = $totalInvest - $totalReturn;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $transactions = ProductionInvestmentTransaction::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  ProductioninvestmentTransaction::with('investment','bank','company','mobileBanking')
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('investment', function($query) use($search) {
                                    $query->where('investor_name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('bank', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductioninvestmentTransaction::with('investment','bank','company','mobileBanking')
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('investment', function($query) use($search) {
                                    $query->where('investor_name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('bank', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['investment_id'] = $value->investment->investor_name;
                $nestedData['amount'] = $value->amount;
                if ($value->purpose == 1) {
                    $nestedData['purpose'] = 'Investment';
                }
                else if ($value->purpose == 2) {
                    $nestedData['purpose'] = 'Profit Return';
                }
                else {
                    $nestedData['purpose'] = 'Main Amount Return';
                }

                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.investment-transaction.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.investment-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.investment-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }

    //server side datatable Production admin view
    public function allTransactionsAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'investment_id',
                            3 => 'amount',
                            4 => 'purpose',
                            5 => 'payment_mode',
                            6 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionInvestmentTransaction::where('company_id',$user->company_id)->count();
        $totalInvest = ProductionInvestmentTransaction::where('company_id',$user->company_id)->
        where('purpose','=','1')->sum('amount');
        $totalReturn = ProductionInvestmentTransaction::where('company_id',$user->company_id)->where('purpose','=','2')->orWhere('purpose','=','3')->sum('amount');

        $totalSum = $totalInvest - $totalReturn;
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $transactions = ProductionInvestmentTransaction::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $transactions =  ProductionInvestmentTransaction::with('investment','bank','mobileBanking')
                            ->where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('investment', function($query) use($search) {
                                        $query->where('investor_name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('bank', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionInvestmentTransaction::with('investment','bank','mobileBanking')
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('investment', function($query) use($search) {
                                        $query->where('investor_name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('bank', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });;
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['investment_id'] = $value->investment->investor_name;
                $nestedData['amount'] = $value->amount;
                if ($value->purpose == 1) {
                    $nestedData['purpose'] = 'Investment';
                }
                else if ($value->purpose == 2) {
                    $nestedData['purpose'] = 'Profit Return';
                }
                else {
                    $nestedData['purpose'] = 'Main Amount Return';
                }

                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }            
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.investment-transaction.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.investment-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.investment-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $investmentTransaction = null;
        $investments = ProductionInvestment::all('id', 'investor_name');
        $banks = Bank::all('id', 'name');
        $mobileBankings = MobileBanking::all('id', 'name');
        $user = Auth::user();
        $accounts = ProductionBankAccount::where('company_id',$user->company_id)->get();
        return view('pages.production.investment-transaction.create', compact('investmentTransaction','investments','banks','mobileBankings','accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'      => 'required',
            'investment_id'  => ['required', Rule::notIn(['','0'])],
            'purpose'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);

        $user = Auth::user();
        DB::transaction(function () use ($request, $user) {
            $transaction = new ProductionInvestmentTransaction;
            $token = time().str_random(10);

            $transaction->date = $request->date;
            $transaction->investment_id = $request->investment_id;
            $transaction->company_id = $user->company_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->token = $token;
            $transaction->purpose = $request->purpose;
            $transaction->payment_mode = $request->payment_mode;
            //$transaction->bank_id = $request->bank_id;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->receiver = $request->receiver;

            $transaction->save();

            // $cashBook = new ProductionCashBook;
            // $cashBook->date = $request->date;
            // $cashBook->purpose = 2;
            // $cashBook->investment_transaction_id = $transaction->id;
            // $cashBook->company_id = $user->company_id;

            // $cashBook->save();

            if ($request->purpose == 1) {
                $investment = ProductionInvestment::find($request->investment_id);
                $investment->invested_amount = $investment->invested_amount + $request->amount;
                $investment->save();
            }
            if ($request->purpose == 3) {
                $investment = ProductionInvestment::find($request->investment_id);
                $investment->invested_amount = $investment->invested_amount - $request->amount;
                $investment->save();
            }

            if ($request->purpose == 1 && $request->payment_mode == 2) {
                $bankTransaction = new ProductionBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $user->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 9;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;
                $bankTransaction->token = $token;

                $bankTransaction->save();

                $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                $bankAccount->save();
            }

            if (($request->purpose == 2 || $request->purpose == 3) && $request->payment_mode == 2) {
                $bankTransaction = new ProductionBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $user->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 10;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;
                $bankTransaction->token = $token;

                $bankTransaction->save();

                $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                $bankAccount->save();
            }
        });

        return redirect()
                    ->route('production.investment-transaction.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = ProductionInvestmentTransaction::find($id);
        //dd($transaction);
        return view('pages.production.investment-transaction.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $investmentTransaction = ProductionInvestmentTransaction::find($id);
        $investments = ProductionInvestment::all('id', 'investor_name');
        $banks = Bank::all('id', 'name');
        $mobileBankings = MobileBanking::all('id', 'name');
        $user = Auth::user();
        $accounts = ProductionBankAccount::where('company_id',$user->company_id)->get();
        return view('pages.production.investment-transaction.edit', compact('investmentTransaction','investments','banks','mobileBankings','accounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date'      => 'required',
            'investment_id'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);
        DB::transaction(function () use ($request, $id) {
            $transaction = ProductionInvestmentTransaction::find($id);

            if ($transaction->payment_mode != 2 && $request->payment_mode == 2) {
                $bankTransaction = new ProductionBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $transaction->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                if ($transaction->purpose == 1) {
                    $bankTransaction->purpose = 9;
                }
                else {
                    $bankTransaction->purpose = 10;
                }
                
                $bankTransaction->token = $transaction->token;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;

                $bankTransaction->save();

                $bankAccount = ProductionBankAccount::find($request->bank_account_id);
                if ($transaction->purpose == 1) {
                    $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                }
                else {
                    $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                }
                $bankAccount->save();
            }

            if ($transaction->purpose == 1 && $transaction->payment_mode == 2 && $request->payment_mode == 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','9'],
                    ['token',$transaction->token]
                ])->first();
                if ($bankTransaction->bank_account_id == $request->bank_account_id) {
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = ($bankAccount->current_amount - $bankTransaction->amount) + $request->amount;
                    $bankAccount->save();
                }

                else{
                    //remove balance from the old account
                    $oldBankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                    $oldBankAccount->current_amount = $oldBankAccount->current_amount - $bankTransaction->amount;
                    $oldBankAccount->save();

                    //add balance to the new account
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                    $bankAccount->save();
                }

                $bankTransaction->date = $request->date;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;

                $bankTransaction->save();
            }

            if ($transaction->purpose != 1 && $transaction->payment_mode == 2 && $request->payment_mode == 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','10'],
                    ['token',$transaction->token]
                ])->first();
                if ($bankTransaction->bank_account_id == $request->bank_account_id) {
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = ($bankAccount->current_amount + $bankTransaction->amount) - $request->amount;
                    $bankAccount->save();
                }

                else{
                    //remove balance from the old account
                    $oldBankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                    $oldBankAccount->current_amount = $oldBankAccount->current_amount + $bankTransaction->amount;
                    $oldBankAccount->save();

                    //add balance to the new account
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                    $bankAccount->save();
                }

                $bankTransaction->date = $request->date;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;

                $bankTransaction->save();
            }

            if ($transaction->purpose == 1 && $transaction->payment_mode == 2 && $request->payment_mode != 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','9'],
                    ['token',$transaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            if ($transaction->purpose != 1 && $transaction->payment_mode == 2 && $request->payment_mode != 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','10'],
                    ['token',$transaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            $transaction->date = $request->date;
            $transaction->investment_id = $request->investment_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->payment_mode = $request->payment_mode;
            //$transaction->bank_id = $request->bank_id;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->details = $request->details;
            $transaction->receiver = $request->receiver;

            $transaction->save();

            if ($transaction->purpose == 1) {
                $investment = ProductionInvestment::find($transaction->investment_id);
                $investment->invested_amount = $investment->invested_amount - $transaction->amount;
                $investment->save();

                $newInvestment = ProductionInvestment::find($request->investment_id);
                $newInvestment->invested_amount = $newInvestment->invested_amount + $request->amount;
                $newInvestment->save();
            }
            if ($transaction->purpose == 3) {
                $investment = ProductionInvestment::find($transaction->investment_id);
                $investment->invested_amount = $investment->invested_amount + $transaction->amount;
                $investment->save();

                $newInvestment = ProductionInvestment::find($request->investment_id);
                $newInvestment->invested_amount = $newInvestment->invested_amount - $request->amount;
                $newInvestment->save();
            }

            // $cashBook = ProductionCashBook::where('purchase_transaction_id',$transaction->id)->first();
            // $cashBook->date = $request->date;
            // $cashBook->save();
        });
        return redirect()
                    ->route('production.investment-transaction.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            // $cashBook = ProductionCashBook::where('purchase_transaction_id',$id)->first();
            // $cashBook->delete();

            $investmentTransaction = ProductionInvestmentTransaction::find($id);

            if ($investmentTransaction->payment_mode == 2 && $investmentTransaction->purpose == 1) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','9'],
                    ['token',$investmentTransaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            if ($investmentTransaction->payment_mode == 2 && $investmentTransaction->purpose != 1) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','10'],
                    ['token',$investmentTransaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            if ($investmentTransaction->purpose == 1) {
                $investment = ProductionInvestment::find($investmentTransaction->investment_id);
                $investment->invested_amount = $investment->invested_amount - $investmentTransaction->amount;
                $investment->save();
            }
            if ($investmentTransaction->purpose == 3) {
                $investment = ProductionInvestment::find($investmentTransaction->investment_id);
                $investment->invested_amount = $investment->invested_amount + $investmentTransaction->amount;
                $investment->save();
            }
            
            $investmentTransaction->delete();
        });

        return redirect()
                    //->back()
                    ->route('production.investment-transaction.index')
                    ->with('warning', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $transactions = ProductionInvestmentTransaction::whereBetween('date', [$start_date, $end_date])->get();

            $amount = 0;
            foreach ($transactions as $key => $value) {
                if ($value->purpose == 1) {
                    $amount =+ $value->amount;
                }
                else {
                    $amount =- $value->amount;
                }
            }           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionInvestmentTransaction::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = 0;
            foreach ($transactions as $key => $value) {
                if ($value->purpose == 1) {
                    $amount =+ $value->amount;
                }
                else {
                    $amount =- $value->amount;
                }
            }  
        }
        return view('pages.production.investment-transaction.report', compact('transactions','start_date','end_date','amount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $transactions = ProductionInvestmentTransaction::whereBetween('date', [$start_date, $end_date])->get();

            $amount = 0;
            foreach ($transactions as $key => $value) {
                if ($value->purpose == 1) {
                    $amount =+ $value->amount;
                }
                else {
                    $amount =- $value->amount;
                }
            }             
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionInvestmentTransaction::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = 0;
            foreach ($transactions as $key => $value) {
                if ($value->purpose == 1) {
                    $amount =+ $value->amount;
                }
                else {
                    $amount =- $value->amount;
                }
            }  
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.production.investment-transaction.print', compact('transactions','start_date','end_date','amount','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $transactions = ProductionInvestmentTransaction::whereBetween('date', [$start_date, $end_date])->get();

            $amount = 0;
            foreach ($transactions as $key => $value) {
                if ($value->purpose == 1) {
                    $amount =+ $value->amount;
                }
                else {
                    $amount =- $value->amount;
                }
            }             
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionInvestmentTransaction::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = 0;
            foreach ($transactions as $key => $value) {
                if ($value->purpose == 1) {
                    $amount =+ $value->amount;
                }
                else {
                    $amount =- $value->amount;
                }
            }  
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.production.investment-transaction.pdf', compact('transactions','start_date','end_date','amount','date'));
        return $pdf->download('InvestmentTransactionReport.pdf');
    }
}
