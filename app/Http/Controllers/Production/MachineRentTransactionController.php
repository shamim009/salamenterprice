<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\ProductionModels\ProductionCustomer;
use App\Models\ProductionModels\ProductionMachineRentTransaction;
use App\Models\ProductionModels\ProductionBankAccount;
use App\Models\ProductionModels\ProductionBankAccountTransaction;
use App\Models\ProductionModels\ProductionCashBook;
use App\MobileBanking;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;
use PDF;

class MachineRentTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.rent-transaction.index');
    }

    //server side datatable super admin view
    public function allTransactions(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'rented_to',
                            4 => 'amount',
                            5 => 'payment_type',
                            6 => 'payment_mode',
                            7 => 'actions',
                        );
        

        $totalData = ProductionMachineRentTransaction::where('purpose','=','2')->count();
        $totalFiltered = $totalData;
        $totalSum = ProductionMachineRentTransaction::where('purpose','=','2')->sum('amount'); 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $transactions = ProductionMachineRentTransaction::where('purpose','=','2')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  ProductionMachineRentTransaction::with('account','company','mobileBanking')
                        ->where('purpose','=','2')
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('rented_to', 'LIKE',"%{$search}%")
                                ->orWhereHas('account', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductionMachineRentTransaction::with('account','company','mobileBanking')
                        ->where('purpose','=','2')
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhere('rented_to', 'LIKE',"%{$search}%")
                                ->orWhereHas('account', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['rented_to'] = $value->rented_to;
                $nestedData['amount'] = $value->amount;
                if ($value->payment_type == 1) {
                    $nestedData['payment_type'] = 'Cash';
                }
                else if ($value->payment_type == 2) {
                    $nestedData['payment_type'] = 'Due';
                }
                else {
                    $nestedData['payment_type'] = 'Advance';
                }

                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.machine-rent-transaction.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.machine-rent-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.machine-rent-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable Production admin view
    public function allTransactionsAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'rented_to',
                            3 => 'amount',
                            4 => 'payment_type',
                            5 => 'payment_mode',
                            6 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionMachineRentTransaction::where('company_id',$user->company_id)->
        where('purpose','=','2')->count();
        $totalSum = ProductionMachineRentTransaction::where('company_id',$user->company_id)->
        where('purpose','=','2')->sum('amount');
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $transactions = ProductionMachineRentTransaction::where('company_id',$user->company_id)
                        ->where('purpose','=','2')
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $transactions =  ProductionMachineRentTransaction::with('account','mobileBanking')
                            ->where('company_id','=',$user->company_id)
                            ->where('purpose','=','2')
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('rented_to', 'LIKE',"%{$search}%")
                                    ->orWhereHas('account', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionMachineRentTransaction::with('account','mobileBanking')            
                            ->where('purpose','=','2')
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('rented_to', 'LIKE',"%{$search}%")
                                    ->orWhereHas('account', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });;
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['rented_to'] = $value->rented_to;
                $nestedData['amount'] = $value->amount;
                if ($value->payment_type == 1) {
                    $nestedData['payment_type'] = 'Cash';
                }
                else if ($value->payment_type == 2) {
                    $nestedData['payment_type'] = 'Due';
                }
                else {
                    $nestedData['payment_type'] = 'Advance';
                }

                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }            
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.machine-rent-transaction.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.machine-rent-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.machine-rent-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $rentTransaction = null;
        $accounts = ProductionBankAccount::where('company_id',$user->company_id)->get();
        $mobileBankings = MobileBanking::all('id', 'name');
        return view('pages.production.rent-transaction.create', compact('rentTransaction','accounts','mobileBankings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'      => 'required',
            'rented_to'      => 'required',
            'payment_type'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);

        $user = Auth::user();
        DB::transaction(function () use ($request, $user) {

            $token = time().str_random(10);

            $transaction = new ProductionMachineRentTransaction;

            $transaction->date = $request->date;
            $transaction->rented_to = $request->rented_to;
            $transaction->company_id = $user->company_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->payment_type = $request->payment_type;
            $transaction->payment_mode = $request->payment_mode;
            $transaction->token = $token;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->receiver = $request->receiver;
            $transaction->purpose = 2;

            $transaction->save();

            $cashBook = new ProductionCashBook;
            $cashBook->date = $request->date;
            $cashBook->purpose = 11;
            $cashBook->rent_transaction_id = $transaction->id;
            $cashBook->company_id = $user->company_id;

            $cashBook->save();

            if ($request->payment_mode == 2) {
                $bankTransaction = new ProductionBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $user->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 11;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;
                $bankTransaction->token = $token;

                $bankTransaction->save();

                $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                $bankAccount->save();

            }
        });

        return redirect()
                    ->route('production.machine-rent-transaction.index')
                    ->with('success', 'Payment Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = ProductionMachineRentTransaction::find($id);
        //dd($transaction);
        return view('pages.production.rent-transaction.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $rentTransaction = ProductionMachineRentTransaction::find($id);
        $accounts = ProductionBankAccount::where('company_id',$user->company_id)->get();
        $mobileBankings = MobileBanking::all('id', 'name');
        return view('pages.production.rent-transaction.edit', compact('rentTransaction','accounts','mobileBankings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date'      => 'required',
            'payment_type'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);
        DB::transaction(function () use ($request, $id) {
            $transaction = ProductionMachineRentTransaction::find($id);
            //$token = time().str_random(10);

            if ($transaction->payment_mode != 2 && $request->payment_mode == 2) {
                $bankTransaction = new ProductionBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $transaction->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 11;
                $bankTransaction->token = $transaction->token;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;

                $bankTransaction->save();

                $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                $bankAccount->save();
            }

            if ($transaction->payment_mode == 2 && $request->payment_mode == 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','11'],
                    ['token',$transaction->token]
                ])->first();
                if ($bankTransaction->bank_account_id == $request->bank_account_id) {
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = ($bankAccount->current_amount - $bankTransaction->amount) + $request->amount;
                    $bankAccount->save();
                }

                else{
                    //remove balance from the old account
                    $oldBankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                    $oldBankAccount->current_amount = $oldBankAccount->current_amount - $bankTransaction->amount;
                    $oldBankAccount->save();

                    //add balance to the new account
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                    $bankAccount->save();
                }

                $bankTransaction->date = $request->date;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;

                $bankTransaction->save();
            }

            if ($transaction->payment_mode == 2 && $request->payment_mode != 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','11'],
                    ['token',$transaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            $transaction->date = $request->date;
            $transaction->rented_to = $request->rented_to;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->payment_type = $request->payment_type;
            $transaction->payment_mode = $request->payment_mode;
            //$transaction->bank_id = $request->bank_id;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->receiver = $request->receiver;
            $transaction->purpose = 2;

            $transaction->save();

            $cashBook = ProductionCashBook::where('rent_transaction_id',$transaction->id)->first();
            $cashBook->date = $request->date;
            $cashBook->save();
        });

        return redirect()
                    ->route('production.machine-rent-transaction.index')
                    ->with('success', 'Payment Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $rentTransaction = ProductionMachineRentTransaction::find($id);
            if ($rentTransaction->payment_mode == 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','11'],
                    ['token',$rentTransaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            $cashBook = ProductionCashBook::where('rent_transaction_id',$id)->first();
            $cashBook->delete();
            $rentTransaction->delete();
        });
        return redirect()
                    //->back()
                    ->route('production.machine-rent-transaction.index')
                    ->with('warning', 'Payment Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $transactions = ProductionMachineRentTransaction::where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRentTransaction::where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionMachineRentTransaction::where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRentTransaction::where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        return view('pages.production.rent-transaction.report', compact('transactions','start_date','end_date','amount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $transactions = ProductionMachineRentTransaction::where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRentTransaction::where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionMachineRentTransaction::where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRentTransaction::where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.production.rent-transaction.print', compact('transactions','start_date','end_date','amount','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $transactions = ProductionMachineRentTransaction::where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRentTransaction::where('purpose','=','2')->whereBetween('date', [$start_date, $end_date])->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionMachineRentTransaction::where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRentTransaction::where('purpose','=','2')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.production.rent-transaction.pdf', compact('transactions','start_date','end_date','amount','date'));
        return $pdf->download('Machine Rent Payment Report.pdf');
    }
}
