<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductionModels\ProductionSale;
use App\Models\ProductionModels\ProductionSaleTransaction;
use App\Models\ProductionModels\ProductionSaleOrder;
use App\Models\ProductionModels\ProductionProduct;
use App\Models\ProductionModels\ProductionCustomer;
use App\Company;
use Illuminate\Validation\Rule;
use DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.sale.index');
    }

    //server side datatable
    public function allSales(Request $request)
    {      
        $columns = array( 
            0 =>'id', 
            1 =>'date',
            2=> 'company_id',
            3=> 'customer_id',
            4=> 'customer',
            5=> 'product_id',
            6=> 'quantity',
            7=> 'rate',
            8=> 'vat',
            9=> 'amount',
            10=> 'actions',
        );

        $totalData = ProductionSale::count();

        $totalAmount = ProductionSale::sum('amount');
        $totalVat = ProductionSale::sum('vat_amount');
        $totalSum = $totalAmount - $totalVat;
        $totalQuantity = ProductionSale::sum('quantity');

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $sales = ProductionSale::with('product', 'customer', 'company')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $sales = ProductionSale::with('product', 'customer', 'company')
            ->where('date','LIKE',"%{$search}%")
            ->orWhere('rate', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhere('quantity', 'LIKE',"%{$search}%")
            ->orWhereHas('product', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('customer', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = ProductionSale::with('product', 'customer', 'company')
            ->where('date','LIKE',"%{$search}%")
            ->orWhere('rate', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhere('quantity', 'LIKE',"%{$search}%")
            ->orWhereHas('product', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('customer', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->count();
        }

        $data = array();
        if(!empty($sales))
        {
            foreach ($sales as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['customer'] = $value->customer_id;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['rate'] = $value->rate;
                $nestedData['vat'] = $value->vat_amount;
                $nestedData['amount'] = $value->amount - $value->vat_amount;
                $nestedData['actions'] = '<div class="btn-group">
                <a href="'.route('production.sale.show',$value->id) .'" class="btn btn-primary btn-sm" title="View">
                View
                </a>
                <a href="'.route('production.sale.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                Update
                </a>
                <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.sale.destroy',$value->id) .'" title="Delete">Delete
                </button>
                </div>';
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data,
            "totalSum"            => $totalSum,
            "totalVat"            => $totalVat,
            "totalQuantity"            => $totalQuantity   
        );

        echo json_encode($json_data);        
    }

    //server side datatable Production admin view
    public function allSalesAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'customer_id',
                            3 => 'customer',
                            4 => 'product_id',
                            5 => 'quantity',
                            6 => 'rate',
                            7 => 'vat',
                            8 => 'amount',
                            9 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionSale::where('company_id',$user->company_id)->count();
        $totalAmount = ProductionSale::where('company_id',$user->company_id)->sum('amount');
        $totalVat = ProductionSale::where('company_id',$user->company_id)->sum('vat_amount');
        $totalQuantity = ProductionSale::where('company_id',$user->company_id)->sum('quantity');
        $totalSum = $totalAmount - $totalVat;
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $sales = ProductionSale::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $sales =  ProductionSale::with('customer','product')
                            ->where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('rate', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionSale::with('customer','product')             
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('rate', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($sales))
        {
            foreach ($sales as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['customer'] = $value->customer_id;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['rate'] = $value->rate;                    
                $nestedData['vat'] = $value->vat_amount;
                $nestedData['amount'] = $value->amount - $value->vat_amount;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.sale.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.sale.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.sale.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,
                    "totalVat"            => $totalVat,
                    "totalQuantity"            => $totalQuantity   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all('id','name');
        $customers = ProductionCustomer::all('id','name');
        $products = ProductionProduct::all('id','name');
        return view('pages.production.sale.create', compact('companies','customers','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'customer_selection'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'unload_place'              => 'required',
        ]);

        // for ($i=0; $i < count($request->product_id) ; $i++) { 
        //     $product = ProductionProduct::find($request->product_id[$i]);
        //     if ($product->stock < $request->quantity[$i]) {
        //         return redirect()
        //                 ->back()
        //                 ->with('warning', "Sorry You Don't Have Enough Product in Store to Sell");
        //     }
        // }

        DB::transaction(function () use ($request) {
            if ($request->customer_selection == 2) {
                $customer = DB::table('production_customers')->insertGetId(
                    [
                        'name' => $request->name,
                        'mobile' => $request->mobile,
                        'address' => $request->address
                    ]
                );
            }
            else {
                $customer = $request->customer_id;
            }

            $user = Auth::user();
            $totalAmount = [];

            for ($i=0; $i < count($request->product_id) ; $i++) { 
                $token = time().str_random(10);
                $sale =  new ProductionSale;

                $sale->date = $request->date;
                $sale->company_id = $user->company_id;
                $sale->customer_id = $customer;
                $sale->unload_place = $request->unload_place;
                $sale->factory_name = $request->factory_name;
                $sale->details = $request->details;
                $sale->invoice = $request->invoice;
                $sale->product_id = $request->product_id[$i];
                $sale->rate = $request->rate[$i];
                $sale->quantity = $request->quantity[$i];
                $sale->vat = $request->vat[$i];
                $sale->token = $token;
                if ($request->rate[$i]) {
                    $sale->amount = $request->rate[$i] * $request->quantity[$i];
                }
                if ($request->vat[$i]) {
                    $sale->vat_amount = ((($request->rate[$i] * $request->quantity[$i]) * $request->vat[$i]) / 100);
                }

                $sale->save();
                // array_push($totalAmount, $sale->amount);

                $product = ProductionProduct::find($request->product_id[$i]);
                $product->stock = $product->stock - $request->quantity[$i];
                $product->sale = $product->sale - $request->quantity[$i];
                $product->save();
            }
        });
        return redirect()
                    ->route('production.sale.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = ProductionSale::find($id);
        return view('pages.production.sale.show',compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = ProductionSale::find($id);
        //$companies = ProductionCompany::all('id','name');
        $products = ProductionProduct::all('id','name');
        $customers = ProductionCustomer::all('id','name');
        return view('pages.production.sale.edit', compact('sale','products','customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'customer_id'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'quantity'              => 'required|numeric',
            'rate'              => 'required|numeric',
        ]);

        
        $sale =  ProductionSale::find($id);

        // if ((($product->stock + $sale->quantity) - $request->quantity) < 0) {
        //     return redirect()
        //                 ->back()
        //                 ->with('warning', "Sorry You Don't Have Enough Product in Store to Sell");
        // }

        DB::transaction(function () use ($request, $sale) {
            $user = Auth::user();

            $product = ProductionProduct::find($sale->product_id);
            $product->stock = ($product->stock + $sale->quantity) - $request->quantity;
            $product->sale = ($product->sale + $sale->quantity) - $request->quantity;
            $product->save();

            $sale->date = $request->date;
            $sale->unload_place = $request->unload_place;
            $sale->factory_name = $request->factory_name;
            $sale->details = $request->details;
            $sale->customer_id = $request->customer_id;
            $sale->rate = $request->rate;
            $sale->invoice = $request->invoice;
            $sale->quantity = $request->quantity;
            $sale->vat = $request->vat;
            $sale->amount = $request->amount;
            $sale->vat_amount = (($request->amount * $request->vat) / 100);

            $sale->save();
        });
        return redirect()
                ->route('production.sale.index')
                ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $sale =  ProductionSale::find($id);

            $product = ProductionProduct::find($sale->product_id);
            $product->stock = $product->stock + $sale->quantity;
            $product->sale = $product->sale + $sale->quantity;
            $product->save();

            $sale->delete();
        });
        return redirect()
                    ->route('production.sale.index')
                    ->with('success', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $sales = ProductionSale::whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = ProductionSale::whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = ProductionSale::whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        return view('pages.production.sale.report', compact('sales','start_date','end_date','amount','quantity','vat_amount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $sales = ProductionSale::whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = ProductionSale::whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = ProductionSale::whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.production.sale.print', compact('sales','start_date','end_date','amount','quantity','vat_amount','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $sales = ProductionSale::whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = ProductionSale::whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = ProductionSale::whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.production.sale.pdf', compact('sales','start_date','end_date','amount','quantity','vat_amount','date'));
        return $pdf->download('SaleReport.pdf');
    }

    //sale invoice
    public function returnViewPage()
    {
        $customers = ProductionCustomer::all('id','name');
        return view('pages.production.sale-invoice.index', compact('customers'));
    }

    public function invoice(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $customer = ProductionCustomer::find($request->customer_id);

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $sales = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        return view('pages.production.sale-invoice.invoice', compact('sales','start_date','end_date','amount','quantity','vat_amount','customer'));
    }

    public function invoicePrint(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $customer = ProductionCustomer::find($request->customer_id);

        if ($user->hasRole('super_admin')) {
            $sales = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.production.sale-invoice.print', compact('sales','start_date','end_date','amount','quantity','vat_amount','date','customer'));
    }

    public function invoiceExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $customer = ProductionCustomer::find($request->customer_id);

        if ($user->hasRole('super_admin')) {
            $sales = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->get();

            $vat_amount = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');
            $total_amount = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::where('customer_id',$customer->id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $sales = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $total_amount = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $vat_amount = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('vat_amount');

            $amount = $total_amount - $vat_amount;

            $quantity = ProductionSale::where('customer_id',$customer->id)->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.production.sale-invoice.pdf', compact('sales','start_date','end_date','amount','quantity','vat_amount','date','customer'));
        return $pdf->download('SaleInvoice.pdf');
    }
}
