<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductionModels\ProductionInvestment;
use App\Models\ProductionModels\ProductionInvestmentTransaction;
use App\Models\ProductionModels\ProductionBankAccount;
use App\Models\ProductionModels\ProductionBankAccountTransaction;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use DB;

class InvestmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $investments = ProductionInvestment::latest()->get();
        }
        else {
            $investments = ProductionInvestment::where('company_id',$user->company_id)->latest()->get();
        }
        return view('pages.production.investment.index',compact('investments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $investment = null;
        return view('pages.production.investment.create', compact('investment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'investor_name'  =>'required',
            'interest_rate'      => 'required|numeric',
        ]);

        $user = Auth::user();

        $investment = new ProductionInvestment;

        $investment->investor_name = $request->investor_name;
        $investment->investor_mobile = $request->investor_mobile;
        $investment->investor_address = $request->investor_address;
        $investment->company_id = $user->company_id;
        $investment->details = $request->details;
        $investment->interest_rate = $request->interest_rate;
        $investment->invested_amount = 0;

        $investment->save();

        return redirect()
                    ->route('production.investment.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $investment = ProductionInvestment::find($id);
        return view('pages.production.investment.edit', compact('investment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'investor_name'  =>'required',
            'interest_rate'      => 'required|numeric',
        ]);

        $user = Auth::user();

        $investment = ProductionInvestment::find($id);

        $investment->investor_name = $request->investor_name;
        $investment->investor_mobile = $request->investor_mobile;
        $investment->investor_address = $request->investor_address;
        $investment->details = $request->details;
        $investment->interest_rate = $request->interest_rate;

        $investment->save();

        return redirect()
                    ->route('production.investment.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $investment = ProductionInvestment::find($id);
        $transactions = ProductionInvestmentTransaction::where('investment_id',$id)->get();
        foreach ($transactions as $investmentTransaction) {
            if ($investmentTransaction->payment_mode == 2 && $investmentTransaction->purpose == 1) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','9'],
                    ['token',$investmentTransaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            if ($investmentTransaction->payment_mode == 2 && $investmentTransaction->purpose != 1) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','10'],
                    ['token',$investmentTransaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }
            $investmentTransaction->delete();
        }
        $investment->delete();

        return redirect()
                    ->route('production.investment.index')
                    ->with('success', 'Deleted Successfully');
    }

    public function report($id)
    {
        $investment = ProductionInvestment::find($id);
        $invests = ProductionInvestmentTransaction::where('investment_id', $id)->where('purpose','=','1')->get();
        $totalInvest = ProductionInvestmentTransaction::where('investment_id', $id)->where('purpose','=','1')->sum('amount');

        $profitreturns = ProductionInvestmentTransaction::where('investment_id', $id)->where('purpose','=','2')->get();
        $totalProfitReturn = ProductionInvestmentTransaction::where('investment_id', $id)->where('purpose','=','2')->sum('amount');

        $mainreturns = ProductionInvestmentTransaction::where('investment_id', $id)->where('purpose','=','3')->get();
        $totalAmountReturn = ProductionInvestmentTransaction::where('investment_id', $id)->where('purpose','=','3')->sum('amount');

        return view('pages.production.investment.report',compact('investment','invests','id','profitreturns','mainreturns','totalInvest','totalProfitReturn','totalAmountReturn'));
    }
}
