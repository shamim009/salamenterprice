<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionExpenseItem;
use App\Models\ProductionModels\ProductionExpense;
use App\Models\ProductionModels\ProductionExpenseTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class ExpenseItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenseItems = ProductionExpenseItem::all();
        return view('pages.production.expense-item.index',compact('expenseItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $expenseItem = null;
        return view('pages.production.expense-item.create', compact('expenseItem'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>'required',
        ]);

        ProductionExpenseItem::create($request->all());
        return redirect()
                    ->route('production.expense-item.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expenseItem = ProductionExpenseItem::find($id);
        return view('pages.production.expense-item.edit', compact('expenseItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  =>'required',
        ]);

        $expenseItem = ProductionExpenseItem::find($id);
        $expenseItem->update($request->all());
        return redirect()
                    ->route('production.expense-item.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expenseItem = ProductionExpenseItem::find($id);
        $expenseItem->delete();
        return redirect()
                    ->route('production.expense-item.index')
                    ->with('warning', 'Deleted Successfully');
    }

    //individual expense item report
    public function report($id)
    {
        $expenseItem = ProductionExpenseItem::find($id);
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $totalExpense = ProductionExpense::where('expenseItem_id', $id)->sum('amount');
            $totalPaid = ProductionExpenseTransaction::where('expenseItem_id', $id)->sum('amount');
        }
        else {
            $totalExpense = ProductionExpense::where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
            $totalPaid = ProductionExpenseTransaction::where('company_id',$user->company_id)->where('expenseItem_id', $id)->sum('amount');
        }
        
        return view('pages.production.expense-item.report',compact('expenseItem','id','totalExpense','totalPaid'));
    }

    //server side datatable
    public function particularExpense(Request $request, $id)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2=> 'company_id',
                            3=> 'amount',
                            4=> 'actions',
                        );
  
        $totalData = ProductionExpense::where('expenseItem_id', $id)->count();
        $totalSum = ProductionExpense::where('expenseItem_id', $id)->sum('amount');
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $expenses = ProductionExpense::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $expenses = ProductionExpense::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('date', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhereHas('expenseitem', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = ProductionExpense::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('date', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhereHas('expenseitem', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['amount'] = $value->amount;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.expense.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.expense.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum   
                    );
            
        echo json_encode($json_data);        
    }

    //server side datatable production admin view
    public function particularExpenseAdminView(Request $request, $id)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'amount',
                            3 => 'details',
                            4 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionExpense::where('company_id',$user->company_id)->where('expenseItem_id', $id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $expenses = ProductionExpense::where('company_id',$user->company_id)
                        ->where('expenseItem_id', $id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $expenses =  ProductionExpense::with('expenseitem')
                            ->where('company_id','=',$user->company_id)
                            ->where('expenseItem_id', $id)
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionExpense::with('expenseitem')             
                            ->where('company_id','=',$user->company_id)
                            ->where('expenseItem_id', $id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['amount'] = $value->amount;
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.expense.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.expense.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    //server side datatable
    public function particularPayment(Request $request, $id)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2=> 'company_id',
                            3=> 'amount',
                            4=> 'actions',
                        );
  
        $totalData = ProductionExpenseTransaction::where('expenseItem_id', $id)->count();
        $totalSum = ProductionExpenseTransaction::where('expenseItem_id', $id)->sum('amount');
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $expenses = ProductionExpenseTransaction::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $expenses = ProductionExpenseTransaction::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('date', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhereHas('expenseitem', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = ProductionExpenseTransaction::with('expenseitem','company')
            ->where('expenseItem_id', $id)
            ->where('date', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhereHas('expenseitem', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['amount'] = $value->amount;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.expense-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.expense-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum   
                    );
            
        echo json_encode($json_data);        
    }

    //server side datatable production admin view
    public function particularPaymentAdminView(Request $request, $id)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'amount',
                            3 => 'details',
                            4 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionExpenseTransaction::where('company_id',$user->company_id)->where('expenseItem_id', $id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $expenses = ProductionExpenseTransaction::where('company_id',$user->company_id)
                        ->where('expenseItem_id', $id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $expenses =  ProductionExpenseTransaction::with('expenseitem')
                            ->where('company_id','=',$user->company_id)
                            ->where('expenseItem_id', $id)
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionExpenseTransaction::with('expenseitem')             
                            ->where('company_id','=',$user->company_id)
                            ->where('expenseItem_id', $id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['amount'] = $value->amount;
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.expense-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.expense-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }
}
