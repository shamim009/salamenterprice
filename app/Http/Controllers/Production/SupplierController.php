<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductionModels\ProductionSupplier;
use App\Models\ProductionModels\ProductionPurchase;
use App\Models\ProductionModels\ProductionPurchaseOrder;
use App\Models\ProductionModels\ProductionPurchaseTransaction;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.supplier.index');
    }

    //server side datatable
    public function allSuppliers(Request $request)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2=> 'mobile',
                            3=> 'address',
                            4=> 'actions',
                        );
  
        $totalData = ProductionSupplier::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $suppliers = ProductionSupplier::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $suppliers =  ProductionSupplier::where('name','LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionSupplier::where('name','LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->count();
        }

        $data = array();
        if(!empty($suppliers))
        {
            foreach ($suppliers as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['name'] = $value->name;
                $nestedData['mobile'] = $value->mobile;
                $nestedData['address'] = $value->address;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('production/supplier/report',$value->id) .'" class="btn btn-primary btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('production.supplier.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.supplier.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supplier = null;
        return view('pages.production.supplier.create', compact('supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        ProductionSupplier::create($request->all());

        return redirect()
                    ->route('production.supplier.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = ProductionSupplier::find($id);
        return view('pages.production.supplier.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        $supplier = ProductionSupplier::find($id);
        $supplier->update($request->all());

        return redirect()
                    ->route('production.supplier.index')
                    ->with('success','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = ProductionSupplier::find($id);
        $supplier->delete();

        return redirect()
                    ->route('production.supplier.index')
                    ->with('warning','Deleted Successfully');
    }

    //autocomplete search of supplier
    public function autoCompleteSearch(Request $request)
    {
        $query = $request->get('term','');
                
        $suppliers = productionSupplier::where('name','LIKE','%'.$query.'%')
                            ->get();

        $results=array();                    
        
        if(count($suppliers ) > 0){
            foreach ($suppliers  as $supplier) {
                $results[] = [ 'id' => $supplier['id'], 'text' => $supplier['name']];                  
            }
            return response()->json($results);
        }
        else{
            $data[] = 'Nothing Found';
            return $data;
        }
    }

    //individual supplier transaction report
    public function report($id)
    {
        $supplier = ProductionSupplier::find($id);
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $purchases = ProductionPurchase::where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $purchase_amount = ProductionPurchase::where('supplier_id','=', $id)->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $transactions = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionPurchaseOrder::where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = ProductionPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $purchases = ProductionPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $purchase_amount = ProductionPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $transactions = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionPurchaseOrder::where('supplier_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = ProductionPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        
        $balance = $purchase_amount - $paid_amount - $returned_amount;

        return view('pages.production.supplier.report',compact('supplier','purchases','transactions','id','balance','purchase_amount','paid_amount','orders','remainingQuantity','purchaseQuantity','returned_amount'));
    }

    public function printReport($id)
    {
        $supplier = ProductionSupplier::find($id);
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $purchases = ProductionPurchase::where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $purchase_amount = ProductionPurchase::where('supplier_id','=', $id)->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $transactions = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionPurchaseOrder::where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = ProductionPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $purchases = ProductionPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $purchase_amount = ProductionPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $transactions = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionPurchaseOrder::where('supplier_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = ProductionPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
             
        $balance = $purchase_amount - $paid_amount - $returned_amount;

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.production.supplier.print',compact('supplier','purchases','transactions','id','balance','purchase_amount','paid_amount','orders','remainingQuantity','purchaseQuantity','date','returned_amount'));
    }

    public function reportExportPDF($id)
    {
        $supplier = ProductionSupplier::find($id);
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $purchases = ProductionPurchase::where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $purchase_amount = ProductionPurchase::where('supplier_id','=', $id)->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $transactions = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionPurchaseOrder::where('supplier_id','=', $id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = ProductionPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $purchases = ProductionPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $purchase_amount = ProductionPurchase::where('supplier_id','=', $id)->where('company_id',$user->company_id)->sum('amount');
            $purchaseQuantity = 0;
            foreach ($purchases as $purchase) {
                $purchaseQuantity += $purchase->quantity;
            }
            $transactions = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionPurchaseTransaction::where('supplier_id', $supplier->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionPurchaseOrder::where('supplier_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderPurchases = ProductionPurchase::where('supplier_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderPurchases as $orderPurchase) {
                    $quantity += $orderPurchase->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
            
        $balance = $purchase_amount - $paid_amount - $returned_amount;

        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.production.supplier.pdf',compact('supplier','purchases','transactions','id','balance','purchase_amount','paid_amount','orders','remainingQuantity','purchaseQuantity','date','returned_amount'));
        return $pdf->download('SupplierReport.pdf');
    }
}
