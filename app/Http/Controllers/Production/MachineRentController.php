<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionMachineRent;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class MachineRentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.machine-rent.index');
    }

    //server side datatable super admin view
    public function allRents(Request $request)
    {     
        $columns = array( 
                            0 => 'id', 
                            1 => 'date',
                            2 => 'company_id',
                            3 => 'rented_to',
                            4 => 'produced',
                            5 => 'rate',
                            6 => 'amount',
                            7 => 'actions',
                        );
        

        $totalData = ProductionMachineRent::count();
        $totalSum = ProductionMachineRent::sum('amount'); 
        $totalQuantity = ProductionMachineRent::sum('produced'); 
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $rents = ProductionMachineRent::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $rents =  ProductionMachineRent::with('company')
                        ->where('produced','LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhere('rented_to', 'LIKE',"%{$search}%")
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductionMachineRent::with('company')
                        ->where('produced','LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhere('rented_to', 'LIKE',"%{$search}%")
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->count();
        }

        $data = array();
        if(!empty($rents))
        {
            foreach ($rents as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['rented_to'] = $value->rented_to;
                $nestedData['produced'] = $value->produced;                    
                $nestedData['rate'] = $value->rate;
                $nestedData['amount'] = $value->amount;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.machine-rent.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.machine-rent.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable Production admin view
    public function allRentsAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'rented_to',
                            3 => 'produced',
                            4 => 'rate',
                            5 => 'amount',
                            6 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionMachineRent::where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData;
        $totalSum = ProductionMachineRent::where('company_id',$user->company_id)->sum('amount'); 
        $totalQuantity = ProductionMachineRent::where('company_id',$user->company_id)->sum('produced');

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $rents = ProductionMachineRent::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $rents =  ProductionMachineRent::where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('produced','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhere('rented_to', 'LIKE',"%{$search}%");
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionMachineRent::where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('produced', 'LIKE',"%{$search}%")
                                    ->orWhere('rented_to', 'LIKE',"%{$search}%");
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($rents))
        {
            foreach ($rents as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['rented_to'] = $value->rented_to;
                $nestedData['produced'] = $value->produced;
                $nestedData['rate'] = $value->rate;                     
                $nestedData['amount'] = $value->amount; 
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.machine-rent.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.machine-rent.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $machineRent = null;
        return view('pages.production.machine-rent.create', compact('machineRent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'date'              => 'required',
            'rented_to'            => 'required',
            'produced'            => 'required',
            'rate'            => 'required',
        ]);
        
        DB::transaction(function () use ($request) {
            $user = Auth::user();

            $rent =  new ProductionMachineRent;

            $rent->date = $request->date;
            $rent->company_id = $user->company_id;
            $rent->rented_to = $request->rented_to;
            $rent->produced = $request->produced; 
            $rent->rate = $request->rate; 
            $rent->amount = $request->amount;          
            $rent->details = $request->details;        

            $rent->save();
        });
        return redirect()
                    ->route('production.machine-rent.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $machineRent = ProductionMachineRent::find($id);
        return view('pages.production.machine-rent.edit', compact('machineRent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'date'              => 'required',
            'rented_to'            => 'required',
            'produced'            => 'required',
            'rate'            => 'required',
        ]);
        
        DB::transaction(function () use ($request, $id) {

            $rent = ProductionMachineRent::find($id);

            $rent->date = $request->date;
            $rent->rented_to = $request->rented_to;
            $rent->produced = $request->produced; 
            $rent->rate = $request->rate; 
            $rent->amount = $request->amount;          
            $rent->details = $request->details;        

            $rent->save();
        });
        return redirect()
                    ->route('production.machine-rent.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rent = ProductionMachineRent::find($id);     
        $rent->delete();
        return redirect()
                    ->route('production.machine-rent.index')
                    ->with('success', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $rents = ProductionMachineRent::whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRent::whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionMachineRent::whereBetween('date', [$start_date, $end_date])->sum('produced');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $rents = ProductionMachineRent::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRent::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionMachineRent::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('produced');
        }
        return view('pages.production.machine-rent.report', compact('rents','start_date','end_date','amount','quantity'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $rents = ProductionMachineRent::whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRent::whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionMachineRent::whereBetween('date', [$start_date, $end_date])->sum('produced');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $rents = ProductionMachineRent::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRent::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionMachineRent::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('produced');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.production.machine-rent.print', compact('rents','start_date','end_date','amount','quantity','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $rents = ProductionMachineRent::whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRent::whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionMachineRent::whereBetween('date', [$start_date, $end_date])->sum('produced');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $rents = ProductionMachineRent::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionMachineRent::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionMachineRent::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('produced');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.production.machine-rent.pdf', compact('rents','start_date','end_date','amount','quantity','date'));
        return $pdf->download('MachineRentReport.pdf');
    }
}
