<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionSale;
use App\Models\ProductionModels\ProductionSaleOrder;
use App\Models\ProductionModels\ProductionCustomer;
use App\Models\ProductionModels\ProductionSaleTransaction;
use App\Models\ProductionModels\ProductionProduct;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Redirect;
use Illuminate\Support\Facades\Auth;

class SaleOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.sale-order.index');
    }

    //server side datatable super admin view
    public function allOrders(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'customer_id',
                            4 => 'product_id',
                            5 => 'quantity',
                            6 => 'invoice',
                            7 => 'actions',
                        );
        

        $totalData = ProductionSaleOrder::count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $orders = ProductionSaleOrder::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $orders =  ProductionSaleOrder::with('customer','product','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhere('invoice', 'LIKE',"%{$search}%")
                        ->orWhereHas('customer', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('product', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductionSaleOrder::with('customer','product','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('invoice', 'LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhereHas('customer', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('product', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->count();
        }

        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;                    
                $nestedData['invoice'] = $value->invoice;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('production/sale/order/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Delivery">
                                        Delivery
                                    </a>
                                    <a href="'.url('production/sale/order/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('production.sale-order.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.sale-order.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.sale-order.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable Production admin view
    public function allOrdersAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'customer_id',
                            3 => 'product_id',
                            4 => 'quantity',
                            5 => 'invoice',
                            6 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionSaleOrder::where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $orders = ProductionSaleOrder::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $orders =  ProductionSaleOrder::with('customer','product')
                            ->where('company_id',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('invoice', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionSaleOrder::with('customer','product')
                            ->where('company_id',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('invoice', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('product', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            ->count();
        }

        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['product_id'] = $value->product->name;
                $nestedData['quantity'] = $value->quantity;     
                $nestedData['invoice'] = $value->invoice;                    
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('production/sale/order/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Delivery">
                                        Delivery
                                    </a>
                                    <a href="'.url('production/sale/order/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('production.sale-order.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.sale-order.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.sale-order.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = ProductionCustomer::all('id','name');
        $products = ProductionProduct::all('id','name');
        return view('pages.production.sale-order.create',compact('customers','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'customer_selection'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'invoice'              => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            if ($request->customer_selection == 2) {
                $customer = DB::table('production_customers')->insertGetId(
                    [
                        'name' => $request->name,
                        'mobile' => $request->mobile,
                        'address' => $request->address
                    ]
                );
            }
            else {
                $customer = $request->customer_id;
            }
            //$totalAmount = [];

            for ($i=0; $i < count($request->product_id) ; $i++) {
                //$token = time().str_random(10); 
                $sale =  new ProductionSaleOrder;

                $sale->date = $request->date;
                $sale->company_id = $user->company_id;
                $sale->customer_id = $customer;
                $sale->invoice = $request->invoice;
                $sale->details = $request->details;
                $sale->product_id = $request->product_id[$i];
                //$sale->rate = $request->rate[$i];
                $sale->quantity = $request->quantity[$i];
                //$sale->token = $token;           

                $sale->save();

                // $product = ProductionProduct::find($request->product_id[$i]);
                // $product->stock = $product->stock + $request->quantity[$i];
                // $product->save();
            }
        });

        return redirect()
                    ->route('production.sale-order.index')
                    ->with('success', 'Sale Order Has Been Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = ProductionSaleOrder::find($id);
        return view('pages.production.sale-order.show',compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = ProductionSaleOrder::find($id);
        return view('pages.production.sale-order.edit',compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'date'              => 'required',
            'quantity'              => 'required',
            'invoice'              => 'required',
        ]);
        $order =  ProductionSaleOrder::find($id);
        DB::transaction(function () use ($request, $order) {

            $order->date = $request->date;
            $order->invoice = $request->invoice;
            $order->details = $request->details;
            $order->quantity = $request->quantity;

            $order->save();
        });
        return redirect()
                    ->route('production.sale-order.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order =  ProductionSaleOrder::find($id);
        DB::transaction(function () use ($order) {
            
            $sales = ProductionSale::where('order_id',$order->id)->get();
            foreach ($sales as $sale) {
                $oldProduct = ProductionProduct::find($sale->product_id);
                $oldProduct->stock = $oldProduct->stock + $sale->quantity;
                $oldProduct->sale = $oldProduct->sale + $sale->quantity;
                $oldProduct->save();

                $oldTransaction = ProductionSaleTransaction::where([
                    ['token', '=', $sale->token],
                    ['purpose', '=', '1']
                ])->first();
                if ($oldTransaction) {
                    $oldTransaction->delete();
                }
                $sale->delete();
            }

            $order->delete();
        });
        return redirect()
                    ->route('production.sale-order.index')
                    ->with('success', 'Deleted Successfully');
    }

    //create sale from delivery
    public function createDelivery($id)
    {
        $order = ProductionSaleOrder::find($id);
        return view('pages.production.sale-order.createDelivery', compact('order'));
    }

    //store purchase from delivery
    public function storeDelivery(Request $request, $id)
    {
        $request->validate([ 
            'quantity'              => 'required|numeric',
            'rate'              => 'required|numeric',
            'date'              => 'required',
        ]);

        $order = ProductionSaleOrder::find($id);

        $product = ProductionProduct::find($order->product_id);

        // if ($product->stock < $request->quantity) {
        //     return redirect()
        //                 ->back()
        //                 ->with('warning', "Sorry You Don't Have Enough Product in Store to Sell");
        // }

        DB::transaction(function () use ($request, $order) {
            $user = Auth::user();

            $token = time().str_random(10); 
            $sale =  new ProductionSale;

            $sale->date = $request->date;
            $sale->company_id = $user->company_id;
            $sale->customer_id = $order->customer_id;
            $sale->order_id = $order->id;
            $sale->details = $request->details;
            $sale->product_id = $order->product_id;
            $sale->rate = $request->rate;
            $sale->quantity = $request->quantity;
            $sale->vat = $request->vat;
            $sale->amount = $request->quantity * $request->rate;
            $sale->vat_amount = ((($request->quantity * $request->rate) * $request->vat) / 100);
            $sale->token = $token;
            $sale->invoice = $request->invoice;
            $sale->unload_place = $request->unload_place;
            $sale->factory_name = $request->factory_name;           

            $sale->save();

            $product = ProductionProduct::find($order->product_id);
            $product->stock = $product->stock - $request->quantity;
            $product->sale = $product->sale - $request->quantity;
            $product->save();
        });

        return redirect()
                    ->route('production.sale-order.index')
                    ->with('success', 'Delivery Added Successfully');
    }

    //delivery report for a particular sale
    public function deliveryReport($id)
    {
        $sales = ProductionSale::where('order_id', $id)->get();
        $order = ProductionSaleOrder::find($id);
        return view('pages.production.sale-order.report',compact('sales','order'));
    }
}
