<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionProduct;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = ProductionProduct::latest()->get();
        return view('pages.production.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = null;
        return view('pages.production.product.create', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
        ]);

        ProductionProduct::create([
            'name'   => $request->name,
            'details'   => $request->details,
        ]);

        return redirect()
                    ->route('production.product.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = ProductionProduct::find($id);
        return view('pages.production.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required|string',
        ]);

        $product = ProductionProduct::find($id);

        $product->update([
            'name'      => $request->name,
            'details'   => $request->details,
        ]);

        return redirect()
                    ->route('production.product.index')
                    ->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = ProductionProduct::find($id);
        $product->delete();

        return redirect()
                    ->route('production.product.index')
                    ->with('warning', 'Deleted Successfully');
    }

    //autocomplete search of product
    public function autoCompleteSearch(Request $request)
    {
        $query = $request->get('term','');
                
        $products = ProductionProduct::where('name','LIKE','%'.$query.'%')
                            ->get();

        $results=array();                    
        
        if(count($products ) > 0){
            foreach ($products  as $product) {
                $results[] = [ 'id' => $product['id'], 'text' => $product['name']];                  
            }
            return response()->json($results);
        }
        else{
            $data[] = 'Nothing Found';
            return $data;
        }
    }
}
