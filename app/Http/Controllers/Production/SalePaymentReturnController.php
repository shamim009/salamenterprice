<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\ProductionModels\ProductionCustomer;
use App\Models\ProductionModels\ProductionSaleTransaction;
use App\Models\ProductionModels\ProductionBankAccount;
use App\Models\ProductionModels\ProductionBankAccountTransaction;
use App\Models\ProductionModels\ProductionCashBook;
use App\MobileBanking;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;
use PDF;

class SalePaymentReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.sale-payment-return.index');
    }

    //server side datatable super admin view
    public function allTransactions(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'customer_id',
                            4 => 'amount',
                            5 => 'payment_mode',
                            6 => 'actions',
                        );
        

        $totalData = ProductionSaleTransaction::where('purpose','=','1')->count();
        $totalSum = ProductionSaleTransaction::where('purpose','=','1')->sum('amount');
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $transactions = ProductionSaleTransaction::where('purpose','=','1')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  ProductionSaleTransaction::with('customer','account','company','mobileBanking')
                        ->where('purpose','=','1')
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('customer', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('account', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductionSaleTransaction::with('customer','account','company','mobileBanking')
                        ->where('purpose','=','1')
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('customer', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('account', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['amount'] = $value->amount;
                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.sale-payment-return.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.sale-payment-return.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('roduction.sale-payment-return.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable Production admin view
    public function allTransactionsAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'customer_id',
                            3 => 'amount',
                            4 => 'payment_mode',
                            5 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionSaleTransaction::where('company_id',$user->company_id)->
        where('purpose','=','1')->count();
        $totalFiltered = $totalData;
        $totalSum = ProductionSaleTransaction::where('company_id',$user->company_id)->
        where('purpose','=','1')->sum('amount'); 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $transactions = ProductionSaleTransaction::where('company_id',$user->company_id)
                        ->where('purpose','=','1')
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $transactions =  ProductionSaleTransaction::with('customer','account','mobileBanking')
                            ->where('company_id','=',$user->company_id)
                            ->where('purpose','=','1')
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('account', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionSaleTransaction::with('customer','account','mobileBanking')            
                            ->where('purpose','=','1')
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('customer', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('account', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });;
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['customer_id'] = $value->customer->name;
                $nestedData['amount'] = $value->amount;
                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }            
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.sale-payment-return.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.sale-payment-return.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.sale-payment-return.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $saleTransaction = null;
        $customers = ProductionCustomer::all('id', 'name');
        $accounts = ProductionBankAccount::where('company_id',$user->company_id)->get();
        $mobileBankings = MobileBanking::all('id', 'name');
        return view('pages.production.sale-payment-return.create', compact('saleTransaction','customers','accounts','mobileBankings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'      => 'required',
            'customer_id'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);

        $user = Auth::user();
        DB::transaction(function () use ($request, $user) {

            $token = time().str_random(10);

            $transaction = new ProductionSaleTransaction;

            $transaction->date = $request->date;
            $transaction->customer_id = $request->customer_id;
            $transaction->company_id = $user->company_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->payment_mode = $request->payment_mode;
            $transaction->token = $token;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->receiver = $request->receiver;
            $transaction->purpose = 1;

            $transaction->save();

            $cashBook = new ProductionCashBook;
            $cashBook->date = $request->date;
            $cashBook->purpose = 4;
            $cashBook->sale_transaction_id = $transaction->id;
            $cashBook->company_id = $user->company_id;

            $cashBook->save();

            if ($request->payment_mode == 2) {
                $bankTransaction = new ProductionBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $user->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 6;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;
                $bankTransaction->token = $token;

                $bankTransaction->save();

                $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                $bankAccount->save();

            }
        });

        return redirect()
                    ->route('production.sale-payment-return.index')
                    ->with('success', 'Payment Return Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = ProductionSaleTransaction::find($id);
        //dd($transaction);
        return view('pages.production.sale-payment-return.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $saleTransaction = ProductionSaleTransaction::find($id);
        $customers = ProductionCustomer::all('id', 'name');
        $accounts = ProductionBankAccount::where('company_id',$user->company_id)->get();
        $mobileBankings = MobileBanking::all('id', 'name');
        return view('pages.production.sale-payment-return.edit', compact('saleTransaction','customers','accounts','mobileBankings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date'      => 'required',
            'customer_id'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);
        DB::transaction(function () use ($request, $id) {
            $transaction = ProductionSaleTransaction::find($id);
            //$token = time().str_random(10);

            if ($transaction->payment_mode != 2 && $request->payment_mode == 2) {
                $bankTransaction = new ProductionBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $transaction->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 6;
                $bankTransaction->token = $transaction->token;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;

                $bankTransaction->save();

                $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                $bankAccount->save();
            }

            if ($transaction->payment_mode == 2 && $request->payment_mode == 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','6'],
                    ['token',$transaction->token]
                ])->first();
                if ($bankTransaction->bank_account_id == $request->bank_account_id) {
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = ($bankAccount->current_amount + $bankTransaction->amount) - $request->amount;
                    $bankAccount->save();
                }

                else{
                    //remove balance from the old account
                    $oldBankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                    $oldBankAccount->current_amount = $oldBankAccount->current_amount + $bankTransaction->amount;
                    $oldBankAccount->save();

                    //add balance to the new account
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = $bankAccount->current_amount - $request->amount;
                    $bankAccount->save();
                }

                $bankTransaction->date = $request->date;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;

                $bankTransaction->save();
            }

            if ($transaction->payment_mode == 2 && $request->payment_mode != 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','6'],
                    ['token',$transaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            $transaction->date = $request->date;
            $transaction->customer_id = $request->customer_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->payment_type = $request->payment_type;
            $transaction->payment_mode = $request->payment_mode;
            //$transaction->bank_id = $request->bank_id;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->receiver = $request->receiver;
            $transaction->purpose = 1;

            $transaction->save();

            $cashBook = ProductionCashBook::where('sale_transaction_id',$transaction->id)->first();
            $cashBook->date = $request->date;
            $cashBook->save();
        });

        return redirect()
                    ->route('production.sale-payment-return.index')
                    ->with('success', 'Payment Added Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $saleTransaction = ProductionSaleTransaction::find($id);
            if ($saleTransaction->payment_mode == 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','6'],
                    ['token',$saleTransaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            $cashBook = ProductionCashBook::where('sale_transaction_id',$id)->first();
            $cashBook->delete();
            $saleTransaction->delete();
        });
        return redirect()
                    //->back()
                    ->route('production.sale-payment-return.index')
                    ->with('warning', 'Payment Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $transactions = ProductionSaleTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionSaleTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionSaleTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionSaleTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        return view('pages.production.sale-payment-return.report', compact('transactions','start_date','end_date','amount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $transactions = ProductionSaleTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionSaleTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionSaleTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionSaleTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.production.sale-payment-return.print', compact('transactions','start_date','end_date','amount','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $transactions = ProductionSaleTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionSaleTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionSaleTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionSaleTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.production.sale-payment-return.pdf', compact('transactions','start_date','end_date','amount','date'));
        return $pdf->download('SalePaymentReturnReport.pdf');
    }
}
