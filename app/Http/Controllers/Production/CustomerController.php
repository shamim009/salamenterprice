<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionCustomer;
use App\Models\ProductionModels\ProductionSale;
use App\Models\ProductionModels\ProductionSaleOrder;
use App\Models\ProductionModels\ProductionSaleTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.customer.index');
    }

    //server side datatable
    public function allCustomers(Request $request)
    {      
        $columns = array( 
            0 =>'id', 
            1 =>'name',
            2=> 'mobile',
            3=> 'address',
            4=> 'actions',
        );

        $totalData = ProductionCustomer::count();

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $customers = ProductionCustomer::offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $customers =  ProductionCustomer::where('name','LIKE',"%{$search}%")
            ->orWhere('mobile', 'LIKE',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = ProductionCustomer::where('name','LIKE',"%{$search}%")
            ->orWhere('mobile', 'LIKE',"%{$search}%")
            ->count();
        }

        $data = array();
        if(!empty($customers))
        {
            foreach ($customers as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['name'] = $value->name;
                $nestedData['mobile'] = $value->mobile;
                $nestedData['address'] = $value->address;
                $nestedData['actions'] = '<div class="btn-group">
                <a href="'.url('production/customer/report',$value->id) .'" class="btn btn-primary btn-sm" title="Report">
                Report
                </a>
                <a href="'.route('production.customer.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                Update
                </a>
                <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.customer.destroy',$value->id) .'" title="Delete">Delete</button>
                </div>';
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer = null;
        return view('pages.production.customer.create', compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        ProductionCustomer::create($request->all());

        return redirect()
        ->route('production.customer.index')
        ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = ProductionCustomer::find($id);
        return view('pages.production.customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        $customer = ProductionCustomer::find($id);
        $customer->update($request->all());

        return redirect()
        ->route('production.customer.index')
        ->with('success','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = ProductionCustomer::find($id);
        $customer->delete();

        return redirect()
        ->route('production.customer.index')
        ->with('success','Deleted Successfully');
    }

    //autocomplete search of customer
    public function autoCompleteSearch(Request $request)
    {
        $query = $request->get('term','');

        $customers = ProductionCustomer::where('name','LIKE','%'.$query.'%')
        ->get();

        $results=array();                    
        
        if(count($customers ) > 0){
            foreach ($customers  as $customer) {
                $results[] = [ 'id' => $customer['id'], 'text' => $customer['name']];                  
            }
            return response()->json($results);
        }
        else{
            $data[] = 'Nothing Found';
            return $data;
        }
    }

    //individual customer transaction report
    public function report($id)
    {
        $customer = ProductionCustomer::find($id);
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $sales = ProductionSale::where('customer_id','=', $id)->orderBy('date', 'desc')->get();

            $sale_amount_wov = ProductionSale::where('customer_id','=', $id)->sum('amount');
            $vat_amount = ProductionSale::where('customer_id','=', $id)->sum('vat_amount');
            $sale_amount = $sale_amount_wov - $vat_amount;

            $saleQuantity = ProductionSale::where('customer_id','=', $id)->sum('quantity');

            $transactions = ProductionSaleTransaction::where('customer_id', $customer->id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionSaleOrder::where('customer_id','=', $id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = ProductionSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $sales = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $sale_amount_wov = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('amount');
            $vat_amount = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('vat_amount');
            $sale_amount = $sale_amount_wov - $vat_amount;

            $saleQuantity = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('quantity');

            $transactions = ProductionSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionSaleOrder::where('customer_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = ProductionSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        $balance = ( $sale_amount + $returned_amount ) - $paid_amount;

        return view('pages.production.customer.report',compact('customer','sales','balance','id','transactions','sale_amount','paid_amount','orders','remainingQuantity','saleQuantity','returned_amount'));
    }

    public function printReport($id)
    {
        $customer = ProductionCustomer::find($id);
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $sales = ProductionSale::where('customer_id','=', $id)->orderBy('date', 'desc')->get();

            $sale_amount_wov = ProductionSale::where('customer_id','=', $id)->sum('amount');
            $vat_amount = ProductionSale::where('customer_id','=', $id)->sum('vat_amount');
            $sale_amount = $sale_amount_wov - $vat_amount;

            $saleQuantity = ProductionSale::where('customer_id','=', $id)->sum('quantity');

            $transactions = ProductionSaleTransaction::where('customer_id', $customer->id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionSaleOrder::where('customer_id','=', $id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = ProductionSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $sales = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $sale_amount_wov = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('amount');
            $vat_amount = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('vat_amount');
            $sale_amount = $sale_amount_wov - $vat_amount;

            $saleQuantity = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('quantity');

            $transactions = ProductionSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionSaleOrder::where('customer_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = ProductionSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        $balance = ( $sale_amount + $returned_amount ) - $paid_amount;
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.production.customer.print',compact('customer','sales','balance','id','transactions','sale_amount','paid_amount','orders','remainingQuantity','saleQuantity','date','returned_amount'));
    }

    public function reportExportPDF($id)
    {
        $customer = ProductionCustomer::find($id);
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $sales = ProductionSale::where('customer_id','=', $id)->orderBy('date', 'desc')->get();

            $sale_amount_wov = ProductionSale::where('customer_id','=', $id)->sum('amount');
            $vat_amount = ProductionSale::where('customer_id','=', $id)->sum('vat_amount');
            $sale_amount = $sale_amount_wov - $vat_amount;

            $saleQuantity = ProductionSale::where('customer_id','=', $id)->sum('quantity');

            $transactions = ProductionSaleTransaction::where('customer_id', $customer->id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionSaleOrder::where('customer_id','=', $id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = ProductionSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        else {
            $sales = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $sale_amount_wov = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('amount');
            $vat_amount = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('vat_amount');
            $sale_amount = $sale_amount_wov - $vat_amount;

            $saleQuantity = ProductionSale::where('customer_id','=', $id)->where('company_id',$user->company_id)->sum('quantity');

            $transactions = ProductionSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();

            $paid_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','2')->sum('amount');
            $returned_amount = ProductionSaleTransaction::where('customer_id', $customer->id)->where('company_id',$user->company_id)->where('purpose','=','1')->sum('amount');

            $orders = ProductionSaleOrder::where('customer_id','=', $id)->where('company_id',$user->company_id)->orderBy('date', 'desc')->get();
            $remainingQuantity = [];
            foreach ($orders as $order) {
                $orderSales = ProductionSale::where('customer_id','=', $id)->where('order_id',$order->id)->get();
                $quantity = 0;
                foreach ($orderSales as $orderSale) {
                    $quantity += $orderSale->quantity;
                }
                $remaining = $order->quantity - $quantity;
                array_push($remainingQuantity, $remaining);
            }
        }
        
        $balance = ( $sale_amount + $returned_amount ) - $paid_amount;
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.production.customer.pdf',compact('customer','sales','balance','id','transactions','sale_amount','paid_amount','orders','remainingQuantity','saleQuantity','date','returned_amount'));
        return $pdf->download('CustomerReport.pdf');
    }
}
