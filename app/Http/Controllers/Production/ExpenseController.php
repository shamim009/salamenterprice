<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionExpense;
use App\Models\ProductionModels\ProductionExpenseItem;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use DateTime;
use Carbon\Carbon;
use PDF;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.expense.index');
    }

    //server side datatable
    public function allExpenses(Request $request)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2=> 'company_id',
                            3=> 'expenseItem_id',
                            4=> 'amount',
                            5=> 'details',
                            6=> 'actions',
                            7=> 'type',
                        );
  
        $totalData = ProductionExpense::count();
        $totalSum = ProductionExpense::sum('amount');
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $expenses = ProductionExpense::with('expenseitem','company')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $expenses = ProductionExpense::with('expenseitem','company')
            ->where('date', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhereHas('expenseitem', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = ProductionExpense::with('expenseitem','company')
            ->where('date', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhereHas('expenseitem', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->orWhereHas('company', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['expenseItem_id'] = $value->expenseitem->name;
                $nestedData['amount'] = $value->amount;
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.expense.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.expense.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $nestedData['type'] = $value->expenseItem_id;
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum   
                    );
            
        echo json_encode($json_data);        
    }

    //server side datatable production admin view
    public function allExpensesAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2=> 'expenseItem_id',
                            3=> 'amount',
                            4=> 'details',
                            5=> 'actions',
                            6=> 'type',
                        );
        
        $user = Auth::user();

        $totalData = ProductionExpense::where('company_id',$user->company_id)->count();
        $totalSum = ProductionExpense::where('company_id',$user->company_id)->sum('amount');
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $expenses = ProductionExpense::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $expenses =  ProductionExpense::with('expenseitem')
                            ->where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionExpense::with('expenseitem')             
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('expenseitem', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['expenseItem_id'] = $value->expenseitem->name;
                $nestedData['amount'] = $value->amount;
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.expense.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.expense.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $nestedData['type'] = $value->expenseItem_id;
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $expenseItems = ProductionExpenseItem::all('name','id');
        $expense = null;
        return view('pages.production.expense.create', compact('expenseItems','expense'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'expenseItem_id'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'amount'            => 'required',
        ]);
        
        DB::transaction(function () use ($request) {
            $user = Auth::user();

            $expense =  new ProductionExpense;

            $expense->date = $request->date;
            $expense->company_id = $user->company_id;
            $expense->expenseItem_id = $request->expenseItem_id;
            $expense->amount = $request->amount;          
            $expense->details = $request->details;        

            $expense->save();
        });
        return redirect()
                    ->route('production.expense.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = ProductionExpense::find($id);
        $expenseItems = ProductionExpenseItem::all('name','id');
        return view('pages.production.expense.edit', compact('expenseItems','expense'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'expenseItem_id'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'amount'            => 'required',
        ]);

        $expense = ProductionExpense::find($id);
        $expense->update([
            'date'      => $request->date,
            'details'   => $request->details,
            'amount'     => $request->amount,
            'expenseItem_id'    => $request->expenseItem_id,
        ]);

        return redirect()
                    ->route('production.expense.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = ProductionExpense::find($id);
        $expense->delete();

        return redirect()
                    ->route('production.expense.index')
                    ->with('warning', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $expenses = ProductionExpense::whereBetween('date', [$start_date, $end_date])->get();
            $amount = ProductionExpense::whereBetween('date', [$start_date, $end_date])->sum('amount');            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $expenses = ProductionExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        return view('pages.production.expense.report', compact('expenses','start_date','end_date','amount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $expenses = ProductionExpense::whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionExpense::whereBetween('date', [$start_date, $end_date])->sum('amount');         
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $expenses = ProductionExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        return view('pages.production.expense.print', compact('expenses','start_date','end_date','amount','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $expenses = ProductionExpense::whereBetween('date', [$start_date, $end_date])->get();
            $amount = ProductionExpense::whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $expenses = ProductionExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionExpense::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();

        $pdf = PDF::loadView('pages.production.expense.pdf', compact('expenses','start_date','end_date','amount','date'));
        return $pdf->download('ExpenseReport.pdf');
    }
}
