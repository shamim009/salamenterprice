<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionRawMaterial;
use App\Models\ProductionModels\ProductionRawMaterialUse;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class RawMaterialUseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.raw-material-use.index');
    }

    //server side datatable super admin view
    public function allUses(Request $request)
    {     
        $columns = array( 
                            0 => 'id', 
                            1 => 'date',
                            2 => 'company_id',
                            3 => 'raw_material_id',
                            4 => 'quantity',
                            5 => 'actions',
                        );
        

        $totalData = ProductionRawMaterialUse::count();
        $totalQuantity = ProductionRawMaterialUse::sum('quantity'); 
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $uses = ProductionRawMaterialUse::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $uses =  ProductionRawMaterialUse::with('rawmaterial','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhereHas('rawmaterial', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductionRawMaterialUse::with('rawmaterial','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhereHas('rawmaterial', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->count();
        }

        $data = array();
        if(!empty($uses))
        {
            foreach ($uses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['raw_material_id'] = $value->rawmaterial->name;
                $nestedData['quantity'] = $value->quantity;                    
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.raw-material-use.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.raw-material-use.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data, 
                    "totalQuantity"            => $totalQuantity
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable Production admin view
    public function allUsesAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'raw_material_id',
                            3 => 'quantity',
                            4 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionRawMaterialUse::where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData;
        $totalQuantity = ProductionRawMaterialUse::where('company_id',$user->company_id)->sum('quantity');

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $uses = ProductionRawMaterialUse::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $uses =  ProductionRawMaterialUse::with('rawmaterial')
                            ->where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('rawmaterial', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionRawMaterialUse::with('rawmaterial')             
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhereHas('rawmaterial', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($uses))
        {
            foreach ($uses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['raw_material_id'] = $value->rawmaterial->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.raw-material-use.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.raw-material-use.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,  
                    "totalQuantity"            => $totalQuantity   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rawmaterials = ProductionRawMaterial::all('id','name');
        return view('pages.production.raw-material-use.create', compact('rawmaterials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'date'              => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();

            for ($i=0; $i < count($request->raw_material_id) ; $i++) {
                $token = time().str_random(10); 
                $use =  new ProductionRawMaterialUse;

                $use->date = $request->date;
                $use->company_id = $user->company_id;
                $use->invoice = $request->invoice;
                $use->details = $request->details;
                $use->raw_material_id = $request->raw_material_id[$i];
                $use->quantity = $request->quantity[$i];

                $use->save();

                $rawmaterial = ProductionRawMaterial::find($request->raw_material_id[$i]);
                $rawmaterial->stock = $rawmaterial->stock - $request->quantity[$i];
                $rawmaterial->used = $rawmaterial->used + $request->quantity[$i];
                $rawmaterial->save();
            }
        });
        return redirect()
                    ->route('production.raw-material-use.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $use = ProductionRawMaterialUse::find($id);
        $rawmaterials = ProductionRawMaterial::all('id','name');
        return view('pages.production.raw-material-use.edit', compact('use','rawmaterials'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'date'              => 'required',
            'quantity'              => 'required',
        ]);

        DB::transaction(function () use ($request, $id) {
            $use =  ProductionRawMaterialUse::find($id);

            $oldRawMaterial = ProductionRawMaterial::find($use->raw_material_id);
            $oldRawMaterial->stock = ($oldRawMaterial->stock + $use->quantity) - $request->quantity;
            $oldRawMaterial->used = ($oldRawMaterial->used - $use->quantity) + $request->quantity;
            $oldRawMaterial->save();

            $use->date = $request->date;
            $use->details = $request->details;
            $use->quantity = $request->quantity;
            $use->invoice = $request->invoice;

            $use->save();
        });
        return redirect()
                    ->route('production.raw-material-use.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $use =  ProductionRawMaterialUse::find($id);

            $oldRawMaterial = ProductionRawMaterial::find($use->raw_material_id);
            $oldRawMaterial->stock = $oldRawMaterial->stock + $use->quantity;
            $oldRawMaterial->used = $oldRawMaterial->used - $use->quantity;
            $oldRawMaterial->save();

            $use->delete();
        });
        return redirect()
                    ->route('production.raw-material-use.index')
                    ->with('success', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $uses = ProductionRawMaterialUse::whereBetween('date', [$start_date, $end_date])->get();

            $quantity = ProductionRawMaterialUse::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $uses = ProductionRawMaterialUse::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $quantity = ProductionRawMaterialUse::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        return view('pages.production.raw-material-use.report', compact('uses','start_date','end_date','quantity'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $uses = ProductionRawMaterialUse::whereBetween('date', [$start_date, $end_date])->get();
            $quantity = ProductionRawMaterialUse::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $uses = ProductionRawMaterialUse::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();
            $quantity = ProductionRawMaterialUse::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.production.raw-material-use.print', compact('uses','start_date','end_date','quantity','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $uses = ProductionRawMaterialUse::whereBetween('date', [$start_date, $end_date])->get();
            $quantity = ProductionRawMaterialUse::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $uses = ProductionRawMaterialUse::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $quantity = ProductionRawMaterialUse::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.production.raw-material-use.pdf', compact('uses','start_date','end_date','quantity','date'));
        return $pdf->download('RawMaterialUseReport.pdf');
    }
}
