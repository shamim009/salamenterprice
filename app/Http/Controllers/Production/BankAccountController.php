<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductionModels\ProductionBankAccount;
use App\Models\ProductionModels\ProductionBankAccountTransaction;
use App\Bank;
use Illuminate\Support\Facades\Auth;

class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('super_admin')) {
            $bankAccounts = ProductionBankAccount::latest()->get();
        }
        else {
            $bankAccounts = ProductionBankAccount::where('company_id',$user->company_id)->latest()->get();
        }
        return view('pages.production.bank-account.index',compact('bankAccounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bankAccount = null;
        $banks = Bank::all('id', 'name');
        return view('pages.production.bank-account.create', compact('bankAccount','banks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'account_no'  =>'required',
            'bank_id'      => 'required',
            'current_amount'      => 'required|numeric',
        ]);

        $user = Auth::user();

        $bankAccount = new ProductionBankAccount;

        $bankAccount->account_no = $request->account_no;
        $bankAccount->bank_id = $request->bank_id;
        $bankAccount->company_id = $user->company_id;
        $bankAccount->details = $request->details;
        $bankAccount->current_amount = $request->current_amount;

        $bankAccount->save();

        return redirect()
                    ->route('production.bank-account.index')
                    ->with('success', 'Account Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bankAccount = ProductionBankAccount::find($id);
        $banks = Bank::all('id', 'name');
        return view('pages.production.bank-account.edit', compact('bankAccount','banks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'account_no'  =>'required',
            'bank_id'      => 'required',
            'current_amount'      => 'required|numeric',
        ]);

        $bankAccount = ProductionBankAccount::find($id);

        $bankAccount->account_no = $request->account_no;
        $bankAccount->bank_id = $request->bank_id;
        $bankAccount->details = $request->details;
        $bankAccount->current_amount = $request->current_amount;

        $bankAccount->save();

        return redirect()
                    ->route('production.bank-account.index')
                    ->with('success', 'Account Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bankAccount = ProductionBankAccount::find($id);
        $transactions = ProductionBankAccountTransaction::where('bank_account_id', $id)->get();
        foreach ($transactions as $transaction) {
            $transaction->delete();
        }
        $bankAccount->delete();
        return redirect()
                    ->route('production.bank-account.index')
                    ->with('warning', 'Account Deleted Successfully');
    }

    public function report($id)
    {
        $bankAccount = ProductionBankAccount::find($id);
        $transactions = ProductionBankAccountTransaction::where('bank_account_id', $id)->get();
        return view('pages.production.bank-account.report',compact('bankAccount','transactions','id'));
    }
}
