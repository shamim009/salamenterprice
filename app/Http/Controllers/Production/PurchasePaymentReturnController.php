<?php

namespace App\Http\Controllers\Production;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\ProductionModels\ProductionSupplier;
use App\Models\ProductionModels\ProductionPurchaseTransaction;
use App\Models\ProductionModels\ProductionCashBook;
use App\Models\ProductionModels\ProductionBankAccount;
use App\Models\ProductionModels\ProductionBankAccountTransaction;
use App\Bank;
use App\MobileBanking;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;
use PDF;

class PurchasePaymentReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.purchase-payment-return.index');
    }

    //server side datatable super admin view
    public function allTransactions(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'supplier_id',
                            4 => 'amount',
                            5 => 'payment_mode',
                            6 => 'actions',
                        );
        

        $totalData = ProductionPurchaseTransaction::where('purpose','=','1')->count();
        $totalFiltered = $totalData;
        $totalSum = ProductionPurchaseTransaction::where('purpose','=','1')->sum('amount'); 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $transactions = ProductionPurchaseTransaction::where('purpose','=','1')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  ProductionPurchaseTransaction::with('supplier','bank','company','mobileBanking')
                        ->where('purpose','=','1')
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('supplier', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('bank', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductionPurchaseTransaction::with('supplier','bank','company','mobileBanking')
                        ->where('purpose','=','1')
                        ->where(function ($query) use($search) {
                            $query->where('amount','LIKE',"%{$search}%")
                                ->orWhere('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('supplier', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('bank', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('mobileBanking', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['amount'] = $value->amount;
                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.purchase-payment-return.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.purchase-payment-return.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.purchase-payment-return.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable production admin view
    public function allTransactionsAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'supplier_id',
                            3 => 'amount',
                            4 => 'payment_mode',
                            5 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionPurchaseTransaction::where('company_id',$user->company_id)->
        where('purpose','=','1')->count();
        $totalSum = ProductionPurchaseTransaction::where('company_id',$user->company_id)->
        where('purpose','=','1')->sum('amount');
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $transactions = ProductionPurchaseTransaction::where('company_id',$user->company_id)
                        ->where('purpose','=','1')
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $transactions =  ProductionPurchaseTransaction::with('supplier','bank','mobileBanking')
                            ->where('company_id','=',$user->company_id)
                            ->where('purpose','=','1')
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('bank', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionPurchaseTransaction::with('supplier','bank','mobileBanking')            ->where('purpose','=','1')
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('bank', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });;
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['amount'] = $value->amount;
                if ($value->payment_mode == 1) {
                    $nestedData['payment_mode'] = 'Hand Cash';
                }
                else if ($value->payment_mode == 2) {
                    $nestedData['payment_mode'] = 'Regular Banking';
                }
                else {
                    $nestedData['payment_mode'] = 'Mobile Banking';
                }            
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.purchase-payment-return.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.purchase-payment-return.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.purchase-payment-return.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $purchaseTransaction = null;
        $suppliers = ProductionSupplier::all('id', 'name');
        $banks = Bank::all('id', 'name');
        $mobileBankings = MobileBanking::all('id', 'name');
        $user = Auth::user();
        $accounts = ProductionBankAccount::where('company_id',$user->company_id)->get();
        return view('pages.production.purchase-payment-return.create', compact('purchaseTransaction','suppliers','banks','mobileBankings','accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'      => 'required',
            'supplier_id'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);

        $user = Auth::user();
        DB::transaction(function () use ($request, $user) {
            $transaction = new ProductionPurchaseTransaction;
            $token = time().str_random(10);

            $transaction->date = $request->date;
            $transaction->supplier_id = $request->supplier_id;
            $transaction->company_id = $user->company_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->token = $token;
            $transaction->payment_mode = $request->payment_mode;
            //$transaction->bank_id = $request->bank_id;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->receiver = $request->receiver;
            $transaction->purpose = 1;

            $transaction->save();

            $cashBook = new ProductionCashBook;
            $cashBook->date = $request->date;
            $cashBook->purpose = 5;
            $cashBook->purchase_transaction_id = $transaction->id;
            $cashBook->company_id = $user->company_id;

            $cashBook->save();

            if ($request->payment_mode == 2) {
                $bankTransaction = new ProductionBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $user->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 7;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;
                $bankTransaction->token = $token;

                $bankTransaction->save();

                $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                $bankAccount->save();

            }
        });

        return redirect()
                    ->route('production.purchase-payment-return.index')
                    ->with('success', 'Payment Return Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = ProductionPurchaseTransaction::find($id);
        //dd($transaction);
        return view('pages.production.purchase-payment-return.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banks = Bank::all('id', 'name');
        $mobileBankings = MobileBanking::all('id', 'name');
        $purchaseTransaction = ProductionPurchaseTransaction::find($id);
        $suppliers = ProductionSupplier::all('id', 'name');
        $user = Auth::user();
        $accounts = ProductionBankAccount::where('company_id',$user->company_id)->get();
        return view('pages.production.purchase-payment-return.edit', compact('purchaseTransaction','suppliers','banks','mobileBankings','accounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date'      => 'required',
            'supplier_id'  => ['required', Rule::notIn(['','0'])],
            'payment_mode'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);
        DB::transaction(function () use ($request, $id) {
            $transaction = ProductionPurchaseTransaction::find($id);

            if ($transaction->payment_mode != 2 && $request->payment_mode == 2) {
                $bankTransaction = new ProductionBankAccountTransaction;

                $bankTransaction->date = $request->date;
                $bankTransaction->company_id = $transaction->company_id;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->purpose = 7;
                $bankTransaction->token = $transaction->token;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;

                $bankTransaction->save();

                $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                $bankAccount->save();
            }

            if ($transaction->payment_mode == 2 && $request->payment_mode == 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','7'],
                    ['token',$transaction->token]
                ])->first();
                if ($bankTransaction->bank_account_id == $request->bank_account_id) {
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = ($bankAccount->current_amount - $bankTransaction->amount) + $request->amount;
                    $bankAccount->save();
                }

                else{
                    //remove balance from the old account
                    $oldBankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                    $oldBankAccount->current_amount = $oldBankAccount->current_amount - $bankTransaction->amount;
                    $oldBankAccount->save();

                    //add balance to the new account
                    $bankAccount = ProductionBankAccount::find($request->bank_account_id);

                    $bankAccount->current_amount = $bankAccount->current_amount + $request->amount;
                    $bankAccount->save();
                }

                $bankTransaction->date = $request->date;
                $bankTransaction->details = $request->details;
                $bankTransaction->amount = $request->amount;
                $bankTransaction->bank_account_id = $request->bank_account_id;
                $bankTransaction->cheque_number = $request->cheque_number;

                $bankTransaction->save();
            }

            if ($transaction->payment_mode == 2 && $request->payment_mode != 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','7'],
                    ['token',$transaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }

            $transaction->date = $request->date;
            $transaction->supplier_id = $request->supplier_id;
            $transaction->details = $request->details;
            $transaction->amount = $request->amount;
            $transaction->payment_mode = $request->payment_mode;
            //$transaction->bank_id = $request->bank_id;
            $transaction->bank_account_id = $request->bank_account_id;
            $transaction->cheque_number = $request->cheque_number;
            $transaction->mobile_banking_id = $request->mobile_banking_id;
            $transaction->phone_number = $request->phone_number;
            $transaction->details = $request->details;
            $transaction->receiver = $request->receiver;

            $transaction->save();

            $cashBook = ProductionCashBook::where('purchase_transaction_id',$transaction->id)->first();
            $cashBook->date = $request->date;
            $cashBook->save();
        });
        return redirect()
                    //->back()
                    ->route('production.supplier.report', ['id' => $request->supplier_id])
                    ->with('success', 'Payment Return Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $cashBook = ProductionCashBook::where('purchase_transaction_id',$id)->first();
            $cashBook->delete();

            $purchaseTransaction = ProductionPurchaseTransaction::find($id);

            if ($purchaseTransaction->payment_mode == 2) {
                $bankTransaction = ProductionBankAccountTransaction::where([
                    ['purpose','=','7'],
                    ['token',$purchaseTransaction->token]
                ])->first();

                $bankAccount = ProductionBankAccount::find($bankTransaction->bank_account_id);

                $bankAccount->current_amount = $bankAccount->current_amount - $bankTransaction->amount;
                $bankAccount->save();

                $bankTransaction->delete();
            }
            
            $purchaseTransaction->delete();
        });

        return redirect()
                    //->back()
                    ->route('production.purchase-payment-return.index')
                    ->with('warning', 'Payment Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $transactions = ProductionPurchaseTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchaseTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionPurchaseTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchaseTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        return view('pages.production.purchase-payment-return.report', compact('transactions','start_date','end_date','amount'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $transactions = ProductionPurchaseTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchaseTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionPurchaseTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchaseTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.production.purchase-payment-return.print', compact('transactions','start_date','end_date','amount','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $transactions = ProductionPurchaseTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchaseTransaction::where('purpose','=','1')->whereBetween('date', [$start_date, $end_date])->sum('amount');           
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $transactions = ProductionPurchaseTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchaseTransaction::where('purpose','=','1')->where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.production.purchase-payment-return.pdf', compact('transactions','start_date','end_date','amount','date'));
        return $pdf->download('PurchasePaymentReturnReport.pdf');
    }
}
