<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionRawMaterial;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class RawMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rawmaterials = ProductionRawMaterial::latest()->get();
        return view('pages.production.raw-material.index', compact('rawmaterials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rawmaterial = null;
        return view('pages.production.raw-material.create', compact('rawmaterial'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
        ]);

        ProductionRawMaterial::create([
            'name'   => $request->name,
            'details'   => $request->details,
        ]);

        return redirect()
                    ->route('production.raw-material.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rawmaterial = ProductionRawMaterial::find($id);
        return view('pages.production.raw-material.edit', compact('rawmaterial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required|string',
        ]);

        $rawmaterial = ProductionRawMaterial::find($id);

        $rawmaterial->update([
            'name'      => $request->name,
            'details'   => $request->details,
        ]);

        return redirect()
                    ->route('production.raw-material.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rawmaterial = ProductionRawMaterial::find($id);
        $rawmaterial->delete();

        return redirect()
                    ->route('production.raw-material.index')
                    ->with('warning', 'Deleted Successfully');
    }

    //autocomplete search of product
    public function autoCompleteSearch(Request $request)
    {
        $query = $request->get('term','');
                
        $rawmaterials = ProductionRawMaterial::where('name','LIKE','%'.$query.'%')
                            ->get();

        $results=array();                    
        
        if(count($rawmaterials ) > 0){
            foreach ($rawmaterials  as $rawmaterial) {
                $results[] = [ 'id' => $rawmaterial['id'], 'text' => $rawmaterial['name']];                  
            }
            return response()->json($results);
        }
        else{
            $data[] = 'Nothing Found';
            return $data;
        }
    }
}
