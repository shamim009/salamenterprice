<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionPurchase;
use App\Models\ProductionModels\ProductionSupplier;
use App\Models\ProductionModels\ProductionPurchaseTransaction;
use App\Models\ProductionModels\ProductionPurchaseOrder;
use App\Models\ProductionModels\ProductionRawMaterial;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.purchase.index');
    }

    //server side datatable super admin view
    public function allPurchases(Request $request)
    {     
        $columns = array( 
                            0 => 'id', 
                            1 => 'date',
                            2 => 'company_id',
                            3 => 'supplier_id',
                            4 => 'supplier',
                            5 => 'raw_material_id',
                            6 => 'quantity',
                            7 => 'rate',
                            8 => 'amount',
                            9 => 'actions',
                        );
        

        $totalData = ProductionPurchase::count();
        $totalSum = ProductionPurchase::sum('amount'); 
        $totalQuantity = ProductionPurchase::sum('quantity'); 
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $purchases = ProductionPurchase::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $purchases =  ProductionPurchase::with('supplier','rawmaterial','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhereHas('supplier', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('rawmaterial', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductionPurchase::with('supplier','rawmaterial','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('rate', 'LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhereHas('supplier', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('rawmaterial', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->count();
        }

        $data = array();
        if(!empty($purchases))
        {
            foreach ($purchases as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['supplier'] = $value->supplier_id;
                $nestedData['raw_material_id'] = $value->rawmaterial->name;
                $nestedData['quantity'] = $value->quantity;                    
                $nestedData['rate'] = $value->rate;
                $nestedData['amount'] = $value->amount;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.purchase.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.purchase.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.purchase.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable Production admin view
    public function allPurchasesAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'supplier_id',
                            3 => 'supplier',
                            4 => 'raw_material_id',
                            5 => 'quantity',
                            6 => 'rate',
                            7 => 'amount',
                            8 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionPurchase::where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData;
        $totalSum = ProductionPurchase::where('company_id',$user->company_id)->sum('amount'); 
        $totalQuantity = ProductionPurchase::where('company_id',$user->company_id)->sum('quantity');

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $purchases = ProductionPurchase::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $purchases =  ProductionPurchase::with('supplier','rawmaterial')
                            ->where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('amount', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('rawmaterial', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionPurchase::with('supplier','rawmaterial')             
                            ->where('company_id','=',$user->company_id)          
                            ->where(function ($query) use($search) {
                                $query->where('amount','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('quantity', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('rawmaterial', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($purchases))
        {
            foreach ($purchases as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['supplier'] = $value->supplier_id;
                $nestedData['raw_material_id'] = $value->rawmaterial->name;
                $nestedData['quantity'] = $value->quantity;
                $nestedData['rate'] = $value->rate;                     
                $nestedData['amount'] = $value->amount; 
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('production.purchase.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.purchase.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.purchase.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum,   
                    "totalQuantity"            => $totalQuantity   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = ProductionSupplier::all('id','name');
        $rawmaterials = ProductionRawMaterial::all('id','name');
        return view('pages.production.purchase.create', compact('suppliers','rawmaterials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'supplier_selection'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            if ($request->supplier_selection == 2) {
                $supplier = DB::table('production_suppliers')->insertGetId(
                    [
                        'name' => $request->name,
                        'mobile' => $request->mobile,
                        'address' => $request->address
                    ]
                );
            }
            else {
                $supplier = $request->supplier_id;
            }
            //$totalAmount = [];

            for ($i=0; $i < count($request->raw_material_id) ; $i++) {
                $token = time().str_random(10); 
                $purchase =  new ProductionPurchase;

                $purchase->date = $request->date;
                $purchase->company_id = $user->company_id;
                $purchase->supplier_id = $supplier;
                $purchase->invoice = $request->invoice;
                $purchase->details = $request->details;
                $purchase->raw_material_id = $request->raw_material_id[$i];
                $purchase->rate = $request->rate[$i];
                $purchase->quantity = $request->quantity[$i];
                $purchase->amount = $request->quantity[$i] * $request->rate[$i];
                $purchase->token = $token;           

                $purchase->save();

                $rawmaterial = ProductionRawMaterial::find($request->raw_material_id[$i]);
                $rawmaterial->stock = $rawmaterial->stock + $request->quantity[$i];
                $rawmaterial->purchase = $rawmaterial->purchase + $request->quantity[$i];
                $rawmaterial->save();
            }
        });
        return redirect()
                    ->route('production.purchase.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = ProductionPurchase::find($id);
        return view('pages.production.purchase.show',compact('purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purchase = ProductionPurchase::find($id);
        $suppliers = ProductionSupplier::all('id','name');
        $rawmaterials = ProductionRawMaterial::all('id','name');
        return view('pages.production.purchase.edit', compact('purchase','suppliers','rawmaterials'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'supplier_id'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'quantity'              => 'required',
        ]);

        DB::transaction(function () use ($request, $id) {
            $purchase =  ProductionPurchase::find($id);

            $oldRawMaterial = ProductionRawMaterial::find($purchase->raw_material_id);
            $oldRawMaterial->stock = ($oldRawMaterial->stock - $purchase->quantity) + $request->quantity;
            $oldRawMaterial->purchase = ($oldRawMaterial->purchase - $purchase->quantity) + $request->quantity;
            $oldRawMaterial->save();

            $purchase->date = $request->date;
            $purchase->details = $request->details;
            $purchase->supplier_id = $request->supplier_id;
            $purchase->rate = $request->rate;
            $purchase->quantity = $request->quantity;
            $purchase->amount = $request->amount;
            $purchase->invoice = $request->invoice;

            $purchase->save();
        });
        return redirect()
                    ->route('production.purchase.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $purchase =  ProductionPurchase::find($id);

            $oldRawMaterial = ProductionRawMaterial::find($purchase->raw_material_id);
            $oldRawMaterial->stock = $oldRawMaterial->stock - $purchase->quantity;
            $oldRawMaterial->purchase = $oldRawMaterial->purchase - $purchase->quantity;
            $oldRawMaterial->save();

            $purchase->delete();
        });
        return redirect()
                    ->route('production.purchase.index')
                    ->with('success', 'Deleted Successfully');
    }

    public function report(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($user->hasRole('super_admin')) {
            $purchases = ProductionPurchase::whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchase::whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionPurchase::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $purchases = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        return view('pages.production.purchase.report', compact('purchases','start_date','end_date','amount','quantity'));
    }

    public function printReport(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $purchases = ProductionPurchase::whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchase::whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionPurchase::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $purchases = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        return view('pages.production.purchase.print', compact('purchases','start_date','end_date','amount','quantity','date'));
    }

    public function reportExportPDF(Request $request){
        $user = Auth::user();
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($user->hasRole('super_admin')) {
            $purchases = ProductionPurchase::whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchase::whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionPurchase::whereBetween('date', [$start_date, $end_date])->sum('quantity');
            
        }
        else {
            $id = $user->company_id;
            //$company = Company::find($id);
            $purchases = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $amount = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $quantity = ProductionPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('quantity');
        }
        $dt = Carbon::now();
        $date = $dt->toDayDateTimeString();
        $pdf = PDF::loadView('pages.production.purchase.pdf', compact('purchases','start_date','end_date','amount','quantity','date'));
        return $pdf->download('PurchaseReport.pdf');
    }
}
