<?php

namespace App\Http\Controllers\Production;

use App\Models\ProductionModels\ProductionPurchase;
use App\Models\ProductionModels\ProductionPurchaseOrder;
use App\Models\ProductionModels\ProductionSupplier;
use App\Models\ProductionModels\ProductionPurchaseTransaction;
use App\Models\ProductionModels\ProductionRawMaterial;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.production.purchase-order.index');
    }

    //server side datatable super admin view
    public function allOrders(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'supplier_id',
                            4 => 'raw_material_id',
                            5 => 'quantity',
                            6 => 'invoice',
                            7 => 'actions',
                        );
        

        $totalData = ProductionPurchaseOrder::count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $orders = ProductionPurchaseOrder::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $orders =  ProductionPurchaseOrder::with('supplier','rawmaterial','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhere('invoice', 'LIKE',"%{$search}%")
                        ->orWhereHas('supplier', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('rawmaterial', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductionPurchaseOrder::with('supplier','rawmaterial','company')
                        ->where('quantity','LIKE',"%{$search}%")
                        ->orWhere('invoice', 'LIKE',"%{$search}%")
                        ->orWhere('date', 'LIKE',"%{$search}%")
                        ->orWhereHas('supplier', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('rawmaterial', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->orWhereHas('company', function($query) use($search) {
                            $query->where('name','LIKE',"%{$search}%");
                        })
                        ->count();
        }

        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['raw_material_id'] = $value->rawmaterial->name;
                $nestedData['quantity'] = $value->quantity;                    
                $nestedData['invoice'] = $value->invoice;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('production/purchase/order/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Delivery">
                                        Delivery
                                    </a>
                                    <a href="'.url('production/purchase/order/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('production.purchase-order.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.purchase-order.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.purchase-order.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable business admin view
    public function allOrdersAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'supplier_id',
                            3 => 'raw_material_id',
                            4 => 'quantity',
                            5 => 'invoice',
                            6 => 'actions',
                        );
        
        $user = Auth::user();

        $totalData = ProductionPurchaseOrder::where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $orders = ProductionPurchaseOrder::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $orders =  ProductionPurchaseOrder::with('supplier','rawmaterial')
                            ->where('company_id',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('invoice', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('rawmaterial', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionPurchaseOrder::with('supplier','rawmaterial')
                            ->where('company_id',$user->company_id)
                            ->where(function ($query) use($search) {
                                $query->where('quantity','LIKE',"%{$search}%")
                                    ->orWhere('date', 'LIKE',"%{$search}%")
                                    ->orWhere('invoice', 'LIKE',"%{$search}%")
                                    ->orWhereHas('supplier', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    })
                                    ->orWhereHas('rawmaterial', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                            })
                            ->count();
        }

        $data = array();
        if(!empty($orders))
        {
            foreach ($orders as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['supplier_id'] = $value->supplier->name;
                $nestedData['raw_material_id'] = $value->rawmaterial->name;
                $nestedData['quantity'] = $value->quantity;     
                $nestedData['invoice'] = $value->invoice;                    
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('production/purchase/order/delivery',$value->id) .'" class="btn btn-primary btn-sm" title="Delivery">
                                        Delivery
                                    </a>
                                    <a href="'.url('production/purchase/order/delivery/report',$value->id) .'" class="btn btn-warning btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('production.purchase-order.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('production.purchase-order.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('production.purchase-order.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = ProductionSupplier::all('id','name');
        $rawmaterials = ProductionRawMaterial::all('id','name');
        return view('pages.production.purchase-order.create',compact('suppliers','rawmaterials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'supplier_selection'  => ['required', Rule::notIn(['','0'])],
            'date'              => 'required',
            'invoice'              => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            if ($request->supplier_selection == 2) {
                $supplier = DB::table('production_suppliers')->insertGetId(
                    [
                        'name' => $request->name,
                        'mobile' => $request->mobile,
                        'address' => $request->address
                    ]
                );
            }
            else {
                $supplier = $request->supplier_id;
            }
            //$totalAmount = [];

            for ($i=0; $i < count($request->raw_material_id) ; $i++) {
                //$token = time().str_random(10); 
                $purchase =  new ProductionPurchaseOrder;

                $purchase->date = $request->date;
                $purchase->company_id = $user->company_id;
                $purchase->supplier_id = $supplier;
                $purchase->invoice = $request->invoice;
                $purchase->details = $request->details;
                $purchase->raw_material_id = $request->raw_material_id[$i];
                //$purchase->rate = $request->rate[$i];
                $purchase->quantity = $request->quantity[$i];
                //$purchase->token = $token;           

                $purchase->save();

                // $product = BusinessProduct::find($request->product_id[$i]);
                // $product->stock = $product->stock + $request->quantity[$i];
                // $product->save();
            }
        });

        return redirect()
                    ->route('production.purchase-order.index')
                    ->with('success', 'Purchase Order Has Been Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = ProductionPurchaseOrder::find($id);
        return view('pages.production.purchase-order.show',compact('purchase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = ProductionPurchaseOrder::find($id);
        return view('pages.production.purchase-order.edit',compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'date'              => 'required',
            'quantity'              => 'required',
            'invoice'              => 'required',
        ]);
        $order =  ProductionPurchaseOrder::find($id);
        DB::transaction(function () use ($request, $order) {

            $order->date = $request->date;
            $order->invoice = $request->invoice;
            $order->details = $request->details;
            $order->quantity = $request->quantity;

            $order->save();
        });
        return redirect()
                    ->route('production.purchase-order.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order =  ProductionPurchaseOrder::find($id);
        DB::transaction(function () use ($order) {
            
            $purchases = ProductionPurchase::where('order_id',$order->id)->get();
            foreach ($purchases as $purchase) {
                $oldRawMaterial = ProductionRawMaterial::find($purchase->raw_material_id);
                $oldRawMaterial->stock = $oldRawMaterial->stock - $purchase->quantity;
                $oldRawMaterial->sale = $oldRawMaterial->sale - $purchase->quantity;
                $oldRawMaterial->save();

                $purchase->delete();
            }

            $order->delete();
        });
        return redirect()
                    ->route('production.purchase-order.index')
                    ->with('success', 'Deleted Successfully');
    }

    //create purchase from delivery
    public function createDelivery($id)
    {
        $order = ProductionPurchaseOrder::find($id);
        return view('pages.production.purchase-order.createDelivery', compact('order'));
    }

    //store purchase from delivery
    public function storeDelivery(Request $request, $id)
    {
        $request->validate([ 
            'quantity'              => 'required',
            'date'              => 'required',
        ]);

        DB::transaction(function () use ($request, $id) {
            $user = Auth::user();
            $order = ProductionPurchaseOrder::find($id);

            $token = time().str_random(10); 
            $purchase =  new ProductionPurchase;

            $purchase->date = $request->date;
            $purchase->company_id = $user->company_id;
            $purchase->supplier_id = $order->supplier_id;
            $purchase->order_id = $order->id;
            $purchase->details = $request->details;
            $purchase->raw_material_id = $order->raw_material_id;
            $purchase->rate = $request->rate;
            $purchase->quantity = $request->quantity;
            $purchase->amount = $request->quantity * $request->rate;
            $purchase->invoice = $request->invoice;
            $purchase->token = $token;           

            $purchase->save();

            $rawmaterial = ProductionRawMaterial::find($order->raw_material_id);
            $rawmaterial->stock = $rawmaterial->stock + $request->quantity;
            $rawmaterial->purchase = $rawmaterial->purchase + $request->quantity;
            $rawmaterial->save();
        });

        return redirect()
                    ->route('production.purchase-order.index')
                    ->with('success', 'Delivery Added Successfully');
    }    

    //delivery report for a particular purchase
    public function deliveryReport($id)
    {
        $purchases = ProductionPurchase::where('order_id', $id)->get();
        $order = ProductionPurchaseOrder::find($id);
        $delivered = 0;
        foreach ($purchases as $purchase) {
            $delivered += $purchase->quantity;
        }
        return view('pages.production.purchase-order.report',compact('purchases','order','delivered'));
    }
}
