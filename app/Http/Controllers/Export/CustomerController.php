<?php

namespace App\Http\Controllers\Export;

use App\Models\ExportModels\ExportCustomer;
use App\Models\ExportModels\ExportSale;
use App\Models\ExportModels\ExportSaleTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.export.customer.index');
    }

    //server side datatable
    public function allCustomers(Request $request)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2=> 'mobile',
                            3=> 'address',
                            4=> 'actions',
                        );
  
        $totalData = ExportCustomer::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $customers = ExportCustomer::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $customers =  ExportCustomer::where('name','LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ExportCustomer::where('name','LIKE',"%{$search}%")
                            ->orWhere('mobile', 'LIKE',"%{$search}%")
                            ->count();
        }

        $data = array();
        if(!empty($customers))
        {
            foreach ($customers as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['name'] = $value->name;
                $nestedData['mobile'] = $value->mobile;
                $nestedData['address'] = $value->address;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.url('export/customer/report',$value->id) .'" class="btn btn-primary btn-sm" title="Report">
                                        Report
                                    </a>
                                    <a href="'.route('export.customer.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('export.customer.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer = null;
        return view('pages.export.customer.create', compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        ExportCustomer::create($request->all());

        return redirect()
                    ->route('export.customer.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = ExportCustomer::find($id);
        return view('pages.export.customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required|string',
            'mobile'    => 'required',
        ]);

        $customer = ExportCustomer::find($id);
        $customer->update($request->all());

        return redirect()
                    ->route('export.customer.index')
                    ->with('success','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = ExportCustomer::find($id);
        $customer->delete();

        return redirect()
                    ->route('export.customer.index')
                    ->with('success','Deleted Successfully');
    }
}
