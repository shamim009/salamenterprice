<?php

namespace App\Http\Controllers\Export;

use App\Models\ExportModels\ExportExpenseItem;
use App\Models\ExportModels\ExportExpense;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ExpenseItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenseItems = ExportExpenseItem::all();
        return view('pages.export.expense-item.index',compact('expenseItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $expenseItem = null;
        return view('pages.export.expense-item.create', compact('expenseItem'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  =>'required',
        ]);

        ExportExpenseItem::create($request->all());
        return redirect()
                    ->route('export.expense-item.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expenseItem = ExportExpenseItem::find($id);
        return view('pages.export.expense-item.edit', compact('expenseItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  =>'required',
        ]);

        $expenseItem = ExportExpenseItem::find($id);
        $expenseItem->update($request->all());
        return redirect()
                    ->route('export.expense-item.index')
                    ->with('success', 'Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expenseItem = ExportExpenseItem::find($id);
        $expenseItem->delete();
        return redirect()
                    ->route('export.expense-item.index')
                    ->with('warning', 'Deleted Successfully');
    }

    //individual expense item report
    public function report($id)
    {
        $expenseItem = ExportExpenseItem::find($id);
        return view('pages.export.expense-item.report',compact('expenseItem','id'));
    }

    //server side datatable
    public function particularExpense(Request $request, $id)
    {      
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2=> 'expenseItem_id',
                            3=> 'amount',
                            4=> 'actions',
                        );
  
        $totalData = ExportExpense::where('expenseItem_id', $id)->count();
        $totalSum = ExportExpense::where('expenseItem_id', $id)->sum('amount');
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $expenses = ExportExpense::with('expenseitem')
            ->where('expenseItem_id', $id)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $expenses = ExportExpense::with('expenseitem')
            ->where('expenseItem_id', $id)
            ->where('date', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhereHas('expenseitem', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();

            $totalFiltered = ExportExpense::with('expenseitem')
            ->where('expenseItem_id', $id)
            ->where('date', 'LIKE',"%{$search}%")
            ->orWhere('amount', 'LIKE',"%{$search}%")
            ->orWhereHas('expenseitem', function($query) use($search) {
                $query->where('name','LIKE',"%{$search}%");
            })
            ->count();
        }

        $data = array();
        if(!empty($expenses))
        {
            foreach ($expenses as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date;
                $nestedData['expenseItem_id'] = $value->expenseitem->name;
                $nestedData['amount'] = $value->amount;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('export.expense.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('export.expense.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "totalSum"            => $totalSum   
                    );
            
        echo json_encode($json_data);        
    }
}
