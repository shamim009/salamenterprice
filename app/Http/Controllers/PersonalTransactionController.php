<?php

namespace App\Http\Controllers;

use App\PersonalTransaction;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PersonalTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$income = PersonalTransaction::where('purpose','=','1')->sum('amount');
    	$expense = PersonalTransaction::where('purpose','=','2')->sum('amount');
        return view('pages.personal-transaction.index',compact('income','expense'));
    }

    //server side datatable super admin view
    public function allTransactions(Request $request)
    {     
        $columns = array( 
                            0 => 'id', 
                            1 => 'date',
                            2 => 'purpose',
                            3 => 'amount',
                            4 => 'received_from',
                            5 => 'expense_for',
                            6 => 'details',
                            7 => 'actions',
                        );
        

        $totalData = PersonalTransaction::count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $transactions = PersonalTransaction::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  PersonalTransaction::where('amount','LIKE',"%{$search}%")
                        ->orWhere('date','LIKE',"%{$search}%")
                        ->orWhere('expense_for','LIKE',"%{$search}%")
                        ->orWhere('received_from','LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = PersonalTransaction::where('amount','LIKE',"%{$search}%")
                        ->orWhere('date','LIKE',"%{$search}%")
                        ->orWhere('expense_for','LIKE',"%{$search}%")
                        ->orWhere('received_from','LIKE',"%{$search}%")
                        ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                if ($value->purpose == 1) {
                    $nestedData['purpose'] = 'Income';
                }
                else {
                    $nestedData['purpose'] = 'Expense';
                }
                $nestedData['amount'] = $value->amount;
                $nestedData['received_from'] = $value->received_from;
                $nestedData['expense_for'] = $value->expense_for;
                $nestedData['details'] = $value->details;
                $nestedData['actions'] = '<div class="btn-group">
                                    <a href="'.route('personal-transaction.show',$value->id) .'" class="btn btn-secondary btn-sm" title="Show">
                                        Show
                                    </a>
                                    <a href="'.route('personal-transaction.edit',$value->id) .'" class="btn btn-success btn-sm" title="Update">
                                        Update
                                    </a>
                                    <button class="btn btn-sm btn-danger btn-delete" data-remote=" '.route('personal-transaction.destroy',$value->id) .'" title="Delete">Delete</button>
                                </div>';
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $personalTransaction = null;
        return view('pages.personal-transaction.create', compact('personalTransaction'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'      => 'required',
            'purpose'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);

        PersonalTransaction::create($request->all());

        return redirect()
                    ->route('personal-transaction.index')
                    ->with('success', 'Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonalTransaction  $personalTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(PersonalTransaction $personalTransaction)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonalTransaction  $personalTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonalTransaction $personalTransaction)
    {
        return view('pages.personal-transaction.edit', compact('personalTransaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonalTransaction  $personalTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonalTransaction $personalTransaction)
    {
        $request->validate([
            'date'      => 'required',
            'purpose'  => ['required', Rule::notIn(['','0'])],
            'amount'    => 'required|numeric',
        ]);

        $personalTransaction->update($request->all());

        return redirect()
                    ->route('personal-transaction.index')
                    ->with('success','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonalTransaction  $personalTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonalTransaction $personalTransaction)
    {
        $personalTransaction->delete();

        return redirect()
                    ->route('personal-transaction.index')
                    ->with('success','Deleted Successfully');
    }
}
