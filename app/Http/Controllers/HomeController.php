<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\BusinessModels\BusinessPurchaseTransaction;
use App\Models\BusinessModels\BusinessPurchase;
use App\Models\BusinessModels\BusinessSaleTransaction;
use App\Models\BusinessModels\BusinessSale;
use App\Models\BusinessModels\BusinessCashBook;
use App\Models\BusinessModels\BusinessBankAccountTransaction;
use App\Models\BusinessModels\BusinessExpense;
use App\Models\BusinessModels\BusinessExpenseTransaction;
use App\Models\BusinessModels\BusinessProduct;
use App\Models\ProductionModels\ProductionCashBook;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Company;
use App\Models\BusinessModels\BusinessFiscalYear;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $date = Carbon::now();
        $date = $date->toDateString();
        $yesterday = Carbon::yesterday();
        $prevDate = $yesterday->toDateString();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        //dd($date);
        if ($user->hasRole('business_admin')) {
            $purchasePaid = BusinessPurchaseTransaction::where('fiscal_year',$fisc_year->id)->where([
                ['company_id',$user->company_id],
                ['purpose','=','2']
            ])->sum('amount');

            $salePaid = BusinessSaleTransaction::where('fiscal_year',$fisc_year->id)->where([
                ['company_id',$user->company_id],
                ['purpose','=','2']
            ])->sum('amount');

            // $bankDepositTrans = BusinessBankAccountTransaction::where([
            //     ['company_id',$user->company_id],
            //     ['purpose','=','1'],
            //     ['date',$date]
            // ])->get();

            // $bankWithdrawTrans = BusinessBankAccountTransaction::where([
            //     ['company_id',$user->company_id],
            //     ['purpose','=','2'],
            //     ['date',$date]
            // ])->get();

            $expensePaid = BusinessExpenseTransaction::where('fiscal_year',$fisc_year->id)->where([
                ['company_id',$user->company_id]
            ])->sum('amount');

            $purchaseQuantity = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where([
                ['company_id',$user->company_id]
            ])->sum('quantity');

            $saleQuantity = BusinessSale::where('fiscal_year',$fisc_year->id)->where([
                ['company_id',$user->company_id]
            ])->sum('quantity');


            $purchaseAmount = BusinessPurchase::where('fiscal_year',$fisc_year->id)->where([
                ['company_id',$user->company_id]
            ])->sum('amount');

            $saleAmount = BusinessSale::where('fiscal_year',$fisc_year->id)->where([
                ['company_id',$user->company_id]
            ])->sum('amount');

            // $prevBankDepositTrans = BusinessBankAccountTransaction::where([
            //     ['company_id',$user->company_id],
            //     ['purpose','=','1'],
            //     ['date',$prevDate]
            // ])->get();

            // $prevBankWithdrawTrans = BusinessBankAccountTransaction::where([
            //     ['company_id',$user->company_id],
            //     ['purpose','=','2'],
            //     ['date',$prevDate]
            // ])->get();

            $expenseAmount = BusinessExpense::where('fiscal_year',$fisc_year->id)->where([
                ['company_id',$user->company_id]
            ])->sum('amount');
            // $balance = 0;
            // foreach ($prevPurchaseTrans as $key => $value) {
            //     $balance -= $value->amount;
            // }

            // foreach ($prevSaleTrans as $key => $value) {
            //     $balance += $value->amount;
            // }

            // foreach ($prevBankWithdrawTrans as $key => $value) {
            //     $balance += $value->amount;
            // }

            // foreach ($prevBankDepositTrans as $key => $value) {
            //     $balance -= $value->amount;
            // }

            // foreach ($prevExpenses as $key => $value) {
            //     $balance -= $value->amount;
            // }

            return view('home',compact('purchasePaid','salePaid','expensePaid','expenseAmount','saleAmount','saleQuantity','purchaseAmount','purchaseQuantity'));
        }
        else {
            return view('home');
        }
    }

    //reset password form
    public function showChangePasswordForm(){
        return view('auth.change-password');
    }

    //reset password
    public function changePassword(Request $request){

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","your current password does not match with the password you provided. please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","new password cannot be the same as your current password. please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->action(
            'HomeController@index'
        )->with("success","password changed successfully !");

    }

    //business cash book
    public function businessCashBook(){
        return view('pages.business.cash-book');
    }

    //server side datatable super admin view
    public function allBusinessCashBook(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'purpose',
                            4 => 'details',
                            5 => 'debit',
                            6 => 'credit',
                            7 => 'balance',
                        );
        
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessCashBook::where('fiscal_year',$fisc_year->id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $transactions = BusinessCashBook::where('fiscal_year',$fisc_year->id)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  BusinessCashBook::with('sale','purchase','company','expense')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('expense', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('expenseitem', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('sale', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('customer', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('account', function($query) use($search) {
                                            $query->where('account_no','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('purchase', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('supplier', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = BusinessCashBook::with('sale','purchase','company','expense')
                        ->where('fiscal_year',$fisc_year->id)
                        ->where(function ($query) use($search) {
                            $query->where('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('expense', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('expenseitem', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('sale', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('customer', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('account', function($query) use($search) {
                                            $query->where('account_no','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('purchase', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('supplier', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            $balance = 0;
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                if ($value->purpose == 1) {
                    
                    $nestedData['purpose'] = 'Sale Payment';
                    $nestedData['details'] = 'Received From '.$value->sale->customer->name.'.';
                    $nestedData['debit'] = $value->sale->amount;
                    $nestedData['credit'] = '';
                    $nestedData['balance'] = $balance += $value->sale->amount;
                }
                else if ($value->purpose == 2) {
                    
                    $nestedData['purpose'] = 'Purchase Payment';
                    $nestedData['details'] = 'Paid to '.$value->purchase->supplier->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->purchase->amount;
                    $nestedData['balance'] = $balance -= $value->purchase->amount;
                }
                else if ($value->purpose == 4) {
                    
                    $nestedData['purpose'] = 'Sale Payment Return';
                    $nestedData['details'] = 'Returned To '.$value->sale->customer->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->sale->amount;
                    $nestedData['balance'] = $balance -= $value->sale->amount;
                }
                else if ($value->purpose == 5) {
                    
                    $nestedData['purpose'] = 'Purchase Payment Return';
                    $nestedData['details'] = 'Returned From '.$value->purchase->supplier->name.'.';
                    $nestedData['debit'] = $value->purchase->amount;
                    $nestedData['credit'] = '';
                    $nestedData['balance'] = $balance += $value->purchase->amount;
                }
                else {
                    $nestedData['purpose'] = 'Expense Payment';
                    $nestedData['details'] = 'Paid to/for '.$value->expense->expenseitem->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->expense->amount;
                    $nestedData['balance'] = $balance -= $value->expense->amount;
                }
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable business admin view
    public function allBusinessCashBookAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'purpose',
                            3 => 'details',
                            4 => 'debit',
                            5 => 'credit',
                            6 => 'balance',
                        );
        
        $user = Auth::user();
        $fisc_year = BusinessFiscalYear::where('status','=','1')->first();
        $totalData = BusinessCashBook::where('fiscal_year',$fisc_year->id)->where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $transactions = BusinessCashBook::where('fiscal_year',$fisc_year->id)
                        ->where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $transactions =  BusinessCashBook::with('expense','sale','purchase')
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                            $query->where('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('expense', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('expenseitem', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('sale', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('customer', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('account', function($query) use($search) {
                                            $query->where('account_no','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('purchase', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('supplier', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = BusinessCashBook::with('expense','sale','purchase')
                            ->where('company_id','=',$user->company_id)
                            ->where('fiscal_year',$fisc_year->id)
                            ->where(function ($query) use($search) {
                            $query->where('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('expense', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('expenseitem', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('sale', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('customer', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('account', function($query) use($search) {
                                            $query->where('account_no','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('purchase', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('supplier', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            $balance = 0;
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                if ($value->purpose == 1) {
                    
                    $nestedData['purpose'] = 'Sale Payment';
                    $nestedData['details'] = 'Received From '.$value->sale->customer->name.'.';
                    $nestedData['debit'] = $value->sale->amount;
                    $nestedData['credit'] = '';
                    $nestedData['balance'] = $balance += $value->sale->amount;
                }
                else if ($value->purpose == 2) {
                    
                    $nestedData['purpose'] = 'Purchase Payment';
                    $nestedData['details'] = 'Paid to '.$value->purchase->supplier->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->purchase->amount;
                    $nestedData['balance'] = $balance -= $value->purchase->amount;
                }

                else if ($value->purpose == 4) {
                    
                    $nestedData['purpose'] = 'Sale Payment Return';
                    $nestedData['details'] = 'Returned To '.$value->sale->customer->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->sale->amount;
                    $nestedData['balance'] = $balance -= $value->sale->amount;
                }
                else if ($value->purpose == 5) {
                    
                    $nestedData['purpose'] = 'Purchase Payment Return';
                    $nestedData['details'] = 'Returned From '.$value->purchase->supplier->name.'.';
                    $nestedData['debit'] = $value->purchase->amount;
                    $nestedData['credit'] = '';
                    $nestedData['balance'] = $balance += $value->purchase->amount;
                }
                else {
                    $nestedData['purpose'] = 'Expense Payment';
                    $nestedData['details'] = 'Paid to/for '.$value->expense->expenseitem->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->expense->amount;
                    $nestedData['balance'] = $balance -= $value->expense->amount;
                }
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    //business cash book
    public function productionCashBook(){
        return view('pages.production.cash-book');
    }

    //server side datatable super admin view
    public function allProductionCashBook(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 =>'company_id',
                            3 => 'purpose',
                            4 => 'details',
                            5 => 'debit',
                            6 => 'credit',
                            7 => 'balance',
                        );
        

        $totalData = ProductionCashBook::count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {

            $transactions = ProductionCashBook::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();

        }
        else {
            $search = $request->input('search.value'); 

            $transactions =  ProductionCashBook::with('sale','purchase','company','expense')
                        ->where(function ($query) use($search) {
                            $query->where('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('expense', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('expenseitem', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('sale', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('customer', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('account', function($query) use($search) {
                                            $query->where('account_no','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('purchase', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('supplier', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                });
                        })
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = ProductionCashBook::with('sale','purchase','company','expense')
                        ->where(function ($query) use($search) {
                            $query->where('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('expense', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('expenseitem', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('sale', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('customer', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('account', function($query) use($search) {
                                            $query->where('account_no','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('company', function($query) use($search) {
                                    $query->where('name','LIKE',"%{$search}%");
                                })
                                ->orWhereHas('purchase', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('supplier', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                });
                        })
                        ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            $balance = 0;
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                $nestedData['company_id'] = $value->company->name;
                if ($value->purpose == 1) {
                    
                    $nestedData['purpose'] = 'Sale Payment';
                    $nestedData['details'] = 'Received From '.$value->sale->customer->name.'.';
                    $nestedData['debit'] = $value->sale->amount;
                    $nestedData['credit'] = '';
                    $nestedData['balance'] = $balance += $value->sale->amount;
                }
                else if ($value->purpose == 2) {
                    
                    $nestedData['purpose'] = 'Purchase Payment';
                    $nestedData['details'] = 'Paid to '.$value->purchase->supplier->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->purchase->amount;
                    $nestedData['balance'] = $balance -= $value->purchase->amount;
                }
                else if ($value->purpose == 4) {
                    
                    $nestedData['purpose'] = 'Sale Payment Return';
                    $nestedData['details'] = 'Returned To '.$value->sale->customer->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->sale->amount;
                    $nestedData['balance'] = $balance -= $value->sale->amount;
                }
                else if ($value->purpose == 5) {
                    
                    $nestedData['purpose'] = 'Purchase Payment Return';
                    $nestedData['details'] = 'Returned From '.$value->purchase->supplier->name.'.';
                    $nestedData['debit'] = $value->purchase->amount;
                    $nestedData['credit'] = '';
                    $nestedData['balance'] = $balance += $value->purchase->amount;
                }
                else {
                    $nestedData['purpose'] = 'Expense Payment';
                    $nestedData['details'] = 'Paid to/for '.$value->expense->expenseitem->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->expense->amount;
                    $nestedData['balance'] = $balance -= $value->expense->amount;
                }
                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }


    //server side datatable Production admin view
    public function allProductionCashBookAdminView(Request $request)
    {     
        $columns = array( 
                            0 =>'id', 
                            1 =>'date',
                            2 => 'purpose',
                            3 => 'details',
                            4 => 'debit',
                            5 => 'credit',
                            6 => 'balance',
                        );
        
        $user = Auth::user();

        $totalData = ProductionCashBook::where('company_id',$user->company_id)->count();
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {
            
            $transactions = ProductionCashBook::where('company_id',$user->company_id)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
        }
        else {
            $search = $request->input('search.value'); 
            
            $transactions =  ProductionCashBook::with('expense','sale','purchase')
                            ->where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                            $query->where('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('expense', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('expenseitem', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('sale', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('customer', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('account', function($query) use($search) {
                                            $query->where('account_no','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('purchase', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('supplier', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                });
                            })
                            
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ProductionCashBook::with('expense','sale','purchase')
                            ->where('company_id','=',$user->company_id)
                            ->where(function ($query) use($search) {
                            $query->where('date', 'LIKE',"%{$search}%")
                                ->orWhereHas('expense', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('expenseitem', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('sale', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('customer', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('account', function($query) use($search) {
                                            $query->where('account_no','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                })
                                ->orWhereHas('purchase', function($query) use($search) {
                                    $query->where('amount','LIKE',"%{$search}%")
                                        ->orWhereHas('supplier', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('bank', function($query) use($search) {
                                            $query->where('name','LIKE',"%{$search}%");
                                        })
                                        ->orWhereHas('mobileBanking', function($query) use($search) {
                                        $query->where('name','LIKE',"%{$search}%");
                                    });
                                });
                            })                     
                            ->count();
        }

        $data = array();
        if(!empty($transactions))
        {
            $balance = 0;
            foreach ($transactions as $key => $value)
            {
                $nestedData['id'] = $key + 1;
                $nestedData['date'] = $value->date->format("d-m-Y");
                if ($value->purpose == 1) {
                    
                    $nestedData['purpose'] = 'Sale Payment';
                    $nestedData['details'] = 'Received From '.$value->sale->customer->name.'.';
                    $nestedData['debit'] = $value->sale->amount;
                    $nestedData['credit'] = '';
                    $nestedData['balance'] = $balance += $value->sale->amount;
                }
                else if ($value->purpose == 2) {
                    
                    $nestedData['purpose'] = 'Purchase Payment';
                    $nestedData['details'] = 'Paid to '.$value->purchase->supplier->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->purchase->amount;
                    $nestedData['balance'] = $balance -= $value->purchase->amount;
                }

                else if ($value->purpose == 4) {
                    
                    $nestedData['purpose'] = 'Sale Payment Return';
                    $nestedData['details'] = 'Returned To '.$value->sale->customer->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->sale->amount;
                    $nestedData['balance'] = $balance -= $value->sale->amount;
                }
                else if ($value->purpose == 5) {
                    
                    $nestedData['purpose'] = 'Purchase Payment Return';
                    $nestedData['details'] = 'Returned From '.$value->purchase->supplier->name.'.';
                    $nestedData['debit'] = $value->purchase->amount;
                    $nestedData['credit'] = '';
                    $nestedData['balance'] = $balance += $value->purchase->amount;
                }
                else {
                    $nestedData['purpose'] = 'Expense Payment';
                    $nestedData['details'] = 'Paid to/for '.$value->expense->expenseitem->name.'.';
                    $nestedData['debit'] = '';
                    $nestedData['credit'] = $value->expense->amount;
                    $nestedData['balance'] = $balance -= $value->expense->amount;
                }
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,   
                    );
            
        echo json_encode($json_data);        
    }

    //business stock report page
    public function businessStockReportPage(){
        return view('pages.business.stock-page');
    }

    public function businessStockReport(Request $request){
        $user = Auth::user();
        $id = $user->company_id;
        $company = Company::find($id);
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($end_date < $start_date){
            return back()->with('warning', 'End Date Need to be Greater than the Start Date!');
        }
        if ($company->parent_business == 1) {
            $purchases = BusinessPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();
            $sales = BusinessSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->get();

            $purchaseAmount = BusinessPurchase::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');
            $saleAmount = BusinessSale::where('company_id',$id)->whereBetween('date', [$start_date, $end_date])->sum('amount');

            $products = BusinessProduct::all();
            $stockAmount = [];
            $productPurchase = [];
            $productSale = [];
            $remainingStorageTotal = [];
            foreach ($products as $product) {
                $purchaseStorage = 0;
                $saleStorage = 0;
                foreach ($purchases as $purchase) {
                    if ($product->id == $purchase->product_id) {
                        $purchaseStorage = $purchaseStorage + $purchase->quantity;
                    }
                }

                foreach ($sales as $sale) {
                    if ($product->id == $sale->product_id) {
                        $saleStorage = $saleStorage + $sale->quantity;
                    }
                }

                $remainingStorage = $purchaseStorage - $saleStorage;
                // foreach ($purchases as $purchase) {
                //     if ($product->id == $purchase->product_id) {
                //         $remainingStorageAmount = $remainingStorage * $purchase->rate;
                //     }
                // }
                $purchase = BusinessPurchase::where([
                    ['company_id',$id],
                    ['product_id',$product->id]
                ])->orderBy('created_at', 'desc')->first();

                //dd($purchase);
                if ($purchase) {
                    $remainingStorageAmount = $remainingStorage * $purchase->rate;
                    array_push($stockAmount, $remainingStorageAmount);
                }
                array_push($productPurchase, $purchaseStorage);
                array_push($productSale, $saleStorage);
                array_push($remainingStorageTotal, $remainingStorage); 
            }
            $finalStockAmount = array_sum($stockAmount);
        }
        return view('pages.business.stock-report', compact('company','purchaseAmount','saleAmount','productPurchase','productSale','sales','purchases','finalStockAmount','start_date','end_date','remainingStorageTotal','products'));
    }
}
