<?php

namespace App\Models\BusinessModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BusinessModels\BusinessCustomer;
use App\Models\BusinessModels\BusinessBankAccount;
use App\MobileBanking;

class BusinessSaleTransaction extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    public function customer()
    {
        return $this->belongsTo(BusinessCustomer::class, 'customer_id')->withTrashed();
    }

    public function account()
    {
        return $this->belongsTo(BusinessBankAccount::class, 'bank_account_id')->withTrashed();
    }

    public function mobileBanking()
    {
        return $this->belongsTo(MobileBanking::class, 'mobile_banking_id');
    }
}
