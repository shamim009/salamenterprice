<?php

namespace App\Models\BusinessModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BusinessModels\BusinessExpenseTransaction;
use App\Models\BusinessModels\BusinessSaleTransaction;
use App\Models\BusinessModels\BusinessPurchaseTransaction;

class BusinessCashBook extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    public function sale()
    {
        return $this->belongsTo(BusinessSaleTransaction::class, 'sale_transaction_id')->withTrashed();
    }

    public function purchase()
    {
        return $this->belongsTo(BusinessPurchaseTransaction::class, 'purchase_transaction_id')->withTrashed();
    }

    public function expense()
    {
        return $this->belongsTo(BusinessExpenseTransaction::class, 'expense_transaction_id')->withTrashed();
    }
}
