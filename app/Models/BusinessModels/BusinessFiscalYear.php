<?php

namespace App\Models\BusinessModels;

use Illuminate\Database\Eloquent\Model;

class BusinessFiscalYear extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
