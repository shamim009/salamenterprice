<?php

namespace App\Models\BusinessModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BusinessModels\BusinessExpenseItem;
use App\Models\BusinessModels\BusinessBankAccount;
use App\Bank;
use App\MobileBanking;
use App\Company;

class BusinessExpenseTransaction extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    public function expenseitem()
    {
        return $this->belongsTo(BusinessExpenseItem::class, 'expenseItem_id')->withTrashed();
    }

    public function account()
    {
        return $this->belongsTo(BusinessBankAccount::class, 'bank_account_id')->withTrashed();
    }

    public function mobileBanking()
    {
        return $this->belongsTo(MobileBanking::class, 'mobile_banking_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
