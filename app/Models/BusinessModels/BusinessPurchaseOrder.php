<?php

namespace App\Models\BusinessModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BusinessModels\BusinessPurchase;
use App\Models\BusinessModels\BusinessSupplier;
use App\Models\BusinessModels\BusinessProduct;
use App\Company;

class BusinessPurchaseOrder extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    // public function purchase()
    // {
    //     return $this->belongsTo(BusinessPurchase::class, 'purchase_id')->withTrashed();
    // }

    public function supplier()
    {
        return $this->belongsTo(BusinessSupplier::class, 'supplier_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(BusinessProduct::class, 'product_id')->withTrashed();
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id')->withTrashed();
    }
}
