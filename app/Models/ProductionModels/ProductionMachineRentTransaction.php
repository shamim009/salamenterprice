<?php

namespace App\Models\ProductionModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ProductionModels\ProductionBankAccount;
use App\MobileBanking;

class ProductionMachineRentTransaction extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    public function account()
    {
        return $this->belongsTo(ProductionBankAccount::class, 'bank_account_id')->withTrashed();
    }

    public function mobileBanking()
    {
        return $this->belongsTo(MobileBanking::class, 'mobile_banking_id');
    }
}
