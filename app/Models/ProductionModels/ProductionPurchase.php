<?php

namespace App\Models\ProductionModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ProductionModels\ProductionSupplier;
use App\Models\ProductionModels\ProductionRawMaterial;
use App\Company;

class ProductionPurchase extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    public function supplier()
    {
        return $this->belongsTo(ProductionSupplier::class, 'supplier_id')->withTrashed();
    }

    public function rawmaterial()
    {
        return $this->belongsTo(ProductionRawMaterial::class, 'raw_material_id')->withTrashed();
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id')->withTrashed();
    }
}
