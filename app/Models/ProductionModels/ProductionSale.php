<?php

namespace App\Models\ProductionModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ProductionModels\ProductionCustomer;
use App\Models\ProductionModels\ProductionProduct;
use App\Company;

class ProductionSale extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    public function customer()
    {
        return $this->belongsTo(ProductionCustomer::class, 'customer_id')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(ProductionProduct::class, 'product_id')->withTrashed();
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id')->withTrashed();
    }
}
