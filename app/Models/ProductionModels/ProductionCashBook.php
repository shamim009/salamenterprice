<?php

namespace App\Models\ProductionModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ProductionModels\ProductionExpenseTransaction;
use App\Models\ProductionModels\ProductionSaleTransaction;
use App\Models\ProductionModels\ProductionPurchaseTransaction;
use App\Models\ProductionModels\ProductionMachineRentTransaction;

class ProductionCashBook extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    public function sale()
    {
        return $this->belongsTo(ProductionSaleTransaction::class, 'sale_transaction_id')->withTrashed();
    }

    public function purchase()
    {
        return $this->belongsTo(ProductionPurchaseTransaction::class, 'purchase_transaction_id')->withTrashed();
    }

    public function expense()
    {
        return $this->belongsTo(ProductionExpenseTransaction::class, 'expense_transaction_id')->withTrashed();
    }

    public function rent()
    {
        return $this->belongsTo(ProductionMachineRentTransaction::class, 'rent_transaction_id')->withTrashed();
    }
}
