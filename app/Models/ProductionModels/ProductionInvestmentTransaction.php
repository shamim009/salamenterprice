<?php

namespace App\Models\ProductionModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Bank;
use App\MobileBanking;
use App\Company;
use App\Models\ProductionModels\ProductionInvestment;
use App\Models\ProductionModels\ProductionBankAccount;

class ProductionInvestmentTransaction extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    public function investment(){
    	return $this->belongsTo(ProductionInvestment::class, 'investment_id')->withTrashed();
    }

    public function account()
    {
        return $this->belongsTo(ProductionBankAccount::class, 'bank_account_id')->withTrashed();
    }

    public function mobileBanking()
    {
        return $this->belongsTo(MobileBanking::class, 'mobile_banking_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id')->withTrashed();
    }
}
