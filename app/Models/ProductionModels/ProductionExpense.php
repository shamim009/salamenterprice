<?php

namespace App\Models\ProductionModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ProductionModels\ProductionExpenseItem;
use App\Company;

class ProductionExpense extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','date'];

    public function expenseitem()
    {
        return $this->belongsTo(ProductionExpenseItem::class, 'expenseItem_id')->withTrashed();
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id')->withTrashed();
    }
}
