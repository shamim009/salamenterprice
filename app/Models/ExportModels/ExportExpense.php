<?php

namespace App\Models\ExportModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ExportModels\ExportExpenseItem;
use App\Models\ExportModels\ExportCompany;

class ExportExpense extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function expenseitem()
    {
        return $this->belongsTo(ExportExpenseItem::class, 'expenseItem_id')->withTrashed();
    }

    public function company()
    {
        return $this->belongsTo(ExportCompany::class, 'company_id')->withTrashed();
    }
}
