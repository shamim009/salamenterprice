<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Loan;

class LoanProfit extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dates = ['date'];

    public function loan(){
    	return $this->belongsTo(Loan::class, 'loan_id')->withTrashed();
    }
}
