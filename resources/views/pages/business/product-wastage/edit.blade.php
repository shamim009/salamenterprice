@extends('layouts.master')
@section('title', 'Wastage')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Edit Wastage</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/product-wastage') }}">Wastages</a></li>
						<li class="breadcrumb-item active">Update</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<form method="POST" action="{{ route('business.product-wastage.update', $wastage->id) }}" id="submitForm">
						@csrf
						@method('PUT')

						{{-- Name --}}
						<div class="form-group">
							<label for="date">
								Date
							</label>

							<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ old('date', optional($wastage)->date) }}">

							@if ($errors->has('date'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('date') }}</strong>
							</span>
							@endif
						</div>

						{{-- Supplier --}}
						<div class="form-group">
							<label for="supplier_id">Select Supplier</label>
							<select name="supplier_id" class="form-control{{ $errors->has('supplier_id') ? ' is-invalid' : '' }}" id="supplier_id">
								<option value="">Select Supplier</option>
								@forelse($suppliers as $supplier)
								<option value="{{ $supplier->id }}" 
									@if( old('supplier_id', optional($wastage)->supplier_id) == $supplier->id )
									selected
									@endif
									>
									{{ $supplier->name }}
								</option>
								@empty
								<option value="">No Supplier Found</option>
								@endforelse
							</select>
							@if ($errors->has('supplier_id'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('supplier_id') }}</strong>
							</span>
							@endif
						</div>
						@role('super_admin')
						{{-- Rate --}}
						<div class="form-group">
							<label for="rate">
								Rate
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('rate') ? ' is-invalid' : '' }}" name="rate" id="rate" value="{{ old('rate', optional($wastage)->rate) }}">

							@if ($errors->has('rate'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('rate') }}</strong>
							</span>
							@endif
						</div>
						@endrole
						{{-- Quantity --}}
						<div class="form-group">
							<label for="quantity">
								Quantity
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" name="quantity" id="quantity" value="{{ old('quantity', optional($wastage)->quantity) }}">

							@if ($errors->has('quantity'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('quantity') }}</strong>
							</span>
							@endif
						</div>
						@role('super_admin')
						{{-- Amount --}}
						<div class="form-group">
							<label for="amount">
								Amount
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" id="amount" value="{{ old('amount', optional($wastage)->amount) }}" readonly="readonly">

							@if ($errors->has('amount'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('amount') }}</strong>
							</span>
							@endif
						</div>
						@endrole

						{{-- Details --}}
						<div class="form-group">
							<label for="address">
								Details
							</label>

							<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($wastage)->details) }}</textarea>

							@if( $errors->has('details'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('details') }}</strong>
							</span>
							@endif
						</div>

						{{-- Save --}}
						<div class="form-group row mb-0">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									{{ __('Save') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
@section('script')
<script>
	{{-- jquery datepicker --}}
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});

	//amount calculation
	$(document).ready(function(){
		var rate=$("#rate");
		var quantity=$("#quantity");
		rate.keyup(function(){
			var total=isNaN(parseFloat(rate.val()* quantity.val())) ? 0 :(rate.val()* quantity.val())
			$("#amount").val(total);
		});
		quantity.keyup(function(){
			var total=isNaN(parseInt(rate.val()* quantity.val())) ? 0 :(rate.val()* quantity.val())
			$("#amount").val(total);
		});
	});

	$('#supplier_id').select2({
		placeholder: 'Select Supplier',

		ajax: {
			url: '{!!URL::route('business.supplier-autocomplete-search')!!}',
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {
					results: data
				};
			},
			cache: true
		},
		theme: "bootstrap"
	});
</script>
@endsection