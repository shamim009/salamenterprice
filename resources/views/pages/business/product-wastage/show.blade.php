@extends('layouts.master')
@section('title', 'Wastage')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">View Wastage</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/product-wastage') }}">Wastages</a></li>
						<li class="breadcrumb-item active">View</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<tbody>
								<tr>
									<td>Date</td>
									<td>{{ $wastage->date->format("d-m-Y") }}</td>
								</tr>
								@role('super_admin')
								<tr>
									<td>Company</td>
									<td>{{ $wastage->company->name }}</td>
								</tr>
								@endrole
								<tr>
									<td>Supplier</td>
									<td>{{ $wastage->supplier->name }}</td>
								</tr>
								<tr>
									<td>Product</td>
									<td>{{ $wastage->product->name }}</td>
								</tr>
								@role('super_admin')
								<tr>
									<td>Rate</td>
									<td>{{ $wastage->rate }}</td>
								</tr>
								@endrole
								<tr>
									<td>Quantity</td>
									<td>
										{{ $wastage->quantity }}
									</td>
								</tr>
								@role('super_admin')
								<tr>
									<td>Amount</td>
									<td>{{ $wastage->amount }}</td>
								</tr>
								@endrole
								<tr>
									<td>Remarks</td>
									<td>{{ $wastage->details }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection