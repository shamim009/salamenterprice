<!DOCTYPE html>
<html lang="en">
<head>
    <style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
</head>
<body>
	@role('super_admin')
    <table>
        <thead>
            <tr>
                <th colspan="7">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="7">{{ config('app.name', 'Laravel') }}</th>
            </tr>          
            <tr>
                <th colspan="7">Expense Payment Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            <tr>
                <th colspan="7">Current Fiscal Year: {{ $fisc_year->title }}</th>
            </tr>
            <tr>
                <th colspan="7">Other Fiscal Year(s) Paid Amount: {{ $otherFiscAmount }}</th>
            </tr>
            <tr>
                <th>SL</th>
                <th>Date</th>                                   
                <th>Company</th>                                
                <th>Expense Type</th>
                <th>Amount</th>                         
                <th>Payment Mode</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transactions as $transaction)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $transaction->date->format("d-m-Y") }}</td>
                <td>{{ $transaction->company->name }}</td>
                <td>{{ $transaction->expenseitem->name }}</td>
                <td>{{ $transaction->amount }}</td>
                @if($transaction->payment_mode == 1)
                <td>Hand Cash</td>
                @elseif($transaction->payment_mode == 2)
                <td>Regular Banking</td>
                @else
                <td>Mobile Banking</td>
                @endif
                <td>{{ $transaction->details }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4">Total</th>
                <th>{{ $amount }}</th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th colspan="7">report generated on: {{ $date }}</th>
            </tr>
        </tfoot>
    </table>
    @endrole
    @role('business_admin')
    <table>
        <thead>
            <tr>
                <th colspan="6">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="6">{{ Auth::user()->company->name }}</th>
            </tr>
            <tr>
                <th colspan="6">Expense Payment Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            <tr>
                <th colspan="6">Current Fiscal Year: {{ $fisc_year->title }}</th>
            </tr>
            <tr>
                <th colspan="6">Other Fiscal Year(s) Paid Amount: {{ $otherFiscAmount }}</th>
            </tr>
            <tr>
                <th>SL</th>
                <th>Date</th>                               
                <th>Expense Type</th>
                <th>Amount</th>                         
                <th>Payment Mode</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transactions as $transaction)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $transaction->date->format("d-m-Y") }}</td>
                <td>{{ $transaction->expenseitem->name }}</td>
                <td>{{ $transaction->amount }}</td>
                @if($transaction->payment_mode == 1)
                <td>Hand Cash</td>
                @elseif($transaction->payment_mode == 2)
                <td>Regular Banking</td>
                @else
                <td>Mobile Banking</td>
                @endif
                <td>{{ $transaction->details }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">Total</th>
                <th>{{ $amount }}</th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <th colspan="6">report generated on: {{ $date }}</th>
            </tr>
        </tfoot>
    </table>
    @endrole
</body>
</html>