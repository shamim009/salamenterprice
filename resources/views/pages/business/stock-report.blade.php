@extends('layouts.master')
@section('title', 'Loan')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-9">
					<h1 class="m-0 text-dark">Stock Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</h1>
				</div><!-- /.col -->
				<div class="col-sm-3">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Stock Report</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				{{-- <div class="card-header">
					<a href="{{ url('bank-account/report/print', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Print" target="_blank">
						<i class="fa fa-print" aria-hidden="true" title="Print"></i> Print
					</a>

					<a href="{{ url('bank-account/report/pdf', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="PDF" target="_blank">
						<i class="fa fa-file-pdf-o" aria-hidden="true" title="PDF"></i> PDF
					</a>

					<a href="{{ url('bank-account/report/excel', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Excel" target="_blank">
						<i class="fa fa-file-excel-o" aria-hidden="true" title="Excel"></i> Excel
					</a>
				</div> --}}
			</div>
			<div class="row">
				<div class="col-12">
					
					<div class="alert alert-primary">
						<table>
							<thead>
								<tr>
									<th>Product</th>
									<th>Purchase</th>									
									<th>Sale</th>
									<th>Remaining</th>
								</tr>
							</thead>
							<tbody>
								@foreach($products as $key => $value)
								@if($productPurchase[$key] > 0 ||  $productSale[$key] > 0)
								<tr>
									<td>{{ $value->name }}</td>
									<td>
										{{ $productPurchase[$key] }}
									</td>
									
									<td>{{ $productSale[$key] }}</td>
									<td>
										{{ $remainingStorageTotal[$key] }}
									</td>
								</tr>
								@endif
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
