@extends('layouts.master')
@section('title', 'Sale Delivery')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Edit Sale Delivery</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/sale') }}">Sales</a></li>
						<li class="breadcrumb-item active">Delivery</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<form method="POST" action="{{ url('business/sale/delivery/edit',$delivery->id) }}">
						@csrf
						<div class="form-group">
							<label for="date">
								Date
							</label>

							<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ $delivery->date }}">

							@if ($errors->has('date'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('date') }}</strong>
							</span>
							@endif
						</div>

						{{-- Fiscal Year --}}
{{-- 						<div class="form-group">
							<label for="fiscal_year">Fiscal Year</label>
							<select name="fiscal_year" class="form-control{{ $errors->has('fiscal_year') ? ' is-invalid' : '' }}" id="fiscal_year">
								<option value="">Select</option>
								@forelse($fisc_years as $year)
									<option value="{{ $year->id }}" 
										@if( old('fiscal_year') == $year->id )
											selected
										@endif
										>
										{{ $year->title }}
									</option>
								@empty
									<option value="">No Fiscal Year Found</option>
								@endforelse
							</select>
							@if ($errors->has('fiscal_year'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('fiscal_year') }}</strong>
								</span>
							@endif
						</div> --}}


						{{-- Quantity --}}
						<div class="form-group">
							<label for="quantity">
								Quantity
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" name="quantity" id="quantity" value="{{ $delivery->quantity }}">

							@if ($errors->has('quantity'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('quantity') }}</strong>
							</span>
							@endif
						</div>

						{{-- Rate --}}
						<div class="form-group">
							<label for="rate">
								Rate
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('rate') ? ' is-invalid' : '' }}" name="rate" id="rate" value="{{ $delivery->rate }}">

							@if ($errors->has('rate'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('rate') }}</strong>
							</span>
							@endif
						</div>

						{{-- invoice --}}
						<div class="form-group">
							<label for="invoice">
								Invoice
							</label>

							<input type="text" class="form-control" name="invoice" id="invoice" value="{{ $delivery->invoice }}">

							@if ($errors->has('invoice'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('invoice') }}</strong>
							</span>
							@endif
						</div>

						{{-- Details --}}
						<div class="form-group">
							<label for="details">
								Remarks
							</label>

							<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ $delivery->details }}</textarea>

							@if( $errors->has('details'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('details') }}</strong>
							</span>
							@endif
						</div>

						{{-- Save --}}
						<div class="form-group row mb-0">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									{{ __('Save') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	{{-- jquery datepicker --}}
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});
</script>
@endsection