@extends('layouts.master')
@section('title', 'Sale')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Edit Sale</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/sale') }}">Sales</a></li>
						<li class="breadcrumb-item active">Update</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<form method="POST" action="{{ route('business.sale.update', $sale->id) }}" id="submitForm">
						@csrf
						@method('PUT')

						{{-- Name --}}
						<div class="form-group">
							<label for="date">
								Date
							</label>

							<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ old('date', optional($sale)->date) }}">

							@if ($errors->has('date'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('date') }}</strong>
							</span>
							@endif
						</div>

						{{-- Fiscal Year --}}
						<div class="form-group">
							<label for="fiscal_year">Fiscal Year</label>
							<select name="fiscal_year" class="form-control{{ $errors->has('fiscal_year') ? ' is-invalid' : '' }}" id="fiscal_year">
								<option value="">Select</option>
								@forelse($fisc_years as $year)
									<option value="{{ $year->id }}" 
										@if( old('fiscal_year', optional($sale)->fiscal_year) == $year->id )
											selected
										@endif
										>
										{{ $year->title }}
									</option>
								@empty
									<option value="">No Fiscal Year Found</option>
								@endforelse
							</select>
							@if ($errors->has('fiscal_year'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('fiscal_year') }}</strong>
								</span>
							@endif
						</div>

						{{-- customer --}}
						<div class="form-group">
							<label for="customer_id">Select customer</label>
							<select name="customer_id" class="form-control{{ $errors->has('customer_id') ? ' is-invalid' : '' }}" id="customer_id">
								<option value="">Select customer</option>
								@forelse($customers as $customer)
								<option value="{{ $customer->id }}" 
									@if( old('customer_id', optional($sale)->customer_id) == $customer->id )
									selected
									@endif
									>
									{{ $customer->name }}
								</option>
								@empty
								<option value="">No customer Found</option>
								@endforelse
							</select>
							@if ($errors->has('customer_id'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('customer_id') }}</strong>
							</span>
							@endif
						</div>

						{{-- Product --}}
						{{-- <div class="form-group">
							<label for="product_id">Select Product</label>
							<select name="product_id" class="form-control{{ $errors->has('product_id') ? ' is-invalid' : '' }}" id="product_id">
								<option value="">Select Product</option>
								@forelse($products as $product)
								<option value="{{ $product->id }}" 
									@if( old('product_id', optional($sale)->product_id) == $product->id )
									selected
									@endif
									>
									{{ $product->name }}
								</option>
								@empty
								<option value="">No Product Found</option>
								@endforelse
							</select>
							@if ($errors->has('product_id'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('product_id') }}</strong>
							</span>
							@endif
						</div> --}}

						{{-- Rate --}}
						<div class="form-group">
							<label for="rate">
								Rate
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('rate') ? ' is-invalid' : '' }}" name="rate" id="rate" value="{{ old('rate', optional($sale)->rate) }}">

							@if ($errors->has('rate'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('rate') }}</strong>
							</span>
							@endif
						</div>

						{{-- Quantity --}}
						<div class="form-group">
							<label for="quantity">
								Quantity
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" name="quantity" id="quantity" value="{{ old('quantity', optional($sale)->quantity) }}">

							@if ($errors->has('quantity'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('quantity') }}</strong>
							</span>
							@endif
						</div>

						{{-- Amount --}}
						<div class="form-group">
							<label for="amount">
								Amount
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" id="amount" value="{{ old('amount', optional($sale)->amount) }}" readonly="readonly">

							@if ($errors->has('amount'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('amount') }}</strong>
							</span>
							@endif
						</div>

						{{-- vat --}}
						<div class="form-group">
							<label for="vat">
								Vat
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('vat') ? ' is-invalid' : '' }}" name="vat" id="vat" value="{{ old('vat', optional($sale)->vat) }}">

							@if ($errors->has('vat'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('vat') }}</strong>
							</span>
							@endif
						</div>

						{{-- Invoice --}}
						<div class="form-group">
							<label for="invoice">Invoice</label>
							<input type="text" class="form-control{{ $errors->has('invoice') ? ' is-invalid' : '' }}" name="invoice" id="invoice" value="{{ old('invoice', optional($sale)->invoice) }}">

							@if ($errors->has('invoice'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('invoice') }}</strong>
							</span>
							@endif
						</div>

						{{-- Invoice --}}
						<div class="form-group">
							<label for="unload_place">Unload Place</label>
							<input type="text" class="form-control{{ $errors->has('unload_place') ? ' is-invalid' : '' }}" name="unload_place" id="unload_place" value="{{ old('unload_place', optional($sale)->unload_place) }}">

							@if ($errors->has('unload_place'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('unload_place') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group">
							<label for="factory_name">Product Factory Name</label>
							<input type="text" class="form-control{{ $errors->has('factory_name') ? ' is-invalid' : '' }}" name="factory_name" id="factory_name" value="{{ old('factory_name', optional($sale)->factory_name) }}">

							@if ($errors->has('factory_name'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('factory_name') }}</strong>
							</span>
							@endif
						</div>

						{{-- Details --}}
						<div class="form-group">
							<label for="address">
								Details
							</label>

							<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($sale)->details) }}</textarea>

							@if( $errors->has('details'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('details') }}</strong>
							</span>
							@endif
						</div>

						{{-- Save --}}
						<div class="form-group row mb-0">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									{{ __('Save') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
@section('script')
<script>
	{{-- jquery datepicker --}}
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});

	//amount calculation
	$(document).ready(function(){
		var rate=$("#rate");
		var quantity=$("#quantity");
		rate.keyup(function(){
			var total=isNaN(parseFloat(rate.val()* quantity.val())) ? 0 :(rate.val()* quantity.val())
			$("#amount").val(total);
		});
		quantity.keyup(function(){
			var total=isNaN(parseInt(rate.val()* quantity.val())) ? 0 :(rate.val()* quantity.val())
			$("#amount").val(total);
		});
	});

	$('#customer_id').select2({
		placeholder: 'Select Customer',

		ajax: {
			url: '{!!URL::route('business.customer-autocomplete-search')!!}',
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {
					results: data
				};
			},
			cache: true
		},
		theme: "bootstrap"
	});

	// $('#product_id').select2({
	// 	placeholder: 'Select Product',

	// 	ajax: {
	// 		url: '{!!URL::route('business.product-autocomplete-search')!!}',
	// 		dataType: 'json',
	// 		delay: 250,
	// 		processResults: function (data) {
	// 			return {
	// 				results: data
	// 			};
	// 		},
	// 		cache: true
	// 	},
	// 	theme: "bootstrap"
	// });
</script>
@endsection