<!DOCTYPE html>
<html lang="en">
<head>
    <style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
</head>
<body>
	@role('super_admin')
    <table>
        <thead>
            <tr>
                <th colspan="9">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="9">{{ config('app.name', 'Laravel') }}</th>
            </tr>          
            <tr>
                <th colspan="9">Sale Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            <tr>
                <th colspan="9">Current Fiscal Year: {{ $fisc_year->title }}</th>
            </tr>
            <tr>
                <th colspan="9">Other Fiscal Year(s) Sale Amount: {{ $other_amount }}</th>
            </tr>
            <tr>
                <th colspan="9">Other Fiscal Year(s) Sale Quantity: {{ $other_quantity }}</th>
            </tr>
            <tr>
                <th colspan="9">Other Fiscal Year(s) Vat Amount: {{ $other_vat_amount }}</th>
            </tr>
            <tr>
                <th>SL</th>
                <th>Date</th>                                   
                <th>Company</th>                                
                <th>Customer</th>
                <th>Product</th>                            
                <th>Quantity</th>
                <th>Rate</th>
                <th>Vat</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sales as $sale)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $sale->date->format("d-m-Y") }}</td>
                <td>{{ $sale->company->name }}</td>
                <td>{{ $sale->customer->name }}</td>
                <td>{{ $sale->product->name }}</td>
                <td>{{ $sale->quantity }}</td>
                <td>{{ $sale->rate }}</td>
                <td>{{ $sale->vat_amount }}</td>
                <td>{{ $sale->amount - $sale->vat_amount }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5">Total</th>
                <th>{{ $quantity }}</th>
                <th></th>
                <th>{{ $vat_amount }}</th>
                <th>{{ $amount }}</th>
            </tr>
            <tr>
                <th colspan="9">report generated on: {{ $date }}</th>
            </tr>
        </tfoot>
    </table>
    @endrole
    @role('business_admin')
    <table>
        <thead>
            <tr>
                <th colspan="8">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="8">{{ Auth::user()->company->name }}</th>
            </tr>
            <tr>
                <th colspan="8">Sale Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            <tr>
                <th colspan="8">Current Fiscal Year: {{ $fisc_year->title }}</th>
            </tr>
            <tr>
                <th colspan="8">Other Fiscal Year(s) Sale Amount: {{ $other_amount }}</th>
            </tr>
            <tr>
                <th colspan="8">Other Fiscal Year(s) Sale Quantity: {{ $other_quantity }}</th>
            </tr>
            <tr>
                <th colspan="8">Other Fiscal Year(s) Vat Amount: {{ $other_vat_amount }}</th>
            </tr>
            <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Customer</th>
                <th>Product</th>                            
                <th>Quantity</th>
                <th>Rate</th>
                <th>Vat</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sales as $sale)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $sale->date->format("d-m-Y") }}</td>
                <td>{{ $sale->customer->name }}</td>
                <td>{{ $sale->product->name }}</td>
                <td>{{ $sale->quantity }}</td>
                <td>{{ $sale->rate }}</td>
                <td>{{ $sale->vat_amount }}</td>
                <td>{{ $sale->amount - $sale->vat_amount }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4">Total</th>
                <th>{{ $quantity }}</th>
                <th></th>
                <th>{{ $vat_amount }}</th>
                <th>{{ $amount }}</th>
            </tr>
            <tr>
                <th colspan="8">report generated on: {{ $date }}</th>
            </tr>
        </tfoot>
    </table>
    @endrole
</body>
</html>