@extends('layouts.master')
@section('title', 'Sale')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Add Sale</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/sale') }}">Sales</a></li>
						<li class="breadcrumb-item active">Create</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<form method="POST" action="{{ route('business.sale.store') }}">
						@csrf
						{{-- Date --}}
						<div class="form-group">
							<label for="date">
								Date
							</label>

							<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ old('date') }}">

							@if ($errors->has('date'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('date') }}</strong>
							</span>
							@endif
						</div>

						{{-- Fiscal Year --}}
						<div class="form-group">
							<label for="fiscal_year">Fiscal Year</label>
							<select name="fiscal_year" class="form-control{{ $errors->has('fiscal_year') ? ' is-invalid' : '' }}" id="fiscal_year">
								<option value="">Select</option>
								@forelse($fisc_years as $year)
									<option value="{{ $year->id }}" 
										@if( old('fiscal_year') == $year->id )
											selected
										@endif
										>
										{{ $year->title }}
									</option>
								@empty
									<option value="">No Fiscal Year Found</option>
								@endforelse
							</select>
							@if ($errors->has('fiscal_year'))
								<span class="invalid-feedback">
									<strong>{{ $errors->first('fiscal_year') }}</strong>
								</span>
							@endif
						</div>

						{{-- customer --}}
						<div class="form-group">
							<label for="customer_selection">Customer</label>
							<select name="customer_selection" class="form-control{{ $errors->has('customer_selection') ? ' is-invalid' : '' }}" id="customer_selection">
								<option value="">Select</option>
								<option value="1" @if( old('customer_selection') == 1 ) selected @endif>Old Customer</option>
								<option value="2" @if( old('customer_selection') == 2 ) selected @endif>New Customer</option>
							</select>
							@if ($errors->has('customer_selection'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('customer_selection') }}</strong>
							</span>
							@endif
						</div>
						<div id="customer_info"></div>


						<div class="row">
							<div class="col-md-1">
								<button id="add_more" class="btn btn-info mt-4"><i class="fa fa-plus" title="Add More Product"></i></button>
							</div>
							<div class="col-md-11">
								<div id="more_product">
									<div class="row">
										<div class="col-md-5">
											{{-- Product --}}
											<div class="form-group">
												<label for="product_id">Select Product</label>
												<select name="product_id[]" class="form-control" id="product_id" required="required">
													<option value="">Select Product</option>
													@forelse($products as $product)
													<option value="{{ $product->id }}" >
														{{ $product->name }}
													</option>
													@empty
													<option value="">No Product Found</option>
													@endforelse
												</select>
											</div>
										</div>
										<div class="col-md-2">
											{{-- Rate --}}
											<div class="form-group">
												<label for="rate">
													Rate
												</label>
												<input type="number" min="0" step="any" class="form-control" name="rate[]" id="rate" required="required">
											</div>
										</div>

										<div class="col-md-3">
											{{-- Quantity --}}
											<div class="form-group">
												<label for="quantity">
													Quantity
												</label>

												<input type="number" min="0" step="any" class="form-control" name="quantity[]" id="quantity" required="required">
											</div>
										</div>

										<div class="col-md-2">
											{{-- Quantity --}}
											<div class="form-group">
												<label for="vat">
													Vat
												</label>

												<input type="number" min="0" step="any" class="form-control" name="vat[]" id="vat" required="required">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						{{-- invoice --}}
						<div class="form-group">
							<label for="invoice">
								Invoice
							</label>

							<input type="text" class="form-control" name="invoice" id="invoice" value="{{ old('invoice') }}">

							@if ($errors->has('invoice'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('invoice') }}</strong>
							</span>
							@endif
						</div>

						{{-- Invoice --}}
						<div class="form-group">
							<label for="unload_place">Unloading Place</label>
							<input type="text" class="form-control{{ $errors->has('unload_place') ? ' is-invalid' : '' }}" name="unload_place" id="unload_place" value="{{ old('unload_place') }}">

							@if ($errors->has('unload_place'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('unload_place') }}</strong>
							</span>
							@endif
						</div>

						<div class="form-group">
							<label for="factory_name">Product Factory Name</label>
							<input type="text" class="form-control{{ $errors->has('factory_name') ? ' is-invalid' : '' }}" name="factory_name" id="factory_name" value="{{ old('factory_name') }}">

							@if ($errors->has('factory_name'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('factory_name') }}</strong>
							</span>
							@endif
						</div>

						{{-- Payment Type --}}

						{{-- <div class="form-group">
							<label for="payment">Payment</label>
							<select name="payment" class="form-control{{ $errors->has('payment') ? ' is-invalid' : '' }}" id="payment">
								<option value="">Select Payment</option>
								<option value="1" {{old('payment') =='1' ? 'selected':''}}>Cash</option>
								<option value="2" {{old('payment') =='2' ? 'selected':''}}>Due</option>
								<option value="3" {{old('payment') =='3' ? 'selected':''}}>Advance</option>
							</select>
							@if ($errors->has('payment'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('payment') }}</strong>
							</span>
							@endif
						</div>
						<div id="amount_info"></div> --}}

						{{-- Details --}}
						<div class="form-group">
							<label for="details">
								Remarks
							</label>

							<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details') }}</textarea>

							@if( $errors->has('details'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('details') }}</strong>
							</span>
							@endif
						</div>

						{{-- Save --}}
						<div class="form-group row mb-0">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									{{ __('Save') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
@section('script')
<script>
	{{-- jquery datepicker --}}
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});

	$('#product_id').select2({
		placeholder: 'Select Product',

		ajax: {
			url: '{!!URL::route('business.product-autocomplete-search')!!}',
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {
					results: data
				};
			},
			cache: true
		},
		theme: "bootstrap"
	});

	$('#customer_selection').on('change', function(){
		var customer_selection = $('#customer_selection').val();
		if (customer_selection == 1) {
			$('#customer_info').html('');
			customer_info = '';
			customer_info += '<div class="form-group"><label for="customer_id">Select Customer</label><select name="customer_id" class="form-control{{ $errors->has('customer_id') ? ' is-invalid' : '' }}" id="customer_id"> <option value="">Select customer</option> @forelse($customers as $customer) <option value="{{ $customer->id }}" @if( old('customer_id') == $customer->id ) selected @endif> {{ $customer->name }} </option> @empty <option value="">No Customer Found</option> @endforelse </select> @if ($errors->has('customer_id')) <span class="invalid-feedback"> <strong>{{ $errors->first('customer_id') }}</strong> </span> @endif </div>';
			$('#customer_info').html(customer_info);
			$('#customer_id').select2({
				placeholder: 'Select customer',

				ajax: {
					url: '{!!URL::route('business.customer-autocomplete-search')!!}',
					dataType: 'json',
					delay: 250,
					processResults: function (data) {
						return {
							results: data
						};
					},
					cache: true
				},
				theme: "bootstrap"
			});
		}

		else if (customer_selection == 2) {
			$('#customer_info').html('');
			customer_info = '';
			customer_info += '{{-- Name --}}<div class="form-group"><label for="name">Name</label><input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name') }}" autofocus required="required">@if ($errors->has('name'))<span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>@endif</div>{{-- Mobile --}}<div class="form-group"><label for="mobile">Mobile</label><input type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" id="mobile" value="{{ old('mobile') }}" required="required">@if ($errors->has('mobile'))<span class="invalid-feedback"><strong>{{ $errors->first('mobile') }}</strong></span>@endif</div>{{-- Address --}}<div class="form-group"><label for="address">Address</label><textarea name="address" class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" cols="30" rows="5">{{ old('address') }}</textarea>@if( $errors->has('address'))<span class="invalid-feedback"><strong>{{ $errors->first('address') }}</strong></span>@endif</div>';
			$('#customer_info').html(customer_info);
		}
		else{
			$('#customer_info').html('');
		}
	});

	$(document).ready(function() {
		var max_fields      = 150;
		var wrapper         = $("#more_product");
		var add_button      = $("#add_more");

		var x = 1;
		$(add_button).click(function(e){
			e.preventDefault();
			if(x < max_fields){
				x++;
				$(wrapper).append('<div class="row"><div class="col-md-5">{{-- Product --}}<div class="form-group"><label for="product_id">Select Product</label><select name="product_id[]" class="form-control product_id" id="product_id" required="required"><option value="">Select Product</option>@forelse($products as $product)<option value="{{ $product->id }}" >{{ $product->name }}</option>@empty<option value="">No Product Found</option>@endforelse </select></div></div><div class="col-md-2">{{-- Rate --}}<div class="form-group"><label for="rate">Rate</label><input type="number" min="0" step="any" class="form-control" name="rate[]" id="rate" required="required"></div></div><div class="col-md-2">{{-- Quantity --}}<div class="form-group"><label for="quantity">Quantity</label><input type="number" min="0" step="any" class="form-control" name="quantity[]" id="quantity" required="required"></div></div><div class="col-md-2">{{-- vat --}}<div class="form-group"><label for="vat">Vat</label><input type="number" min="0" step="any" class="form-control" name="vat[]" id="vat" required="required"></div></div><div class="col-sm-1"><a href="#" class="remove_field"><button style="margin-top: 30px;" class="btn btn-info"><i class="fa fa-minus" title="Remove Item"></i></button></a></div></div>');

				$('.product_id').select2({
					placeholder: 'Select Product',

					ajax: {
						url: '{!!URL::route('business.product-autocomplete-search')!!}',
						dataType: 'json',
						delay: 250,
						processResults: function (data) {
							return {
								results: data
							};
						},
						cache: true
					},
					theme: "bootstrap"
				});
			}
		});

		$(wrapper).on("click",".remove_field", function(e){
			e.preventDefault(); 
			$(this).parent().parent('div').remove(); 
			x--;
		})
	});

	// $('#payment').on('change', function(){
	// 	var payment = $('#payment').val();
	// 	if (payment == 3) {
	// 		$('#amount_info').html('');
	// 		amount_info = '';
	// 		amount_info += '<div class="form-group"><label for="paid_amount">Paid Amount</label><input type="number" step="any" min="0" class="form-control" name="paid_amount" id="paid_amount" required="required" value="{{ old('paid_amount') }}" ></div>';
	// 		$('#amount_info').html(amount_info);
	// 	}

	// 	else{
	// 		$('#amount_info').html('');
	// 	}
	// });
</script>
@endsection