<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }} | Supplier Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link href="{{ asset('css/adminlte.min.css') }}" rel="stylesheet">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-12">
          <p style="text-align: center;">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</p>
          <h2 class="page-header" style="text-align: center;"> 
            @if(Auth::user()->company)
            {{ Auth::user()->company->name }}
            @else
            {{ config('app.name', 'Laravel') }}
            @endif
          </h2>
          <small class="float-right">Date: {{ $date }}</small>
        </div>
        <!-- /.col -->
      </div>
      <br>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-8 invoice-col">
          <h4><strong>{{ $supplier->name }}'s Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</strong></h4>
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-12 table-responsive">
          <h4>Order Table</h4>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Date</th>
                <th>Product</th>              
                <th>Order Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
                <th>Purchased</th>
                <th>Remaining</th>
              </tr>
            </thead>
            <tbody>
                @php
			    $orderQuantity = 0;
				$orderAmount = 0;
				$orderPurchased = 0;
				$orderRemaining = 0;
			    @endphp
              @foreach($orders as $key => $value)
              <tr class='clickable-row' data-href='{{ route('business.purchase-order.show',$value->id) }}'>
                <td>{{ $value->date->format("d-m-Y") }}</td>
                <td>
                  {{ $value->product->name }}
                </td>
                <td>
                  {{ $value->quantity }}
                </td>
                <td>
                  {{ $value->rate }}
                </td>
                <td>
                  {{ $value->amount }}
                </td>
                <td>
                  {{ $value->quantity - $remainingQuantity[$key] }}
                </td>
                <td>{{ $remainingQuantity[$key] }}</td>
                @php
				$orderQuantity += $value->quantity;
				$orderAmount += $value->amount;
				$orderPurchased += ($value->quantity - $remainingQuantity[$key]);
				$orderRemaining += $remainingQuantity[$key];
				@endphp
              </tr>
              @endforeach
            </tbody>
            <tfoot>
				<tr>
					<th>Total</th>
					<th></th>
					<th>{{ $orderQuantity }}</th>
					<th></th>
					<th>{{ $orderAmount }}</th>
					<th>{{ $orderPurchased }}</th>
					<th>{{ $orderRemaining }}</th>
				</tr>
			</tfoot>
          </table>

          <h4>Purchase Table</h4>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Product</th>              
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
                <th>Delivered</th>
                <th>Remaining</th>
              </tr>
            </thead>
            <tbody>
                @php
				$purchaseDelivered = 0;
				$purchaseRemaining = 0;
			    @endphp
              @foreach($purchases as $purchase)
              <tr class='clickable-row' data-href='{{ route('business.purchase.show',$purchase->id) }}'>
                <td>{{ $loop->iteration }}</td>
                <td><span class="hidden_date">{{ $purchase->date }}</span>{{ $purchase->date->format("d-m-Y") }}</td>
                <td>
                  {{ $purchase->product->name }}
                </td>
                <td>
                  {{ $purchase->quantity }}
                </td>
                <td>{{ $purchase->rate }}</td>
                <td>{{ $purchase->amount }}</td>
                @php
                $delivered = \App\Models\BusinessModels\BusinessPurchaseDelivery::where('purchase_id', $purchase->id)->sum('quantity');
                $purchaseDelivered += $delivered;
				$purchaseRemaining += ($purchase->quantity - $delivered);
                @endphp
                <td>{{ $delivered }}</td>
                <td>{{ $purchase->quantity - $delivered }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th>{{ $purchaseQuantity }}</th>
                <th></th>
                <th>{{ $purchase_amount }}</th>
                <th>{{ $purchaseDelivered }}</th>
				<th>{{ $purchaseRemaining }}</th>
              </tr>
            </tfoot>
          </table>

          <h6>Delivery Table</h6>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Product</th>              
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
                <th>Purchased</th>
                <th>Delivered</th>
                <th>Remaining</th>
              </tr>
            </thead>
            <tbody>
                @php
				$totalPurchased = 0;
				$totalDelivered = 0;
				$totalRemaining = 0;
			    @endphp
              @foreach($deliveries as $delivery)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td><span class="hidden_date">{{ $delivery->date }}</span>{{ $delivery->date->format("d-m-Y") }}</td>
                <td>
                  {{ $delivery->product->name }}
                </td>
                <td>
                  {{ $delivery->quantity }}
                </td>
                <td>{{ $delivery->rate }}</td>
                <td>{{ $delivery->amount }}</td>
                @php
                $delivered = \App\Models\BusinessModels\BusinessPurchaseDelivery::where('purchase_id',$delivery->purchase_id)->where('id','<',$delivery->id)->sum('quantity');
                $purchased = \App\Models\BusinessModels\BusinessPurchase::find($delivery->purchase_id);
                $totalPurchased += $purchased->quantity;
				$totalDelivered += ($delivery->quantity + $delivered);
				$totalRemaining += ($purchased->quantity - ($delivered + $delivery->quantity));
                @endphp
                <td>{{ $purchased->quantity }}</td>
                <td>{{ $delivery->quantity + $delivered }}</td>
                <td>{{ $purchased->quantity - ($delivered + $delivery->quantity) }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th>{{ $delivered_quantity }}</th>
                <th></th>
                <th>{{ $delivered_amount }}</th>
                <th>{{ $totalPurchased }}</th>
				<th>{{ $totalDelivered }}</th>
				<th>{{ $totalRemaining }}</th>
              </tr>
            </tfoot>
          </table>
          
          <h4>Payment Table</h4>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Receiver</th>
              </tr>
            </thead>
            <tbody>
              @foreach($transactions as $transaction)
              @if($transaction->purpose == 2)
              <tr>
                <td>{{ $transaction->date->format("d-m-Y") }}</td>
                <td>{{ $transaction->amount }}</td>
                <td>{{ $transaction->receiver }}</td>
              </tr>
              @endif
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>Total Paid</th>
                <th>{{ $paid_amount }}</th>
                <th></th>
              </tr>
            </tfoot>
          </table>

          <h4>Payment Return Table</h4>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Receiver</th>
              </tr>
            </thead>
            <tbody>
              @foreach($transactions as $transaction)
              @if($transaction->purpose == 1)
              <tr>
                <td>{{ $transaction->date->format("d-m-Y") }}</td>
                <td>{{ $transaction->amount }}</td>
                <td>{{ $transaction->receiver }}</td>
              </tr>
              @endif
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>Total Paid</th>
                <th>{{ $returned_amount}}</th>
                <th></th>
              </tr>
            </tfoot>
          </table>
          
          <h4>Transaction Table</h4>
          <table class="table table-striped">
            <tbody>
              <tr>
                <th>Purchase Amount</th>
                <th></th>
                <th>{{ $purchase_amount }}</th>
                <th></th>
              </tr>
              <tr>
                <th>Paid Amount</th>
                <th></th>
                <th>{{ $paid_amount }}</th>
                <th></th>
              </tr>
              <tr>
                <th>Returned Amount</th>
                <th></th>
                <th>{{ $returned_amount }}</th>
                <th></th>
              </tr>
              <tr>
                <th>Balance</th>
                <th></th>
                <th>{{ $balance }}</th>
                <th></th>
              </tr>
              <tr>
                <th>Previous Balance</th>
                <th></th>
                <th>{{ $prev_balance }}</th>
                <th></th>
              </tr>
              <tr>
                <th>Total</th>
                <th></th>
                <th>{{ $balance + $prev_balance }}</th>
                <th></th>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- ./wrapper -->
</body>
</html>
