@extends('layouts.master')
@section('title', 'Supplier')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-9">
					<h1 class="m-0 text-dark">{{ $supplier->name }}'s Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</h1>
				</div><!-- /.col -->
				<div class="col-sm-3">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/supplier') }}">Suppliers</a></li>
						<li class="breadcrumb-item active">Report</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<form method="POST" action="{{url('business/supplier/report/print')}}" class="d-inline">
						@csrf
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="id" id="id" class="form-control" value="{{ $id }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i>Print</button>
					</form>

					<form method="POST" action="{{url('business/supplier/report/pdf')}}" class="d-inline">
						@csrf
						<input type="hidden" name="id" id="id" class="form-control" value="{{ $id }}">
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</button>
					</form>

					<form method="POST" action="{{url('business/supplier/report/excel')}}" class="d-inline">
						@csrf
						<input type="hidden" name="id" id="id" class="form-control" value="{{ $id }}">
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel</button>
					</form>
				</div>
				<div class="card-body">
                    <div class="table-responsive">
                        <h6>Order Table</h6>
                        <table class="table table-bordered" id="orderTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>Date</th>
									<th>Product</th>							
									<th>Order Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
									<th>Purchased</th>
									<th>Remaining</th>
								</tr>
							</thead>
							<tbody>
							    @php
							    $orderQuantity = 0;
								$orderAmount = 0;
								$orderPurchased = 0;
								$orderRemaining = 0;
							    @endphp
								@foreach($orders as $key => $value)
								<tr class='clickable-row' data-href='{{ route('business.purchase-order.show',$value->id) }}'>
									<td>{{ $value->date->format("d-m-Y") }}</td>
									<td>
										{{ $value->product->name }}
									</td>
									<td>
										{{ $value->quantity }}
									</td>
									<td>
										{{ $value->rate }}
									</td>
									<td>
										{{ $value->amount }}
									</td>
									<td>
										{{ $value->quantity - $remainingQuantity[$key] }}
									</td>
									<td>{{ $remainingQuantity[$key] }}</td>
									@php
									$orderQuantity += $value->quantity;
									$orderAmount += $value->amount;
									$orderPurchased += ($value->quantity - $remainingQuantity[$key]);
									$orderRemaining += $remainingQuantity[$key];
									@endphp
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total</th>
									<th></th>
									<th>{{ $orderQuantity }}</th>
									<th></th>
									<th>{{ $orderAmount }}</th>
									<th>{{ $orderPurchased }}</th>
									<th>{{ $orderRemaining }}</th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>

                        <h6>Purchase Table</h6>
                        <table class="table table-bordered" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>SL</th>
									<th>Date</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
									<th>Delivered</th>
									<th>Remaining</th>
								</tr>
							</thead>
							<tbody>
							    @php
								$purchaseDelivered = 0;
								$purchaseRemaining = 0;
							    @endphp
								@foreach($purchases as $purchase)
								<tr class='clickable-row' data-href='{{ route('business.purchase.show',$purchase->id) }}'>
									<td>{{ $loop->iteration }}</td>
									<td><span class="hidden_date">{{ $purchase->date }}</span>{{ $purchase->date->format("d-m-Y") }}</td>
									<td>
										{{ $purchase->product->name }}
									</td>
									<td>
										{{ $purchase->quantity }}
									</td>
									<td>{{ $purchase->rate }}</td>
									<td>{{ $purchase->amount }}</td>
									@php
									$delivered = \App\Models\BusinessModels\BusinessPurchaseDelivery::where('purchase_id', $purchase->id)->sum('quantity');
									$purchaseDelivered += $delivered;
									$purchaseRemaining += ($purchase->quantity - $delivered);
									@endphp
									<td>{{ $delivered }}</td>
									<td>{{ $purchase->quantity - $delivered }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total</th>
									<th></th>
									<th></th>
									<th>{{ $purchaseQuantity }}</th>
									<th></th>
									<th>{{ $purchase_amount }}</th>
									<th>{{ $purchaseDelivered }}</th>
									<th>{{ $purchaseRemaining }}</th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>

                        <h6>Delivery Table</h6>
                        <table class="table table-bordered" id="deliveryTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>SL</th>
									<th>Date</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
									<th>Purchased</th>
									<th>Delivered</th>
									<th>Remaining</th>
								</tr>
							</thead>
							<tbody>
							    @php
								$totalPurchased = 0;
								$totalDelivered = 0;
								$totalRemaining = 0;
							    @endphp
								@foreach($deliveries as $delivery)
								<tr class='clickable-row' data-href='{{ url('business/purchase/delivery/view',$delivery->id) }}'>
									<td>{{ $loop->iteration }}</td>
									<td><span class="hidden_date">{{ $delivery->date }}</span>{{ $delivery->date->format("d-m-Y") }}</td>
									<td>
										{{ $delivery->product->name }}
									</td>
									<td>
										{{ $delivery->quantity }}
									</td>
									<td>{{ $delivery->rate }}</td>
									<td>{{ $delivery->amount }}</td>
									@php
									$delivered = \App\Models\BusinessModels\BusinessPurchaseDelivery::where('purchase_id',$delivery->purchase_id)->where('id','<',$delivery->id)->sum('quantity');
                					$purchased = \App\Models\BusinessModels\BusinessPurchase::find($delivery->purchase_id);
                					$totalPurchased += $purchased->quantity;
                					$totalDelivered += ($delivery->quantity + $delivered);
                					$totalRemaining += ($purchased->quantity - ($delivered + $delivery->quantity));
									@endphp
									<td>{{ $purchased->quantity }}</td>
									<td>{{ $delivery->quantity + $delivered }}</td>
									<td>{{ $purchased->quantity - ($delivered + $delivery->quantity) }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total</th>
									<th></th>
									<th></th>
									<th>{{ $delivered_quantity }}</th>
									<th></th>
									<th>{{ $delivered_amount }}</th>
									<th>{{ $totalPurchased }}</th>
									<th>{{ $totalDelivered }}</th>
									<th>{{ $totalRemaining }}</th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>

                        <h6>Payment Table</h6>
                        <table class="table table-bordered" id="paymentTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>Date</th>
									<th>Amount</th>
									<th>Receiver</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($transactions as $transaction)
								@if($transaction->purpose == 2)
								<tr class='clickable-row' data-href='{{ route('business.purchase-payment.show',$transaction->id) }}'>
									<td><span class="hidden_date">{{ $transaction->date }}</span>{{ $transaction->date->format("d-m-Y") }}</td>
									<td>{{ $transaction->amount }}</td>
									<td>{{ $transaction->receiver }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('business.purchase-payment.edit', $transaction->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<form action="{{ route('business.purchase-payment.destroy', $transaction->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
									</td>
								</tr>
								@endif
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total Paid</th>
									<th>{{ $paid_amount }}</th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>

                        <h6>Payment Return Table</h6>
                        <table class="table table-bordered" id="returnTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>Date</th>
									<th>Amount</th>
									<th>Receiver</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($transactions as $transaction)
								@if($transaction->purpose == 1)
								<tr class='clickable-row' data-href='{{ route('business.purchase-payment-return.show',$transaction->id) }}'>
									<td><span class="hidden_date">{{ $transaction->date }}</span>{{ $transaction->date->format("d-m-Y") }}</td>
									<td>{{ $transaction->amount }}</td>
									<td>{{ $transaction->receiver }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('business.purchase-payment-return.edit', $transaction->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<form action="{{ route('business.purchase-payment-return.destroy', $transaction->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
									</td>
								</tr>
								@endif
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total Returned</th>
									<th>{{ $returned_amount }}</th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>
						<h4>Transaction Table</h4>
						<table class="table table-bordered" width="100%" cellspacing="0">
							<tbody>
								<tr>
									<th>Purchase Amount</th>
									<th></th>
									<th>{{ $purchase_amount }}</th>
									<th></th>
								</tr>
								<tr>
									<th>Paid Amount</th>
									<th></th>
									<th>{{ $paid_amount }}</th>
									<th></th>
								</tr>
								<tr>
									<th>Returned Amount</th>
									<th></th>
									<th>{{ $returned_amount }}</th>
									<th></th>
								</tr>
								<tr>
									<th>Balance</th>
									<th></th>
									<th>{{ $balance }}</th>
									<th></th>
								</tr>
								<tr>
									<th>Previous Balance</th>
									<th></th>
									<th>{{ $prev_balance }}</th>
									<th></th>
								</tr>
								<tr>
									<th>Total</th>
									<th></th>
									<th>{{ $balance + $prev_balance }}</th>
									<th></th>
								</tr>
							</tbody>
						</table>
                    </div>
                </div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	jQuery(document).ready(function($) {
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
	});
	$(document).ready(function () {
		$('#purchaseTable').DataTable();
		$('#paymentTable').DataTable();
		$('#returnTable').DataTable();
		$('#orderTable').DataTable();
	});
</script>
@endsection