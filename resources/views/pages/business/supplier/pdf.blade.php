<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PDF</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<style type="text/css">
		td, th {
			font-size: 8px;
		}
	</style>
</head>
<body>
	<div class="card">
		<div class="card-header">
			<h2 style="text-align: center;"> 
				@if(Auth::user()->company)
				{{ Auth::user()->company->name }}
				@else
				{{ config('app.name', 'Laravel') }}
				@endif
			</h2>
			<h2 style="text-align: center;">{{ $supplier->name }}'s Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</h2>
			<div class="card-body">
				<div class="table-responsive">
					<h4>Order Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Product</th>							
								<th>Order Quantity</th>
								<th>Rate</th>
								<th>Amount</th>
								<th>Purchased</th>
								<th>Remaining</th>
							</tr>
						</thead>
						<tbody>
							@foreach($orders as $key => $value)
							<tr>
								<td>{{ $value->date->format("d-m-Y") }}</td>
								<td>
									{{ $value->product->name }}
								</td>
								<td>
									{{ $value->quantity }}
								</td>
								<td>
									{{ $value->rate }}
								</td>
								<td>
									{{ $value->amount }}
								</td>
								<td>
									{{ $value->quantity - $remainingQuantity[$key] }}
								</td>
								<td>{{ $remainingQuantity[$key] }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					
					<h4>Purchase Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>SL</th>
								<th>Date</th>
								<th>Product</th>							
								<th>Quantity</th>
								<th>Rate</th>
								<th>Amount</th>
								<th>Delivered</th>
								<th>Remaining</th>
							</tr>
						</thead>
						<tbody>
							@foreach($purchases as $purchase)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $purchase->date->format("d-m-Y") }}</td>
								<td>
									{{ $purchase->product->name }}
								</td>
								<td>
									{{ $purchase->quantity }}
								</td>
								<td>{{ $purchase->rate }}</td>
								<td>{{ $purchase->amount }}</td>
								@php
								$delivered = \App\Models\BusinessModels\BusinessPurchaseDelivery::where('purchase_id', $purchase->id)->sum('quantity');
								@endphp
								<td>{{ $delivered }}</td>
								<td>{{ $purchase->quantity - $delivered }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total</th>
								<th></th>
								<th></th>
								<th>{{ $purchaseQuantity }}</th>
								<th></th>
								<th>{{ $purchase_amount }}</th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>

					<h4>Delivery Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>SL</th>
								<th>Date</th>
								<th>Product</th>							
								<th>Quantity</th>
								<th>Rate</th>
								<th>Amount</th>
								<th>Purchased</th>
								<th>Delivered</th>
								<th>Remaining</th>
							</tr>
						</thead>
						<tbody>
							@foreach($deliveries as $delivery)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $delivery->date->format("d-m-Y") }}</td>
								<td>
									{{ $delivery->product->name }}
								</td>
								<td>
									{{ $delivery->quantity }}
								</td>
								<td>{{ $delivery->rate }}</td>
								<td>{{ $delivery->amount }}</td>
								@php
								$delivered = \App\Models\BusinessModels\BusinessPurchaseDelivery::where('purchase_id',$delivery->purchase_id)->where('id','<',$delivery->id)->sum('quantity');
								$purchased = \App\Models\BusinessModels\BusinessPurchase::find($delivery->purchase_id);
								@endphp
								<td>{{ $purchased->quantity }}</td>
								<td>{{ $delivery->quantity + $delivered }}</td>
								<td>{{ $purchased->quantity - ($delivered + $delivery->quantity) }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total</th>
								<th></th>
								<th></th>
								<th>{{ $delivered_quantity }}</th>
								<th></th>
								<th>{{ $delivered_amount }}</th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>

					<h4>Payment Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Amount</th>
								<th>Receiver</th>
							</tr>
						</thead>
						<tbody>
							@foreach($transactions as $transaction)
							@if($transaction->purpose == 2)
							<tr>
								<td>{{ $transaction->date->format("d-m-Y") }}</td>
								<td>{{ $transaction->amount }}</td>
								<td>{{ $transaction->receiver }}</td>
							</tr>
							@endif
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total Paid</th>
								<th>{{ $paid_amount }}</th>
								<th></th>
							</tr>
						</tfoot>
					</table>

					<h4>Payment Return Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Amount</th>
								<th>Receiver</th>
							</tr>
						</thead>
						<tbody>
							@foreach($transactions as $transaction)
							@if($transaction->purpose == 1)
							<tr>
								<td>{{ $transaction->date->format("d-m-Y") }}</td>
								<td>{{ $transaction->amount }}</td>
								<td>{{ $transaction->receiver }}</td>
							</tr>
							@endif
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total Returned</th>
								<th>{{ $returned_amount }}</th>
								<th></th>
							</tr>
						</tfoot>
					</table>

					<h4>Transaction Table</h4>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>Purchase Amount</th>
								<th></th>
								<th>{{ $purchase_amount }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Paid Amount</th>
								<th></th>
								<th>{{ $paid_amount }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Returned Amount</th>
								<th></th>
								<th>{{ $returned_amount }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Balance</th>
								<th></th>
								<th>{{ $balance }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Previous Balance</th>
								<th></th>
								<th>{{ $prev_balance }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Total</th>
								<th></th>
								<th>{{ $balance + $prev_balance }}</th>
								<th></th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>