@extends('layouts.master')
@section('title', 'Supplier')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-9">
					<h1 class="m-0 text-dark">Accounts Payable</h1>
				</div><!-- /.col -->
				<div class="col-sm-3">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/supplier') }}">Suppliers</a></li>
						<li class="breadcrumb-item active">Payable</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
				</div>
				<div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>SL</th>
									<th>Supplier</th>
									<th>Purchase Quantity</th>
									<th>Purchase Amount</th>								
									<th>Paid Amount</th>
									<th>Returned Amount</th>
									<th>Balance</th>
								</tr>
							</thead>
							<tbody>
								@foreach($suppliers as $key => $value)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $value->name }}</td>
									<td>
										{{ $pQuantity[$key] }}
									</td>
									<td>
										{{ $pAmount[$key] }}
									</td>
									<td>{{ $pPaid[$key] }}</td>
									<td>{{ $pReturned[$key] }}</td>
									<td>{{ $pBalance[$key] }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total</th>
									<th></th>
									<th>{{ array_sum($pQuantity) }}</th>
									<th>{{ array_sum($pAmount) }}</th>
									<th>{{ array_sum($pPaid) }}</th>
									<th>{{ array_sum($pReturned) }}</th>
									<th>{{ array_sum($pBalance) }}</th>
								</tr>
							</tfoot>
                        </table>
                    </div>
                </div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	$(document).ready(function () {
		$('#purchaseTable').DataTable();
	});
</script>
@endsection