@extends('layouts.master')
@section('title', 'Fiscal Year')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Fiscal Years</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Fiscal Year</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					{{-- <a href="{{ url('bank-account/print') }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Print" target="_blank">
						<i class="fa fa-print" aria-hidden="true" title="Print"></i> Print
					</a>

					<a href="{{ url('bank-account/pdf') }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="PDF" target="_blank">
						<i class="fa fa-file-pdf-o" aria-hidden="true" title="PDF"></i> PDF
					</a>

					<a href="{{ url('bank-account/excel') }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Excel" target="_blank">
						<i class="fa fa-file-excel-o" aria-hidden="true" title="Excel"></i> Excel
					</a> --}}
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Sl</th>
									<th>Year</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@forelse($years as $year)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $year->title }}</td>
									<td id="confirmed{{$year->id}}">{!!$year->status == 1 ? "<span class='badge badge-primary'>Active</span>" : "<span class='badge badge-warning'>Inactive</span>"!!}</td>	
									<td>
										<div class="btn-group">
											<a href="{{ route('business.fiscal-year.edit', $year->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>

											@if($year->status == 0)
											<a href="{{ url('business/fiscal-year/activate', $year->id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Activate">
												Activate
											</a>
											@endif
										</div>
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

@endsection

@section('script')
<script>
	//server side data table
	$(document).ready(function () {
		$('#dataTable').DataTable();
	});

	// $('#dataTable').on('click', '.btn-active[data-remote]', function (e) { 
 //        e.preventDefault();
 //        $.ajaxSetup({
 //            headers: {
 //                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
 //            }
 //        });
 //        var url = $(this).data('remote');
 //        // console.log(url);
 //        // confirm then
 //        if (confirm('are you sure you want to activate this year?')) {
 //            $.ajax({
 //                url: url,
 //                type: 'POST',
 //                dataType: 'json',
 //                data: {submit: true}
 //            }).always(function (data) {
 //                console.log(data);
 //                $('#dataTable').DataTable().draw();
 //            });
 //        }
 //    });
</script>
@endsection