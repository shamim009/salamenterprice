{{-- Name --}}
<div class="form-group">
	<label for="title">
		Year Title
	</label>

	<input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="title" value="{{ old('title', optional($year)->title) }}">

	@if ($errors->has('title'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('title') }}</strong>
	</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>