@extends('layouts.master')
@section('title', 'Purchase Transaction')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Update Purchase Payment Return</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/purchase') }}">Purchases</a></li>
						<li class="breadcrumb-item active">Payment</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<form method="POST" action="{{ route('business.purchase-payment-return.update', $purchaseTransaction->id) }}">
						@csrf
						@method('PUT')

						@include('pages.business.purchase-payment-return.form')
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection