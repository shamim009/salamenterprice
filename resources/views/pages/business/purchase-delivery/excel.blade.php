<!DOCTYPE html>
<html lang="en">
<head>
    <style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
</head>
<body>
	@role('super_admin')
    <table>
        <thead>
            <tr>
                <th colspan="8">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="8">{{ config('app.name', 'Laravel') }}</th>
            </tr>          
            <tr>
                <th colspan="8">Purchase Delivery Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            <tr>
                <th colspan="8">Current Fiscal Year: {{ $fisc_year->title }}</th>
            </tr>
            <tr>
                <th colspan="8">Other Fiscal Year(s) Purchase Delivery Amount: {{ $otherFiscAmount }}</th>
            </tr>
            <tr>
                <th colspan="8">Other Fiscal Year(s) Purchase Delivery Quantity: {{ $otherFiscQuantity }}</th>
            </tr>
            <tr>
                <th>SL</th>
                <th>Date</th>                                   
                <th>Company</th>                                
                <th>Supplier</th>
                <th>Product</th>                            
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($purchases as $purchase)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $purchase->date->format("d-m-Y") }}</td>
                <td>{{ $purchase->company->name }}</td>
                <td>{{ $purchase->supplier->name }}</td>
                <td>{{ $purchase->product->name }}</td>
                <td>{{ $purchase->quantity }}</td>
                <td>{{ $purchase->rate }}</td>
                <td>{{ $purchase->amount }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5">Total</th>
                <th>{{ $quantity }}</th>
                <th></th>
                <th>{{ $amount }}</th>
            </tr>
            <tr>
                <th colspan="8">report generated on: {{ $date }}</th>
            </tr>
        </tfoot>
    </table>
    @endrole
    @role('business_admin')
    <table>
        <thead>
            <tr>
                <th colspan="7">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="7">{{ Auth::user()->company->name }}</th>
            </tr>
            <tr>
                <th colspan="7">Purchase Delivery Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            <tr>
                <th colspan="7">Current Fiscal Year: {{ $fisc_year->title }}</th>
            </tr>
            <tr>
                <th colspan="7">Other Fiscal Year(s) Purchase Delivery Amount: {{ $otherFiscAmount }}</th>
            </tr>
            <tr>
                <th colspan="7">Other Fiscal Year(s) Purchase Delivery Quantity: {{ $otherFiscQuantity }}</th>
            </tr>
            <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Supplier</th>
                <th>Product</th>                            
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($purchases as $purchase)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $purchase->date->format("d-m-Y") }}</td>
                <td>{{ $purchase->supplier->name }}</td>
                <td>{{ $purchase->product->name }}</td>
                <td>{{ $purchase->quantity }}</td>
                <td>{{ $purchase->rate }}</td>
                <td>{{ $purchase->amount }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4">Total</th>
                <th>{{ $quantity }}</th>
                <th></th>
                <th>{{ $amount }}</th>
            </tr>
            <tr>
                <th colspan="7">report generated on: {{ $date }}</th>
            </tr>
        </tfoot>
    </table>
    @endrole
</body>
</html>