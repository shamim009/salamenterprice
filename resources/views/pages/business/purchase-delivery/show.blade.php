@extends('layouts.master')
@section('title', 'Purchase Delivery')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">View Purchase Delivery</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/purchase') }}">Purchases</a></li>
						<li class="breadcrumb-item active">Delivery</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<tbody>
								<tr>
									<td>Date</td>
									<td>{{ $delivery->date->format("d-m-Y") }}</td>
								</tr>
								@role('super_admin')
								<tr>
									<td>Company</td>
									<td>{{ $delivery->company->name }}</td>
								</tr>
								@endrole
								<tr>
									<td>Supplier</td>
									<td>{{ $delivery->supplier->name }}</td>
								</tr>
								<tr>
									<td>Product</td>
									<td>{{ $delivery->product->name }}</td>
								</tr>
								<tr>
									<td>Rate</td>
									<td>{{ $delivery->rate }}</td>
								</tr>
								<tr>
									<td>Quantity</td>
									<td>
										{{ $delivery->quantity }}
									</td>
								</tr>
								<tr>
									<td>Amount</td>
									<td>{{ $delivery->amount }}</td>
								</tr>
								<tr>
									<td>Invoice</td>
									<td>
										{{ $delivery->invoice }}
									</td>
								</tr>
								<tr>
									<td>Remarks</td>
									<td>{{ $delivery->details }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection