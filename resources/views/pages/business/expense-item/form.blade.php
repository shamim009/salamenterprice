{{-- Name --}}
<div class="form-group">
	<label for="name">
		Name
	</label>

	<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name', optional($expenseItem)->name) }}">

	@if ($errors->has('name'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('name') }}</strong>
		</span>
	@endif
</div>

<div class="form-group">
	<label for="type">Fiscal Year</label>
	<select name="type" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" id="type">
		<option value="">Select</option>
		<option value="0" 
			@if( old('type', optional($expenseItem)->type) == 0 )
				selected
			@endif
			>
			Irregular
		</option>
		<option value="1" 
			@if( old('type', optional($expenseItem)->type) == 1 )
				selected
			@endif
			>
			Regular
		</option>
	</select>
	@if ($errors->has('type'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('type') }}</strong>
		</span>
	@endif
</div>

{{-- Name --}}
<div class="form-group">
	<label for="amount">
		Amount
	</label>

	<input type="text" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" id="amount" value="{{ old('amount', optional($expenseItem)->amount) }}">

	@if ($errors->has('amount'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('amount') }}</strong>
		</span>
	@endif
</div>

{{-- Remarks --}}
<div class="form-group">
	<label for="details">
		Remarks
	</label>

	<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($expenseItem)->details) }}</textarea>

	@if( $errors->has('details'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('details') }}</strong>
		</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>