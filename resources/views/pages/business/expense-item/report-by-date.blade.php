@extends('layouts.master')
@section('title', 'Expense')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-9">
                    <h1 class="m-0 text-dark">{{ $expenseItem->name }} Expense Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-3">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('business/expense-item') }}">Expense Items</a></li>
                        <li class="breadcrumb-item active">Report</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>Current Fiscal Year: {{ $fisc_year->title }}</p>
                            <p>Other Fiscal Year(s) Expense: {{ $otherExpense }}</p>
                            <p>Other Fiscal Year(s) Paid Amount: {{ $othertotalPaid }}</p>
                        </div>
                        <div class="col-sm-6">
                            <p>Total Expense: {{ $totalExpense }}</p>
                            <p>Total Paid: {{ $totalPaid }}</p>
                            <p>Balance: {{ $totalExpense - $totalPaid }}</p>
                        </div>
                    </div>
                    <form method="POST" action="{{url('business/expense-item/report/print')}}" class="d-inline">
                        @csrf
                        <input type="hidden" name="id" id="id" class="form-control" value="{{ $expenseItem->id }}">
                        <input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
                        <input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
                        <button type="submit" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i>Print</button>
                    </form>

                    <form method="POST" action="{{url('business/expense-item/report/pdf')}}" class="d-inline">
                        @csrf
                        <input type="hidden" name="id" id="id" class="form-control" value="{{ $expenseItem->id }}">
                        <input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
                        <input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
                        <button type="submit" class="btn btn-info"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</button>
                    </form>

                    <form method="POST" action="{{url('business/expense-item/report/excel')}}" class="d-inline">
                        @csrf
                        <input type="hidden" name="id" id="id" class="form-control" value="{{ $expenseItem->id }}">
                        <input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
                        <input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
                        <button type="submit" class="btn btn-info"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel</button>
                    </form>              
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h6>Expense Table</h6>
                        @role('super_admin')
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Company</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($expenses as $expense)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $expense->date->format("d-m-Y") }}</td>
                                    <td>{{ $expense->company->name }}</td>
                                    <td>{{ $expense->amount }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3" style="text-align:right">Total:</th>
                                    <th>{{ $totalExpense }}</th>
                                </tr>
                            </tfoot>
                        </table>
                        @endrole

                        @role('business_admin')
                        <table class="table table-bordered" id="adminDataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($expenses as $expense)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $expense->date->format("d-m-Y") }}</td>
                                    <td>{{ $expense->amount }}</td>
                                    <td>{{ $expense->details }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" style="text-align:right">Total:</th>
                                    <th>{{ $totalExpense }}</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        @endrole
                        <br>
                        <h6>Expense Payment Table</h6>
                        @role('super_admin')
                        <table class="table table-bordered" id="paymentDataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Company</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $payment->date->format("d-m-Y") }}</td>
                                    <td>{{ $payment->company->name }}</td>
                                    <td>{{ $payment->amount }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3" style="text-align:right">Total:</th>
                                    <th>{{ $totalPaid }}</th>
                                </tr>
                            </tfoot>
                        </table>
                        @endrole

                        @role('business_admin')
                        <table class="table table-bordered" id="adminPaymentDataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $payment->date->format("d-m-Y") }}</td>
                                    <td>{{ $payment->amount }}</td>
                                    <td>{{ $payment->details }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" style="text-align:right">Total:</th>
                                    <th>{{ $totalPaid }}</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        @endrole
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection