<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }} | Expense Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link href="{{ asset('css/adminlte.min.css') }}" rel="stylesheet">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-12">
          <p style="text-align: center;">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</p>
          <h2 class="page-header" style="text-align: center;"> 
            @if(Auth::user()->company)
            {{ Auth::user()->company->name }}
            @else
            {{ config('app.name', 'Laravel') }}
            @endif
          </h2>
          <small class="float-right">Date: {{ $date }}</small>
        </div>
        <!-- /.col -->
      </div>
      <br>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-12 invoice-col">
          <h4><strong>{{ $expenseItem->name }} Expense Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</strong></h4>
          <div class="row">
            <div class="col-sm-6">
                <p>Current Fiscal Year: {{ $fisc_year->title }}</p>
                <p>Other Fiscal Year(s) Expense: {{ $otherExpense }}</p>
                <p>Other Fiscal Year(s) Paid Amount: {{ $othertotalPaid }}</p>
            </div>
            <div class="col-sm-6">
                <p>Total Expense: {{ $totalExpense }}</p>
                <p>Total Paid: {{ $totalPaid }}</p>
                <p>Balance: {{ $totalExpense - $totalPaid }}</p>
            </div>
          </div>
        </div>
        <!-- /.col -->
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <h4>Expense Table</h4>
      <div class="row">
        <div class="col-12 table-responsive">
          @role('super_admin')
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>                 
                <th>Company</th>                
                <th>Remarks</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach($expenses as $expense)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $expense->date->format("d-m-Y") }}</td>
                <td>{{ $expense->company->name }}</td>
                <td>{{ $expense->details }}</td>
                <td>{{ $expense->amount }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="4">Total</th>
                <th>{{ $totalExpense }}</th>
              </tr>
            </tfoot>
          </table>
          @endrole
          @role('business_admin')
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Remarks</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach($expenses as $expense)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $expense->date->format("d-m-Y") }}</td>
                <td>{{ $expense->details }}</td>
                <td>{{ $expense->amount }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="3">Total</th>
                <th>{{ $totalExpense }}</th>
              </tr>
            </tfoot>
          </table>
          @endrole
        </div>
        <!-- /.col -->
      </div>

      <h4>Payment Table</h4>
      <div class="row">
        <div class="col-12 table-responsive">
          @role('super_admin')
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>                 
                <th>Company</th>                
                <th>Remarks</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach($payments as $payment)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $payment->date->format("d-m-Y") }}</td>
                <td>{{ $payment->company->name }}</td>
                <td>{{ $payment->details }}</td>
                <td>{{ $payment->amount }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="4">Total</th>
                <th>{{ $totalPaid }}</th>
              </tr>
            </tfoot>
          </table>
          @endrole
          @role('business_admin')
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Remarks</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach($payments as $payment)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $payment->date->format("d-m-Y") }}</td>
                <td>{{ $payment->details }}</td>
                <td>{{ $payment->amount }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="3">Total</th>
                <th>{{ $totalPaid }}</th>
              </tr>
            </tfoot>
          </table>
          @endrole
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- ./wrapper -->
</body>
</html>
