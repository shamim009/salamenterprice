@extends('layouts.master')
@section('title', 'Expense')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-9">
                    <h1 class="m-0 text-dark">{{ $expenseItem->name }} Expense Report</h1>
                </div><!-- /.col -->
                <div class="col-sm-3">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('business/expense-item') }}">Expense Items</a></li>
                        <li class="breadcrumb-item active">Report</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>Current Fiscal Year: {{ $fisc_year->title }}</p>
                            <p>Other Fiscal Year(s) Expense: {{ $otherExpense }}</p>
                            <p>Other Fiscal Year(s) Paid Amount: {{ $othertotalPaid }}</p>
                        </div>
                        <div class="col-sm-6">
                            <p>Total Expense: {{ $totalExpense }}</p>
                            <p>Total Paid: {{ $totalPaid }}</p>
                            <p>Balance: {{ $totalExpense - $totalPaid }}</p>
                        </div>
                    </div>
                    <form method="POST" action="{{url('business/expense-item/report')}}" class="form-inline">
                        @csrf
                        <input type="hidden" name="id" id="id" class="form-control" value="{{ $expenseItem->id }}">
                        <label for="inlineFormCustomSelect">Start Date </label>
                        <input type="text" name="start_date" id="start_date" class="form-control" placeholder="Start Date">
                        <label for="inlineFormCustomSelect">End Date </label>
                        <input type="text" name="end_date" id="end_date" class="form-control" placeholder="End Date">
                        <button type="submit" class="btn btn-info"><i class="fa fa-line-chart" aria-hidden="true"></i> Report</button>
                    </form>              
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h6>Expense Table</h6>
                        @role('super_admin')
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Company</th>
                                    <th>Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th colspan="3" style="text-align:right">Total:</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        @endrole

                        @role('business_admin')
                        <table class="table table-bordered" id="adminDataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Remarks</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                        @endrole
                        <br>
                        <h6>Expense Payment Table</h6>
                        @role('super_admin')
                        <table class="table table-bordered" id="paymentDataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Company</th>
                                    <th>Amount</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th colspan="3" style="text-align:right">Total:</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        @endrole

                        @role('business_admin')
                        <table class="table table-bordered" id="adminPaymentDataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Remarks</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                        @endrole
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('script')
<script>
	//server side data table
    $(document).ready(function () {
        $('#dataTable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('business/all-expenses',$id) }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{ csrf_token() }}"}
            },
            "columns": [
            { "data": "id" },
            { "data": "date" },
            { "data": "company_id" },
            { "data": "amount" },
            { "data": "actions" },
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = json.totalSum;
                
                // Total over this page
                pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 3 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                    );
            }	 
        });

        $('#adminDataTable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('business/admin/all-expenses',$id) }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{ csrf_token() }}"}
            },
            "columns": [
            { "data": "id" },
            { "data": "date" },
            { "data": "amount" },
            { "data": "details" },
            { "data": "actions" },
            ]  
        });

        $('#paymentDataTable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('business/all-expense-transactions',$id) }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{ csrf_token() }}"}
            },
            "columns": [
            { "data": "id" },
            { "data": "date" },
            { "data": "company_id" },
            { "data": "amount" },
            { "data": "actions" },
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = json.totalSum;
                
                // Total over this page
                pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 3 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                    );
            }    
        });

        $('#adminPaymentDataTable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('business/admin/all-expense-transactions',$id) }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{ csrf_token() }}"}
            },
            "columns": [
            { "data": "id" },
            { "data": "date" },
            { "data": "amount" },
            { "data": "details" },
            { "data": "actions" },
            ]  
        });
    });

    //delete a row using ajax
    $('#dataTable').on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_method: 'DELETE', submit: true}
            }).always(function (data) {
                $('#dataTable').DataTable().draw(false);
            });
        }
    });

    $('#adminDataTable').on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_method: 'DELETE', submit: true}
            }).always(function (data) {
                $('#adminDataTable').DataTable().draw(false);
            });
        }
    });

    $('#paymentDataTable').on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_method: 'DELETE', submit: true}
            }).always(function (data) {
                $('#adminDataTable').DataTable().draw(false);
            });
        }
    });

    $('#adminPaymentDataTable').on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_method: 'DELETE', submit: true}
            }).always(function (data) {
                $('#adminDataTable').DataTable().draw(false);
            });
        }
    });

    $( function() {
        $( "#start_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });

        $( "#end_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });
    });
</script>
@endsection