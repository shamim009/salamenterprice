@role('super_admin')
<div class="form-group">
	<label for="company_id">Select Company</label>
	<select name="company_id" class="form-control{{ $errors->has('company_id') ? ' is-invalid' : '' }}" id="company_id">
		<option value="">Select</option>
		@forelse($companies as $company) 
		<option value="{{ $company->id }}" @if( old('company_id', optional($remark)->company_id) == $company->id ) selected @endif> {{ $company->name }} 
		</option> 
		@empty 
		<option value="">No Company Found</option> 
		@endforelse
	</select> 
	@if ($errors->has('company_id'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('company_id') }}</strong> 
	</span> 
	@endif 
</div>
@endrole 

{{-- Remarks --}}
<div class="form-group">
	<label for="details">
		Remarks
	</label>

	<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($remark)->details) }}</textarea>

	@if( $errors->has('details'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('details') }}</strong>
	</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>