<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }} | Expense Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link href="{{ asset('css/adminlte.min.css') }}" rel="stylesheet">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-12">
          <p style="text-align: center;">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</p>
          <h2 class="page-header" style="text-align: center;"> 
            @if(Auth::user()->company)
            {{ Auth::user()->company->name }}
            @else
            {{ config('app.name', 'Laravel') }}
            @endif
          </h2>
          <small class="float-right">Date: {{ $date }}</small>
        </div>
        <!-- /.col -->
      </div>
      <br>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-8 invoice-col">
          <h4><strong>Expense Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</strong></h4>
          <h5>Current Fiscal Year: {{ $fisc_year->title }}</h5>
          <h5>Other Fiscal Year(s) Expense Amount: {{ $otherFiscSum }}</h5>
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-12 table-responsive">
          @role('super_admin')
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>                 
                <th>Company</th>                
                <th>Expense Type</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach($expenses as $expense)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $expense->date->format("d-m-Y") }}</td>
                <td>{{ $expense->company->name }}</td>
                <td>{{ $expense->expenseitem->name }}</td>
                <td>{{ $expense->amount }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="4">Total</th>
                <th>{{ $amount }}</th>
              </tr>
            </tfoot>
          </table>
          @endrole
          @role('business_admin')
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Expense Type</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach($expenses as $expense)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $expense->date->format("d-m-Y") }}</td>
                <td>{{ $expense->expenseitem->name }}</td>
                <td>{{ $expense->amount }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="3">Total</th>
                <th>{{ $amount }}</th>
              </tr>
            </tfoot>
          </table>
          @endrole
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- ./wrapper -->
</body>
</html>
