<!DOCTYPE html>
<html lang="en">
<head>
	<style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
</head>
<body>
	@role('super_admin')
	<table>
		<thead>
			<tr>
                <th colspan="5">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="5">{{ config('app.name', 'Laravel') }}</th>
            </tr>
			<tr>
                <th colspan="5">Expense Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            <tr>
                <th colspan="5">Current Fiscal Year: {{ $fisc_year->title }}</th>
            </tr>
            <tr>
                <th colspan="5">Other Fiscal Year(s) Expense Amount: {{ $otherFiscSum }}</th>
            </tr>
			<tr>
				<th>SL</th>
				<th>Date</th>									
				<th>Company</th>								
				<th>Expense Type</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody>
			@foreach($expenses as $expense)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $expense->date->format("d-m-Y") }}</td>
				<td>{{ $expense->company->name }}</td>
				<td>{{ $expense->expenseitem->name }}</td>
				<td>{{ $expense->amount }}</td>
			</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<th colspan="4">Total</th>
				<th>{{ $amount }}</th>
			</tr>
			<tr>
                <th colspan="5">report generated on: {{ $date }}</th>
            </tr>
		</tfoot>
	</table>
	@endrole
	@role('business_admin')
	<table>
		<thead>
			<tr>
                <th colspan="4">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="4">{{ config('app.name', 'Laravel') }}</th>
            </tr>
			<tr>
                <th colspan="4">Expense Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            <tr>
                <th colspan="4">Current Fiscal Year: {{ $fisc_year->title }}</th>
            </tr>
            <tr>
                <th colspan="4">Other Fiscal Year(s) Expense Amount: {{ $otherFiscSum }}</th>
            </tr>
			<tr>
				<th>SL</th>
				<th>Date</th>
				<th>Expense Type</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody>
			@foreach($expenses as $expense)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $expense->date->format("d-m-Y") }}</td>
				<td>{{ $expense->expenseitem->name }}</td>
				<td>{{ $expense->amount }}</td>
			</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<th colspan="3">Total</th>
				<th>{{ $amount }}</th>
			</tr>
			<tr>
                <th colspan="4">report generated on: {{ $date }}</th>
            </tr>
		</tfoot>
	</table>
	@endrole
</body>
</html>