@extends('layouts.master')
@section('title', 'Sale Payment')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Sale Payment Return Details</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('business/sale') }}">Sales</a></li>
						<li class="breadcrumb-item active">View</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<tbody>
								<tr>
									<td>Date</td>
									<td>{{ $transaction->date->format("d-m-Y") }}</td>
								</tr>

								@role('super_admin')
								<tr>
									<td>Company</td>
									<td>{{ $transaction->company->name }}</td>
								</tr>

								@endrole

								<tr>
									<td>Customer</td>
									<td>{{ $transaction->customer->name }}</td>
								</tr>

								<tr>
									<td>Amount</td>
									<td>{{ $transaction->amount }}</td>
								</tr>
								<tr>
									<td>Payment Mode</td>
									@if($transaction->payment_mode == 1)
									<td>{{ 'Hand Cash' }}</td>
									@elseif($transaction->payment_mode == 2)
									<td>{{ 'Regular Banking' }}</td>
									@else 
									<td>{{ 'Mobile Banking' }}</td>
									@endif
								</tr>
								@if($transaction->payment_mode == 2)
								<tr>
									<td>Account</td>
									<td>{{ $transaction->account->account_no }}</td>
								</tr>
								<tr>
									<td>Cheque Number</td>
									<td>{{ $transaction->cheque_number }}</td>
								</tr>
								@endif

								@if($transaction->payment_mode == 3)
								<tr>
									<td>Mobile Banking</td>
									<td>{{ $transaction->mobileBanking->name }}</td>
								</tr>
								<tr>
									<td>Phone Number</td>
									<td>
										{{ $transaction->phone_number }}
									</td>
								</tr>
								@endif
								<tr>
									<td>Receiver</td>
									<td>{{ $transaction->receiver }}</td>
								</tr>
								<tr>
									<td>Remarks</td>
									<td>{{ $transaction->details }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection