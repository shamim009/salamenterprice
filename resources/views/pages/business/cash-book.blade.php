@extends('layouts.master')
@section('title', 'Cash Book')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Cash Book</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Cash Book</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<div class="table-responsive">
						@role('super_admin')
						<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>									
									<th>Company</th>								
									<th>Purpose</th>
									<th>Details</th>							
									<th>Debit</th>
									<th>Credit</th>
									<th>Balance</th>
								</tr>
							</thead>
						</table>
						@endrole
						
						@role('business_admin')
						<table class="table table-bordered table-hover" id="adminDataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>										
									<th>Purpose</th>
									<th>Details</th>							
									<th>Debit</th>
									<th>Credit</th>
									<th>Amount</th>
								</tr>
							</thead>
						</table>
						@endrole
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	//server side data table
	$(document).ready(function () {
		$('#dataTable').DataTable({
			"order": [[ 1, "asc" ]],
			"processing": true,
			"serverSide": true,
			"ajax":{
				"url": "{{ url('business/full-cash-book') }}",
				"dataType": "json",
				"type": "POST",
				"data":{ _token: "{{ csrf_token() }}"}
			},
			"columns": [
			{ "data": "id" },
			{ "data": "date" },
			{ "data": "company_id" },
			{ "data": "purpose" },
			{ "data": "details" },
			{ "data": "debit" },
			{ "data": "credit" },
			{ "data": "balance" }
			],	 
		});


		$('#adminDataTable').DataTable({
			"order": [[ 1, "asc" ]],
			"processing": true,
			"serverSide": true,
			"ajax":{
				"url": "{{ url('business/admin/full-cash-book') }}",
				"dataType": "json",
				"type": "POST",
				"data":{ _token: "{{ csrf_token() }}"}
			},
			"columns": [
			{ "data": "id" },
			{ "data": "date" },
			{ "data": "purpose" },
			{ "data": "details" },
			{ "data": "debit" },
			{ "data": "credit" },
			{ "data": "balance" }
			],	 
		});
	});
</script>
@endsection
