@extends('layouts.master')
@section('title', 'Purchase')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-8">
					<h1 class="m-0 text-dark">Purchases from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</h1>
					<h5>Current Fiscal Year: {{ $fisc_year->title }}</h5>
                    <h5>Other Fiscal Year(s) Purchase Amount: {{ $otherFiscAmount }}</h5>
                    <h5>Other Fiscal Year(s) Purchase Quantity: {{ $otherFiscQuantity }}</h5>
				</div><!-- /.col -->
				<div class="col-sm-4">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Purchases</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<form method="POST" action="{{url('business/purchase/report/print')}}" class="d-inline">
						@csrf
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i>Print</button>
					</form>

					<form method="POST" action="{{url('business/purchase/report/pdf')}}" class="d-inline">
						@csrf
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</button>
					</form>

					<form method="POST" action="{{url('business/purchase/report/excel')}}" class="d-inline">
						@csrf
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel</button>
					</form>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						@role('super_admin')
						<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>									
									<th>Company</th>								
									<th>Supplier</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($purchases as $purchase)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $purchase->date->format("d-m-Y") }}</td>
									<td>{{ $purchase->company->name }}</td>
									<td>{{ $purchase->supplier->name }}</td>
									<td>{{ $purchase->product->name }}</td>
									<td>{{ $purchase->quantity }}</td>
									<td>{{ $purchase->rate }}</td>
									<td>{{ $purchase->amount }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="5">Total</th>
									<th>{{ $quantity }}</th>
									<th></th>
									<th>{{ $amount }}</th>
								</tr>
							</tfoot>
						</table>
						@endrole
						
						@role('business_admin')
						<table class="table table-bordered table-hover" id="adminDataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>
									<th>Supplier</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($purchases as $purchase)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $purchase->date->format("d-m-Y") }}</td>
									<td>{{ $purchase->supplier->name }}</td>
									<td>{{ $purchase->product->name }}</td>
									<td>{{ $purchase->quantity }}</td>
									<td>{{ $purchase->rate }}</td>
									<td>{{ $purchase->amount }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="4">Total</th>
									<th>{{ $quantity }}</th>
									<th></th>
									<th>{{ $amount }}</th>
								</tr>
							</tfoot>
						</table>
						@endrole
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
