<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta content="text/html" http-equiv="Content-Type" />
	<title>PDF</title>
	<style type="text/css">
	.card {
		position: relative;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-ms-flex-direction: column;
		flex-direction: column;
		min-width: 0;
		word-wrap: break-word;
		background-color: #fff;
		background-clip: border-box;
		border: 1px solid rgba(0,0,0,.125);
		border-radius: .25rem;
	}

	.card-header {
		padding: .75rem 1.25rem;
		margin-bottom: 0;
		background-color: rgba(0,0,0,.03);
		border-bottom: 1px solid rgba(0,0,0,.125);
	}

	.card-body {
		-webkit-box-flex: 1;
		-ms-flex: 1 1 auto;
		flex: 1 1 auto;
		padding: 1.25rem;
	}

	.table-responsive {
		display: block;
		width: 100%;
		overflow-x: auto;
		-webkit-overflow-scrolling: touch;
		-ms-overflow-style: -ms-autohiding-scrollbar;
	}

	.table {
		width: 100%;
		max-width: 100%;
		margin-bottom: 1rem;
		background-color: transparent;
	}

	.card-footer {
		padding: .75rem 1.25rem;
		background-color: rgba(0,0,0,.03);
		border-top: 1px solid rgba(0,0,0,.125);
	}

	.card-footer:last-child {
		border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);
	}

	.table td, .table th {
		padding: .75rem;
		vertical-align: top;
		border-top: 1px solid #dee2e6;
	}

	.table-striped tbody tr:nth-of-type(odd) {
		background-color: rgba(0,0,0,.05);
	}
	table {
		border-collapse: collapse;
	}
	tbody {
		display: table-row-group;
		vertical-align: middle;
		border-color: inherit;
		text-align: center;
	}
	tr {
		display: table-row;
		vertical-align: inherit;
		border-color: inherit;
	}

	tfoot {
		display: table-footer-group;
		vertical-align: middle;
		border-color: inherit;
	}
	td, th {
		display: table-cell;
		vertical-align: inherit;
		text-align: -internal-center;
	}
	.table thead th {
		vertical-align: bottom;
		border-bottom: 2px solid #dee2e6;
	}
</style>
</head>
<body>
	<div class="card">
		<div class="card-header">
			<h2 style="text-align: center;"> 
	            @if(Auth::user()->company)
	            {{ Auth::user()->company->name }}
	            @else
	            {{ config('app.name', 'Laravel') }}
	            @endif
	        </h2>
			<h2 style="text-align: center;">Purchases from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</h2>
			<h5>Current Fiscal Year: {{ $fisc_year->title }}</h5>
            <h5>Other Fiscal Year(s) Purchase Amount: {{ $otherFiscAmount }}</h5>
            <h5>Other Fiscal Year(s) Purchase Quantity: {{ $otherFiscQuantity }}</h5>
			<div class="card-body">
				<div class="table-responsive">
					@role('super_admin')
					<table class="table table-striped">
						<thead>
							<tr>
								<th>SL</th>
								<th>Date</th>									
								<th>Company</th>								
								<th>Supplier</th>
								<th>Product</th>							
								<th>Quantity</th>
								<th>Rate</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							@foreach($purchases as $purchase)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $purchase->date->format("d-m-Y") }}</td>
								<td>{{ $purchase->company->name }}</td>
								<td>{{ $purchase->supplier->name }}</td>
								<td>{{ $purchase->product->name }}</td>
								<td>{{ $purchase->quantity }}</td>
								<td>{{ $purchase->rate }}</td>
								<td>{{ $purchase->amount }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th colspan="5">Total</th>
								<th>{{ $quantity }}</th>
								<th></th>
								<th>{{ $amount }}</th>
							</tr>
						</tfoot>
					</table>
					@endrole
					@role('business_admin')
					<table class="table table-striped">
						<thead>
							<tr>
								<th>SL</th>
								<th>Date</th>
								<th>Supplier</th>
								<th>Product</th>							
								<th>Quantity</th>
								<th>Rate</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							@foreach($purchases as $purchase)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $purchase->date->format("d-m-Y") }}</td>
								<td>{{ $purchase->supplier->name }}</td>
								<td>{{ $purchase->product->name }}</td>
								<td>{{ $purchase->quantity }}</td>
								<td>{{ $purchase->rate }}</td>
								<td>{{ $purchase->amount }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th colspan="4">Total</th>
								<th>{{ $quantity }}</th>
								<th></th>
								<th>{{ $amount }}</th>
							</tr>
						</tfoot>
					</table>
					@endrole
				</div>
			</div>
			<div class="card-footer">
				<small class="float-right">pdf generated on: {{ $date }}</small>
			</div>
		</div>
	</div>
</body>
</html>