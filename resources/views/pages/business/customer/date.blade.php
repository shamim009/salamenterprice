@extends('layouts.master')
@section('title', 'Customer')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Customer Report</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Customers</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<form method="POST" action="{{url('business/customer/report')}}" class="form-inline">
						@csrf
						<input type="hidden" name="id" id="id" class="form-control" value="{{ $id }}">
						<label for="inlineFormCustomSelect">Start Date </label>
						<input type="text" name="start_date" id="start_date" class="form-control" placeholder="Start Date">
						<label for="inlineFormCustomSelect">End Date </label>
						<input type="text" name="end_date" id="end_date" class="form-control" placeholder="End Date">
						<button type="submit" class="btn btn-info"><i class="fa fa-line-chart" aria-hidden="true"></i> Report</button>
					</form>
				</div>

				<div class="card-body">
                    <div class="table-responsive">

                    	<h6>Order Table</h6>
                        <table class="table table-bordered" id="orderTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>Date</th>
									<th>Product</th>							
									<th>Order Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
									<th>Sold</th>
									<th>Remaining</th>
								</tr>
							</thead>
							<tbody>
							    @php
							    $orderQuantity = 0;
								$orderAmount = 0;
								$orderPurchased = 0;
								$orderRemaining = 0;
							    @endphp
								@foreach($orders as $key => $value)
								<tr class='clickable-row' data-href='{{ route('business.sale-order.show',$value->id) }}'>
									<td>{{ $value->date->format("d-m-Y") }}</td>
									<td>
										{{ $value->product->name }}
									</td>
									<td>
										{{ $value->quantity }}
									</td>
									<td>
										{{ $value->rate }}
									</td>
									<td>
										{{ $value->amount }}
									</td>
									<td>
										{{ $value->quantity - $remainingQuantity[$key] }}
									</td>
									<td>{{ $remainingQuantity[$key] }}</td>
									@php
									$orderQuantity += $value->quantity;
									$orderAmount += $value->amount;
									$orderPurchased += ($value->quantity - $remainingQuantity[$key]);
									$orderRemaining += $remainingQuantity[$key];
									@endphp
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total</th>
									<th></th>
									<th>{{ $orderQuantity }}</th>
									<th></th>
									<th>{{ $orderAmount }}</th>
									<th>{{ $orderPurchased }}</th>
									<th>{{ $orderRemaining }}</th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>

                        <h6>Sale Table</h6>
                        <table class="table table-bordered" id="saleTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>SL</th>
									<th>Date</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($sales as $sale)
								<tr class='clickable-row' data-href='{{ route('business.sale.show',$sale->id) }}'>
									<td>{{ $loop->iteration }}</td>
									<td><span class="hidden_date">{{ $sale->date }}</span>{{ $sale->date->format("d-m-Y") }}</td>
									<td>{{ $sale->product->name }}</td>			
									<td>{{ $sale->quantity }}</td>
									<td>{{ $sale->rate }}</td>
									<td>{{ ($sale->amount - $sale->vat_amount) }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>

                        <h6>Delivery Table</h6>
                        <table class="table table-bordered" id="deliveryTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>SL</th>
									<th>Date</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($deliveries as $delivery)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td><span class="hidden_date">{{ $delivery->date }}</span>{{ $delivery->date->format("d-m-Y") }}</td>
									<td>{{ $delivery->product->name }}</td>			
									<td>{{ $delivery->quantity }}</td>
									<td>{{ $delivery->rate }}</td>
									<td>{{ $delivery->amount }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total</th>
									<th></th>
									<th></th>
									<th>{{ $delivery_quantity }}</th>
									<th></th>
									<th>{{ $delivery_amount }}</th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>

                        <h6>Payment Table</h6>
                        <table class="table table-bordered" id="paymentTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>Date</th>
									<th>Amount</th>
									<th>Receiver</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($transactions as $transaction)
								@if($transaction->purpose == 2)
								<tr class='clickable-row' data-href='{{ route('business.sale-payment.show',$transaction->id) }}'>
									<td><span class="hidden_date">{{ $transaction->date }}</span>{{ $transaction->date->format("d-m-Y") }}</td>
									<td>{{ $transaction->amount }}</td>
									<td>{{ $transaction->receiver }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('business.sale-payment.edit', $transaction->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<form action="{{ route('business.sale-payment.destroy', $transaction->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
									</td>
								</tr>
								@endif
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total Paid</th>
									<th>{{ $paid_amount }}</th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>

                        <h6>Payment Return Table</h6>
                        <table class="table table-bordered" id="returnTable" width="100%" cellspacing="0">
                            <thead>
								<tr>
									<th>Date</th>
									<th>Amount</th>
									<th>Receiver</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($transactions as $transaction)
								@if($transaction->purpose == 1)
								<tr class='clickable-row' data-href='{{ route('business.sale-payment-return.show',$transaction->id) }}'>
									<td><span class="hidden_date">{{ $transaction->date }}</span>{{ $transaction->date->format("d-m-Y") }}</td>
									<td>{{ $transaction->amount }}</td>
									<td>{{ $transaction->receiver }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('business.sale-payment-return.edit', $transaction->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<form action="{{ route('business.sale-payment-return.destroy', $transaction->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
									</td>
								</tr>
								@endif
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Total Returned</th>
									<th>{{ $returned_amount }}</th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
                        </table>
                        <br><br>
                        
						<h4>Transaction Table</h4>
						<table class="table table-bordered" width="100%" cellspacing="0">
							<tbody>
								<tr>
									<th>Sale Amount</th>
									<th></th>
									<th>{{ $sale_amount }}</th>
									<th></th>
								</tr>
								<tr>
									<th>Paid Amount</th>
									<th></th>
									<th>{{ $paid_amount }}</th>
									<th></th>
								</tr>
								<tr>
									<th>Returned Amount</th>
									<th></th>
									<th>{{ $returned_amount }}</th>
									<th></th>
								</tr>
								<tr>
									<th>Balance</th>
									<th></th>
									<th>{{ $balance }}</th>
									<th></th>
								</tr>
								<tr>
									<th>Previous Balance</th>
									<th></th>
									<th>{{ $prev_balance }}</th>
									<th></th>
								</tr>

								<tr>
									<th>Total</th>
									<th></th>
									<th>{{ $prev_balance + $balance }}</th>
									<th></th>
								</tr>
							</tbody>
						</table>
                    </div>
                </div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	$( function() {
		$( "#start_date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});

		$( "#end_date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});

	$(document).ready(function () {
		$('#saleTable').DataTable({
			"footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
			    total = api
			    .column( 5 )
			    .data()
			    .reduce( function (a, b) {
			    	return intVal(a) + intVal(b);
			    }, 0);
			    
			    // Total over this page
			    pageTotal = api
			    .column( 5, { page: 'current'} )
			    .data()
			    .reduce( function (a, b) {
			    	return intVal(a) + intVal(b);
			    }, 0);
                
                // Update footer
                $( api.column( 5 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                );

                // Total over all pages
                totalQuantity = api
			    .column( 3 )
			    .data()
			    .reduce( function (a, b) {
			    	return intVal(a) + intVal(b);
			    }, 0);
                
                // Total over this page
                pageTotalQuantity = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 3 ).footer() ).html(
                    ''+pageTotalQuantity +' ( '+ totalQuantity +' total)'
                );
            }
		});
		$('#paymentTable').DataTable();
		$('#returnTable').DataTable();
		$('#orderTable').DataTable();
		$('#deliveryTable').DataTable();
	});
</script>
@endsection
