<!DOCTYPE html>
<html lang="en">
<head>
    <style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="5">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="5">{{ config('app.name', 'Laravel') }}</th>
            </tr>          
            <tr>
                <th colspan="5">{{ $customer->name }} Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
        </thead>
    </table>

    <table>
        <thead>
            <tr>
                <th colspan="4">Order Table</th>
            </tr>
            <tr>
                <th>Date</th>
                <th>Product</th>                            
                <th>Order Quantity</th>
                <th>Remaining</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $key => $value)
            <tr>
                <td>{{ $value->date->format("d-m-Y") }}</td>
                <td>
                    {{ $value->product->name }}
                </td>
                <td>
                    {{ $value->quantity }}
                </td>
                <td>{{ $remainingQuantity[$key] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
    <table>
        <thead>
            <tr>
                <th colspan="6">Sale Table</th>
            </tr>
            <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Product</th>                            
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sales as $sale)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $sale->date->format("d-m-Y") }}</td>
                <td>{{ $sale->product->name }}</td>         
                <td>{{ $sale->quantity }}</td>
                <td>{{ $sale->rate }}</td>
                <td>{{ ($sale->amount - $sale->vat_amount) }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th>{{ $saleQuantity }}</th>
                <th></th>
                <th>{{ $sale_amount }}</th>
            </tr>
        </tfoot>
    </table>

    <table>
        <thead>
            <tr>
                <th colspan="6">Delivery Table</th>
            </tr>
            <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Product</th>                            
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($deliveries as $delivery)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $delivery->date->format("d-m-Y") }}</td>
                <td>{{ $delivery->product->name }}</td>         
                <td>{{ $delivery->quantity }}</td>
                <td>{{ $delivery->rate }}</td>
                <td>{{ $delivery->amount }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th>{{ $delivery_quantity }}</th>
                <th></th>
                <th>{{ $delivery_amount }}</th>
            </tr>
        </tfoot>
    </table>

    <table>
        <thead>
            <tr>
                <th colspan="3">Payment Table</th>
            </tr>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Receiver</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transactions as $transaction)
            @if($transaction->purpose == 2)
            <tr>
                <td>{{ $transaction->date->format("d-m-Y") }}</td>
                <td>{{ $transaction->amount }}</td>
                <td>{{ $transaction->receiver }}</td>
            </tr>
            @endif
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Total Paid</th>
                <th>{{ $paid_amount }}</th>
                <th></th>
            </tr>
        </tfoot>
    </table>

    <table>
        <thead>
            <tr>
                <th colspan="3">Payment Return Table</th>
            </tr>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Receiver</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transactions as $transaction)
            @if($transaction->purpose == 1)
            <tr>
                <td>{{ $transaction->date->format("d-m-Y") }}</td>
                <td>{{ $transaction->amount }}</td>
                <td>{{ $transaction->receiver }}</td>
            </tr>
            @endif
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Total Returned</th>
                <th>{{ $returned_amount }}</th>
                <th></th>
            </tr>
        </tfoot>
    </table>

    <table>
        <thead>
            <tr>
                <th colspan="4">Transaction Table</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Sale Amount</th>
                <th></th>
                <th>{{ $sale_amount }}</th>
                <th></th>
            </tr>
            <tr>
                <th>Paid Amount</th>
                <th></th>
                <th>{{ $paid_amount }}</th>
                <th></th>
            </tr>
            <tr>
                <th>Returned Amount</th>
                <th></th>
                <th>{{ $returned_amount }}</th>
                <th></th>
            </tr>
            <tr>
                <th>Balance</th>
                <th></th>
                <th>{{ $balance }}</th>
                <th></th>
            </tr>
            <tr>
                <th>Previous Balance</th>
                <th></th>
                <th>{{ $prev_balance }}</th>
                <th></th>
            </tr>
            <tr>
                <th>Total</th>
                <th></th>
                <th>{{ $balance + $prev_balance }}</th>
                <th></th>
            </tr>
        </tbody>
    </table>

    <table>
        <tfoot>
            <tr>
                <th colspan="5">report generated on: {{ $date }}</th>
            </tr>
        </tfoot>
    </table>
</body>
</html>