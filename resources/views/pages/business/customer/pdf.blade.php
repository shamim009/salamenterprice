<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PDF</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
	<div class="card">
		<div class="card-header">
			<h2 style="text-align: center;"> 
	            @if(Auth::user()->company)
	            {{ Auth::user()->company->name }}
	            @else
	            {{ config('app.name', 'Laravel') }}
	            @endif
	        </h2>
			<h2 style="text-align: center;">{{ $customer->name }}'s Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</h2>
			<div class="card-body">
				<div class="table-responsive">

					<h4>Order Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Product</th>							
								<th>Order Quantity</th>
								<th>Remaining</th>
							</tr>
						</thead>
						<tbody>
							@foreach($orders as $key => $value)
							<tr>
								<td>{{ $value->date->format("d-m-Y") }}</td>
								<td>
									{{ $value->product->name }}
								</td>
								<td>
									{{ $value->quantity }}
								</td>
								<td>{{ $remainingQuantity[$key] }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					
					<h4>Sale Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>SL</th>
								<th>Date</th>
								<th>Product</th>							
								<th>Quantity</th>
								<th>Rate</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							@foreach($sales as $sale)
							<tr class='clickable-row' data-href='{{ route('business.sale.show',$sale->id) }}'>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $sale->date->format("d-m-Y") }}</td>
								<td>{{ $sale->product->name }}</td>			
								<td>{{ $sale->quantity }}</td>
								<td>{{ $sale->rate }}</td>
								<td>{{ ($sale->amount - $sale->vat_amount) }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total</th>
								<th></th>
								<th></th>
								<th>{{ $saleQuantity }}</th>
								<th></th>
								<th>{{ $sale_amount }}</th>
							</tr>
						</tfoot>
					</table>

					<h4>Delivery Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>SL</th>
								<th>Date</th>
								<th>Product</th>							
								<th>Quantity</th>
								<th>Rate</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							@foreach($deliveries as $delivery)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $delivery->date->format("d-m-Y") }}</td>
								<td>{{ $delivery->product->name }}</td>			
								<td>{{ $delivery->quantity }}</td>
								<td>{{ $delivery->rate }}</td>
								<td>{{ $delivery->amount }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total</th>
								<th></th>
								<th></th>
								<th>{{ $delivery_quantity }}</th>
								<th></th>
								<th>{{ $delivery_amount }}</th>
							</tr>
						</tfoot>
					</table>

					<h4>Payment Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Amount</th>
								<th>Receiver</th>
							</tr>
						</thead>
						<tbody>
							@foreach($transactions as $transaction)
							@if($transaction->purpose == 2)
							<tr>
								<td>{{ $transaction->date->format("d-m-Y") }}</td>
								<td>{{ $transaction->amount }}</td>
								<td>{{ $transaction->receiver }}</td>
							</tr>
							@endif
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total Paid</th>
								<th>{{ $paid_amount }}</th>
								<th></th>
							</tr>
						</tfoot>
					</table>

					<h4>Payment Return Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Amount</th>
								<th>Receiver</th>
							</tr>
						</thead>
						<tbody>
							@foreach($transactions as $transaction)
							@if($transaction->purpose == 1)
							<tr>
								<td>{{ $transaction->date->format("d-m-Y") }}</td>
								<td>{{ $transaction->amount }}</td>
								<td>{{ $transaction->receiver }}</td>
							</tr>
							@endif
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total Returned</th>
								<th>{{ $returned_amount }}</th>
								<th></th>
							</tr>
						</tfoot>
					</table>

					<h4>Transaction Table</h4>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>Sale Amount</th>
								<th></th>
								<th>{{ $sale_amount }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Paid Amount</th>
								<th></th>
								<th>{{ $paid_amount }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Returned Amount</th>
								<th></th>
								<th>{{ $returned_amount }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Balance</th>
								<th></th>
								<th>{{ $balance }}</th>
								<th></th>
							</tr>
							<tr>
				                <th>Previous Balance</th>
				                <th></th>
				                <th>{{ $prev_balance }}</th>
				                <th></th>
				            </tr>
				            <tr>
				                <th>Total</th>
				                <th></th>
				                <th>{{ $balance + $prev_balance }}</th>
				                <th></th>
				            </tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>