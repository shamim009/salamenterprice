<div class="form-group">
	<label for="date">
		Date
	</label>

	<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ old('date', optional($delivery)->date) }}">

	@if ($errors->has('date'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('date') }}</strong>
	</span>
	@endif
</div>

{{-- challan --}}
<div class="form-group">
	<label for="challan">Challan</label>
	<input type="text" class="form-control{{ $errors->has('challan') ? ' is-invalid' : '' }}" name="challan" id="challan" value="{{ old('challan', optional($delivery)->challan) }}">

	@if ($errors->has('challan'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('challan') }}</strong>
	</span>
	@endif
</div>

{{-- Rate --}}
<div class="form-group">
	<label for="rate">
		Rate
	</label>

	<input type="number" min="0" step="any" class="form-control{{ $errors->has('rate') ? ' is-invalid' : '' }}" name="rate" id="rate" value="{{ old('rate', optional($delivery)->rate) }}">

	@if ($errors->has('rate'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('rate') }}</strong>
	</span>
	@endif
</div>

{{-- Quantity --}}
<div class="form-group">
	<label for="quantity">
		Quantity
	</label>

	<input type="number" min="0" step="any" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" name="quantity" id="quantity" value="{{ old('quantity', optional($delivery)->quantity) }}">

	@if ($errors->has('quantity'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('quantity') }}</strong>
	</span>
	@endif
</div>

{{-- truck --}}
<div class="form-group">
	<label for="truck">Truck</label>
	<input type="text" class="form-control{{ $errors->has('truck') ? ' is-invalid' : '' }}" name="truck" id="truck" value="{{ old('truck', optional($delivery)->truck) }}">

	@if ($errors->has('truck'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('truck') }}</strong>
	</span>
	@endif
</div>
				
{{-- delivery_cost --}}
<div class="form-group">
	<label for="delivery_cost">
		Delivery Cost
	</label>

	<input type="number" min="0" class="form-control{{ $errors->has('delivery_cost') ? ' is-invalid' : '' }}" name="delivery_cost" id="delivery_cost" value="{{ old('delivery_cost', optional($delivery)->delivery_cost) }}">

	@if ($errors->has('delivery_cost'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('delivery_cost') }}</strong>
	</span>
	@endif
</div>

{{-- employee_cost --}}
<div class="form-group">
	<label for="employee_cost">
		Employee Cost
	</label>

	<input type="number" min="0" class="form-control{{ $errors->has('employee_cost') ? ' is-invalid' : '' }}" name="employee_cost" id="employee_cost" value="{{ old('employee_cost', optional($delivery)->employee_cost) }}">

	@if ($errors->has('employee_cost'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('employee_cost') }}</strong>
	</span>
	@endif
</div>

{{-- other_cost --}}
<div class="form-group">
	<label for="other_cost">
		Other Cost
	</label>

	<input type="number" min="0" class="form-control{{ $errors->has('other_cost') ? ' is-invalid' : '' }}" name="other_cost" id="other_cost" value="{{ old('other_cost', optional($delivery)->other_cost) }}">

	@if ($errors->has('other_cost'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('other_cost') }}</strong>
	</span>
	@endif
</div>

{{-- Details --}}
<div class="form-group">
	<label for="details">
		Remarks
	</label>

	<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($delivery)->details) }}</textarea>

	@if( $errors->has('details'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('details') }}</strong>
	</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>


@section('script')
<script>
	{{-- jquery datepicker --}}
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});
</script>
@endsection