@extends('layouts.master')
@section('title', 'Purchase')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Purchase Orders</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Purchases</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<h5>Current Fiscal Year: {{ $fisc_year->title }}</h5>
					<p>To see the other fiscal years purchase order, please select the fiscal year first.</p>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						@role('super_admin')
						<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>									
									<th>Company</th>								
									<th>Supplier</th>									
									<th>Invoice</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
									<th>Purchased</th>
									<th>Remaining</th>
									<th>Action</th>
								</tr>
							</thead>
							<tfoot>
                                <tr>
                                    <th colspan="6" style="text-align:right">Total:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
						</table>
						@endrole
						
						@role('business_admin')
						<table class="table table-bordered table-hover" id="adminDataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>
									<th>Supplier</th>
									<th>Invoice</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
									<th>Purchased</th>
									<th>Remaining</th>									
									<th>Action</th>
								</tr>
							</thead>
							<tfoot>
                                <tr>
                                    <th colspan="5" style="text-align:right">Total:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
						</table>
						@endrole
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	//server side data table
	$(document).ready(function () {
		$('#dataTable').DataTable({
			"order": [[ 1, "desc" ]],
			"processing": true,
			"serverSide": true,
			"ajax":{
				"url": "{{ url('business/all-purchase-orders') }}",
				"dataType": "json",
				"type": "POST",
				"data":{ _token: "{{ csrf_token() }}"}
			},
			"columns": [
			{ "data": "id" },
			{ "data": "date" },
			{ "data": "company_id" },
			{ "data": "supplier_id" },
			{ "data": "invoice" },
			{ "data": "product_id" },
			{ "data": "quantity" },
			{ "data": "rate" },
			{ "data": "amount" },
			{ "data": "purchased" },
			{ "data": "remaining" },
			{ "data": "actions" }
			],	 
			
			"footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = json.totalSum;
                
                // Total over this page
                pageTotal = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 8 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                );

                // Total over all pages
                totalQuantity = json.totalQuantity;
                console.log(totalQuantity);
                
                // Total over this page
                pageTotalQuantity = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 6 ).footer() ).html(
                    ''+pageTotalQuantity +' ( '+ totalQuantity +' total)'
                );
                
                // Total over all pages
                totalPurchased = json.totalPurchased;
                //console.log(totalDelivered);
                
                // Total over this page
                pageTotalPurchased = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 9 ).footer() ).html(
                    ''+pageTotalPurchased +' ( '+ totalPurchased +' total)'
                );
                
                totalRemaining = json.totalRemaining;
                //console.log(totalDelivered);
                
                // Total over this page
                pageTotalRemaining = api
                .column( 10, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 10 ).footer() ).html(
                    ''+pageTotalRemaining +' ( '+ totalRemaining +' total)'
                );
            }
		});


		$('#adminDataTable').DataTable({
			"order": [[ 1, "desc" ]],
			"processing": true,
			"serverSide": true,
			"ajax":{
				"url": "{{ url('business/admin/all-purchase-orders') }}",
				"dataType": "json",
				"type": "POST",
				"data":{ _token: "{{ csrf_token() }}"}
			},
			"columns": [
			{ "data": "id" },
			{ "data": "date" },
			{ "data": "supplier_id" },
			{ "data": "invoice" },
			{ "data": "product_id" },
			{ "data": "quantity" },
			{ "data": "rate" },			
			{ "data": "amount" },
			{ "data": "purchased" },
			{ "data": "remaining" },
			{ "data": "actions" }
			],	
			
			"footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                //console.log(json);
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = json.totalSum;
                
                // Total over this page
                pageTotal = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 7 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                );

                // Total over all pages
                totalQuantity = json.totalQuantity;
                //console.log(totalQuantity);
                
                // Total over this page
                pageTotalQuantity = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 5 ).footer() ).html(
                    ''+pageTotalQuantity +' ( '+ totalQuantity +' total)'
                );
                
                // Total over all pages
                totalPurchased = json.totalPurchased;
                //console.log(totalDelivered);
                
                // Total over this page
                pageTotalPurchased = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 8 ).footer() ).html(
                    ''+pageTotalPurchased +' ( '+ totalPurchased +' total)'
                );
                
                totalRemaining = json.totalRemaining;
                //console.log(totalDelivered);
                
                // Total over this page
                pageTotalRemaining = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 9 ).footer() ).html(
                    ''+pageTotalRemaining +' ( '+ totalRemaining +' total)'
                );
            }
		});
	});

    //delete a row using ajax
    $('#dataTable').on('click', '.btn-delete[data-remote]', function (e) { 
    	e.preventDefault();
    	$.ajaxSetup({
    		headers: {
    			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
    	});
    	var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
        	$.ajax({
        		url: url,
        		type: 'POST',
        		dataType: 'json',
        		data: {_method: 'DELETE', submit: true}
        	}).always(function (data) {
        		$('#dataTable').DataTable().draw(false);
        	});
        }
    });

    $('#adminDataTable').on('click', '.btn-delete[data-remote]', function (e) { 
    	e.preventDefault();
    	$.ajaxSetup({
    		headers: {
    			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    		}
    	});
    	var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
        	$.ajax({
        		url: url,
        		type: 'POST',
        		dataType: 'json',
        		data: {_method: 'DELETE', submit: true}
        	}).always(function (data) {
        		$('#adminDataTable').DataTable().draw(false);
        	});
        }
    });
</script>
@endsection
