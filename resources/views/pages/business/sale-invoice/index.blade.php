@extends('layouts.master')
@section('title', 'Sale')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Sale Invoice</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item active">Sales</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <form method="POST" action="{{url('business/sale/invoice')}}" class="form-inline">
                        @csrf
                        <label for="start_date">Start Date </label>
                        <input type="text" name="start_date" id="start_date" class="form-control" placeholder="Start Date" required="required">
                        <label for="end_date">End Date </label>
                        <input type="text" name="end_date" id="end_date" class="form-control" placeholder="End Date" required="required">
                        <label for="customer_id">Customer</label>
                        <select name="customer_id" class="form-control" id="customer_id" required="required">
                            <option value="">Select</option>
                            @forelse($customers as $customer)
                            <option value="{{ $customer->id }}">
                                {{ $customer->name }}
                            </option>
                            @empty
                            <option value="">No Customer Found</option>
                            @endforelse
                        </select>
                        <button type="submit" class="btn btn-info"><i class="fa fa-line-chart" aria-hidden="true"></i> Report</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('script')
<script>
    $( function() {
        $( "#start_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });

        $( "#end_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });
    });

    $('#customer_id').select2({
        placeholder: 'Select customer',

        ajax: {
            url: '{!!URL::route('business.customer-autocomplete-search')!!}',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
        theme: "bootstrap"
    });
</script>
@endsection

