<!DOCTYPE html>
<html lang="en">
<head>
    <style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
</head>
<body>
	@role('super_admin')
    <table>
        <thead>
            <tr>
                <th colspan="8">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="8">{{ config('app.name', 'Laravel') }}</th>
            </tr>          
            <tr>
                <th colspan="8">Sales to {{ $customer->name }} from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            <tr>
                <th>SL</th>
                <th>Date</th>                 
                <th>Company</th>                
                <th>Product</th>
                <th>Invoice</th>              
                <th>Quantity</th>
                <th>Rate</th>
                <th>Vat</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sales as $sale)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $sale->date->format("d-m-Y") }}</td>
                <td>{{ $sale->company->name }}</td>
                <td>{{ $sale->product->name }}</td>
                <td>{{ $sale->invoice }}</td>
                <td>{{ $sale->quantity }}</td>
                <td>{{ $sale->rate }}</td>
                <td>{{ $sale->vat_amount }}</td>
                <td>{{ $sale->amount - $sale->vat_amount }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5">Total</th>
                <th>{{ $quantity }}</th>
                <th></th>
                <th>{{ $vat_amount }}</th>
                <th>{{ $amount }}</th>
            </tr>
            <tr>
                <th colspan="8">report generated on: {{ $date }}</th>
            </tr>
        </tfoot>
    </table>
    @endrole
    @role('business_admin')
    <table>
        <thead>
            <tr>
                <th colspan="7">بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ</th>
            </tr>
            <tr>
                <th colspan="7">{{ Auth::user()->company->name }}</th>
            </tr>
            <tr>
                <th colspan="7">Sales to {{ $customer->name }} from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</th>
            </tr>
            
            <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Product</th>
                <th>Invoice</th>              
                <th>Quantity</th>
                <th>Rate</th>
                <th>Vat</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sales as $sale)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $sale->date->format("d-m-Y") }}</td>
                <td>{{ $sale->product->name }}</td>
                <td>{{ $sale->invoice }}</td>
                <td>{{ $sale->quantity }}</td>
                <td>{{ $sale->rate }}</td>
                <td>{{ $sale->vat_amount }}</td>
                <td>{{ $sale->amount - $sale->vat_amount }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4">Total</th>
                <th>{{ $quantity }}</th>
                <th></th>
                <th>{{ $vat_amount }}</th>
                <th>{{ $amount }}</th>
            </tr>
            <tr>
                <th colspan="7">report generated on: {{ $date }}</th>
            </tr>
        </tfoot>
    </table>
    @endrole
</body>
</html>