@extends('layouts.master')
@section('title', 'Raw Material')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Raw Materials</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Raw Materials</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Sl</th>							
									<th>Name</th>
									<th>Purchase</th>
									<th>Use</th>
									<th>Stock</th>
									<th>Remarks</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@forelse($rawmaterials as $rawmaterial)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $rawmaterial->name }}</td>
									<td>{{ $rawmaterial->purchase }}</td>
									<td>{{ $rawmaterial->used }}</td>
									<td>{{ $rawmaterial->stock }}</td>
									<td>{{ $rawmaterial->details }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('production.raw-material.edit', $rawmaterial->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<form action="{{ route('production.raw-material.destroy', $rawmaterial->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	//server side data table
	$(document).ready(function () {
		$('#dataTable').DataTable({   
		});
	});
</script>
@endsection