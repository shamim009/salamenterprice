{{-- Name --}}
<div class="form-group">
	<label for="date">
		Date
	</label>

	<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ old('date', optional($machineRent)->date) }}">

	@if ($errors->has('date'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('date') }}</strong>
	</span>
	@endif
</div>

{{-- Name --}}
<div class="form-group">
	<label for="rented_to">
		Rented To
	</label>

	<input type="text" class="form-control{{ $errors->has('rented_to') ? ' is-invalid' : '' }}" name="rented_to" id="rented_to" value="{{ old('rented_to', optional($machineRent)->rented_to) }}">

	@if ($errors->has('rented_to'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('rented_to') }}</strong>
	</span>
	@endif
</div>

{{-- Rate --}}
<div class="form-group">
	<label for="rate">
		Rate
	</label>

	<input type="number" min="0" step="any" class="form-control{{ $errors->has('rate') ? ' is-invalid' : '' }}" name="rate" id="rate" value="{{ old('rate', optional($machineRent)->rate) }}">

	@if ($errors->has('rate'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('rate') }}</strong>
	</span>
	@endif
</div>

{{-- produced --}}
<div class="form-group">
	<label for="produced">
		Produced Quantity
	</label>

	<input type="number" min="0" step="any" class="form-control{{ $errors->has('produced') ? ' is-invalid' : '' }}" name="produced" id="produced" value="{{ old('produced', optional($machineRent)->produced) }}">

	@if ($errors->has('produced'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('produced') }}</strong>
	</span>
	@endif
</div>
{{-- Amount --}}
<div class="form-group">
	<label for="amount">
		Amount
	</label>

	<input type="number" min="0" step="any" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" id="amount" value="{{ old('amount', optional($machineRent)->amount) }}" readonly="readonly">

	@if ($errors->has('amount'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('amount') }}</strong>
	</span>
	@endif
</div>

{{-- Remarks --}}
<div class="form-group">
	<label for="details">
		Remarks
	</label>

	<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($machineRent)->details) }}</textarea>

	@if( $errors->has('details'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('details') }}</strong>
	</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>

@section('script')
<script>
	{{-- jquery datepicker --}}
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});

	//amount calculation
	$(document).ready(function(){
		var rate=$("#rate");
		var produced=$("#produced");
		rate.keyup(function(){
			var total=isNaN(parseFloat(rate.val()* produced.val())) ? 0 :(rate.val()* produced.val())
			$("#amount").val(total);
		});
		produced.keyup(function(){
			var total=isNaN(parseInt(rate.val()* produced.val())) ? 0 :(rate.val()* produced.val())
			$("#amount").val(total);
		});
	});
</script>
@endsection