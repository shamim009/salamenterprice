@extends('layouts.master')
@section('title', 'Purchase')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-8">
					<h1 class="m-0 text-dark">Purchase Payments from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</h1>
				</div><!-- /.col -->
				<div class="col-sm-4">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Purchases</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<form method="POST" action="{{url('production/purchase-transaction/report/print')}}" class="d-inline">
						@csrf
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i>Print</button>
					</form>

					<form method="POST" action="{{url('production/purchase-transaction/report/pdf')}}" class="d-inline">
						@csrf
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</button>
					</form>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						@role('super_admin')
						<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>									
									<th>Company</th>								
									<th>Supplier</th>
									<th>Amount</th>							
									<th>Payment Type</th>
									<th>Payment Mode</th>
								</tr>
							</thead>
							<tbody>
								@foreach($transactions as $transaction)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $transaction->date->format("d-m-Y") }}</td>
									<td>{{ $transaction->company->name }}</td>
									<td>{{ $transaction->supplier->name }}</td>
									<td>{{ $transaction->amount }}</td>
									@if($transaction->payment_type == 1)
									<td>Cash</td>
									@elseif($transaction->payment_type == 2)
									<td>Due</td>
									@else
									<td>Advance</td>
									@endif
									@if($transaction->payment_mode == 1)
									<td>Hand Cash</td>
									@elseif($transaction->payment_mode == 2)
									<td>Regular Banking</td>
									@else
									<td>Mobile Banking</td>
									@endif
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="4"></th>
									<th>{{ $amount }}</th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
						@endrole
						
						@role('production_admin')
						<table class="table table-bordered table-hover" id="adminDataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>								
									<th>Supplier</th>
									<th>Amount</th>							
									<th>Payment Type</th>
									<th>Payment Mode</th>
								</tr>
							</thead>
							<tbody>
								@foreach($transactions as $transaction)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $transaction->date->format("d-m-Y") }}</td>
									<td>{{ $transaction->supplier->name }}</td>
									<td>{{ $transaction->amount }}</td>
									@if($transaction->payment_type == 1)
									<td>Cash</td>
									@elseif($transaction->payment_type == 2)
									<td>Due</td>
									@else
									<td>Advance</td>
									@endif
									@if($transaction->payment_mode == 1)
									<td>Hand Cash</td>
									@elseif($transaction->payment_mode == 2)
									<td>Regular Banking</td>
									@else
									<td>Mobile Banking</td>
									@endif
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="3"></th>
									<th>{{ $amount }}</th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
						@endrole
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
