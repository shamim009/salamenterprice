@extends('layouts.master')
@section('title', 'Bank Account')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Transaction Report</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('production/bank-account') }}">Accounts</a></li>
						<li class="breadcrumb-item active">Report</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					{{-- <a href="{{ url('bank-account/report/print', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Print" target="_blank">
						<i class="fa fa-print" aria-hidden="true" title="Print"></i> Print
					</a>

					<a href="{{ url('bank-account/report/pdf', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="PDF" target="_blank">
						<i class="fa fa-file-pdf-o" aria-hidden="true" title="PDF"></i> PDF
					</a>

					<a href="{{ url('bank-account/report/excel', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Excel" target="_blank">
						<i class="fa fa-file-excel-o" aria-hidden="true" title="Excel"></i> Excel
					</a> --}}
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					
					<div class="alert alert-primary">
						<button type="button" class="close">
							<i class="fa fa-money fa-2x" aria-hidden="true"></i>
						</button>
						<h4>Transaction Table</h4>
						<table>
							<thead>
								<tr>
									<th>Purpose</th>
									<th>Date</th>
									<th>Amount</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($transactions as $transaction)
								<tr>
									<td>
										@if($transaction->purpose == 1)
										Deposit
										@elseif($transaction->purpose == 2)
										Withdraw
										@elseif($transaction->purpose == 3)
										Sale Payment
										@elseif($transaction->purpose == 4)
										Purchase Payment
										@elseif($transaction->purpose == 6)
										Sale Payment Return
										@elseif($transaction->purpose == 7)
										Purchase Payment Return	
										@elseif($transaction->purpose == 9)
										Investment
										@elseif($transaction->purpose == 10)
										Investment Return
										@else
										Expense Payment
										@endif
									</td>
									<td>{{ $transaction->date->format("d-m-Y") }}</td>
									<td>{{ $transaction->amount }}</td>
									<td>
										@if($transaction->purpose == 1 || $transaction->purpose == 2)
										<div class="btn-group">
											<a href="{{ route('production.bank-transaction.edit', $transaction->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<form action="{{ route('production.bank-transaction.destroy', $transaction->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Balance</th>
									<th></th>
									<th>{{ $bankAccount->current_amount }}</th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
