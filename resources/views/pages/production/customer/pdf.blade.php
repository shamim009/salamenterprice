<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PDF</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
	<div class="card">
		<div class="card-header">
			<h2 style="text-align: center;">{{ $customer->name }}'s Report </h2>
			<div class="card-body">
				<div class="table-responsive">
					<h4>Sale Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>SL</th>
								<th>Date</th>
								<th>Product</th>							
								<th>Quantity</th>
								<th>Rate</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							@foreach($sales as $sale)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $sale->date->format("d-m-Y") }}</td>
								<td>{{ $sale->product->name }}</td>						
								<td>{{ $sale->quantity }}</td>
								<td>{{ $sale->rate }}</td>
								<td>{{ $sale->amount }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total</th>
								<th></th>
								<th></th>
								<th>{{ $saleQuantity }}</th>
								<th></th>
								<th>{{ $sale_amount }}</th>
							</tr>
						</tfoot>
					</table>
					<h4>Payment Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Purpose</th>
								<th>Date</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							@foreach($transactions as $transaction)
							<tr>
								<td>
									@if($transaction->purpose == 1)
									Payment Return
									@else
									Payment
									@endif
								</td>
								<td>{{ $transaction->date->format("d-m-Y") }}</td>
								<td>{{ $transaction->amount }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total Paid</th>
								<th></th>
								<th>{{ $paid_amount - $returned_amount }}</th>
								<th></th>
							</tr>
						</tfoot>
					</table>

					<h4>Order Remaining Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Product</th>							
								<th>Order Quantity</th>
								<th>Remaining</th>
							</tr>
						</thead>
						<tbody>
							@foreach($orders as $key => $value)
							@if($remainingQuantity[$key] >0)
							<tr>
								<td>{{ $value->date->format("d-m-Y") }}</td>
								<td>
									{{ $value->product->name }}
								</td>
								<td>
									{{ $value->quantity }}
								</td>
								<td>{{ $remainingQuantity[$key] }}</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>

					<h4>Transaction Table</h4>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>Sale</th>
								<th>{{ $sale_amount }}</th>
							</tr>
							<tr>
								<th>Paid Amount</th>
								<th>{{ $paid_amount }}</th>
							</tr>
							<tr>
								<th>Returned Amount</th>
								<th>{{ $returned_amount }}</th>
							</tr>
							<tr>
								<th>Balance</th>
								<th>{{ $balance }}</th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>