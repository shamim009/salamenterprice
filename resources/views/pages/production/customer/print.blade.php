<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }} | Customer Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link href="{{ asset('css/adminlte.min.css') }}" rel="stylesheet">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> 
            @if(Auth::user()->company)
            {{ Auth::user()->company->name }}
            @else
            {{ config('app.name', 'Laravel') }}
            @endif
            <small class="float-right">Date: {{ $date }}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <br>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-8 invoice-col">
          <h4><strong>{{ $customer->name }}'s Report</strong></h4>
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-12 table-responsive">
          <h4>Sale Table</h4>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Product</th>              
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach($sales as $sale)
              <tr class='clickable-row' data-href='{{ route('business.sale.show',$sale->id) }}'>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $sale->date->format("d-m-Y") }}</td>
                <td>{{ $sale->product->name }}</td>               
                <td>{{ $sale->quantity }}</td>
                <td>{{ $sale->rate }}</td>
                <td>{{ $sale->amount }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>Total</th>
                <th></th>
                <th></th>
                <th>{{ $saleQuantity }}</th>
                <th></th>
                <th>{{ $sale_amount }}</th>
              </tr>
            </tfoot>
          </table>
          <h4>Payment Table</h4>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Purpose</th>
                <th>Date</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach($transactions as $transaction)
              <tr>
                <td>
                  @if($transaction->purpose == 1)
                  Payment Return
                  @else
                  Payment
                  @endif
                </td>
                <td>{{ $transaction->date->format("d-m-Y") }}</td>
                <td>{{ $transaction->amount }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>Total Paid</th>
                <th></th>
                <th>{{ $paid_amount - $returned_amount }}</th>
                <th></th>
              </tr>
            </tfoot>
          </table>
          <h4>Order Remaining Table</h4>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Date</th>
                <th>Product</th>              
                <th>Order Quantity</th>
                <th>Remaining</th>
              </tr>
            </thead>
            <tbody>
              @foreach($orders as $key => $value)
              @if($remainingQuantity[$key] >0)
              <tr class='clickable-row' data-href='{{ route('business.sale-order.show',$value->id) }}'>
                <td>{{ $value->date->format("d-m-Y") }}</td>
                <td>
                  {{ $value->product->name }}
                </td>
                <td>
                  {{ $value->quantity }}
                </td>
                <td>{{ $remainingQuantity[$key] }}</td>
              </tr>
              @endif
              @endforeach
            </tbody>
          </table>
          <h4>Transaction Table</h4>
          <table class="table table-striped">
            <tbody>
              <tr>
                <th>Sale</th>
                <th>{{ $sale_amount }}</th>
              </tr>
              <tr>
                <th>Paid Amount</th>
                <th>{{ $paid_amount }}</th>
              </tr>
              <tr>
                <th>Returned Amount</th>
                <th>{{ $returned_amount }}</th>
              </tr>
              <tr>
                <th>Balance</th>
                <th>{{ $balance }}</th>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- ./wrapper -->
</body>
</html>
