<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PDF</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
	<div class="card">
		<div class="card-header">
			<h2 style="text-align: center;">{{ $supplier->name }}'s Report </h2>
			<div class="card-body">
				<div class="table-responsive">
					<h4>Purchase Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>SL</th>
								<th>Date</th>
								<th>Raw Material</th>							
								<th>Quantity</th>
								<th>Rate</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							@foreach($purchases as $purchase)
							<tr class='clickable-row'>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $purchase->date->format("d-m-Y") }}</td>
								<td>
									{{ $purchase->rawmaterial->name }}
								</td>
								<td>
									{{ $purchase->quantity }}
								</td>
								<td>{{ $purchase->rate }}</td>
								<td>{{ $purchase->amount }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total</th>
								<th></th>
								<th></th>
								<th>{{ $purchaseQuantity }}</th>
								<th></th>
								<th>{{ $purchase_amount }}</th>
							</tr>
						</tfoot>
					</table>
					<h4>Payment Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Purpose</th>
								<th>Date</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							@foreach($transactions as $transaction)
							<tr>
								<td>
									@if($transaction->purpose == 1)
									Payment Return
									@else
									Payment
									@endif
								</td>
								<td>{{ $transaction->date->format("d-m-Y") }}</td>
								<td>{{ $transaction->amount }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th>Total Paid</th>
								<th></th>
								<th>{{ $paid_amount - $returned_amount}}</th>
								<th></th>
							</tr>
						</tfoot>
					</table>

					<h4>Order Remaining Table</h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th>Raw Material</th>							
								<th>Order Quantity</th>
								<th>Remaining</th>
							</tr>
						</thead>
						<tbody>
							@foreach($orders as $key => $value)
							@if($remainingQuantity[$key] >0)
							<tr class='clickable-row' data-href='{{ route('business.purchase-order.show',$value->id) }}'>
								<td>{{ $value->date->format("d-m-Y") }}</td>
								<td>
									{{ $value->rawmaterial->name }}
								</td>
								<td>
									{{ $value->quantity }}
								</td>
								<td>{{ $remainingQuantity[$key] }}</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>

					<h4>Transaction Table</h4>
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>Purchase Amount</th>
								<th></th>
								<th>{{ $purchase_amount }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Paid Amount</th>
								<th></th>
								<th>{{ $paid_amount }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Returned Amount</th>
								<th></th>
								<th>{{ $returned_amount }}</th>
								<th></th>
							</tr>
							<tr>
								<th>Balance</th>
								<th></th>
								<th>{{ $balance }}</th>
								<th></th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>