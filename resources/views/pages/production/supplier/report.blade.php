@extends('layouts.master')
@section('title', 'Supplier')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">{{ $supplier->name }}'s Report</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('production/supplier') }}">Suppliers</a></li>
						<li class="breadcrumb-item active">Report</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<a href="{{ url('production/supplier/report/print', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Print" target="_blank">
						<i class="fa fa-print" aria-hidden="true" title="Print"></i> Print
					</a>

					<a href="{{ url('production/supplier/report/pdf', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="PDF" target="_blank">
						<i class="fa fa-file-pdf-o" aria-hidden="true" title="PDF"></i> PDF
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="row">
						{{-- Total Purchase count --}}
						<div class="col-sm-6">
							<div class="alert alert-secondary">
								<button type="button" class="close">
									<i class="fa fa-money fa-2x" aria-hidden="true"></i>
								</button>
								<h4>Purchase Table</h4>
								<table>
									<thead>
										<tr>
											<th>SL</th>
											<th>Date</th>
											<th>Raw Material</th>	
											<th>Quantity</th>
											<th>Rate</th>
											<th>Amount</th>
										</tr>
									</thead>
									<tbody>
										@foreach($purchases as $purchase)
										<tr class='clickable-row' data-href='{{ route('production.purchase.show',$purchase->id) }}'>
											<td>{{ $loop->iteration }}</td>
											<td>{{ $purchase->date->format("d-m-Y") }}</td>
											<td>
												{{ $purchase->rawmaterial->name }}
											</td>
											<td>
												{{ $purchase->quantity }}
											</td>
											<td>{{ $purchase->rate }}</td>
											<td>{{ $purchase->amount }}</td>
										</tr>
										@endforeach
									</tbody>
									<tfoot>
										<tr>
											<th>Total</th>
											<th></th>
											<th></th>
											<th>{{ $purchaseQuantity }}</th>
											<th></th>
											<th>{{ $purchase_amount }}</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>

						{{-- Total purchase count --}}
						<div class="col-sm-6">
							<div class="alert alert-primary">
								<button type="button" class="close">
									<i class="fa fa-money fa-2x" aria-hidden="true"></i>
								</button>
								<h4>Payment Table</h4>
								<table>
									<thead>
										<tr>
											<th>Purpose</th>
											<th>Date</th>
											<th>Amount</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										@foreach($transactions as $transaction)
										<tr @if($transaction->purpose == 2) class='clickable-row' data-href='{{ route('production.purchase-payment.show',$transaction->id) }}' @else class='clickable-row' data-href='{{ route('production.purchase-payment-return.show',$transaction->id) }}' @endif>
											<td>
												@if($transaction->purpose == 1)
												Payment Return
												@else
												Payment
												@endif
											</td>
											<td>{{ $transaction->date->format("d-m-Y") }}</td>
											<td>{{ $transaction->amount }}</td>
											@if($transaction->purpose == 2)
											<td>
												<div class="btn-group">
													<a href="{{ route('production.purchase-payment.edit', $transaction->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
														Update
													</a>
													<form action="{{ route('production.purchase-payment.destroy', $transaction->id) }}" method="POST">
														@csrf
														@method('DELETE')
														<button class="btn btn-danger btn-sm">Delete</button>
													</form>
												</div>
											</td>

											@else
											<td>
												<div class="btn-group">
													<a href="{{ route('production.purchase-payment-return.edit', $transaction->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
														Update
													</a>
													<form action="{{ route('production.purchase-payment-return.destroy', $transaction->id) }}" method="POST">
														@csrf
														@method('DELETE')
														<button class="btn btn-danger btn-sm">Delete</button>
													</form>
												</div>
											</td>
											@endif
										</tr>
										@endforeach
									</tbody>
									<tfoot>
										<tr>
											<th>Total Paid</th>
											<th></th>
											<th>{{ $paid_amount - $returned_amount}}</th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>            
					</div>
				</div>
				<div class="col-12">
					<div class="row">
						{{-- Total Purchase count --}}
						<div class="col-sm-6">
							<div class="alert alert-secondary">
								<button type="button" class="close">
									<i class="fa fa-money fa-2x" aria-hidden="true"></i>
								</button>
								<h4>Order Remaining Table</h4>
								<table>
									<thead>
										<tr>
											<th>Date</th>
											<th>Raw Material</th>	
											<th>Order Quantity</th>
											<th>Remaining</th>
										</tr>
									</thead>
									<tbody>
										@foreach($orders as $key => $value)
										@if($remainingQuantity[$key] >0)
										<tr class='clickable-row' data-href='{{ route('production.purchase-order.show',$value->id) }}'>
											<td>{{ $value->date->format("d-m-Y") }}</td>
											<td>
												{{ $value->rawmaterial->name }}
											</td>
											<td>
												{{ $value->quantity }}
											</td>
											<td>{{ $remainingQuantity[$key] }}</td>
										</tr>
										@endif
										@endforeach
									</tbody>
								</table>
							</div>
						</div>

						{{-- Total purchase count --}}
						<div class="col-sm-6">
							<div class="alert alert-primary">
								<button type="button" class="close">
									<i class="fa fa-money fa-2x" aria-hidden="true"></i>
								</button>
								<h4>Transaction Table</h4>
								<table>
									<tbody>
										<tr>
											<th>Purchase Amount</th>
											<th></th>
											<th>{{ $purchase_amount }}</th>
											<th></th>
										</tr>
										<tr>
											<th>Paid Amount</th>
											<th></th>
											<th>{{ $paid_amount }}</th>
											<th></th>
										</tr>
										<tr>
											<th>Returned Amount</th>
											<th></th>
											<th>{{ $returned_amount }}</th>
											<th></th>
										</tr>
										<tr>
											<th>Balance</th>
											<th></th>
											<th>{{ $balance }}</th>
											<th></th>
										</tr>
									</tbody>
								</table>
							</div>
						</div>            
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	jQuery(document).ready(function($) {
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
	});
</script>
@endsection