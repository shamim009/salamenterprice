{{-- Name --}}
<div class="form-group">
	<label for="investor_name">
		Investor Name
	</label>

	<input type="text" class="form-control{{ $errors->has('investor_name') ? ' is-invalid' : '' }}" name="investor_name" id="investor_name" value="{{ old('investor_name', optional($investment)->investor_name) }}">

	@if ($errors->has('investor_name'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('investor_name') }}</strong>
	</span>
	@endif
</div>

{{-- Name --}}
<div class="form-group">
	<label for="investor_mobile">
		Investor Mobile
	</label>

	<input type="text" class="form-control{{ $errors->has('investor_mobile') ? ' is-invalid' : '' }}" name="investor_mobile" id="investor_mobile" value="{{ old('investor_mobile', optional($investment)->investor_mobile) }}">

	@if ($errors->has('investor_mobile'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('investor_mobile') }}</strong>
	</span>
	@endif
</div>

{{-- Remarks --}}
<div class="form-group">
	<label for="investor_address">
		Investor Address
	</label>

	<textarea name="investor_address" class="form-control {{ $errors->has('investor_address') ? ' is-invalid' : '' }}" id="investor_address" cols="30" rows="5">{{ old('investor_address', optional($investment)->investor_address) }}</textarea>

	@if( $errors->has('investor_address'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('investor_address') }}</strong>
	</span>
	@endif
</div>

{{-- Amount --}}
<div class="form-group">
	<label for="interest_rate">
		Profit Rate
	</label>

	<input type="number" min="0" step="any" class="form-control{{ $errors->has('interest_rate') ? ' is-invalid' : '' }}" name="interest_rate" id="interest_rate" value="{{ old('interest_rate', optional($investment)->interest_rate) }}">

	@if ($errors->has('interest_rate'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('interest_rate') }}</strong>
	</span>
	@endif
</div> 

{{-- Remarks --}}
<div class="form-group">
	<label for="details">
		Remarks
	</label>

	<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($investment)->details) }}</textarea>

	@if( $errors->has('details'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('details') }}</strong>
	</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>