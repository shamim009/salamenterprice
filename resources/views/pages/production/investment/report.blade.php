@extends('layouts.master')
@section('title', 'Investment')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ $investment->investor_name }}'s Investment Report</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('production/investment') }}">Investments</a></li>
                        <li class="breadcrumb-item active">Report</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <p>Total Invest: {{ $totalInvest }}</p>
                    <p>Profit Return: {{ $totalProfitReturn }}</p>
                    <p>Main Amount Return: {{ $totalAmountReturn }}</p>
                    <p>Balance: {{ $investment->invested_amount }}</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h6>Investment Table</h6>
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($invests as $invest)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $invest->date->format("d-m-Y") }}</td>
                                    <td>{{ $invest->amount }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" style="text-align:right">Total:</th>
                                    <th>{{ $totalInvest }}</th>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                        <h6>Profit Return Table</h6>
                        <table class="table table-bordered" id="profitReturn" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($profitreturns as $profitreturn)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $profitreturn->date->format("d-m-Y") }}</td>
                                    <td>{{ $profitreturn->amount }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" style="text-align:right">Total:</th>
                                    <th>{{ $totalProfitReturn }}</th>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                        <h6>Main Amount Return Table</h6>
                        <table class="table table-bordered" id="mainAmountReturn" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($mainreturns as $mainreturn)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $mainreturn->date->format("d-m-Y") }}</td>
                                    <td>{{ $mainreturn->amount }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" style="text-align:right">Total:</th>
                                    <th>{{ $totalAmountReturn }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('script')
<script>
    //server side data table
    $(document).ready(function () {
        $('#dataTable').DataTable({ 
        });

        $('#profitReturn').DataTable({  
        });

        $('#mainAmountReturn').DataTable({  
        });
    });
</script>
@endsection