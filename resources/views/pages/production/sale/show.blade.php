@extends('layouts.master')
@section('title', 'Sale')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">View Sale</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('production/sale') }}">Sales</a></li>
						<li class="breadcrumb-item active">View</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<tbody>
								<tr>
									<td>Date</td>
									<td>{{ $sale->date->format("d-m-Y") }}</td>
								</tr>
								@role('super_admin')
								<tr>
									<td>Company</td>
									<td>{{ $sale->company->name }}</td>
								</tr>
								@endrole
								<tr>
									<td>Customer</td>
									<td>{{ $sale->customer->name }}</td>
								</tr>
								<tr>
									<td>Product</td>
									<td>{{ $sale->product->name }}</td>
								</tr>

								<tr>
									<td>Invoice</td>
									<td>{{ $sale->invoice }}</td>
								</tr>
								<tr>
									<td>Unload Place</td>
									<td>{{ $sale->unload_place }}</td>
								</tr>

								<tr>
									<td>Product Factory</td>
									<td>{{ $sale->factory_name }}</td>
								</tr>
								<tr>
									<td>Rate</td>
									<td>{{ $sale->rate }}</td>
								</tr>
								<tr>
									<td>Quantity</td>
									<td>
										{{ $sale->quantity }}
									</td>
								</tr>
								<tr>
									<td>Amount</td>
									<td>{{ $sale->amount }}</td>
								</tr>
								<tr>
									<td>Vat</td>
									<td>{{ $sale->vat_amount }}</td>
								</tr>
								<tr>
									<td>Amount (with vat)</td>
									<td>{{ $sale->amount - $sale->vat_amount }}</td>
								</tr>
								<tr>
									<td>Remarks</td>
									<td>{{ $sale->details }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection