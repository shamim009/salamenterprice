@extends('layouts.master')
@section('title', 'Sale')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Sales</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item active">Sales</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <form method="POST" action="{{url('production/sale/report')}}" class="form-inline">
                        @csrf
                        <label for="inlineFormCustomSelect">Start Date </label>
                        <input type="text" name="start_date" id="start_date" class="form-control" placeholder="Start Date">
                        <label for="inlineFormCustomSelect">End Date </label>
                        <input type="text" name="end_date" id="end_date" class="form-control" placeholder="End Date">
                        <button type="submit" class="btn btn-info"><i class="fa fa-line-chart" aria-hidden="true"></i> Report</button>
                    </form>
                </div>
                <div class="card-body">
                    <div class="table-responsive">

                        @role('super_admin')
                        <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Date</th>                                   
                                    <th>Company</th>                                
                                    <th>Customer</th>
                                    <th>Customer</th>
                                    <th>Product</th>                            
                                    <th>Quantity</th>
                                    <th>Rate</th>
                                    <th>Vat</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th colspan="6" style="text-align:right">Total:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        @endrole
                        
                        @role('production_admin')
                        <table class="table table-bordered table-hover" id="adminDataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Date</th>
                                    <th>Customer</th>
                                    <th>Customer</th>
                                    <th>Product</th>                            
                                    <th>Quantity</th>
                                    <th>Rate</th>
                                    <th>Vat</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th colspan="5" style="text-align:right">Total:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        @endrole
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('script')
<script>
	//server side data table
    $(document).ready(function () {
        $('#dataTable').DataTable({
            "order": [[ 1, "desc" ]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('production/all-sales') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{ csrf_token() }}"}
            },
            "columns": [
            { "data": "id" },
            { "data": "date" },
            { "data": "company_id" },
            { "data": "customer_id" },
            { "data": "customer" },
            { "data": "product_id" },
            { "data": "quantity" },
            { "data": "rate" },
            { "data": "vat" },
            { "data": "amount" },
            { "data": "actions" },
            ],

            createdRow: function( row, data, dataIndex ) {
                //console.log(data);
                var url = "/production/customer/report/" + data['customer'];
                //console.log(url);
                // Set the data-status attribute, and add a class
                $( row ).addClass('clickable-row')
                    .click(function() {
                            window.location = url;
                        });
            },
            "columnDefs": [
                {
                    "targets": [ 4 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = json.totalQuantity;
                
                // Total over this page
                pageTotal = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 6 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                    );

                // Total over all pages
                amount = json.totalSum;
                
                // amount over this page
                pageamount = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 9 ).footer() ).html(
                    ''+pageamount +' ( '+ amount +' total)'
                );

                // Total over all pages
                vat = json.totalVat;
                
                // vat over this page
                pagevat = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 8 ).footer() ).html(
                    ''+pagevat +' ( '+ vat +' total)'
                );
            }	 
        });

        $('#adminDataTable').DataTable({
            "order": [[ 1, "desc" ]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('production/admin/all-sales') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{ csrf_token() }}"}
            },
            "columns": [
            { "data": "id" },
            { "data": "date" },
            { "data": "customer_id" },
            { "data": "customer" },
            { "data": "product_id" },
            { "data": "quantity" },
            { "data": "rate" },
            { "data": "vat" },
            { "data": "amount" },
            { "data": "actions" }
            ],

            createdRow: function( row, data, dataIndex ) {
                //console.log(data);
                var url = "/production/customer/report/" + data['customer'];
                //console.log(url);
                // Set the data-status attribute, and add a class
                $( row ).addClass('clickable-row')
                    .click(function() {
                            window.location = url;
                        });
            },
            "columnDefs": [
                {
                    "targets": [ 3 ],
                    "visible": false,
                    "searchable": false
                }
            ],

            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = json.totalQuantity;
                
                // Total over this page
                pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 5 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                    );

                // Total over all pages
                amount = json.totalSum;
                
                // amount over this page
                pageamount = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 8 ).footer() ).html(
                    ''+pageamount +' ( '+ amount +' total)'
                );

                // Total over all pages
                vat = json.totalVat;
                
                // vat over this page
                pagevat = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 7 ).footer() ).html(
                    ''+pagevat +' ( '+ vat +' total)'
                );
            }   
        });
    });

    //delete a row using ajax
    $('#dataTable').on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_method: 'DELETE', submit: true}
            }).always(function (data) {
                $('#dataTable').DataTable().draw(false);
            });
        }
    });

    $('#adminDataTable').on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_method: 'DELETE', submit: true}
            }).always(function (data) {
                $('#adminDataTable').DataTable().draw(false);
            });
        }
    });

    $( function() {
        $( "#start_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });

        $( "#end_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });
    });
</script>
@endsection

