@extends('layouts.master')
@section('title', 'Sale')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-8">
					<h1 class="m-0 text-dark">Sales to {{ $customer->name }} from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</h1>
				</div><!-- /.col -->
				<div class="col-sm-4">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Sales</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<form method="POST" action="{{url('production/sale/invoice/print')}}" class="d-inline">
						@csrf
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<input type="hidden" name="customer_id" id="customer_id" class="form-control" value="{{ $customer->id }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i>Print</button>
					</form>

					<form method="POST" action="{{url('production/sale/invoice/pdf')}}" class="d-inline">
						@csrf
						<input type="hidden" name="start_date" id="start_date" class="form-control" value="{{ $start_date }}">
						<input type="hidden" name="end_date" id="end_date" class="form-control" value="{{ $end_date }}">
						<input type="hidden" name="customer_id" id="customer_id" class="form-control" value="{{ $customer->id }}">
						<button type="submit" class="btn btn-info"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</button>
					</form>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						@role('super_admin')
						<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>									
									<th>Company</th>								
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Vat</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($sales as $sale)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $sale->date->format("d-m-Y") }}</td>
									<td>{{ $sale->company->name }}</td>
									<td>{{ $sale->product->name }}</td>
									<td>{{ $sale->quantity }}</td>
									<td>{{ $sale->rate }}</td>
									<td>{{ $sale->vat_amount }}</td>
									<td>{{ $sale->amount - $sale->vat_amount }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="4"></th>
									<th>{{ $quantity }}</th>
									<th></th>
									<th>{{ $vat_amount }}</th>
									<th>{{ $amount }}</th>
								</tr>
							</tfoot>
						</table>
						@endrole
						
						@role('production_admin')
						<table class="table table-bordered table-hover" id="adminDataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Vat</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($sales as $sale)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $sale->date->format("d-m-Y") }}</td>
									<td>{{ $sale->product->name }}</td>
									<td>{{ $sale->quantity }}</td>
									<td>{{ $sale->rate }}</td>
									<td>{{ $sale->vat_amount }}</td>
									<td>{{ $sale->amount - $sale->vat_amount }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="3"></th>
									<th>{{ $quantity }}</th>
									<th></th>
									<th>{{ $vat_amount }}</th>
									<th>{{ $amount }}</th>
								</tr>
							</tfoot>
						</table>
						@endrole
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection
