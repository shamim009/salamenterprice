<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }} | Sale Report</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link href="{{ asset('css/adminlte.min.css') }}" rel="stylesheet">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body onload="window.print();">
  <div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> 
            @if(Auth::user()->company)
            {{ Auth::user()->company->name }}
            @else
            {{ config('app.name', 'Laravel') }}
            @endif
            <small class="float-right">Date: {{ $date }}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <br>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-8 invoice-col">
          <h4><strong>Production Report from {{ Carbon\Carbon::parse($start_date)->format("d-m-Y") }} to {{ Carbon\Carbon::parse($end_date)->format("d-m-Y") }}</strong></h4>
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-12 table-responsive">
          @role('super_admin')
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>                 
                <th>Company</th>                
                <th>Product</th>              
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody>
              @foreach($productions as $production)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $production->date->format("d-m-Y") }}</td>
                <td>{{ $production->company->name }}</td>
                <td>{{ $production->product->name }}</td>
                <td>{{ $production->quantity }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="4"></th>
                <th>{{ $quantity }}</th>
              </tr>
            </tfoot>
          </table>
          @endrole
          @role('production_admin')
          <table class="table table-striped">
            <thead>
              <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Product</th>              
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody>
              @foreach($productions as $production)
              <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $production->date->format("d-m-Y") }}</td>
                <td>{{ $production->product->name }}</td>
                <td>{{ $production->quantity }}</td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="3"></th>
                <th>{{ $quantity }}</th>
              </tr>
            </tfoot>
          </table>
          @endrole
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- ./wrapper -->
</body>
</html>
