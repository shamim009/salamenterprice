@extends('layouts.master')
@section('title', 'Purchase Order')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Add Purchase Order</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('production/purchase') }}">Purchases</a></li>
						<li class="breadcrumb-item active">Order</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<form method="POST" action="{{ route('production.purchase-order.store') }}">
						@csrf
						{{-- Date --}}
						<div class="form-group">
							<label for="date">
								Date
							</label>

							<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ old('date') }}">

							@if ($errors->has('date'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('date') }}</strong>
							</span>
							@endif
						</div>

						{{-- supplier --}}
						<div class="form-group">
							<label for="supplier_selection">Supplier</label>
							<select name="supplier_selection" class="form-control{{ $errors->has('supplier_selection') ? ' is-invalid' : '' }}" id="supplier_selection">
								<option value="">Select</option>
								<option value="1" @if( old('supplier_selection') == 1 ) selected @endif>Old Supplier</option>
								<option value="2" @if( old('supplier_selection') == 2 ) selected @endif>New Supplier</option>
							</select>
							@if ($errors->has('supplier_selection'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('supplier_selection') }}</strong>
							</span>
							@endif
						</div>
						<div id="supplier_info"></div>

						{{-- Invoice --}}
						<div class="form-group">
							<label for="invoice">
								Invoice
							</label>

							<input type="text" class="form-control{{ $errors->has('invoice') ? ' is-invalid' : '' }}" name="invoice" id="invoice" value="{{ old('invoice') }}">

							@if ($errors->has('invoice'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('invoice') }}</strong>
							</span>
							@endif
						</div>

						<div class="row">
							<div class="col-md-1">
								<button id="add_more" class="btn btn-info mt-4"><i class="fa fa-plus" title="Add More Product"></i></button>
							</div>
							<div class="col-md-11">
								<div id="more_product">
									<div class="row">
										<div class="col-md-6">
											{{-- Product --}}
											<div class="form-group">
												<label for="raw_material_id">Raw Material</label>
												<select name="raw_material_id[]" class="form-control" id="raw_material_id" required="required">
													<option value="">Select</option>
													@forelse($rawmaterials as $rawmaterial)
													<option value="{{ $rawmaterial->id }}" >
														{{ $rawmaterial->name }}
													</option>
													@empty
													<option value="">No Raw Material Found</option>
													@endforelse
												</select>
											</div>
										</div>

										<div class="col-md-6">
											{{-- Quantity --}}
											<div class="form-group">
												<label for="quantity">
													Quantity
												</label>

												<input type="number" min="0" step="any" class="form-control" name="quantity[]" id="quantity" required="required">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						{{-- Details --}}
						<div class="form-group">
							<label for="details">
								Remarks
							</label>

							<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details') }}</textarea>

							@if( $errors->has('details'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('details') }}</strong>
							</span>
							@endif
						</div>

						{{-- Save --}}
						<div class="form-group row mb-0">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									{{ __('Save') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	{{-- jquery datepicker --}}
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});

	$('#raw_material_id').select2({
		placeholder: 'Select',

		ajax: {
			url: '{!!URL::route('production.raw-material-autocomplete-search')!!}',
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {
					results: data
				};
			},
			cache: true
		},
		theme: "bootstrap"
	});

	$('#supplier_selection').on('change', function(){
		var supplier_selection = $('#supplier_selection').val();
		if (supplier_selection == 1) {
			$('#supplier_info').html('');
			supplier_info = '';
			supplier_info += '<div class="form-group"><label for="supplier_id">Select Supplier</label><select name="supplier_id" class="form-control{{ $errors->has('supplier_id') ? ' is-invalid' : '' }}" id="supplier_id"> <option value="">Select supplier</option> @forelse($suppliers as $supplier) <option value="{{ $supplier->id }}" @if( old('supplier_id') == $supplier->id ) selected @endif> {{ $supplier->name }} </option> @empty <option value="">No supplier Found</option> @endforelse </select> @if ($errors->has('supplier_id')) <span class="invalid-feedback"> <strong>{{ $errors->first('supplier_id') }}</strong> </span> @endif </div>';
			$('#supplier_info').html(supplier_info);
			$('#supplier_id').select2({
				placeholder: 'Select Supplier',

				ajax: {
					url: '{!!URL::route('production.supplier-autocomplete-search')!!}',
					dataType: 'json',
					delay: 250,
					processResults: function (data) {
						return {
							results: data
						};
					},
					cache: true
				},
				theme: "bootstrap"
			});
		}

		else if (supplier_selection == 2) {
			$('#supplier_info').html('');
			supplier_info = '';
			supplier_info += '{{-- Name --}}<div class="form-group"><label for="name">Name</label><input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name') }}" autofocus required="required">@if ($errors->has('name'))<span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>@endif</div>{{-- Mobile --}}<div class="form-group"><label for="mobile">Mobile</label><input type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" id="mobile" value="{{ old('mobile') }}" required="required">@if ($errors->has('mobile'))<span class="invalid-feedback"><strong>{{ $errors->first('mobile') }}</strong></span>@endif</div>{{-- Address --}}<div class="form-group"><label for="address">Address</label><textarea name="address" class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" cols="30" rows="5">{{ old('address') }}</textarea>@if( $errors->has('address'))<span class="invalid-feedback"><strong>{{ $errors->first('address') }}</strong></span>@endif</div>';
			$('#supplier_info').html(supplier_info);
		}
		else{
			$('#supplier_info').html('');
		}
	});

	$(document).ready(function() {
		var max_fields      = 150;
		var wrapper         = $("#more_product");
		var add_button      = $("#add_more");

		var x = 1;
		$(add_button).click(function(e){
			e.preventDefault();
			if(x < max_fields){
				x++;
				$(wrapper).append('<div class="row"><div class="col-md-6">{{-- Product --}}<div class="form-group"><label for="raw_material_id">Select Product</label><select name="raw_material_id[]" class="form-control raw_material_id" id="raw_material_id" required="required"><option value="">Select</option>@forelse($rawmaterials as $rawmaterial)<option value="{{ $rawmaterial->id }}" >{{ $rawmaterial->name }}</option>@empty<option value="">No Raw Material Found</option>@endforelse </select></div></div><div class="col-md-5">{{-- Quantity --}}<div class="form-group"><label for="quantity">Quantity</label><input type="number" min="0" step="any" class="form-control" name="quantity[]" id="quantity" required="required"></div></div><div class="col-sm-1"><a href="#" class="remove_field"><button style="margin-top: 30px;" class="btn btn-info"><i class="fa fa-minus" title="Remove Item"></i></button></a></div></div>');

				$('.raw_material_id').select2({
					placeholder: 'Select Product',

					ajax: {
						url: '{!!URL::route('production.raw-material-autocomplete-search')!!}',
						dataType: 'json',
						delay: 250,
						processResults: function (data) {
							return {
								results: data
							};
						},
						cache: true
					},
					theme: "bootstrap"
				});
			}
		});

		$(wrapper).on("click",".remove_field", function(e){
			e.preventDefault(); 
			$(this).parent().parent('div').remove(); 
			x--;
		})
	});
</script>
@endsection