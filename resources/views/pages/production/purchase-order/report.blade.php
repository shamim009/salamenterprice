@extends('layouts.master')
@section('title', 'Purchase Delivery')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Delivery Report</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('production/purchase-order') }}">Purchase Orders</a></li>
						<li class="breadcrumb-item active">Delivery Report</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card">
				<div class="card-header">
					<p>Remaining Quantity: {{ $order->quantity - $delivered }}</p>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						@role('super_admin')
						<table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>									
									<th>Company</th>								
									<th>Supplier</th>
									<th>Raw Material</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@forelse($purchases as $purchase)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $purchase->date->format("d-m-Y") }}</td>
									<td>{{ $purchase->company->name }}</td>
									<td>{{ $purchase->supplier->name }}</td>
									<td>{{ $purchase->rawmaterial->name }}</td>				 <td>{{ $purchase->quantity }}</td>
									<td>{{ $purchase->rate }}</td>
									<td>{{ $purchase->amount }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('production.purchase.edit', $purchase->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<a href="{{ route('production.purchase.show', $purchase->id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="View">
												View
											</a>
											<form action="{{ route('production.purchase.destroy', $purchase->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
							<tfoot>
								<tr>
									<th colspan="5" style="text-align:right">Total:</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>	
						</table>
						@endrole
						
						@role('production_admin')
						<table class="table table-bordered table-hover" id="adminDataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>
									<th>Supplier</th>
									<th>Raw Material</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@forelse($purchases as $purchase)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $purchase->date->format("d-m-Y") }}</td>
									<td>{{ $purchase->supplier->name }}</td>
									<td>{{ $purchase->rawmaterial->name }}</td>				 <td>{{ $purchase->quantity }}</td>
									<td>{{ $purchase->rate }}</td>
									<td>{{ $purchase->amount }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('production.purchase.edit', $purchase->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<a href="{{ route('production.purchase.show', $purchase->id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="View">
												View
											</a>
											<form action="{{ route('production.purchase.destroy', $purchase->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
							<tfoot>
								<tr>
									<th colspan="4" style="text-align:right">Total:</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>	
						</table>
						@endrole
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	//server side data table
	$(document).ready(function () {
		$('#dataTable').DataTable({
			"footerCallback": function ( row, data, start, end, display ) {
				var api = this.api(), data;

				var json = api.ajax.json();

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                	return typeof i === 'string' ?
                	i.replace(/[\$,]/g, '')*1 :
                	typeof i === 'number' ?
                	i : 0;
                };
                
                // Total over all pages
                total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // Total over this page
                pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 5 ).footer() ).html(
                	''+pageTotal +' ( '+ total +' total)'
                	);

                // Total over all pages
                amount = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // amount over this page
                pageamount = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 7 ).footer() ).html(
                	''+pageamount +' ( '+ amount +' total)'
                	);
            }   
        });

        $('#adminDataTable').DataTable({
        	"footerCallback": function ( row, data, start, end, display ) {
				var api = this.api(), data;

				var json = api.ajax.json();

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                	return typeof i === 'string' ?
                	i.replace(/[\$,]/g, '')*1 :
                	typeof i === 'number' ?
                	i : 0;
                };
                
                // Total over all pages
                total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // Total over this page
                pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 4 ).footer() ).html(
                	''+pageTotal +' ( '+ total +' total)'
                	);

                // Total over all pages
                amount = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // amount over this page
                pageamount = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 6 ).footer() ).html(
                	''+pageamount +' ( '+ amount +' total)'
                	);
            }
        });
	});
</script>
@endsection