@extends('layouts.master')
@section('title', 'Company')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Update Company</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('company') }}">Companies</a></li>
						<li class="breadcrumb-item active">Update</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<form method="POST" action="{{ route('company.update', $company->id) }}">
						@csrf
						@method('PUT')

						<div class="form-group">
							<label for="parent_business">Parent Business Type</label>
							<select name="parent_business" class="form-control{{ $errors->has('parent_business') ? ' is-invalid' : '' }}" id="parent_business" disabled="disabled">
								<option value="">Select</option>
								<option value="1" {{old('parent_business', optional($company)->parent_business) =='1' ? 'selected':''}}>Regular Business</option>
								<option value="2" {{old('parent_business', optional($company)->parent_business) =='2' ? 'selected':''}}>Export Import Business</option>
								<option value="3" {{old('parent_business', optional($company)->parent_business) =='3' ? 'selected':''}}>Production Business</option>
							</select>
							@if ($errors->has('parent_business'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('parent_business') }}</strong>
							</span>
							@endif
						</div>

						{{-- Name --}}
						<div class="form-group">
							<label for="name">
								Name
							</label>

							<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name', optional($company)->name) }}">

							@if ($errors->has('name'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>

						{{-- Address --}}
						<div class="form-group">
							<label for="address">
								Address
							</label>

							<textarea name="address" class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" cols="30" rows="5">{{ old('address', optional($company)->address) }}</textarea>

							@if( $errors->has('address'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
							@endif
						</div>

						{{-- Initial Investment --}}
						<div class="form-group">
							<label for="initial_amount">
								Initial Investment
							</label>

							<input type="number" min="0" step="any" class="form-control{{ $errors->has('initial_amount') ? ' is-invalid' : '' }}" name="initial_amount" id="initial_amount" value="{{ old('initial_amount', optional($company)->initial_amount) }}">

							@if( $errors->has('initial_amount'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('initial_amount') }}</strong>
							</span>
							@endif
						</div>

						{{-- Save --}}
						<div class="form-group row mb-0">
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary">
									{{ __('Save') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection