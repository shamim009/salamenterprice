<h5>Company Info</h5>
{{-- Parent Business Type --}}

<div class="form-group">
	<label for="parent_business">Parent Business Type</label>
	<select name="parent_business" class="form-control{{ $errors->has('parent_business') ? ' is-invalid' : '' }}" id="parent_business">
		<option value="">Select</option>
		<option value="1" {{old('parent_business', optional($company)->parent_business) =='1' ? 'selected':''}}>Regular Business</option>
		<option value="2" {{old('parent_business', optional($company)->parent_business) =='2' ? 'selected':''}}>Export Import Business</option>
		<option value="3" {{old('parent_business', optional($company)->parent_business) =='3' ? 'selected':''}}>Production Business</option>
	</select>
	@if ($errors->has('parent_business'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('parent_business') }}</strong>
	</span>
	@endif
</div>

{{-- Name --}}
<div class="form-group">
	<label for="name">
		Name
	</label>

	<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name', optional($company)->name) }}">

	@if ($errors->has('name'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('name') }}</strong>
		</span>
	@endif
</div>

{{-- Address --}}
<div class="form-group">
	<label for="address">
		Address
	</label>

	<textarea name="address" class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" cols="30" rows="5">{{ old('address', optional($company)->address) }}</textarea>

	@if( $errors->has('address'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('address') }}</strong>
		</span>
	@endif
</div>

{{-- Initial Investment --}}
<div class="form-group">
	<label for="initial_amount">
		Initial Investment
	</label>

	<input type="number" min="0" step="any" class="form-control{{ $errors->has('initial_amount') ? ' is-invalid' : '' }}" name="initial_amount" id="initial_amount" value="{{ old('initial_amount', optional($company)->initial_amount) }}">

	@if( $errors->has('initial_amount'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('initial_amount') }}</strong>
		</span>
	@endif
</div>

<h5>User Info</h5>

{{-- Name --}}
<div class="form-group">
	<label for="user_name">
		Name
	</label>

	<input type="text" class="form-control{{ $errors->has('user_name') ? ' is-invalid' : '' }}" name="user_name" id="user_name" value="{{ old('user_name') }}">

	@if ($errors->has('user_name'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('user_name') }}</strong>
		</span>
	@endif
</div>

{{-- Email --}}
<div class="form-group">
	<label for="email">
		Email
	</label>

	<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" value="{{ old('email') }}">

	@if ($errors->has('email'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('email') }}</strong>
		</span>
	@endif
</div>

{{-- password --}}
<div class="form-group">
	<label for="password">
		Password
	</label>

	<input type="text" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" value="{{ old('password') }}">

	@if ($errors->has('password'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('password') }}</strong>
		</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>