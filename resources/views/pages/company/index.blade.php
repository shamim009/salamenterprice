@extends('layouts.master')
@section('title', 'Companies')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Companies</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item active">Companies</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Sl</th>
									<th>Name</th>
									<th>Parent</th>
									<th>Address</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@forelse($companies as $company)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $company->name }}</td>
									@if($company->parent_business == 1)
									<td>{{ 'Regular Business' }}</td>
									@elseif($company->parent_business == 2)
									<td>{{ 'Export Business' }}</td>
									@else
									<td>{{ 'Production Business' }}</td>
									@endif
									<td>{{ $company->address }}</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('company.edit', $company->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<form action="{{ route('company.destroy', $company->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>

											<a href="{{ url('company/report', $company->id) }}" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Report">
												Report
											</a>
										</div>
									</td>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	//server side data table
	$(document).ready(function () {
		$('#dataTable').DataTable();
	});
</script>
@endsection