@extends('layouts.master')
@section('title', 'Company')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-8">
					<h4 class="m-0 text-dark">{{ $company->name }} Report From {{ $start_date }} To {{ $end_date }}</h4>
				</div><!-- /.col -->
				<div class="col-sm-4">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('/company') }}">Company</a></li>
						<li class="breadcrumb-item active">Report</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card card-warning">
				<div class="card-body">
					<div class="table-responsive">
						<h4>Sale Table</h4>
						<table class="table table-bordered table-hover" id="saleTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>									
									<th>Customer</th>								
									<th>Product</th>                            
                                    <th>Quantity</th>
                                    <th>Rate</th>
                                    <th>Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($sales as $sale)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $sale->date->format("d-m-Y") }}</td>
									<td>{{ $sale->customer->name }}</td>
									<td>{{ $sale->product->name }}</td>
									<td>{{ $sale->quantity }}</td>
									<td>{{ $sale->rate }}</td>
									<td>{{ $sale->amount - $sale->vat_amount }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4">Total:</td>
									<td>{{ $saleQuantity }}</td>
									<td></td>
									<td>{{ $saleAmount }}</td>
								</tr>
							</tfoot>
						</table>
						<br>
						<h4>Purchase Table</h4>
						<table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>SL</th>
									<th>Date</th>							
									<th>Supplier</th>
									<th>Product</th>							
									<th>Quantity</th>
									<th>Rate</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($purchases as $purchase)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $purchase->date->format("d-m-Y") }}</td>
									<td>{{ $purchase->supplier->name }}</td>
									<td>{{ $purchase->product->name }}</td>
									<td>{{ $purchase->quantity }}</td>
									<td>{{ $purchase->rate }}</td>
									<td>{{ $purchase->amount }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4">Total:</td>
									<td>{{ $purchaseQuantity }}</td>
									<td></td>
									<td>{{ $purchaseAmount }}</td>
								</tr>
							</tfoot>
						</table>
						<br>
						<h4>Expense Table</h4>
						<table class="table table-bordered table-hover" id="expenseTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Sl</th>
									<th>Date</th>
                                    <th>Expense Type</th>
                                    <th>Amount</th>
                                    <th>Remarks</th>
								</tr>
							</thead>
							<tbody>
								@foreach($expenses as $expense)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $expense->date->format("d-m-Y") }}</td>
									<td>{{ $expense->expenseitem->name }}</td>
									<td>{{ $expense->amount }}</td>
									<td>{{ $expense->details }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="3">Total:</td>
									<td>{{ $expenseAmount }}</td>
									<td></td>
								</tr>
							</tfoot>
						</table>
						<br>
						<h4>Profit Table</h4>
						<table class="table table-bordered table-hover" id="profitTable" width="100%" cellspacing="0">
							<tr>
								<td>Purchase</td>
								<td>{{ $purchaseAmount }}</td>
							</tr>
							<tr>
								<td>Purchase Quantity</td>
								<td>{{ $purchaseQuantity }}</td>
							</tr>
							<tr>
								<td>Sale</td>
								<td>{{ $saleAmount }}</td>
							</tr>
							<tr>
								<td>Sale Quantity</td>
								<td>{{ $saleQuantity }}</td>
							</tr>
							<tr>
								<td>Expense</td>
								<td>{{ $expenseAmount }}</td>
							</tr>
							<tr>
								<td>Stock</td>
								<td>{{ $finalStockAmount }}</td>
							</tr>

							<tr>
								<td>Stock Quantity</td>
								<td>{{ $purchaseQuantity - $saleQuantity }}</td>
							</tr>
							<tr>
								<td>Total</td>
								<td>{{ ($saleAmount + $finalStockAmount) - ($purchaseAmount + $expenseAmount) }}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	//server side data table
	$(document).ready(function () {
		$('#saleTable').DataTable({
			"order": [[ 1, "desc" ]],
		});

		$('#purchaseTable').DataTable({
			"order": [[ 1, "desc" ]],
		});

		$('#expenseTable').DataTable({
			"order": [[ 1, "desc" ]]	 
		});

		$('#profitTable').DataTable({
		});
	});
</script>
@endsection
