@extends('layouts.master')
@section('title', 'Loan')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">{{ $loan->loan_from }} Loan Report</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('loan') }}">Loans</a></li>
						<li class="breadcrumb-item active">Report</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					{{-- <a href="{{ url('bank-account/report/print', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Print" target="_blank">
						<i class="fa fa-print" aria-hidden="true" title="Print"></i> Print
					</a>

					<a href="{{ url('bank-account/report/pdf', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="PDF" target="_blank">
						<i class="fa fa-file-pdf-o" aria-hidden="true" title="PDF"></i> PDF
					</a>

					<a href="{{ url('bank-account/report/excel', $id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Excel" target="_blank">
						<i class="fa fa-file-excel-o" aria-hidden="true" title="Excel"></i> Excel
					</a> --}}
				</div>

				<div class="card-body">
					<div class="table-responsive">
						<h6>Profits Table</h6>
						<table class="table table-bordered" id="profitTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Sl</th>
									<th>Month</th>
									<th>Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($profits as $profit)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ date('F, Y', strtotime($profit->date)) }}</td>
									<td>{{ $profit->amount }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="2" style="text-align:right">Total:</th>
									<th></th>
								</tr>
							</tfoot>
						</table>

						<h6>Profit Return Table</h6>
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Sl</th>
									<th>Date</th>
									<th>Amount</th>
									<th>Payment Mode</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($profitreturns as $profitreturn)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $profitreturn->date->format("d-m-Y") }}</td>
									<td>{{ $profitreturn->amount }}</td>
									<td>
										@if($profitreturn->payment_mode == 1)
										Hand Cash
										@elseif($profitreturn->payment_mode == 2)
										Regular Banking
										@else
										Mobile Banking
										@endif
									</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('loan-transaction.edit', $profitreturn->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<form action="{{ route('loan-transaction.destroy', $profitreturn->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="2" style="text-align:right">Total:</th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>

						<h6>Main Amount Return Table</h6>
						<table class="table table-bordered" id="mainDataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Sl</th>
									<th>Date</th>
									<th>Amount</th>
									<th>Payment Mode</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($mainreturns as $mainreturn)
								<tr>
									<td>{{ $loop->iteration }}</td>
									<td>{{ $mainreturn->date->format("d-m-Y") }}</td>
									<td>{{ $mainreturn->amount }}</td>
									<td>
										@if($mainreturn->payment_mode == 1)
										Hand Cash
										@elseif($mainreturn->payment_mode == 2)
										Regular Banking
										@else
										Mobile Banking
										@endif
									</td>
									<td>
										<div class="btn-group">
											<a href="{{ route('loan-transaction.edit', $mainreturn->id) }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
												Update
											</a>
											<form action="{{ route('loan-transaction.destroy', $mainreturn->id) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger btn-sm">Delete</button>
											</form>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="2" style="text-align:right">Total:</th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('script')
<script>
	//server side data table
    $(document).ready(function () {
        $('#dataTable').DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // Total over this page
                pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 2 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                    );
            }	 
        });

        $('#mainDataTable').DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // Total over this page
                pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 2 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                    );
            }    
        });

        $('#profitTable').DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                	return intVal(a) + intVal(b);
                }, 0);
                
                // Total over this page
                pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 2 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                    );
            }    
        });
    });
</script>
@endsection
