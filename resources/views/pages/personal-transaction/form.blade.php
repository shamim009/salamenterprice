{{-- Date --}}
<div class="form-group">
	<label for="date">
		Date
	</label>

	<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ old('date', optional($personalTransaction)->date) }}">

	@if ($errors->has('date'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('date') }}</strong>
	</span>
	@endif
</div>

{{-- Supplier --}}
<div class="form-group">
	<label for="purpose">Select Purpose</label>
	<select name="purpose" class="form-control{{ $errors->has('purpose') ? ' is-invalid' : '' }}" id="purpose">
		<option value="">Select</option>
		<option value="1" {{old('purpose', optional($personalTransaction)->purpose) =='1' ? 'selected':''}}>Income</option>
		<option value="2" {{old('purpose', optional($personalTransaction)->purpose) =='2' ? 'selected':''}}>Expense</option>
	</select>
	@if ($errors->has('purpose'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('purpose') }}</strong>
	</span>
	@endif
</div>

<div id="income_div">
	{{-- received_from --}}
	<div class="form-group">
		<label for="received_from">
			Received From
		</label>

		<input type="text" class="form-control{{ $errors->has('received_from') ? ' is-invalid' : '' }}" name="received_from" id="received_from" value="{{ old('received_from', optional($personalTransaction)->received_from) }}">

		@if ($errors->has('received_from'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('received_from') }}</strong>
		</span>
		@endif
	</div>
</div>

<div id="expense_div">
	{{-- expense_for --}}
	<div class="form-group">
		<label for="expense_for">Expense For</label>
		<input type="text" class="form-control{{ $errors->has('expense_for') ? ' is-invalid' : '' }}" name="expense_for" id="expense_for" value="{{ old('expense_for', optional($personalTransaction)->expense_for) }}">

		@if ($errors->has('expense_for'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('expense_for') }}</strong>
		</span>
		@endif
	</div>
</div>

{{-- name --}}
<div class="form-group">
	<label for="amount">
		Amount
	</label>

	<input type="text" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" id="amount" value="{{ old('amount', optional($personalTransaction)->amount) }}">

	@if ($errors->has('amount'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('amount') }}</strong>
	</span>
	@endif
</div>

{{-- details --}}
<div class="form-group">
	<label for="details">Remarks</label>
	<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($personalTransaction)->details) }}</textarea>

	@if ($errors->has('details'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('details') }}</strong>
	</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>

@section('script')
<script>
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});
	//show or hide banking and mobile banking section on page load
	$(document).ready(function(){
		var type = $('#purpose').val();
		if (type == 1) {
			$('#expense_div').hide();
			$('#income_div').show();
		}

		else if (type == 2) {
			$('#income_div').hide();
			$('#expense_div').show();
		}
		else{
			$('#income_div').hide();
			$('#expense_div').hide();
		}
	});

	$('#purpose').on('change', function(){
		var type = $('#purpose').val();
		if (type == 1) {
			$('#expense_div').hide();
			$('#income_div').show();
		}

		else if (type == 2) {
			$('#income_div').hide();
			$('#expense_div').show();
		}
		else{
			$('#income_div').hide();
			$('#expense_div').hide();
		}
	});
</script>
@endsection