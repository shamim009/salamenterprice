{{-- Supplier --}}
<div class="form-group">
	<label for="type">Select Type</label>
	<select name="type" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" id="type">
		<option value="">Select</option>
		<option value="1" {{old('type', optional($index)->type) =='1' ? 'selected':''}}>Person</option>
		<option value="2" {{old('type', optional($index)->type) =='2' ? 'selected':''}}>Company</option>
	</select>
	@if ($errors->has('type'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('type') }}</strong>
	</span>
	@endif
</div>

{{-- name --}}
<div class="form-group">
	<label for="name">
		Name
	</label>

	<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ old('name', optional($index)->name) }}">

	@if ($errors->has('name'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('name') }}</strong>
	</span>
	@endif
</div>

{{-- Address --}}
<div class="form-group">
	<label for="address">Address</label>
	<textarea name="address" class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" cols="30" rows="5">{{ old('address', optional($index)->address) }}</textarea>

	@if ($errors->has('address'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('address') }}</strong>
	</span>
	@endif
</div>
<div id="mobile_div">
	{{-- mobile --}}
	<div class="form-group">
		<label for="mobile">
			Mobile
		</label>

		<input type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" id="mobile" value="{{ old('mobile', optional($index)->mobile) }}">

		@if ($errors->has('mobile'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('mobile') }}</strong>
		</span>
		@endif
	</div>
</div>

<div id="contacts_div">
	{{-- contacts --}}
	<div class="form-group">
		<label for="contacts">Contacts</label>
		<textarea name="contacts" class="form-control {{ $errors->has('contacts') ? ' is-invalid' : '' }}" id="contacts" cols="30" rows="5">{{ old('contacts', optional($index)->contacts) }}</textarea>

		@if ($errors->has('contacts'))
		<span class="invalid-feedback">
			<strong>{{ $errors->first('contacts') }}</strong>
		</span>
		@endif
	</div>
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>

@section('script')
<script>
	//show or hide banking and mobile banking section on page load
	$(document).ready(function(){
		var type = $('#type').val();
		if (type == 1) {
			$('#contacts_div').hide();
			$('#mobile_div').show();
		}

		else if (type == 2) {
			$('#mobile_div').hide();
			$('#contacts_div').show();
		}
		else{
			$('#mobile_div').hide();
			$('#contacts_div').hide();
		}
	});

	$('#type').on('change', function(){
		var type = $('#type').val();
		if (type == 1) {
			$('#contacts_div').hide();
			$('#mobile_div').show();
		}

		else if (type == 2) {
			$('#mobile_div').hide();
			$('#contacts_div').show();
		}
		else{
			$('#mobile_div').hide();
			$('#contacts_div').hide();
		}
	});
</script>
@endsection