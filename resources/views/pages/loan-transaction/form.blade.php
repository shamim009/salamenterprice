{{-- Date --}}
<div class="form-group">
	<label for="date">
		Date
	</label>

	<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ old('date', optional($loanTransaction)->date) }}">

	@if ($errors->has('date'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('date') }}</strong>
	</span>
	@endif
</div>

{{-- Loan --}}
<div class="form-group">
	<label for="loan_id">Select Loan</label>
	<select name="loan_id" class="form-control{{ $errors->has('loan_id') ? ' is-invalid' : '' }}" id="loan_id">
		<option value="">Select Loan</option>
		@forelse($loans as $loan)
		<option value="{{ $loan->id }}" 
			@if( old('loan_id', optional($loanTransaction)->loan_id) == $loan->id )
			selected
			@endif
			>
			{{ $loan->loan_from.', '.$loan->amount }}
		</option>
		@empty
		<option value="">No Loan Found</option>
		@endforelse
	</select>
	@if ($errors->has('loan_id'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('loan_id') }}</strong>
	</span>
	@endif
</div>

{{-- Amount --}}
<div class="form-group">
	<label for="amount">
		Amount
	</label>

	<input type="number" min="0" step="any" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" id="amount" value="{{ old('amount', optional($loanTransaction)->amount) }}">

	@if ($errors->has('amount'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('amount') }}</strong>
	</span>
	@endif
</div>

{{-- Payment Type --}}

<div class="form-group">
	<label for="purpose">Purpose</label>
	<select name="purpose" class="form-control{{ $errors->has('purpose') ? ' is-invalid' : '' }}" id="purpose">
		<option value="">Select</option>
		<option value="1" {{old('purpose', optional($loanTransaction)->purpose) =='1' ? 'selected':''}}>Profit Return</option>
		<option value="2" {{old('purpose', optional($loanTransaction)->purpose) =='2' ? 'selected':''}}>Main Amount Return</option>
	</select>
	@if ($errors->has('purpose'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('purpose') }}</strong>
	</span>
	@endif
</div>

{{-- Payment Mode --}}

<div class="form-group">
	<label for="payment_mode">Payment Mode</label>
	<select name="payment_mode" class="form-control{{ $errors->has('payment_mode') ? ' is-invalid' : '' }}" id="payment_mode">
		<option value="">Select</option>
		<option value="1" {{old('payment_mode', optional($loanTransaction)->payment_mode) =='1' ? 'selected':''}}>Hand Cash</option>
		<option value="2" {{old('payment_mode', optional($loanTransaction)->payment_mode) =='2' ? 'selected':''}}>Regular Banking</option>
		<option value="3" {{old('payment_mode', optional($loanTransaction)->payment_mode) =='3' ? 'selected':''}}>Mobile Banking</option>
	</select>
	@if ($errors->has('payment_mode'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('payment_mode') }}</strong>
	</span>
	@endif
</div>

{{-- Banking Section --}}
<div id="banking_section">
	<div class="form-group">
		<label for="bank_id">Select Bank</label>
		<select name="bank_id" class="form-control{{ $errors->has('bank_id') ? ' is-invalid' : '' }}" id="bank_id">
			<option value="">Select Bank</option>
			@forelse($banks as $bank) 
			<option value="{{ $bank->id }}" @if( old('bank_id', optional($loanTransaction)->bank_id) == $bank->id ) selected @endif> {{ $bank->name }} 
			</option> 
			@empty 
			<option value="">No Bank Found</option> 
			@endforelse
		</select> 
		@if ($errors->has('bank_id'))
			<span class="invalid-feedback">
				<strong>{{ $errors->first('bank_id') }}</strong> 
			</span> 
		@endif 
	</div> 
	{{-- account number --}}
	<div class="form-group">
		<label for="bank_account_number">Account Number</label>
		<input type="text" class="form-control{{ $errors->has('bank_account_number') ? ' is-invalid' : '' }}" name="bank_account_number" id="bank_account_number" value="{{ old('bank_account_number', optional($loanTransaction)->bank_account_number) }}">
		@if ($errors->has('bank_account_number'))
			<span class="invalid-feedback">
				<strong>{{ $errors->first('bank_account_number') }}</strong>
			</span>
		@endif
	</div>
	<div class="form-group">
		<label for="cheque_number">Cheque Number</label>
		<input type="text" class="form-control{{ $errors->has('cheque_number') ? ' is-invalid' : '' }}" name="cheque_number" id="cheque_number" value="{{ old('cheque_number', optional($loanTransaction)->cheque_number) }}">
		@if ($errors->has('cheque_number'))
			<span class="invalid-feedback">
				<strong>{{ $errors->first('cheque_number') }}</strong>
			</span>
		@endif
	</div>	
</div>

{{-- Mobile Banking Section --}}
<div id="mobile_banking_section">
	<div class="form-group">
		<label for="mobile_banking_id">Select Mobile Banking</label>
		<select name="mobile_banking_id" class="form-control{{ $errors->has('mobile_banking_id') ? ' is-invalid' : '' }}" id="mobile_banking_id">
			<option value="">Select Mobile Banking</option> 
			@forelse($mobileBankings as $mobileBanking) 
			<option value="{{ $mobileBanking->id }}" @if( old('mobile_banking_id', optional($loanTransaction)->mobile_banking_id) == $mobileBanking->id ) selected @endif> 
				{{ $mobileBanking->name }} 
			</option> 
			@empty 
			<option value="">No Mobile Banking Found</option> 
			@endforelse 
		</select> 
		@if ($errors->has('mobile_banking_id')) 
			<span class="invalid-feedback"> 
				<strong>{{ $errors->first('mobile_banking_id') }}</strong> 
			</span> 
		@endif 
	</div> 
	{{-- phone number --}}
	<div class="form-group">
		<label for="phone_number">Phone  Number</label>
		<input type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" id="phone_number" value="{{ old('phone_number', optional($loanTransaction)->phone_number) }}">
		@if ($errors->has('phone_number'))
			<span class="invalid-feedback">
				<strong>{{ $errors->first('phone_number') }}</strong>
			</span>
		@endif
	</div>
</div>

{{-- Receiver --}}
<div class="form-group">
	<label for="receiver">
		Receiver
	</label>

	<input type="text" class="form-control{{ $errors->has('receiver') ? ' is-invalid' : '' }}" name="receiver" id="receiver" value="{{ old('receiver', optional($loanTransaction)->receiver) }}">

	@if ($errors->has('receiver'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('receiver') }}</strong>
	</span>
	@endif
</div>

{{-- Remarks --}}
<div class="form-group">
	<label for="details">Remarks</label>
	<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($loanTransaction)->details) }}</textarea>

	@if ($errors->has('details'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('details') }}</strong>
	</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>

@section('script')
<script>
	{{-- jquery datepicker --}}
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});

	//show or hide banking and mobile banking section on page load
	$(document).ready(function(){
		var payment_mode = $('#payment_mode').val();
		if (payment_mode == 2) {
			$('#mobile_banking_section').hide();
			$('#banking_section').show();
		}

		else if (payment_mode == 3) {
			$('#banking_section').hide();
			$('#mobile_banking_section').show();
		}
		else{
			$('#mobile_banking_section').hide();
			$('#banking_section').hide();
		}
	});

	$('#bank_id').select2({
		placeholder: 'Select Bank',

		ajax: {
			url: '{!!URL::route('bank-autocomplete-search')!!}',
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {
					results: data
				};
			},
			cache: true
		},
		theme: "bootstrap"
	});

	$('#payment_mode').on('change', function(){
		var payment_mode = $('#payment_mode').val();
		if (payment_mode == 2) {
			$('#mobile_banking_section').hide();
			$('#banking_section').show();
		}

		else if (payment_mode == 3) {
			$('#banking_section').hide();
			$('#mobile_banking_section').show();
		}
		else{
			$('#mobile_banking_section').hide();
			$('#banking_section').hide();
		}
	});
</script>
@endsection