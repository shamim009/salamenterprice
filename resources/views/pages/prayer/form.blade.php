{{-- Date --}}
<div class="form-group">
	<label for="date">
		Date
	</label>

	<input type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" id="date" value="{{ old('date', optional($prayer)->date) }}">

	@if ($errors->has('date'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('date') }}</strong>
	</span>
	@endif
</div>

{{-- Supplier --}}
<div class="form-group">
	<label for="prayer_name">Select Prayer</label>
	<select name="prayer_name" class="form-control{{ $errors->has('prayer_name') ? ' is-invalid' : '' }}" id="prayer_name">
		<option value="">Select</option>
		<option value="Fajr" {{old('prayer_name', optional($prayer)->prayer_name) =='Fajr' ? 'selected':''}}>Fajr</option>
		<option value="Zuhr" {{old('prayer_name', optional($prayer)->prayer_name) =='Zuhr' ? 'selected':''}}>Zuhr</option>
		<option value="Asr" {{old('prayer_name', optional($prayer)->prayer_name) =='Asr' ? 'selected':''}}>Asr</option>
		<option value="Maghrib" {{old('prayer_name', optional($prayer)->prayer_name) =='Maghrib' ? 'selected':''}}>Maghrib</option>
		<option value="Isha" {{old('prayer_name', optional($prayer)->prayer_name) =='Isha' ? 'selected':''}}>Isha</option>
	</select>
	@if ($errors->has('prayer_name'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('prayer_name') }}</strong>
	</span>
	@endif
</div>

{{-- Supplier --}}
<div class="form-group">
	<label for="status">Status</label>
	<select name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" id="status">
		<option value="0" {{old('status', optional($prayer)->status) =='0' ? 'selected':''}}>Pending</option>
		<option value="1" {{old('status', optional($prayer)->status) =='1' ? 'selected':''}}>Completed</option>
	</select>
	@if ($errors->has('status'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('status') }}</strong>
	</span>
	@endif
</div>

{{-- Address --}}
<div class="form-group">
	<label for="details">Remarks</label>
	<textarea name="details" class="form-control {{ $errors->has('details') ? ' is-invalid' : '' }}" id="details" cols="30" rows="5">{{ old('details', optional($prayer)->details) }}</textarea>

	@if ($errors->has('details'))
	<span class="invalid-feedback">
		<strong>{{ $errors->first('details') }}</strong>
	</span>
	@endif
</div>

{{-- Save --}}
<div class="form-group row mb-0">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary">
			{{ __('Save') }}
		</button>
	</div>
</div>

@section('script')
<script>
	$( function() {
		$( "#date" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
		});
	});
</script>
@endsection