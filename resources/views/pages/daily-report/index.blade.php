@extends('layouts.master')
@section('title', 'Home')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Report of {{ Carbon\Carbon::parse($date)->format("d-m-Y") }}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <form method="POST" action="{{url('business/daily-report')}}" class="form-inline">
                        @csrf
                        <label for="inlineFormCustomSelect">Start Date </label>
                        <input type="text" name="date" id="purchase_start_date" class="form-control" placeholder="Date">

                        <button type="submit" class="btn btn-info"><i class="fa fa-line-chart" aria-hidden="true"></i> Report</button>
                    </form>
                </div>

                <div class="card-body">
                    <div class="btn-group">
                        <a href="{{ url('business/purchase/create') }}"><button class="btn btn-primary btn-sm">Add Purchase</button></a>
                        <a href="{{ url('business/purchase/payment/create') }}"><button class="btn btn-secondary btn-sm ">Add Purchase Payment</button></a>
                        <a href="{{ url('business/purchase-payment-return/create') }}"><button class="btn btn-success btn-sm">Add Purchase Payment Return </button></a>
                        <a href="{{ url('business/sale/create') }}"><button class="btn btn-warning btn-sm">Add Sale</button></a>
                        <a href="{{ url('business/sale/payment/create') }}"><button class="btn btn-primary btn-sm">Add Sale Payment</button></a>
                        <a href="{{ url('business/sale-payment-return/create') }}"><button class="btn btn-secondary btn-sm">Add Sale Payment Return</button></a>
                        <a href="{{ url('business/expense/create') }}"><button class="btn btn-success btn-sm">Add Expense</button></a>
                        <a href="{{ url('business/expense-transaction/create') }}"><button class="btn btn-warning btn-sm">Add Expense Payment</button></a>
                    </div>

                    <div class="table-responsive">
                        <h4>Purchase Table</h4>
                        <table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Supplier</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($purchases as $purchase)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $purchase->date->format('d-m-Y') }}
                                    </td>
                                    <td>
                                        {{$purchase->supplier->name}}
                                    </td>
                                    <td>{{$purchase->product_id}}</td>
                                    <td>{{$purchase->quantity}}</td>
                                    <td>{{$purchase->rate}}</td>
                                    <td>{{$purchase->amount}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="4" style="text-align:right">Total:</th>
                                <th>{{$purchase_quantity}}</th>
                                <th></th>
                                <th>{{$purchase_amount}}</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h4>Purchase Payment Table</h4>
                        <table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Supplier</th>
                                <th>Amount</th>
                                <th>Payment Mood</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($purchase_payments as $purchase_payment)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $purchase_payment->date->format('d-m-Y') }}
                                    </td>
                                    <td>
                                        {{$purchase_payment->supplier->name}}
                                    </td>
                                    <td>{{$purchase_payment->amount}}</td>

                                    @if($purchase_payment->payment_mode==1)

                                            <td>Hand Cash</td>

                                    @elseif($purchase_payment->payment_mode==2)
                                    <td>Regular Banking</td>

                                    @else
                                    <td>Mobile Banking</td>

                                @endif
                                    <td>{{$purchase_payment->details}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3" style="text-align:right">Total:</th>
                                <th>{{$purchase_paid}}</th>
                                <th></th>
                                <th></th>

                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h4>Purchase Payment Returns Table</h4>
                        <table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Supplier</th>
                                <th>Amount</th>
                                <th>Payment Mood</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($purchase_returns as $purchase_return)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $purchase_return->date->format('d-m-Y') }}
                                    </td>
                                    <td>
                                        {{$purchase_return->supplier->name}}
                                    </td>
                                    <td>{{$purchase_return->amount}}</td>

                                    @if($purchase_return->payment_mode==1)

                                        <td>Hand Cash</td>

                                    @elseif($purchase_return->payment_mode==2)
                                        <td>Regular Banking</td>

                                    @else
                                        <td>Mobile Banking</td>

                                    @endif
                                    <td>{{$purchase_return->details}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3" style="text-align:right">Total:</th>
                                <th>{{ $purchase_returned }}</th>
                                <th></th>
                                <th></th>

                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h4>Sale Table</h4>
                        <table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Rate</th>
                                <th>Vat</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sale as $sales)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $sales->date->format('d-m-Y') }}
                                    </td>
                                    <td>
                                        {{$sales->customer->name}}
                                    </td>
                                    <td>{{$sales->product->name}}</td>
                                    <td>{{$sales->quantity}}</td>
                                    <td>{{$sales->rate}}</td>
                                    <td>{{$sales->vat}}</td>
                                    <td>{{$sales->amount - $sales->vat}}</td>
                                    

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="4" style="text-align:right">Total:</th>
                                <th>{{$sale_quantity}}</th>
                                <th></th>
                                <th>{{$totalVat}}</th>
                                <th>{{$sale_amount}}</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h4>Sale Payment Table</h4>
                        <table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Payment Mode</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sale_payments as $sale_payment)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $sale_payment->date->format('d-m-Y') }}
                                    </td>
                                    <td>
                                        {{$sale_payment->customer->name}}
                                    </td>
                                    <td>{{$sale_payment->amount}}</td>
                                    @if($sale_payment->payment_mode==1)

                                        <td>Hand Cash</td>

                                    @elseif($sale_payment->payment_mode==2)
                                        <td>Regular Banking</td>

                                    @else
                                        <td>Mobile Banking</td>

                                    @endif
                                    <td>{{$sale_payment->details}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3" style="text-align:right">Total:</th>
                                <th>{{$sale_paid}}</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h4>Sale Payment Return Table</h4>
                        <table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th>Payment Mode</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sale_returns as $sale_return)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $sale_return->date->format('d-m-Y') }}
                                    </td>
                                    <td>
                                        {{$sale_return->customer->name}}
                                    </td>
                                    <td>{{$sale_return->amount}}</td>
                                    @if($sale_return->payment_mode==1)

                                        <td>Hand Cash</td>

                                    @elseif($sale_return->payment_mode==2)
                                        <td>Regular Banking</td>

                                    @else
                                        <td>Mobile Banking</td>

                                    @endif
                                    <td>{{$sale_return->details}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3" style="text-align:right">Total:</th>
                                <th>{{$sale_returned}}</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h4>Expense Table</h4>
                        <table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Expense Type</th>
                                <th>Amount</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($expense as $expenses)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $expenses->date->format('d-m-Y') }}
                                    </td>
                                    <td>
                                        {{$expenses->expenseItem->name}}
                                    </td>
                                    <td>{{$expenses->amount}}</td>
                                    <td>{{$expenses->details}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3" style="text-align:right">Total:</th>
                                <th>{{$expense_amount}}</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <h4>Expense Payment Table</h4>
                        <table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Date</th>
                                <th>Expense Item</th>
                                <th>Amount</th>
                                <th>Payment Mood</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($expense_payments as $expense_payment)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        {{ $expense_payment->date->format('d-m-Y') }}
                                    </td>
                                    <td>
                                        {{$expense_payment->expenseItem->name}}
                                    </td>
                                    <td>{{$expense_payment->amount}}</td>
                                    @if($expense_payment->payment_mode==1)

                                        <td>Hand Cash</td>

                                    @elseif($expense_payment->payment_mode==2)
                                        <td>Regular Banking</td>

                                    @else
                                        <td>Mobile Banking</td>

                                    @endif
                                    <td>{{$expense_payment->details}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3" style="text-align:right">Total:</th>
                                <th>{{$expense_paid}}</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            </div>

        </div>
        <!-- Main content -->
    </div>
@endsection

@section('script')
    <script>

        $( function() {
            $( "#purchase_start_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
            });

            $( "#expense_end_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
            });
        });
    </script>

@endsection