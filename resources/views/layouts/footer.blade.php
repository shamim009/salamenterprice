 <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-sm-none d-md-block">
      
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ Carbon\Carbon::now()->year }} {{ config('app.name', 'Laravel') }}.</strong> All rights reserved.
</footer>