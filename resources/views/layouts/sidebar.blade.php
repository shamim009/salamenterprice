<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<div class="brand">
		{{-- <a href="{{ url('/') }}">
			<img src="{{ asset('images/brandimage.jpg') }}" class="" alt="{{ config('app.name', 'Laravel') }}">
		</a> --}}
	</div>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
			<!-- Add icons to the links using the .nav-icon class
				with font-awesome or any other icon font library -->
				@role('super_admin')
				<li class="nav-item">
					<a href="{{ url('/home') }}" class="nav-link">
						<i class="nav-icon fa fa-dashboard"></i>
						<p>
							Dashboard
						</p>
					</a>
				</li>

				<li class="{{ \Request::is('business/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Business
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/fiscal-year') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Fiscal Years</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/fiscal-year/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Fiscal Year</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/sale') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sales</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/sale-payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Payments</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/sale/invoice') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Invoice</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/purchase') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchases</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/purchase-payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchase Payments</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/expense') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expenses</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/expense-transaction') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expense Payments</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/product') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Products</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/customer') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Customesr</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/supplier') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Suppliers</p>
							</a>
						</li>					
					</ul>
				</li>

				<li class="{{ \Request::is('production/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Production
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/sale') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sales</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/sale-payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Payments</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/sale/invoice') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Invoice</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/purchase') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchases</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/purchase-payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchase Payments</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/expense') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expenses</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/expense-transaction') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expense Payments</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/product') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Products</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/customer') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Customesr</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/supplier') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Suppliers</p>
							</a>
						</li>					
					</ul>
				</li>


				<li class="{{ \Request::is('company/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Company
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('company') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Companies</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('company.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Company</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('loan/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Loan
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('loan') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Loans</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('loan.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Loan</p>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="{{ route('loan-transaction.index') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payments</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('loan-transaction.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Loan Payment</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('cc-loan/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							CC Loan
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('cc-loan') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Loans</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('cc-loan.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Loan</p>
							</a>
						</li>
						
						<li class="nav-item">
							<a href="{{ route('cc-loan-withdraw.index') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Withdraws</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('cc-loan-withdraw.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Withdraw</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('cc-loan-deposit.index') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Deposits</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('cc-loan-deposit.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Deposit</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('personal-transaction/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Personal Account
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('personal-transaction') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Transactions</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('personal-transaction.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Transaction</p>
							</a>
						</li>
					</ul>
				</li>
				<li class="{{ \Request::is('index/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Index
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('index') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Index</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('index.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Index</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('prayer/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Prayer
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('prayer') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Prayer</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('prayer.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Prayer</p>
							</a>
						</li>
					</ul>
				</li>
				@endrole
				@role('business_admin')
				<li class="nav-item">
					<a href="{{ url('/home') }}" class="nav-link">
						<i class="nav-icon fa fa-dashboard"></i>
						<p>
							Dashboard
						</p>
					</a>
				</li>

				<li class="{{ \Request::is('business/fiscal-year/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Fiscal Year
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/fiscal-year') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Fiscal Years</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/fiscal-year/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Fiscal Year</p>
							</a>
						</li>
					</ul>
				</li>

				{{-- Purchase --}}

				<li class="{{ \Request::is('business/purchase/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-gear"></i>
						<p>
							Purchase
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/purchase-order') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchase Orders</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/purchase-order/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Purchase Order</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/purchase') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchases</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/purchase/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Purchase</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/purchase/delivery') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Deliveries</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/purchase-payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payments</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/purchase/payment/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/purchase-payment-return') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payment Returns</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/purchase-payment-return/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment Return</p>
							</a>
						</li>
					</ul>
				</li>
				
				{{-- Sale --}}
				<li class="{{ \Request::is('sale/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Sale
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/sale-order') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Orders</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/sale-order/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Sale Order</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/sale') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sales</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/sale/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Sale</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/sale/delivery') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Deliveries</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/sale/invoice') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Invoice</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/sale/payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payments</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/sale/payment/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/sale-payment-return') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payment Returns</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/sale-payment-return/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment Return</p>
							</a>
						</li>
					</ul>
				</li>
				
				{{-- Expense --}}
				<li class="{{ \Request::is('expense/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-hdd-o"></i>
						<p>
							Expense
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/expense-item') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expense Items</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/expense-item/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Expense Item</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/expense') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expenses</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/expense/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Expense</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/expense-transaction') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payments</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/expense-transaction/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('business/cash-book/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Cash Book
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/cash-book') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Cash Book</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/sale/payment/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Payment</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/expense-transaction/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expense Payment</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/purchase/payment/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchase Payment</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="nav-item">
					<a href="{{ url('business/stock/report') }}" class="nav-link">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>Stock Report</p>
					</a>
				</li>

				<li class="{{ \Request::is('business/daily-report/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="{{ url('business/daily-report')}}" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Daily Report
						</p>
					</a>
				</li>

				<li class="{{ \Request::is('business/account/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Account
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/account-payable') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Accounts Payable</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/account-receivable') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Accounts Receivable</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/cash/flow') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Cash </p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('business/supplier/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Supplier
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/supplier') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Suppliers</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/supplier/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Supplier</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('business/customer/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-th"></i>
						<p>
							Customer
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/customer') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Customers</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/customer/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Customer</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('bank-account/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Bank Account
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/bank-account') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Accounts</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('business.bank-account.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add New Account</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('business/bank-transaction') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Transactions</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ route('business.bank-transaction.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Transaction</p>
							</a>
						</li>
					</ul>
				</li>

				{{-- Product --}}

				<li class="{{ \Request::is('product/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Product
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/product') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Products</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/product/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Product</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('category/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Category
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/category') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Categories</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/category/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Category</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('business/product-wastage/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Wastage
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/product-wastage') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Wastages</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('business/product-wastage/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Wastage</p>
							</a>
						</li>
					</ul>
				</li>
				
				<li class="{{ \Request::is('remark/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Remark
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('business/remark') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Remarks</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('business.remark.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Remark</p>
							</a>
						</li>
					</ul>
				</li>
				
				@endrole

				@role('export_import_admin')
				<li class="nav-item">
					<a href="{{ url('/home') }}" class="nav-link">
						<i class="nav-icon fa fa-dashboard"></i>
						<p>
							Dashboard
						</p>
					</a>
				</li>

				<li class="{{ \Request::is('sale/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Sale
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('export/sale') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sales</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('export/sale/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Sale</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('export/sale/payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payment</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('export/purchase/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-gear"></i>
						<p>
							Purchase
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('export/purchase') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchases</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('export/purchase/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Purchase</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('export/purchase/payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payment</p>
							</a>
						</li>
					</ul>
				</li>


				<li class="{{ \Request::is('expense/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-hdd-o"></i>
						<p>
							Expense
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('export/expense') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expenses</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('export/expense/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Expense</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('export/expense-item') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expense Items</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('export/expense-item/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Expense Item</p>
							</a>
						</li>
					</ul>
				</li>

				{{-- <li class="{{ \Request::is('loan/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Loans
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('loan') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Loans</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('loan.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Loan</p>
							</a>
						</li>
					</ul>
				</li> --}}

{{-- 				<li class="{{ \Request::is('bank-account/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Bank Account
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('bank-account') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Accounts</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('bank-account.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add New Account</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ route('bank-transaction.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Transaction</p>
							</a>
						</li>
					</ul>
				</li> --}}

				{{-- Product --}}

				<li class="{{ \Request::is('product/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Product
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('export/product') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Products</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('export/product/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Product</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('category/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Category
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('export/category') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Categories</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('export/category/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Category</p>
							</a>
						</li>
					</ul>
				</li>

				{{-- <li class="{{ \Request::is('company/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Company
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('export/company') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Companies</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('export/company/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Company</p>
							</a>
						</li>
					</ul>
				</li> --}}

				<li class="{{ \Request::is('export/supplier/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Supplier
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('export/supplier') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Suppliers</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('export/supplier/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Supplier</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('export/customer/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-th"></i>
						<p>
							Customer
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('export/customer') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Customers</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('export/customer/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Customer</p>
							</a>
						</li>
					</ul>
				</li>
				@endrole
				@role('production_admin')
				<li class="nav-item">
					<a href="{{ url('/home') }}" class="nav-link">
						<i class="nav-icon fa fa-dashboard"></i>
						<p>
							Dashboard
						</p>
					</a>
				</li>

				{{-- Purchase --}}

				<li class="{{ \Request::is('production/purchase/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-gear"></i>
						<p>
							Purchase
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/purchase') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchases</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/purchase/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Purchase</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/purchase-order') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchase Orders</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/purchase-order/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Purchase Order</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/purchase-payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payments</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/purchase/payment/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/purchase-payment-return') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payment Returns</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/purchase-payment-return/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment Return</p>
							</a>
						</li>
					</ul>
				</li>
				
				{{-- Sale --}}
				<li class="{{ \Request::is('sale/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Sale
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/sale') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sales</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/sale/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Sale</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/sale-order') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Orders</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/sale-order/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Sale Order</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/sale/payment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payments</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/sale/payment/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/sale/invoice') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Invoice</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/sale-payment-return') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payment Returns</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/sale-payment-return/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment Return</p>
							</a>
						</li>
					</ul>
				</li>
				
				{{-- Expense --}}
				<li class="{{ \Request::is('expense/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-hdd-o"></i>
						<p>
							Expense
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/expense') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expenses</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/expense/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Expense</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/expense-item') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expense Items</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/expense-item/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Expense Item</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/expense-transaction') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Payments</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/expense-transaction/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('production/cash-book/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Cash Book
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/cash-book') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Cash Book</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/sale/payment/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Sale Payment</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/expense-transaction/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Expense Payment</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/purchase/payment/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Purchase Payment</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('raw-material-use/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Raw Material Uses
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/raw-material-use') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Uses</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/raw-material-use/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add New</p>
							</a>
						</li>
					</ul>
				</li>


				<li class="{{ \Request::is('produced-product/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Produced Product
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/produced-product') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Produced Products</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/produced-product/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add New</p>
							</a>
						</li>
					</ul>
				</li>

				{{-- <li class="nav-item">
					<a href="{{ url('production/stock/report') }}" class="nav-link">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>Stock Report</p>
					</a>
				</li> --}}

				<li class="{{ \Request::is('investment/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-hdd-o"></i>
						<p>
							Investment
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/investment') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Investments</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/investment/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Investment</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/investment-transaction') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Investment Transaction</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/investment-transaction/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Transaction</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('machine-rent/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-hdd-o"></i>
						<p>
							Machine Rent
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/machine-rent') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Machine Rents</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/machine-rent/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Machine Rent</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/machine-rent-transaction') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Machine Rent Payments</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/machine-rent-transaction/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Payment</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('production/supplier/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Supplier
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/supplier') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Suppliers</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/supplier/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Supplier</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('production/customer/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-th"></i>
						<p>
							Customer
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/customer') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Customers</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/customer/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Customer</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('bank-account/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Bank Account
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/bank-account') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Accounts</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('production.bank-account.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add New Account</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('production/bank-transaction') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Transactions</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{ route('production.bank-transaction.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Transaction</p>
							</a>
						</li>
					</ul>
				</li>

				{{-- Product --}}

				<li class="{{ \Request::is('product/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Product
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/product') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Products</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/product/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Product</p>
							</a>
						</li>
					</ul>
				</li>

				<li class="{{ \Request::is('raw-material/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-shopping-cart"></i>
						<p>
							Raw Material
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/raw-material') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Raw Materials</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/raw-material/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Raw Material</p>
							</a>
						</li>
					</ul>
				</li>

				{{-- <li class="{{ \Request::is('production/product-wastage/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Wastage
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/product-wastage') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Wastages</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('production/product-wastage/create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Wastage</p>
							</a>
						</li>
					</ul>
				</li>
				
				<li class="{{ \Request::is('remark/*') ? 'nav-item has-treeview menu-open' : 'nav-item has-treeview'  }}">
					<a href="#" class="nav-link item">
						<i class="nav-icon fa fa-users"></i>
						<p>
							Remark
							<i class="right fa fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{ url('production/remark') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Remarks</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('production.remark.create') }}" class="nav-link">
								<i class="fa fa-circle-o nav-icon"></i>
								<p>Add Remark</p>
							</a>
						</li>
					</ul>
				</li> --}}
				@endrole
			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>
