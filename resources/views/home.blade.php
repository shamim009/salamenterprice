@extends('layouts.master')
@section('title', 'Home')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @role('super_admin')
            <!-- Info boxes -->
{{--             <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1"><i class="fa fa-gear"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Requisitios</span>
                            <span class="info-box-number">
                                100
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-th"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Categories</span>
                            <span class="info-box-number">5</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                fix for small devices only
                <div class="clearfix hidden-md-up"></div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fa fa-shopping-cart"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Products</span>
                            <span class="info-box-number">10</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Users</span>
                            <span class="info-box-number">50</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div> --}}
            <!-- /.row -->
            @endrole

            @role('business_admin')
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        {{-- Total Purchase count --}}
                        <div class="col-sm-4">
                            
                        </div>

                        {{-- Total purchase count --}}
                        <div class="col-sm-4">
                            <div class="alert alert-secondary">
                                <button type="button" class="close">
                                    <i class="fa fa-money fa-2x" aria-hidden="true"></i>
                                </button>
                                <h4>Purchase Table</h4>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Purchase Quantity</td>
                                            <td>{{ $purchaseQuantity }}</td>
                                        </tr>
                                        <tr>
                                            <td>Purchase Amount</td>
                                            <td>{{ $purchaseAmount }}</td>
                                        </tr>
                                        <tr>
                                            <td>Purchase Paid</td>
                                            <td>{{ $purchasePaid }}</td>
                                        </tr>

                                        <tr>
                                            <td>Balance</td>
                                            <td>{{ $purchaseAmount - $purchasePaid }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> 

                        <div class="col-sm-4">
                            
                        </div>            
                    </div>
                </div>
            </div>

            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header">
                            <form method="POST" action="{{url('business/purchase/report')}}" class="form-inline">
                                @csrf
                                <label for="inlineFormCustomSelect">Start Date </label>
                                <input type="text" name="start_date" id="purchase_start_date" class="form-control" placeholder="Start Date">
                                <label for="inlineFormCustomSelect">End Date </label>
                                <input type="text" name="end_date" id="purchase_end_date" class="form-control" placeholder="End Date">
                                <button type="submit" class="btn btn-info"><i class="fa fa-line-chart" aria-hidden="true"></i> Report</button>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="purchaseTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Supplier</th>
                                            <th>Supplier</th>
                                            <th>Product</th>                            
                                            <th>Quantity</th>
                                            <th>Rate</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th colspan="5" style="text-align:right">Total:</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        {{-- Total Purchase count --}}
                        <div class="col-sm-4">
                            
                        </div>

                        {{-- Total purchase count --}}
                        <div class="col-sm-4">
                            <div class="alert alert-primary">
                                <button type="button" class="close">
                                    <i class="fa fa-money fa-2x" aria-hidden="true"></i>
                                </button>
                                <h4>Sale Table</h4>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Sale Quantity</td>
                                            <td>{{ $saleQuantity }}</td>
                                        </tr>
                                        <tr>
                                            <td>Sale Amount</td>
                                            <td>{{ $saleAmount }}</td>
                                        </tr>
                                        <tr>
                                            <td>Sale Paid</td>
                                            <td>{{ $salePaid }}</td>
                                        </tr>

                                        <tr>
                                            <td>Balance</td>
                                            <td>{{ $saleAmount - $salePaid }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> 

                        <div class="col-sm-4">
                            
                        </div>            
                    </div>
                </div>
            </div>

            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header">
                            <form method="POST" action="{{url('business/sale/report')}}" class="form-inline">
                                @csrf
                                <label for="inlineFormCustomSelect">Start Date </label>
                                <input type="text" name="start_date" id="sale_start_date" class="form-control" placeholder="Start Date">
                                <label for="inlineFormCustomSelect">End Date </label>
                                <input type="text" name="end_date" id="sale_end_date" class="form-control" placeholder="End Date">
                                <button type="submit" class="btn btn-info"><i class="fa fa-line-chart" aria-hidden="true"></i> Report</button>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="saleTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Customer</th>
                                            <th>Customer</th>
                                            <th>Product</th>                            
                                            <th>Quantity</th>
                                            <th>Rate</th>
                                            <th>Vat</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th colspan="5" style="text-align:right">Total:</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="row">
                <div class="col-12">
                    <div class="row">
                        {{-- Total Purchase count --}}
                        <div class="col-sm-4">
                            
                        </div>

                        {{-- Total purchase count --}}
                        <div class="col-sm-4">
                            <div class="alert alert-primary">
                                <button type="button" class="close">
                                    <i class="fa fa-money fa-2x" aria-hidden="true"></i>
                                </button>
                                <h4>Expense Table</h4>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Total Expense</td>
                                            <td>{{ $expenseAmount }}</td>
                                        </tr>
                                        <tr>
                                            <td>Expense Paid</td>
                                            <td>{{ $expensePaid }}</td>
                                        </tr>

                                        <tr>
                                            <td>Balance</td>
                                            <td>{{ $expenseAmount - $expensePaid }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> 

                        <div class="col-sm-4">
                            
                        </div>            
                    </div>
                </div>
            </div>

            <section class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header">
                            <form method="POST" action="{{url('business/expense/report')}}" class="form-inline">
                                @csrf
                                <label for="inlineFormCustomSelect">Start Date </label>
                                <input type="text" name="start_date" id="expense_start_date" class="form-control" placeholder="Start Date">
                                <label for="inlineFormCustomSelect">End Date </label>
                                <input type="text" name="end_date" id="expense_end_date" class="form-control" placeholder="End Date">
                                <button type="submit" class="btn btn-info"><i class="fa fa-line-chart" aria-hidden="true"></i> Report</button>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="expenseTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Date</th>
                                            <th>Expense Type</th>
                                            <th>Amount</th>
                                            <th>Remarks</th>
                                            <th>Action</th>
                                            <th>Type</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3" style="text-align:right">Total:</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endrole

            {{-- <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Monthly Recap Report</h5>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-wrench"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                                        <a href="#" class="dropdown-item">Action</a>
                                        <a href="#" class="dropdown-item">Another action</a>
                                        <a href="#" class="dropdown-item">Something else here</a>
                                        <a class="dropdown-divider"></a>
                                        <a href="#" class="dropdown-item">Separated link</a>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-tool" data-widget="remove">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <p class="text-center">
                                        <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
                                    </p>

                                    <div class="chart">
                                        <!-- Sales Chart Canvas -->
                                        <canvas id="salesChart" height="180" style="height: 180px;"></canvas>
                                    </div>
                                    <!-- /.chart-responsive -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <p class="text-center">
                                        <strong>Goal Completion</strong>
                                    </p>

                                    <div class="progress-group">
                                        Add Products to Cart
                                        <span class="float-right"><b>160</b>/200</span>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-primary" style="width: 80%"></div>
                                        </div>
                                    </div>
                                    <!-- /.progress-group -->

                                    <div class="progress-group">
                                        Complete Purchase
                                        <span class="float-right"><b>310</b>/400</span>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-danger" style="width: 75%"></div>
                                        </div>
                                    </div>

                                    <!-- /.progress-group -->
                                    <div class="progress-group">
                                        <span class="progress-text">Visit Premium Page</span>
                                        <span class="float-right"><b>480</b>/800</span>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-success" style="width: 60%"></div>
                                        </div>
                                    </div>

                                    <!-- /.progress-group -->
                                    <div class="progress-group">
                                        Send Inquiries
                                        <span class="float-right"><b>250</b>/500</span>
                                        <div class="progress progress-sm">
                                            <div class="progress-bar bg-warning" style="width: 50%"></div>
                                        </div>
                                    </div>
                                    <!-- /.progress-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- ./card-body -->
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-sm-3 col-6">
                                    <div class="description-block border-right">
                                        <span class="description-percentage text-success"><i class="fa fa-caret-up"></i> 17%</span>
                                        <h5 class="description-header">$35,210.43</h5>
                                        <span class="description-text">TOTAL REVENUE</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3 col-6">
                                    <div class="description-block border-right">
                                        <span class="description-percentage text-warning"><i class="fa fa-caret-left"></i> 0%</span>
                                        <h5 class="description-header">$10,390.90</h5>
                                        <span class="description-text">TOTAL COST</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3 col-6">
                                    <div class="description-block border-right">
                                        <span class="description-percentage text-success"><i class="fa fa-caret-up"></i> 20%</span>
                                        <h5 class="description-header">$24,813.53</h5>
                                        <span class="description-text">TOTAL PROFIT</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3 col-6">
                                    <div class="description-block">
                                        <span class="description-percentage text-danger"><i class="fa fa-caret-down"></i> 18%</span>
                                        <h5 class="description-header">1200</h5>
                                        <span class="description-text">GOAL COMPLETIONS</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div> --}}
            <!-- /.row -->
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!--/. container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')
<script>
    //server side data table
    $(document).ready(function () {
        
        $('#purchaseTable').DataTable({
            "order": [[ 1, "desc" ]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('business/admin/all-purchases') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{ csrf_token() }}"}
            },
            "columns": [
            { "data": "id" },
            { "data": "date" },
            { "data": "supplier_id" },
            { "data": "supplier" },
            { "data": "product_id" },
            { "data": "quantity" },
            { "data": "rate" },
            { "data": "amount" },
            { "data": "actions" }
            ],
            createdRow: function( row, data, dataIndex ) {
                //console.log(data);
                var url = "/business/supplier/report/" + data['supplier'];
                //console.log(url);
                // Set the data-status attribute, and add a class
                $( row ).addClass('clickable-row')
                    .click(function() {
                            window.location = url;
                        });
            },
            "columnDefs": [
                {
                    "targets": [ 3 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = json.totalSum;
                
                // Total over this page
                pageTotal = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 7 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                );

                // Total over all pages
                totalQuantity = json.totalQuantity;
                
                // Total over this page
                pageTotalQuantity = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 5 ).footer() ).html(
                    ''+pageTotalQuantity +' ( '+ totalQuantity +' total)'
                );
            }            
        });

        $('#saleTable').DataTable({
            "order": [[ 1, "desc" ]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('business/admin/all-sales') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{ csrf_token() }}"}
            },
            "columns": [
            { "data": "id" },
            { "data": "date" },
            { "data": "customer_id" },
            { "data": "customer" },
            { "data": "product_id" },
            { "data": "quantity" },
            { "data": "rate" },
            { "data": "vat" },
            { "data": "amount" },
            { "data": "actions" }
            ],

            createdRow: function( row, data, dataIndex ) {
                //console.log(data);
                var url = "/business/customer/report/" + data['customer'];
                //console.log(url);
                // Set the data-status attribute, and add a class
                $( row ).addClass('clickable-row')
                    .click(function() {
                            window.location = url;
                        });
            },
            "columnDefs": [
                {
                    "targets": [ 3 ],
                    "visible": false,
                    "searchable": false
                }
            ],

            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = json.totalQuantity;
                
                // Total over this page
                pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 5 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                    );

                // Total over all pages
                amount = json.totalSum;
                
                // amount over this page
                pageamount = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 8 ).footer() ).html(
                    ''+pageamount +' ( '+ amount +' total)'
                );

                // Total over all pages
                vat = json.totalVat;
                
                // vat over this page
                pagevat = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 7 ).footer() ).html(
                    ''+pagevat +' ( '+ vat +' total)'
                );
            }   
        });

        $('#expenseTable').DataTable({
            "order": [[ 1, "desc" ]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "{{ url('business/admin/all-expenses') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{ csrf_token() }}"}
            },
            "columns": [
            { "data": "id" },
            { "data": "date" },
            { "data": "expenseItem_id" },
            { "data": "amount" },
            { "data": "details" },
            { "data": "actions" },
            { "data": "type" },
            ],
            createdRow: function( row, data, dataIndex ) {
                //console.log(data);
                var url = "/business/expense-item/report/" + data['type'];
                //console.log(url);
                // Set the data-status attribute, and add a class
                $( row ).addClass('clickable-row')
                    .click(function() {
                            window.location = url;
                        });
            },
            "columnDefs": [
                {
                    "targets": [ 6 ],
                    "visible": false,
                    "searchable": false
                }
            ],

            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                var json = api.ajax.json();
                
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };
                
                // Total over all pages
                total = json.totalSum;
                
                // Total over this page
                pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                
                // Update footer
                $( api.column( 3 ).footer() ).html(
                    ''+pageTotal +' ( '+ total +' total)'
                    );
            },
        });
    });

    //delete a row using ajax
    $('#purchaseTable').on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_method: 'DELETE', submit: true}
            }).always(function (data) {
                $('#adminDataTable').DataTable().draw(false);
            });
        }
    });

    $('#saleTable').on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_method: 'DELETE', submit: true}
            }).always(function (data) {
                $('#adminDataTable').DataTable().draw(false);
            });
        }
    });

    $( function() {
        $( "#sale_start_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });

        $( "#sale_end_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });
    });

    $( function() {
        $( "#purchase_start_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });

        $( "#purchase_end_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });
    });

    $('#expenseTable').on('click', '.btn-delete[data-remote]', function (e) { 
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = $(this).data('remote');
        // confirm then
        if (confirm('are you sure you want to delete this?')) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: {_method: 'DELETE', submit: true}
            }).always(function (data) {
                console.log(data);
                $('#adminDataTable').DataTable().draw(false);
            });
        }
    });

    $( function() {
        $( "#expense_start_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });

        $( "#expense_end_date" ).datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });
    });
</script>
@endsection