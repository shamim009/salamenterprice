<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->	
	<link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon"/>
	<!--===============================================================================================-->
	<link href="{{ asset('css/login/bootstrap.min.css') }}" rel="stylesheet">
	<!--===============================================================================================-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link href="{{ asset('css/login/icon-font.min.css') }}" rel="stylesheet">
	<!--===============================================================================================-->
	<link href="{{ asset('css/login/animate.css') }}" rel="stylesheet">
	<!--===============================================================================================-->	
	<link href="{{ asset('css/login/hamburgers.min.css') }}" rel="stylesheet">
	<!--===============================================================================================-->
	<link href="{{ asset('css/login/select2.min.css') }}" rel="stylesheet">
	<!--===============================================================================================-->
	<link href="{{ asset('css/login/util.css') }}" rel="stylesheet">
	<link href="{{ asset('css/login/main.css') }}" rel="stylesheet">
	<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url({{ asset('images/bg-01.jpg') }});">
			<div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
				@if (session('error'))
				<div class="alert alert-danger">
					{{ session('error') }}
				</div>
				@endif
				@if (session('success'))
				<div class="alert alert-success">
					{{ session('success') }}
				</div>
				@endif
				<form class="login100-form validate-form" method="POST" action="{{ url('change-password') }}">
					@csrf
					<span class="login100-form-title p-b-55">
						{{ config('app.name', 'Laravel') }}
					</span>

					<div class="wrap-input100 validate-input m-b-16">
                        <input class="input100 {{ $errors->has('current-password') ? ' is-invalid' : '' }}" type="password" name="current-password" placeholder="Current Password" value="{{ old('current-password') }}" required >
						<span class="focus-input100"></span>
						@if ($errors->has('current-password'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('current-password') }}</strong>
						</span>
						@endif
					</div>

					<div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
						<input class="input100 {{ $errors->has('new-password') ? ' is-invalid' : '' }}" type="password" name="new-password" placeholder="New Password" required>
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
						@if ($errors->has('new-password'))
						<span class="invalid-feedback">
							<strong>{{ $errors->first('new-password') }}</strong>
						</span>
						@endif
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter Password">
						<input class="input100" type="password" name="new-password_confirmation" placeholder="Confirm Password" required>
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					
					<div class="container-login100-form-btn p-t-25">
						<button class="login100-form-btn">
							Change Password
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
	<!--===============================================================================================-->	
	<script src="{{ asset('js/login/jquery-3.2.1.min.js') }}"></script>
	<!--===============================================================================================-->
	<script src="{{ asset('js/login/popper.js') }}"></script>
	<script src="{{ asset('js/login/bootstrap.min.js') }}"></script>
	<!--===============================================================================================-->
	<script src="{{ asset('js/login/select2.min.js') }}"></script>
	<!--===============================================================================================-->
	<script src="{{ asset('js/login/main.js') }}"></script>

</body>
</html>