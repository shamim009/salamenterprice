<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Auth::routes();

//bank search via select2
Route::get('bank-autocomplete-search','BankAccountController@autoCompleteSearch')->name('bank-autocomplete-search');

Route::group(['middleware' => ['auth']], function() {
	//change password
	Route::get('change-password','HomeController@showChangePasswordForm');
	Route::post('change-password','HomeController@changePassword');
});
Route::get('/home', 'HomeController@index')->name('home');

//only super admin routes
Route::group(['middleware' => ['role:super_admin']], function() {
	//personal transactions serverside data table processing
	Route::post('all-personal-transactions', 'PersonalTransactionController@allTransactions');
	//personal indices serverside data table processing
	Route::post('all-indices', 'IndexController@allIndices');
	Route::post('all-prayers', 'PrayerController@allPrayers');
	//company report
	Route::get('company/report/{id}', 'CompanyController@returnDatePage');
	Route::post('company/report/by-date/{id}', 'CompanyController@report')->name('company.report');
	//loan report
	Route::get('loan/report/{id}', 'LoanController@report')->name('loan.report');
	//loan transactions serverside data table processing
	Route::post('all-loan-transactions', 'LoanTransactionController@allTransactions');
	//resource routes
	Route::resource('company', 'CompanyController');
	Route::resource('loan', 'LoanController');
	Route::resource('loan-transaction', 'LoanTransactionController');
	Route::resource('personal-transaction', 'PersonalTransactionController');
	Route::resource('index', 'IndexController');
	Route::resource('cc-loan', 'CcLoanController');
	Route::resource('cc-loan-withdraw', 'CcLoanWithdrawController');
	Route::resource('cc-loan-deposit', 'CcLoanDepositController');
	Route::resource('prayer', 'PrayerController');

});



//regular business routes
Route::group(['prefix' => 'business', 'as' => 'business.', 'middleware' => ['role:super_admin|business_admin']], function() {

	//server side processing of data tables
	//suppliers
	Route::post('all-suppliers', 'Business\SupplierController@allSuppliers');
	//customers
	Route::post('all-customers', 'Business\CustomerController@allCustomers');
	//expenses for super admin
	Route::post('all-expenses', 'Business\ExpenseController@allExpenses');
	//expense datatable view for business admin
	Route::post('admin/all-expenses', 'Business\ExpenseController@allExpensesAdminView');
	//expenses for business admin
	Route::post('admin/all-expenses/{id}', 'Business\ExpenseItemController@particularExpenseAdminView');
	//all expenses of a particular item
	Route::post('all-expenses/{id}', 'Business\ExpenseItemController@particularExpense');
	//expense payments for super admin
	Route::post('all-expense-transactions/{id}', 'Business\ExpenseItemController@particularPayment');
	//expense payments for business admin
	Route::post('admin/all-expense-transactions/{id}', 'Business\ExpenseItemController@particularPaymentAdminView');
	//purchases for super admin
	Route::post('all-purchases', 'Business\PurchaseController@allPurchases');
	//purchases for business admin
	Route::post('admin/all-purchases', 'Business\PurchaseController@allPurchasesAdminView');
	//purchase-deliveries for super admin
	Route::post('all-purchase-deliveries', 'Business\PurchaseDeliveryController@allPurchaseDeliveries');
	//purchase-deliveries for business admin
	Route::post('admin/all-purchase-deliveries', 'Business\PurchaseDeliveryController@allPurchaseDeliveriesAdminView');
	//sale-deliveries for super admin
	Route::post('all-sale-deliveries', 'Business\SaleDeliveryController@allSaleDeliveries');
	//sale-deliveries for business admin
	Route::post('admin/all-sale-deliveries', 'Business\SaleDeliveryController@allSaleDeliveriesAdminView');
	//wastage datatable view for suprer admin
	Route::post('all-wastages', 'Business\ProductWastageController@allWastages');
	//wastage datatable view for business admin
	Route::post('admin/all-wastages', 'Business\ProductWastageController@allWastagesAdminView');
	//purchase order datatable view for suprer admin
	Route::post('all-purchase-orders', 'Business\PurchaseOrderController@allOrders');
	//purchase order datatable view for business admin
	Route::post('admin/all-purchase-orders', 'Business\PurchaseOrderController@allOrdersAdminView');
	//sale order datatable view for suprer admin
	Route::post('all-sale-orders', 'Business\SaleOrderController@allOrders');
	//sale order datatable view for business admin
	Route::post('admin/all-sale-orders', 'Business\SaleOrderController@allOrdersAdminView');
	//purchase transactions datatable view for suprer admin
	Route::post('all-purchase-transactions', 'Business\PurchaseTransactionController@allTransactions');
	//purchase order datatable view for business admin
	Route::post('admin/all-purchase-transactions', 'Business\PurchaseTransactionController@allTransactionsAdminView');
	//expense transactions datatable view for suprer admin
	Route::post('all-expense-transactions', 'Business\ExpenseTransactionController@allTransactions');
	//expense payments datatable view for business admin
	Route::post('admin/all-expense-transactions', 'Business\ExpenseTransactionController@allTransactionsAdminView');
	//sale transactions datatable view for suprer admin
	Route::post('all-sale-transactions', 'Business\SaleTransactionController@allTransactions');
	//sale order datatable view for business admin
	Route::post('admin/all-sale-transactions', 'Business\SaleTransactionController@allTransactionsAdminView');
	//sale transactions datatable view for suprer admin
	Route::post('all-sale-payment-returns', 'Business\SalePaymentReturnController@allTransactions');
	//sale order datatable view for business admin
	Route::post('admin/all-sale-payment-returns', 'Business\SalePaymentReturnController@allTransactionsAdminView');
	//sale transactions datatable view for suprer admin
	Route::post('all-purchase-payment-returns', 'Business\PurchasePaymentReturnController@allTransactions');
	//purchase order datatable view for business admin
	Route::post('admin/all-purchase-payment-returns', 'Business\PurchasePaymentReturnController@allTransactionsAdminView');
	//bank transactions datatable view for suprer admin
	Route::post('all-bank-transactions', 'Business\BankAccountTransactionController@allTransactions');
	//bank order datatable view for business admin
	Route::post('admin/all-bank-transactions', 'Business\BankAccountTransactionController@allTransactionsAdminView');
	//sales for super admin
	Route::post('all-sales', 'Business\SaleController@allSales');
	//sales for business admin
	Route::post('admin/all-sales', 'Business\SaleController@allSalesAdminView');

	//customer report
	Route::get('customer/report/{id}', 'Business\CustomerController@returnDatePage')->name('customer.report');
	Route::post('customer/report', 'Business\CustomerController@report');
	Route::post('customer/report/print', 'Business\CustomerController@printReport');
	Route::post('customer/report/pdf', 'Business\CustomerController@reportExportPDF');
	Route::post('customer/report/excel', 'Business\CustomerController@reportExportExcel');

	//supplier report
	Route::get('supplier/report/{id}', 'Business\SupplierController@returnDatePage')->name('supplier.report');
	Route::post('supplier/report', 'Business\SupplierController@report');
	Route::post('supplier/report/print', 'Business\SupplierController@printReport');
	Route::post('supplier/report/pdf', 'Business\SupplierController@reportExportPDF');
	Route::post('supplier/report/excel', 'Business\SupplierController@reportExportExcel');

	//purchase report
	Route::post('purchase/report', 'Business\PurchaseController@report')->name('purchase.report');
	Route::post('purchase/report/print', 'Business\PurchaseController@printReport');
	Route::post('purchase/report/pdf', 'Business\PurchaseController@reportExportPDF');
	Route::post('purchase/report/excel', 'Business\PurchaseController@reportExportExcel');

	//purchase transaction report
	Route::post('purchase-transaction/report', 'Business\PurchaseTransactionController@report');
	Route::post('purchase-transaction/report/print', 'Business\PurchaseTransactionController@printReport');
	Route::post('purchase-transaction/report/pdf', 'Business\PurchaseTransactionController@reportExportPDF');
	Route::post('purchase-transaction/report/excel', 'Business\PurchaseTransactionController@reportExportExcel');

	//purchase payment report
	Route::post('purchase-payment-return/report', 'Business\PurchasePaymentReturnController@report');
	Route::post('purchase-payment-return/report/print', 'Business\PurchasePaymentReturnController@printReport');
	Route::post('purchase-payment-return/report/pdf', 'Business\PurchasePaymentReturnController@reportExportPDF');
	Route::post('purchase-payment-return/report/excel', 'Business\PurchasePaymentReturnController@reportExportExcel');

	//sale report
	Route::post('sale/report', 'Business\SaleController@report')->name('sale.report');
	Route::post('sale/report/print', 'Business\SaleController@printReport');
	Route::post('sale/report/pdf', 'Business\SaleController@reportExportPDF');
	Route::post('sale/report/excel', 'Business\SaleController@reportExportExcel');

	//sale invoice/bill menu
	Route::get('sale/invoice', 'Business\SaleController@returnViewPage');
	Route::post('sale/invoice', 'Business\SaleController@invoice');
	Route::post('sale/invoice/print', 'Business\SaleController@invoicePrint');
	Route::post('sale/invoice/pdf', 'Business\SaleController@invoiceExportPDF');
	Route::post('sale/invoice/excel', 'Business\SaleController@invoiceExportExcel');

	//sale transaction report
	Route::post('sale-transaction/report', 'Business\SaleTransactionController@report');
	Route::post('sale-transaction/report/print', 'Business\SaleTransactionController@printReport');
	Route::post('sale-transaction/report/pdf', 'Business\SaleTransactionController@reportExportPDF');
	Route::post('sale-transaction/report/excel', 'Business\SaleTransactionController@reportExportExcel');

	//sale payment return report
	Route::post('sale-payment-return/report', 'Business\SalePaymentReturnController@report');
	Route::post('sale-payment-return/report/print', 'Business\SalePaymentReturnController@printReport');
	Route::post('sale-payment-return/report/pdf', 'Business\SalePaymentReturnController@reportExportPDF');
	Route::post('sale-payment-return/report/excel', 'Business\SalePaymentReturnController@reportExportExcel');

	//expense report
	Route::post('expense/report', 'Business\ExpenseController@report')->name('expense.report');
	Route::post('expense/report/print', 'Business\ExpenseController@printReport');
	Route::post('expense/report/pdf', 'Business\ExpenseController@reportExportPDF');
	Route::post('expense/report/excel', 'Business\ExpenseController@reportExportExcel');

	//expense transaction report
	Route::post('expense-transaction/report', 'Business\ExpenseTransactionController@report');
	Route::post('expense-transaction/report/print', 'Business\ExpenseTransactionController@printReport');
	Route::post('expense-transaction/report/pdf', 'Business\ExpenseTransactionController@reportExportPDF');
	Route::post('expense-transaction/report/excel', 'Business\ExpenseTransactionController@reportExportExcel');

	//cash book report
	Route::get('cash-book', 'HomeController@businessCashBook');

	//accounts payable report
	Route::get('account-payable', 'Business\SupplierController@payableAmount');

	//account receivable report
	Route::get('account-receivable', 'Business\CustomerController@payableAmount');

	//cash in hand/bank report
	Route::get('cash/flow', 'Business\FiscalYearController@cashReport');

	//cash book datatable view for suprer admin
	Route::post('full-cash-book', 'HomeController@allBusinessCashBook');
	//cash book datatable view for business admin
	Route::post('admin/full-cash-book', 'HomeController@allBusinessCashBookAdminView');

	//supplier search via select2
	Route::get('supplier-autocomplete-search','Business\SupplierController@autoCompleteSearch')->name('supplier-autocomplete-search');

	//product search via select2
	Route::get('product-autocomplete-search','Business\ProductController@autoCompleteSearch')->name('product-autocomplete-search');

	//customer search via select2
	Route::get('customer-autocomplete-search','Business\CustomerController@autoCompleteSearch')->name('customer-autocomplete-search');

	//particular expense report
	Route::get('expense-item/report/{id}', 'Business\ExpenseItemController@report');
	Route::post('expense-item/report', 'Business\ExpenseItemController@byDateReport');
	Route::post('expense-item/report/print', 'Business\ExpenseItemController@printReport');
	Route::post('expense-item/report/pdf', 'Business\ExpenseItemController@reportExportPDF');
	Route::post('expense-item/report/excel', 'Business\ExpenseItemController@reportExportExcel');

	//purchase payment create
	Route::get('purchase/payment/create', 'Business\PurchaseTransactionController@create');

	//purchase payment index page
	Route::get('purchase/payment', 'Business\PurchaseTransactionController@index');

	//sale payment index page
	Route::get('sale/payment', 'Business\SaleTransactionController@index');

	//sale payment create
	Route::get('sale/payment/create', 'Business\SaleTransactionController@create');

	//bank account transaction report
	//Route::get('bank-account/report/{id}', 'Business\BankAccountController@report');

	//purchase delivery create
	Route::get('purchase/order/delivery/{id}', 'Business\PurchaseOrderController@createDelivery');
	Route::post('purchase/order/delivery/{id}', 'Business\PurchaseOrderController@storeDelivery');

	//sale delivery create
	Route::get('sale/order/delivery/{id}', 'Business\SaleOrderController@createDelivery');
	Route::post('sale/order/delivery/{id}', 'Business\SaleOrderController@storeDelivery');

	//purchase delivery report
	Route::get('purchase/order/delivery/report/{id}', 'Business\PurchaseOrderController@deliveryReport')->name('purchase.order.delivery.report');

	//sale delivery report
	Route::get('sale/order/delivery/report/{id}', 'Business\SaleOrderController@deliveryReport')->name('sale.order.delivery.report');

	//sale deliveries
	Route::get('sale/delivery', 'Business\SaleDeliveryController@index');

	//sale delivery create
	Route::get('sale/delivery/{id}', 'Business\SaleDeliveryController@create');
	Route::post('sale/delivery/{id}', 'Business\SaleDeliveryController@store');

	//sale delivery update
	Route::get('sale/delivery/edit/{id}', 'Business\SaleDeliveryController@edit');
	Route::post('sale/delivery/edit/{id}', 'Business\SaleDeliveryController@update');

	//view delivery details
	Route::get('sale/delivery/view/{id}', 'Business\SaleDeliveryController@show');

	//destroy delivery
	Route::delete('sale/delivery/destroy/{id}', 'Business\SaleDeliveryController@destroy');

	//sale delivery report
	Route::get('sale/delivery/report/{id}', 'Business\SaleDeliveryController@deliveryReport')->name('sale.delivery.report');

	//sale delivery report
	Route::get('sale/delivery/report/{id}', 'Business\SaleDeliveryController@deliveryReport');

	Route::post('sale/delivery/report/by/date', 'Business\SaleDeliveryController@report')->name('sale.delivery.report');
	Route::post('sale/delivery/report/print', 'Business\SaleDeliveryController@printReport');
	Route::post('sale/delivery/report/pdf', 'Business\SaleDeliveryController@reportExportPDF');
	Route::post('sale/delivery/report/excel', 'Business\SaleDeliveryController@reportExportExcel');

	//purchase deliveries
	Route::get('purchase/delivery', 'Business\PurchaseDeliveryController@index');

	//purchase delivery create
	Route::get('purchase/delivery/{id}', 'Business\PurchaseDeliveryController@create');
	Route::post('purchase/delivery/{id}', 'Business\PurchaseDeliveryController@store');

	//purchase delivery update
	Route::get('purchase/delivery/edit/{id}', 'Business\PurchaseDeliveryController@edit');
	Route::post('purchase/delivery/edit/{id}', 'Business\PurchaseDeliveryController@update');

	//view delivery details
	Route::get('purchase/delivery/view/{id}', 'Business\PurchaseDeliveryController@show');

	//destroy delivery
	Route::delete('purchase/delivery/destroy/{id}', 'Business\PurchaseDeliveryController@destroy');

	//purchase delivery report
	Route::get('purchase/delivery/report/{id}', 'Business\PurchaseDeliveryController@deliveryReport');

	Route::post('purchase/delivery/report/by/date', 'Business\PurchaseDeliveryController@report')->name('purchase.delivery.report');
	Route::post('purchase/delivery/report/print', 'Business\PurchaseDeliveryController@printReport');
	Route::post('purchase/delivery/report/pdf', 'Business\PurchaseDeliveryController@reportExportPDF');
	Route::post('purchase/delivery/report/excel', 'Business\PurchaseDeliveryController@reportExportExcel');

	//bank account report
	Route::get('bank-account/report/{id}', 'Business\BankAccountController@report');

	//stock report
	Route::get('stock/report', 'HomeController@businessStockReportPage');
	Route::post('stock/report', 'HomeController@businessStockReport');

	//active fiscal year
	Route::get('fiscal-year/activate/{id}', 'Business\FiscalYearController@activateCurrentYear');
	//Daily Report
    Route::get('daily-report','Business\DailyReportController@index' );
    Route::post('daily-report','Business\DailyReportController@byDate' );

	Route::resource('customer', 'Business\CustomerController');
	Route::resource('company', 'Business\CompanyController');
	Route::resource('supplier', 'Business\SupplierController');
	Route::resource('product', 'Business\ProductController');
	Route::resource('category', 'Business\CategoryController');
	Route::resource('purchase', 'Business\PurchaseController');
	Route::resource('sale-order', 'Business\SaleOrderController');
	Route::resource('purchase-order', 'Business\PurchaseOrderController');
	Route::resource('sale', 'Business\SaleController');
	Route::resource('expense', 'Business\ExpenseController');
	Route::resource('expense-item', 'Business\ExpenseItemController');
	Route::resource('remark', 'Business\RemarkController');
	Route::resource('product-wastage', 'Business\ProductWastageController');
	Route::resource('sale-payment', 'Business\SaleTransactionController');
	Route::resource('purchase-payment', 'Business\PurchaseTransactionController');
	Route::resource('bank-account', 'Business\BankAccountController');
	Route::resource('bank-transaction', 'Business\BankAccountTransactionController');
	Route::resource('expense-transaction', 'Business\ExpenseTransactionController');
	Route::resource('sale-payment-return', 'Business\SalePaymentReturnController');
	Route::resource('purchase-payment-return', 'Business\PurchasePaymentReturnController');
	Route::resource('fiscal-year', 'Business\FiscalYearController');

});

Route::group(['prefix' => 'export', 'as' => 'export.', 'middleware' => ['role:super_admin|export_import_admin']], function() {

	//server side processing of data tables
	Route::post('all-suppliers', 'Export\SupplierController@allSuppliers');
	Route::post('all-customers', 'Export\CustomerController@allCustomers');
	Route::post('all-employees', 'Export\EmployeeController@allEmployees');
	Route::post('all-expenses', 'Export\ExpenseController@allExpenses');
	Route::post('all-expenses/{id}', 'Export\ExpenseItemController@particularExpense');
	Route::post('all-purchases', 'Export\PurchaseController@allPurchases');
	Route::post('all-sales', 'Export\SaleController@allSales');

	//customer report
	Route::get('customer/report/{id}', 'Export\CustomerController@report')->name('customer.report');

	//supplier report
	Route::get('supplier/report/{id}', 'Export\SupplierController@report')->name('supplier.report');

	//supplier search via select2
	Route::get('supplier-autocomplete-search','Export\SupplierController@autoCompleteSearch')->name('supplier-autocomplete-search');

	//product search via select2
	Route::get('product-autocomplete-search','Export\ProductController@autoCompleteSearch')->name('product-autocomplete-search');

	//customer search via select2
	Route::get('customer-autocomplete-search','Export\CustomerController@autoCompleteSearch')->name('customer-autocomplete-search');

	//particular expense report
	Route::get('expense-item/report/{id}', 'Export\ExpenseItemController@report');

	//purchase payment
	Route::get('purchase/payment', 'Export\PurchaseTransactionController@create');

	//sale payment
	Route::get('sale/payment', 'Export\SaleTransactionController@create');

	Route::resource('customer', 'Export\CustomerController');
	Route::resource('company', 'Export\CompanyController');
	Route::resource('supplier', 'Export\SupplierController');
	Route::resource('product', 'Export\ProductController');
	Route::resource('category', 'Export\CategoryController');
	Route::resource('purchase', 'Export\PurchaseController');
	Route::resource('sale', 'Export\SaleController');
	Route::resource('expense', 'Export\ExpenseController');
	Route::resource('expense-item', 'Export\ExpenseItemController');
	Route::resource('sale-payment', 'Export\SaleTransactionController');
	Route::resource('purchase-payment', 'Export\PurchaseTransactionController');
	//Route::resource('bank-account', 'Export\BankAccountController');
	//Route::resource('bank-transaction', 'Export\BankTransactionController');


});

//production routes
Route::group(['prefix' => 'production', 'as' => 'production.', 'middleware' => ['role:super_admin|production_admin']], function() {

	//server side processing of data tables
	Route::post('all-suppliers', 'Production\SupplierController@allSuppliers');
	Route::post('all-customers', 'Production\CustomerController@allCustomers');
	Route::post('all-employees', 'Production\EmployeeController@allEmployees');
	Route::post('all-expenses', 'Production\ExpenseController@allExpenses');
	Route::post('all-expenses/{id}', 'Production\ExpenseItemController@particularExpense');
	Route::post('admin/all-expenses/{id}', 'Production\ExpenseItemController@particularExpenseAdminView');
	Route::post('all-expense-transactions/{id}', 'Production\ExpenseItemController@particularPayment');
	Route::post('admin/all-expense-transactions/{id}', 'Production\ExpenseItemController@particularPaymentAdminView');
	//purchase datatable view for suprer admin
	Route::post('all-purchases', 'Production\PurchaseController@allPurchases');
	//purchase datatable view for Production admin
	Route::post('admin/all-purchases', 'Production\PurchaseController@allPurchasesAdminView');

	//machine rents datatable view for suprer admin
	Route::post('all-machine-rents', 'Production\MachineRentController@allRents');
	//purchase datatable view for Production admin
	Route::post('admin/all-machine-rents', 'Production\MachineRentController@allRentsAdminView');

	//production datatable view for suprer admin
	Route::post('all-productions', 'Production\ProducedProductController@allProductions');
	//production datatable view for Production admin
	Route::post('admin/all-productions', 'Production\ProducedProductController@allProductionsAdminView');

	//raw material use datatable view for suprer admin
	Route::post('all-raw-material-uses', 'Production\RawMaterialUseController@allUses');
	//raw material use datatable view for Production admin
	Route::post('admin/all-raw-material-uses', 'Production\RawMaterialUseController@allUsesAdminView');

	//wastage datatable view for suprer admin
	Route::post('all-wastages', 'Production\ProductWastageController@allWastages');
	//wastage datatable view for Production admin
	Route::post('admin/all-wastages', 'Production\ProductWastageController@allWastagesAdminView');

	//expense datatable view for Production admin
	Route::post('admin/all-expenses', 'Production\ExpenseController@allExpensesAdminView');

	//purchase order datatable view for suprer admin
	Route::post('all-purchase-orders', 'Production\PurchaseOrderController@allOrders');
	//purchase order datatable view for Production admin
	Route::post('admin/all-purchase-orders', 'Production\PurchaseOrderController@allOrdersAdminView');

	//sale order datatable view for suprer admin
	Route::post('all-sale-orders', 'Production\SaleOrderController@allOrders');
	//sale order datatable view for Production admin
	Route::post('admin/all-sale-orders', 'Production\SaleOrderController@allOrdersAdminView');

	//purchase transactions datatable view for suprer admin
	Route::post('all-purchase-transactions', 'Production\PurchaseTransactionController@allTransactions');
	//purchase order datatable view for Production admin
	Route::post('admin/all-purchase-transactions', 'Production\PurchaseTransactionController@allTransactionsAdminView');

	//investment transactions datatable view for suprer admin
	Route::post('all-investment-transactions', 'Production\InvestmentTransactionController@allTransactions');
	//investment order datatable view for Production admin
	Route::post('admin/all-investment-transactions', 'Production\InvestmentTransactionController@allTransactionsAdminView');

	//expense transactions datatable view for suprer admin
	Route::post('all-expense-transactions', 'Production\ExpenseTransactionController@allTransactions');
	//expense order datatable view for Production admin
	Route::post('admin/all-expense-transactions', 'Production\ExpenseTransactionController@allTransactionsAdminView');

	//sale transactions datatable view for suprer admin
	Route::post('all-sale-transactions', 'Production\SaleTransactionController@allTransactions');
	//sale order datatable view for Production admin
	Route::post('admin/all-sale-transactions', 'Production\SaleTransactionController@allTransactionsAdminView');

	//sale transactions datatable view for suprer admin
	Route::post('all-rent-transactions', 'Production\MachineRentTransactionController@allTransactions');
	//sale order datatable view for Production admin
	Route::post('admin/all-rent-transactions', 'Production\MachineRentTransactionController@allTransactionsAdminView');

	//sale transactions datatable view for suprer admin
	Route::post('all-sale-payment-returns', 'Production\SalePaymentReturnController@allTransactions');
	//sale order datatable view for Production admin
	Route::post('admin/all-sale-payment-returns', 'Production\SalePaymentReturnController@allTransactionsAdminView');

	//sale transactions datatable view for suprer admin
	Route::post('all-purchase-payment-returns', 'Production\PurchasePaymentReturnController@allTransactions');
	//purchase order datatable view for Production admin
	Route::post('admin/all-purchase-payment-returns', 'Production\PurchasePaymentReturnController@allTransactionsAdminView');

	//bank transactions datatable view for suprer admin
	Route::post('all-bank-transactions', 'Production\BankAccountTransactionController@allTransactions');
	//bank order datatable view for Production admin
	Route::post('admin/all-bank-transactions', 'Production\BankAccountTransactionController@allTransactionsAdminView');

	Route::post('all-sales', 'Production\SaleController@allSales');

	Route::post('admin/all-sales', 'Production\SaleController@allSalesAdminView');

	//customer report
	Route::get('customer/report/{id}', 'Production\CustomerController@report')->name('customer.report');
	Route::get('customer/report/print/{id}', 'Production\CustomerController@printReport');
	Route::get('customer/report/pdf/{id}', 'Production\CustomerController@reportExportPDF');

	//supplier report
	Route::get('supplier/report/{id}', 'Production\SupplierController@report')->name('supplier.report');
	Route::get('supplier/report/print/{id}', 'Production\SupplierController@printReport');
	Route::get('supplier/report/pdf/{id}', 'Production\SupplierController@reportExportPDF');

	//purchase report
	Route::post('purchase/report', 'Production\PurchaseController@report')->name('purchase.report');
	Route::post('purchase/report/print', 'Production\PurchaseController@printReport');
	Route::post('purchase/report/pdf', 'Production\PurchaseController@reportExportPDF');


	//produced-product report
	Route::post('produced-product/report', 'Production\ProducedProductController@report')->name('produced-product.report');
	Route::post('produced-product/report/print', 'Production\ProducedProductController@printReport');
	Route::post('produced-product/report/pdf', 'Production\ProducedProductController@reportExportPDF');

	//raw-material-use report
	Route::post('raw-material-use/report', 'Production\RawMaterialUseController@report')->name('raw-material-use.report');
	Route::post('raw-material-use/report/print', 'Production\RawMaterialUseController@printReport');
	Route::post('raw-material-use/report/pdf', 'Production\RawMaterialUseController@reportExportPDF');

	//purchase transaction report
	Route::post('purchase-transaction/report', 'Production\PurchaseTransactionController@report');
	Route::post('purchase-transaction/report/print', 'Production\PurchaseTransactionController@printReport');
	Route::post('purchase-transaction/report/pdf', 'Production\PurchaseTransactionController@reportExportPDF');

	//machine rent transaction report
	Route::post('machine-rent-transaction/report', 'Production\MachineRentTransactionController@report');
	Route::post('machine-rent-transaction/report/print', 'Production\MachineRentTransactionController@printReport');
	Route::post('machine-rent-transaction/report/pdf', 'Production\MachineRentTransactionController@reportExportPDF');

	//investment transaction report
	Route::post('investment-transaction/report', 'Production\InvestmentTransactionController@report');
	Route::post('investment-transaction/report/print', 'Production\InvestmentTransactionController@printReport');
	Route::post('investment-transaction/report/pdf', 'Production\InvestmentTransactionController@reportExportPDF');

	//purchase transaction report
	Route::post('purchase-payment-return/report', 'Production\PurchasePaymentReturnController@report');
	Route::post('purchase-payment-return/report/print', 'Production\PurchasePaymentReturnController@printReport');
	Route::post('purchase-payment-return/report/pdf', 'Production\PurchasePaymentReturnController@reportExportPDF');

	//sale report
	Route::post('sale/report', 'Production\SaleController@report')->name('sale.report');
	Route::post('sale/report/print', 'Production\SaleController@printReport');
	Route::post('sale/report/pdf', 'Production\SaleController@reportExportPDF');

	//machine-rent report
	Route::post('machine-rent/report', 'Production\MachineRentController@report')->name('machine-rent.report');
	Route::post('machine-rent/report/print', 'Production\MachineRentController@printReport');
	Route::post('machine-rent/report/pdf', 'Production\MachineRentController@reportExportPDF');

	//sale invoice/bill menu
	Route::get('sale/invoice', 'Production\SaleController@returnViewPage');
	Route::post('sale/invoice', 'Production\SaleController@invoice');
	Route::post('sale/invoice/print', 'Production\SaleController@invoicePrint');
	Route::post('sale/invoice/pdf', 'Production\SaleController@invoiceExportPDF');

	//sale transaction report
	Route::post('sale-transaction/report', 'Production\SaleTransactionController@report');
	Route::post('sale-transaction/report/print', 'Production\SaleTransactionController@printReport');
	Route::post('sale-transaction/report/pdf', 'Production\SaleTransactionController@reportExportPDF');

	//sale transaction report
	Route::post('sale-payment-return/report', 'Production\SalePaymentReturnController@report');
	Route::post('sale-payment-return/report/print', 'Production\SalePaymentReturnController@printReport');
	Route::post('sale-payment-return/report/pdf', 'Production\SalePaymentReturnController@reportExportPDF');

	//expense report
	Route::post('expense/report', 'Production\ExpenseController@report')->name('expense.report');
	Route::post('expense/report/print', 'Production\ExpenseController@printReport');
	Route::post('expense/report/pdf', 'Production\ExpenseController@reportExportPDF');

	//expense transaction report
	Route::post('expense-transaction/report', 'Production\ExpenseTransactionController@report');
	Route::post('expense-transaction/report/print', 'Production\ExpenseTransactionController@printReport');
	Route::post('expense-transaction/report/pdf', 'Production\ExpenseTransactionController@reportExportPDF');

	//cash book report
	Route::get('cash-book', 'HomeController@productionCashBook');

	//expense transactions datatable view for suprer admin
	Route::post('full-cash-book', 'HomeController@allProductionCashBook');
	//expense order datatable view for Production admin
	Route::post('admin/full-cash-book', 'HomeController@allProductionCashBookAdminView');

	//supplier search via select2
	Route::get('supplier-autocomplete-search','Production\SupplierController@autoCompleteSearch')->name('supplier-autocomplete-search');

	//product search via select2
	Route::get('product-autocomplete-search','Production\ProductController@autoCompleteSearch')->name('product-autocomplete-search');

	//raw-material search via select2
	Route::get('raw-material-autocomplete-search','Production\RawMaterialController@autoCompleteSearch')->name('raw-material-autocomplete-search');

	//customer search via select2
	Route::get('customer-autocomplete-search','Production\CustomerController@autoCompleteSearch')->name('customer-autocomplete-search');

	//particular expense report
	Route::get('expense-item/report/{id}', 'Production\ExpenseItemController@report');

	//purchase payment create
	Route::get('purchase/payment/create', 'Production\PurchaseTransactionController@create');

	//payment index page
	Route::get('purchase/payment', 'Production\PurchaseTransactionController@index');

	//sale payment index page
	Route::get('sale/payment', 'Production\SaleTransactionController@index');

	//sale payment create
	Route::get('sale/payment/create', 'Production\SaleTransactionController@create');

	//purchase delivery create
	Route::get('purchase/order/delivery/{id}', 'Production\PurchaseOrderController@createDelivery');
	Route::post('purchase/order/delivery/{id}', 'Production\PurchaseOrderController@storeDelivery');

	//sale delivery create
	Route::get('sale/order/delivery/{id}', 'Production\SaleOrderController@createDelivery');
	Route::post('sale/order/delivery/{id}', 'Production\SaleOrderController@storeDelivery');

	//purchase delivery report
	Route::get('purchase/order/delivery/report/{id}', 'Production\PurchaseOrderController@deliveryReport')->name('purchase.order.delivery.report');

	//sale delivery report
	Route::get('sale/order/delivery/report/{id}', 'Production\SaleOrderController@deliveryReport')->name('sale.order.delivery.report');

	//sale delivery create
	Route::get('sale/delivery/{id}', 'Production\SaleDeliveryController@create');
	Route::post('sale/delivery/{id}', 'Production\SaleDeliveryController@store');

	//sale delivery update
	Route::get('sale/delivery/edit/{id}', 'Production\SaleDeliveryController@edit');
	Route::post('sale/delivery/edit/{id}', 'Production\SaleDeliveryController@update');

	//view delivery details
	Route::get('sale/delivery/view/{id}', 'Production\SaleDeliveryController@show');

	//destroy delivery
	Route::delete('sale/delivery/destroy/{id}', 'Production\SaleDeliveryController@destroy');

	//sale delivery report
	Route::get('sale/delivery/report/{id}', 'Production\SaleDeliveryController@deliveryReport')->name('sale.delivery.report');

	//bank account report
	Route::get('bank-account/report/{id}', 'Production\BankAccountController@report');

	//investment report
	Route::get('investment/report/{id}', 'Production\InvestmentController@report');

	//stock report
	Route::get('stock/report', 'HomeController@ProductionStockReportPage');
	Route::post('stock/report', 'HomeController@ProductionStockReport');

	Route::resource('customer', 'Production\CustomerController');
	Route::resource('company', 'Production\CompanyController');
	Route::resource('supplier', 'Production\SupplierController');
	Route::resource('product', 'Production\ProductController');
	Route::resource('raw-material', 'Production\RawMaterialController');
	Route::resource('category', 'Production\CategoryController');
	Route::resource('purchase', 'Production\PurchaseController');
	Route::resource('sale-order', 'Production\SaleOrderController');
	Route::resource('purchase-order', 'Production\PurchaseOrderController');
	Route::resource('sale', 'Production\SaleController');
	Route::resource('expense', 'Production\ExpenseController');
	Route::resource('expense-item', 'Production\ExpenseItemController');
	Route::resource('remark', 'Production\RemarkController');
	Route::resource('product-wastage', 'Production\ProductWastageController');
	Route::resource('sale-payment', 'Production\SaleTransactionController');
	Route::resource('purchase-payment', 'Production\PurchaseTransactionController');
	Route::resource('bank-account', 'Production\BankAccountController');
	Route::resource('bank-transaction', 'Production\BankAccountTransactionController');
	Route::resource('expense-transaction', 'Production\ExpenseTransactionController');
	Route::resource('raw-material-use', 'Production\RawMaterialUseController');
	Route::resource('produced-product', 'Production\ProducedProductController');
	Route::resource('sale-payment-return', 'Production\SalePaymentReturnController');
	Route::resource('purchase-payment-return', 'Production\PurchasePaymentReturnController');
	Route::resource('investment', 'Production\InvestmentController');
	Route::resource('investment-transaction', 'Production\InvestmentTransactionController');
	Route::resource('machine-rent', 'Production\MachineRentController');
	Route::resource('machine-rent-transaction', 'Production\MachineRentTransactionController');


});
